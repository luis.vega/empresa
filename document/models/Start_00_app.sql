-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.25-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para tabla abt_empresa_2.bf_activities
CREATE TABLE IF NOT EXISTS `bf_activities` (
  `activity_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `activity` varchar(255) NOT NULL,
  `module` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `deleted` tinyint(12) NOT NULL DEFAULT '0',
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla abt_empresa_2.bf_activities: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `bf_activities` DISABLE KEYS */;
INSERT INTO `bf_activities` (`activity_id`, `user_id`, `activity`, `module`, `created_on`, `deleted`) VALUES
	(1, 1, 'logged in from: 127.0.0.1', 'users', '2017-11-03 20:55:44', 0);
/*!40000 ALTER TABLE `bf_activities` ENABLE KEYS */;

-- Volcando estructura para tabla abt_empresa_2.bf_ci3_sessions
CREATE TABLE IF NOT EXISTS `bf_ci3_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla abt_empresa_2.bf_ci3_sessions: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `bf_ci3_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `bf_ci3_sessions` ENABLE KEYS */;

-- Volcando estructura para tabla abt_empresa_2.bf_email_queue
CREATE TABLE IF NOT EXISTS `bf_email_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `to_email` varchar(254) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `alt_message` text,
  `max_attempts` int(11) NOT NULL DEFAULT '3',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `success` tinyint(1) NOT NULL DEFAULT '0',
  `date_published` datetime DEFAULT NULL,
  `last_attempt` datetime DEFAULT NULL,
  `date_sent` datetime DEFAULT NULL,
  `csv_attachment` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla abt_empresa_2.bf_email_queue: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `bf_email_queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `bf_email_queue` ENABLE KEYS */;

-- Volcando estructura para tabla abt_empresa_2.bf_login_attempts
CREATE TABLE IF NOT EXISTS `bf_login_attempts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla abt_empresa_2.bf_login_attempts: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `bf_login_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `bf_login_attempts` ENABLE KEYS */;

-- Volcando estructura para tabla abt_empresa_2.bf_permissions
CREATE TABLE IF NOT EXISTS `bf_permissions` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(100) NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla abt_empresa_2.bf_permissions: ~44 rows (aproximadamente)
/*!40000 ALTER TABLE `bf_permissions` DISABLE KEYS */;
INSERT INTO `bf_permissions` (`permission_id`, `name`, `description`, `status`) VALUES
	(2, 'Site.Content.View', 'Allow users to view the Content Context', 'active'),
	(3, 'Site.Reports.View', 'Allow users to view the Reports Context', 'active'),
	(4, 'Site.Settings.View', 'Allow users to view the Settings Context', 'active'),
	(5, 'Site.Developer.View', 'Allow users to view the Developer Context', 'active'),
	(6, 'Bonfire.Roles.Manage', 'Allow users to manage the user Roles', 'active'),
	(7, 'Bonfire.Users.Manage', 'Allow users to manage the site Users', 'active'),
	(8, 'Bonfire.Users.View', 'Allow users access to the User Settings', 'active'),
	(9, 'Bonfire.Users.Add', 'Allow users to add new Users', 'active'),
	(10, 'Bonfire.Database.Manage', 'Allow users to manage the Database settings', 'active'),
	(11, 'Bonfire.Emailer.Manage', 'Allow users to manage the Emailer settings', 'active'),
	(12, 'Bonfire.Logs.View', 'Allow users access to the Log details', 'active'),
	(13, 'Bonfire.Logs.Manage', 'Allow users to manage the Log files', 'active'),
	(14, 'Bonfire.Emailer.View', 'Allow users access to the Emailer settings', 'active'),
	(15, 'Site.Signin.Offline', 'Allow users to login to the site when the site is offline', 'active'),
	(16, 'Bonfire.Permissions.View', 'Allow access to view the Permissions menu unders Settings Context', 'active'),
	(17, 'Bonfire.Permissions.Manage', 'Allow access to manage the Permissions in the system', 'active'),
	(18, 'Bonfire.Modules.Add', 'Allow creation of modules with the builder.', 'active'),
	(19, 'Bonfire.Modules.Delete', 'Allow deletion of modules.', 'active'),
	(20, 'Permissions.Administrator.Manage', 'To manage the access control permissions for the Administrator role.', 'active'),
	(21, 'Permissions.Editor.Manage', 'To manage the access control permissions for the Editor role.', 'active'),
	(23, 'Permissions.User.Manage', 'To manage the access control permissions for the User role.', 'active'),
	(24, 'Permissions.Developer.Manage', 'To manage the access control permissions for the Developer role.', 'active'),
	(26, 'Activities.Own.View', 'To view the users own activity logs', 'active'),
	(27, 'Activities.Own.Delete', 'To delete the users own activity logs', 'active'),
	(28, 'Activities.User.View', 'To view the user activity logs', 'active'),
	(29, 'Activities.User.Delete', 'To delete the user activity logs, except own', 'active'),
	(30, 'Activities.Module.View', 'To view the module activity logs', 'active'),
	(31, 'Activities.Module.Delete', 'To delete the module activity logs', 'active'),
	(32, 'Activities.Date.View', 'To view the users own activity logs', 'active'),
	(33, 'Activities.Date.Delete', 'To delete the dated activity logs', 'active'),
	(34, 'Bonfire.UI.Manage', 'Manage the Bonfire UI settings', 'active'),
	(35, 'Bonfire.Settings.View', 'To view the site settings page.', 'active'),
	(36, 'Bonfire.Settings.Manage', 'To manage the site settings.', 'active'),
	(37, 'Bonfire.Activities.View', 'To view the Activities menu.', 'active'),
	(38, 'Bonfire.Database.View', 'To view the Database menu.', 'active'),
	(39, 'Bonfire.Migrations.View', 'To view the Migrations menu.', 'active'),
	(40, 'Bonfire.Builder.View', 'To view the Modulebuilder menu.', 'active'),
	(41, 'Bonfire.Roles.View', 'To view the Roles menu.', 'active'),
	(42, 'Bonfire.Sysinfo.View', 'To view the System Information page.', 'active'),
	(43, 'Bonfire.Translate.Manage', 'To manage the Language Translation.', 'active'),
	(44, 'Bonfire.Translate.View', 'To view the Language Translate menu.', 'active'),
	(45, 'Bonfire.UI.View', 'To view the UI/Keyboard Shortcut menu.', 'active'),
	(48, 'Bonfire.Profiler.View', 'To view the Console Profiler Bar.', 'active'),
	(49, 'Bonfire.Roles.Add', 'To add New Roles', 'active');
/*!40000 ALTER TABLE `bf_permissions` ENABLE KEYS */;

-- Volcando estructura para tabla abt_empresa_2.bf_roles
CREATE TABLE IF NOT EXISTS `bf_roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(60) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `can_delete` tinyint(1) NOT NULL DEFAULT '1',
  `login_destination` varchar(255) NOT NULL DEFAULT '/',
  `default_context` varchar(255) DEFAULT 'content',
  `deleted` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla abt_empresa_2.bf_roles: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `bf_roles` DISABLE KEYS */;
INSERT INTO `bf_roles` (`role_id`, `role_name`, `description`, `default`, `can_delete`, `login_destination`, `default_context`, `deleted`) VALUES
	(1, 'Administrator', 'Has full control over every aspect of the site.', 0, 0, '', 'content', 0),
	(2, 'Editor', 'Can handle day-to-day management, but does not have full power.', 0, 1, '', 'content', 0),
	(4, 'User', 'This is the default user with access to login.', 1, 0, '', 'content', 0),
	(6, 'Developer', 'Developers typically are the only ones that can access the developer tools. Otherwise identical to Administrators, at least until the site is handed off.', 0, 1, '', 'content', 0);
/*!40000 ALTER TABLE `bf_roles` ENABLE KEYS */;

-- Volcando estructura para tabla abt_empresa_2.bf_role_permissions
CREATE TABLE IF NOT EXISTS `bf_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla abt_empresa_2.bf_role_permissions: ~59 rows (aproximadamente)
/*!40000 ALTER TABLE `bf_role_permissions` DISABLE KEYS */;
INSERT INTO `bf_role_permissions` (`role_id`, `permission_id`) VALUES
	(1, 2),
	(1, 3),
	(1, 4),
	(1, 5),
	(1, 6),
	(1, 7),
	(1, 8),
	(1, 9),
	(1, 10),
	(1, 11),
	(1, 12),
	(1, 13),
	(1, 14),
	(1, 15),
	(1, 16),
	(1, 17),
	(1, 18),
	(1, 19),
	(1, 20),
	(1, 21),
	(1, 23),
	(1, 24),
	(1, 26),
	(1, 27),
	(1, 28),
	(1, 29),
	(1, 30),
	(1, 31),
	(1, 32),
	(1, 33),
	(1, 34),
	(1, 35),
	(1, 36),
	(1, 37),
	(1, 38),
	(1, 39),
	(1, 40),
	(1, 41),
	(1, 42),
	(1, 43),
	(1, 44),
	(1, 45),
	(1, 48),
	(1, 49),
	(2, 2),
	(2, 3),
	(6, 2),
	(6, 3),
	(6, 4),
	(6, 5),
	(6, 6),
	(6, 7),
	(6, 8),
	(6, 9),
	(6, 10),
	(6, 11),
	(6, 12),
	(6, 13),
	(6, 48);
/*!40000 ALTER TABLE `bf_role_permissions` ENABLE KEYS */;

-- Volcando estructura para tabla abt_empresa_2.bf_schema_version
CREATE TABLE IF NOT EXISTS `bf_schema_version` (
  `type` varchar(40) NOT NULL,
  `version` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla abt_empresa_2.bf_schema_version: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `bf_schema_version` DISABLE KEYS */;
INSERT INTO `bf_schema_version` (`type`, `version`) VALUES
	('core', 44);
/*!40000 ALTER TABLE `bf_schema_version` ENABLE KEYS */;

-- Volcando estructura para tabla abt_empresa_2.bf_sessions
CREATE TABLE IF NOT EXISTS `bf_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla abt_empresa_2.bf_sessions: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `bf_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `bf_sessions` ENABLE KEYS */;

-- Volcando estructura para tabla abt_empresa_2.bf_settings
CREATE TABLE IF NOT EXISTS `bf_settings` (
  `name` varchar(30) NOT NULL,
  `module` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla abt_empresa_2.bf_settings: ~36 rows (aproximadamente)
/*!40000 ALTER TABLE `bf_settings` DISABLE KEYS */;
INSERT INTO `bf_settings` (`name`, `module`, `value`) VALUES
	('auth.allow_name_change', 'core', '1'),
	('auth.allow_register', 'core', '1'),
	('auth.allow_remember', 'core', '1'),
	('auth.do_login_redirect', 'core', '1'),
	('auth.login_type', 'core', 'username'),
	('auth.name_change_frequency', 'core', '1'),
	('auth.name_change_limit', 'core', '1'),
	('auth.password_force_mixed_case', 'core', '0'),
	('auth.password_force_numbers', 'core', '0'),
	('auth.password_force_symbols', 'core', '0'),
	('auth.password_min_length', 'core', '8'),
	('auth.password_show_labels', 'core', '0'),
	('auth.remember_length', 'core', '1209600'),
	('auth.user_activation_method', 'core', '0'),
	('auth.use_extended_profile', 'core', '0'),
	('auth.use_usernames', 'core', '1'),
	('form_save', 'core.ui', 'ctrl+s/⌘+s'),
	('goto_content', 'core.ui', 'alt+c'),
	('mailpath', 'email', '/usr/sbin/sendmail'),
	('mailtype', 'email', 'text'),
	('password_iterations', 'users', '8'),
	('protocol', 'email', 'mail'),
	('sender_email', 'email', ''),
	('site.languages', 'core', 'a:3:{i:0;s:7:"english";i:1;s:10:"portuguese";i:2;s:7:"persian";}'),
	('site.list_limit', 'core', '25'),
	('site.offline_reason', 'core', ''),
	('site.show_front_profiler', 'core', '1'),
	('site.show_profiler', 'core', '1'),
	('site.status', 'core', '1'),
	('site.system_email', 'core', 'admin@gob.bo'),
	('site.title', 'core', 'ABT - PAPMP'),
	('smtp_host', 'email', ''),
	('smtp_pass', 'email', ''),
	('smtp_port', 'email', ''),
	('smtp_timeout', 'email', ''),
	('smtp_user', 'email', '');
/*!40000 ALTER TABLE `bf_settings` ENABLE KEYS */;

-- Volcando estructura para tabla abt_empresa_2.bf_users
CREATE TABLE IF NOT EXISTS `bf_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL DEFAULT '4',
  `email` varchar(254) NOT NULL,
  `username` varchar(30) NOT NULL DEFAULT '',
  `password_hash` char(60) DEFAULT NULL,
  `reset_hash` varchar(40) DEFAULT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_ip` varchar(45) NOT NULL DEFAULT '',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `reset_by` int(10) DEFAULT NULL,
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_message` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT '',
  `display_name_changed` date DEFAULT NULL,
  `timezone` varchar(40) NOT NULL DEFAULT 'UM6',
  `language` varchar(20) NOT NULL DEFAULT 'english',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `activate_hash` varchar(40) NOT NULL DEFAULT '',
  `force_password_reset` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla abt_empresa_2.bf_users: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `bf_users` DISABLE KEYS */;
INSERT INTO `bf_users` (`id`, `role_id`, `email`, `username`, `password_hash`, `reset_hash`, `last_login`, `last_ip`, `created_on`, `deleted`, `reset_by`, `banned`, `ban_message`, `display_name`, `display_name_changed`, `timezone`, `language`, `active`, `activate_hash`, `force_password_reset`) VALUES
	(1, 1, 'admin@abt.gob.bo', 'admin', '$2a$08$K8H8TBIXdk1O3S7MdpiA0uySOJ//LTUtwNr1JFXZoZ6FXIMC3Yorm', NULL, '2017-11-03 20:55:44', '127.0.0.1', '2017-11-03 20:28:06', 0, NULL, 0, NULL, 'admin', NULL, 'UM6', 'english', 1, '', 0);
/*!40000 ALTER TABLE `bf_users` ENABLE KEYS */;

-- Volcando estructura para tabla abt_empresa_2.bf_user_cookies
CREATE TABLE IF NOT EXISTS `bf_user_cookies` (
  `user_id` bigint(20) unsigned NOT NULL,
  `token` varchar(128) NOT NULL,
  `created_on` datetime NOT NULL,
  KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla abt_empresa_2.bf_user_cookies: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `bf_user_cookies` DISABLE KEYS */;
/*!40000 ALTER TABLE `bf_user_cookies` ENABLE KEYS */;

-- Volcando estructura para tabla abt_empresa_2.bf_user_meta
CREATE TABLE IF NOT EXISTS `bf_user_meta` (
  `meta_id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) NOT NULL DEFAULT '',
  `meta_value` text,
  PRIMARY KEY (`meta_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla abt_empresa_2.bf_user_meta: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `bf_user_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `bf_user_meta` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

-- Adicion de Registro para el Grupo de Usuarios Propietario Empresa
INSERT INTO `sys_grupo` (`CodGrupo`, `NombreGrupo`, `Descripcion`, `CodGrupoPadre`, `Estado`) VALUES ('', 'Propiertario / Empresa', 'Acceso para Grupo de usuarios de Empresa Forestal', '0', '1');


-- Adicion de Campo para Bonfire Roles con la tabla grupo de usuario de la ABT

ALTER TABLE `bf_roles` ADD COLUMN `grupo_usuario_id` INT NULL AFTER `description`;

-- Agregacion de Admin ABT en Bonfire es Admin
UPDATE `bf_roles` SET `grupo_usuario_id`='1',
`description`='Tiene control total sobre cada aspecto del sitio.' WHERE  `role_id`=1;
-- Agregacion de Edit ABT en Bonfire es Develop
UPDATE `bf_roles` SET `grupo_usuario_id`='1',
 `description`='Los desarrolladores suelen ser los únicos que pueden acceder a las herramientas de desarrollo. De lo contrario, idéntico a los Administradores, al menos hasta que el sitio sea entregado.'
 WHERE  `role_id`=6;

-- Agregacion de Funcionario ABT en Bonfire es Edit
UPDATE `bf_roles` SET `grupo_usuario_id`='8',
`description`='Funcionario ABT que puede manejar la administración de informacion sobre las empresas, solicitudes de trámite, etc.' 
 WHERE  `role_id`=2;

-- Agregacion de Propietario Empresa en Bonfire es User
UPDATE `bf_roles` SET `grupo_usuario_id`='75',
`description`='Este es el usuario predeterminado con acceso para iniciar sesión, administracion de su empresa.'
 WHERE  `role_id`=4;
