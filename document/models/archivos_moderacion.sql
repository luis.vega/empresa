CREATE TABLE `abt_documento_moderacion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('solicitud') NOT NULL COMMENT 'Indica donde se esta haciendo la moderacion del documento',
  `item_id` int(11) NOT NULL COMMENT 'Indica la clave ajena donde pertenece la moderacion',
  `documento_id` int(11) NOT NULL,
  `estado` enum('pendiente','aprobado','rechazado') NOT NULL DEFAULT 'pendiente',
  `observacion` text,
  `fecha_moderacion` datetime NOT NULL,
  `usuario_codigo` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type_item_id_documento_id` (`type`,`item_id`,`documento_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
