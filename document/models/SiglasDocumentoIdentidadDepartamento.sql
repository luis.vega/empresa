ALTER TABLE `departamento`
	ADD COLUMN `Sigla_extension_ci` VARCHAR(5) NOT NULL  
	COMMENT 'Sigla para el Documento de Identificacion nacional' AFTER `Superficie`;

-- Santa Cruz
UPDATE `departamento` SET `Sigla_extension_ci`='SC' WHERE  `CodDepartamento`=1;

-- Beni
UPDATE `departamento` SET `Sigla_extension_ci`='BN' WHERE  `CodDepartamento`=2;

-- Pando
UPDATE `departamento` SET `Sigla_extension_ci`='PA' WHERE  `CodDepartamento`=3;


-- La Paz
UPDATE `departamento` SET `Sigla_extension_ci`='LP' WHERE  `CodDepartamento`=4;

-- Oruro
UPDATE `departamento` SET `Sigla_extension_ci`='OR' WHERE  `CodDepartamento`=5;

-- Potosi
UPDATE `departamento` SET `Sigla_extension_ci`='PT' WHERE  `CodDepartamento`=6;


-- Chuquisaca
UPDATE `departamento` SET `Sigla_extension_ci`='CH' WHERE  `CodDepartamento`=7;

-- Tarija
UPDATE `departamento` SET `Sigla_extension_ci`='TA' WHERE  `CodDepartamento`=8;

-- Cochabamba
UPDATE `departamento` SET `Sigla_extension_ci`='CB' WHERE  `CodDepartamento`=9;