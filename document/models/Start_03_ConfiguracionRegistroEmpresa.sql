ALTER TABLE `tec_tipoactindustrial`
	ADD COLUMN `habilitado` BIGINT NULL DEFAULT NULL;

TRUNCATE `tec_tipoactindustrial`;

INSERT INTO `tec_tipoactindustrial` VALUES ('1', 'Aserradero', '1');
INSERT INTO `tec_tipoactindustrial` VALUES ('2', 'Barraca', '1');
INSERT INTO `tec_tipoactindustrial` VALUES ('3', 'Carboneras', '1');
INSERT INTO `tec_tipoactindustrial` VALUES ('4', 'Carpinterías', '1');
INSERT INTO `tec_tipoactindustrial` VALUES ('5', 'Comercializadora Productos Secundarios', '0');
INSERT INTO `tec_tipoactindustrial` VALUES ('6', 'Comercializadora de Muebles', '0');
INSERT INTO `tec_tipoactindustrial` VALUES ('13', 'Aprovechamiento Forestal', '0');
INSERT INTO `tec_tipoactindustrial` VALUES ('8', 'Desmontadora', '1');
INSERT INTO `tec_tipoactindustrial` VALUES ('9', 'Empresa de Servicios', '0');
INSERT INTO `tec_tipoactindustrial` VALUES ('10', 'Exportadora', '1');
INSERT INTO `tec_tipoactindustrial` VALUES ('11', 'Maestranza', '0');
INSERT INTO `tec_tipoactindustrial` VALUES ('12', 'Parqueteadora', '0');
INSERT INTO `tec_tipoactindustrial` VALUES ('14', 'Aprovechamiento de Productos Forestales No Maderables', '0');


-- Nuevos Registros de Actividad
INSERT INTO `tec_tipoactindustrial` (`codtipoactindustrial`, `descripcion`, `habilitado`) 
VALUES ('15', 'Laminadora', '1');

INSERT INTO `tec_tipoactindustrial` (`codtipoactindustrial`, `descripcion`, `habilitado`) 
VALUES ('16', 'Beneficiadora de Castaña', '1');

INSERT INTO `tec_tipoactindustrial` (`codtipoactindustrial`, `descripcion`, `habilitado`) 
VALUES ('17', 'Palmitera', '1');

INSERT INTO `tec_tipoactindustrial` (`codtipoactindustrial`, `descripcion`, `habilitado`) 
VALUES ('18', 'Planta de tableros', '1');

INSERT INTO `tec_tipoactindustrial` (`codtipoactindustrial`, `descripcion`, `habilitado`) 
VALUES ('19', 'Tratamiento y/o preservación', '1');

INSERT INTO `tec_tipoactindustrial` (`codtipoactindustrial`, `descripcion`, `habilitado`) 
VALUES ('20', 'Aprovechamiento Integral del bosque', '1');

INSERT INTO `tec_tipoactindustrial` (`codtipoactindustrial`, `descripcion`, `habilitado`) 
VALUES ('21', 'Importadora', '1');

INSERT INTO `tec_tipoactindustrial` (`codtipoactindustrial`, `descripcion`, `habilitado`) 
VALUES ('22', 'Comercializadoras de  productos no maderables de bosque natural', '1');

INSERT INTO `tec_tipoactindustrial` (`codtipoactindustrial`, `descripcion`, `habilitado`) 
VALUES ('23', 'Centro de Almacenamiento', '1');

INSERT INTO `tec_tipoactindustrial` (`codtipoactindustrial`, `descripcion`, `habilitado`) 
VALUES ('24', 'Comercializadoras de  Carbón', '1');

INSERT INTO `tec_tipoactindustrial` (`codtipoactindustrial`, `descripcion`, `habilitado`) 
VALUES ('25', 'Comercializadoras de  productos provenientes de plantaciones', '1');

INSERT INTO `tec_tipoactindustrial` (`codtipoactindustrial`, `descripcion`, `habilitado`) 
VALUES ('26', 'Consumidor final de productos forestales', '1');

INSERT INTO `tec_tipoactindustrial` (`codtipoactindustrial`, `descripcion`, `habilitado`) 
VALUES ('27', 'Comercializadoras de  tableros aglomerados', '1');

-- *********************** Configuracion Aserradero ***********************
TRUNCATE `atec_categoria_actividad`;
-- Aserradero con la Categoria A
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`, `descripcion`) 
	VALUES ('1', '1', '> 10001 m3r');
-- Aserradero con la Categoria B
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`, `descripcion`) 
VALUES ('2', '1', '5001 – 10000 m3r');
-- Aserradero con la Categoria C
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`, `descripcion`) 
VALUES ('3', '1', '1001 – 5000 m3r');
-- Aserradero con la Categoria D
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`, `descripcion`) 
VALUES ('4', '1', '<1000 m3r');

-- Aserradero con la Categoria E
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`, `descripcion`) 
VALUES ('6', '1', 'Ilimitado');

-- *********************** Configuracion Laminadora ***********************
-- Laminadora con la Categoria U
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`) VALUES ('5', '15');

-- *********************** Configuracion Beneficiadora de Castaña ***********************
-- Beneficiadora de Castaña con Categoria U
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`) VALUES ('5', '16');

-- *********************** Configuracion Palmiteras ***********************
-- Palmiteras con Categoria U
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`) VALUES ('5', '17');

-- *********************** Configuracion Carboneras ***********************
-- Carboneras con Categoria A
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`, `descripcion`) 
VALUES ('1', '3', '>1001 Tn');
-- Carboneras con Categoria B
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`, `descripcion`) 
VALUES ('2', '3', '501 - 1000 Tn');
-- Carboneras con Categoria C
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`, `descripcion`) 
VALUES ('3', '3', '<500 Tn');

-- *********************** Configuracion Carpinteria ***********************
-- Carpinteria con Categoria A
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad` , `descripcion`) 
VALUES ('1', '4', '>1000001 pt');
-- Carpinteria con Categoria B
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`, `descripcion`) 
VALUES ('2', '4', '500001 - 1000000 pt');
-- Carpinteria con Categoria C
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`, `descripcion`) 
VALUES ('3', '4', '100001 - 500000 pt');
-- Carpinteria con Categoria D
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`, `descripcion`) 
VALUES ('4', '4', '50001 - 100000 pt');
-- Carpinteria con Categoria E
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`, `descripcion`) 
VALUES ('6', '4', '<50000 pt');

-- *********************** Configuracion Plantas de tableros aglomerados ***********************
-- Plantas de tableros aglomerados con la Categoria U
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`) VALUES ('5', '18');

-- *********************** Configuracion Tratamiento y/o preservación ***********************
-- Tratamiento y/o preservación con la Categoria U
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`) VALUES ('5', '19');

-- *********************** Configuracion Desmontadoras ***********************
-- Desmontadoras con la Categoria U
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`) VALUES ('5', '8');

-- *********************** Configuracion Manejo Integral Forestal ***********************
-- Manejo Integral Forestal con la Categoria U
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`) VALUES ('5', '20');


-- *********************** Configuracion Barracas ***********************
-- Barracas con Categoria A
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`,`descripcion`) 
VALUES ('1', '2', '>2000001 pt');
-- Barracas con Categoria B
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`, `descripcion`) 
VALUES ('2', '2', '1000001 - 2000000 pt');
-- Barracas con Categoria C
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`, `descripcion`) 
VALUES ('3', '2', '100001 - 1000000 pt');
-- Barracas con Categoria D
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`, `descripcion`) 
VALUES ('4', '2', '50001 - 100000 pt');
-- Barracas con Categoria E
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`, `descripcion`) 
VALUES ('6', '2', '<50000 pt');

-- *********************** Configuracion Exportadoras ***********************
-- Exportadoras con la Categoria U
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`) VALUES ('5', '10');

-- *********************** Configuracion Importadoras ***********************
-- Importadoras con la Categoria U
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`) VALUES ('5', '21');

-- *********************** Configuracion Comercializadoras de  productos no maderables de bosque natural ***********************
-- Comercializadoras de  productos no maderables de bosque natural con la Categoria U
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`) VALUES ('5', '22');

-- *********************** Configuracion Centro de Almacenamiento ***********************
-- Comercializadoras de  Carbón con la Categoria U
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`) VALUES ('5', '23');

-- *********************** Configuracion Comercializadoras de  Carbón ***********************
-- Comercializadoras de  Carbón con la Categoria U
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`) VALUES ('5', '24');

-- *********************** Configuracion Comercializadoras de  productos provenientes de plantaciones ***********************
-- Comercializadoras de  productos provenientes de plantaciones con la Categoria U
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`) VALUES ('5', '25');

-- *********************** Configuracion Consumidor final de productos forestales ***********************
-- Consumidor final de productos forestales con la Categoria U
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`) VALUES ('5', '26');

-- *********************** Configuracion Comercializadoras de  tableros aglomerados ***********************
-- Comercializadoras de  tableros aglomerados con la Categoria U
INSERT INTO `atec_categoria_actividad` (`id_categoria`, `id_actividad`) VALUES ('5', '27');


-- ************ Configuracion de Precios Actividad por Categoria ******************
TRUNCATE `abt_precio_actividad_categoria`;
-- Aserradero
INSERT INTO `abt_precio_actividad_categoria` 
(`actividad_id`, `categoria_id`, `monto`, `moneda_iso`, `moneda_simbolo`, `tipo_tramite`) 
VALUES 
('1', '1', '400', 'USD', '$', 'I'),
('1', '2', '300', 'USD', '$', 'I'),
('1', '3', '150', 'USD', '$', 'I'),
('1', '4', '100', 'USD', '$', 'I'),
('1', '6', '0', 'USD', '$', 'I');

-- Laminadora
INSERT INTO `abt_precio_actividad_categoria` 
(`actividad_id`, `categoria_id`, `monto`, `moneda_iso`, `moneda_simbolo`, `tipo_tramite`) 
VALUES ('15', '5', '300', 'USD', '$', 'I');
-- Beneficiadora de Castaña
INSERT INTO `abt_precio_actividad_categoria` 
(`actividad_id`, `categoria_id`, `monto`, `moneda_iso`, `moneda_simbolo`, `tipo_tramite`) 
VALUES ('16', '5', '200', 'USD', '$', 'I');
-- Palmitera
INSERT INTO `abt_precio_actividad_categoria` 
(`actividad_id`, `categoria_id`, `monto`, `moneda_iso`, `moneda_simbolo`, `tipo_tramite`) 
VALUES ('17', '5', '200', 'USD', '$', 'I');
-- Carboneras
INSERT INTO `abt_precio_actividad_categoria` 
(`actividad_id`, `categoria_id`, `monto`, `moneda_iso`, `moneda_simbolo`, `tipo_tramite`) 
VALUES 
('3', '1', '400', 'USD', '$', 'I'),
('3', '2', '250', 'USD', '$', 'I'),
('3', '3', '150', 'USD', '$', 'I');
-- Carpinterías
INSERT INTO `abt_precio_actividad_categoria` 
(`actividad_id`, `categoria_id`, `monto`, `moneda_iso`, `moneda_simbolo`, `tipo_tramite`) 
VALUES 
('4', '1', '300', 'USD', '$', 'I'),
('4', '2', '150', 'USD', '$', 'I'),
('4', '3', '75', 'USD', '$', 'I'),
('4', '4', '30', 'USD', '$', 'I'),
('4', '6', '0', 'USD', '$', 'I');
-- Planta de tableros
INSERT INTO `abt_precio_actividad_categoria` 
(`actividad_id`, `categoria_id`, `monto`, `moneda_iso`, `moneda_simbolo`, `tipo_tramite`) 
VALUES ('18', '5', '300', 'USD', '$', 'I');
-- Tratamiento y/o preservación
INSERT INTO `abt_precio_actividad_categoria` 
(`actividad_id`, `categoria_id`, `monto`, `moneda_iso`, `moneda_simbolo`, `tipo_tramite`) 
VALUES ('19', '5', '300', 'USD', '$', 'I');
-- Manejo Integral Forestal
INSERT INTO `abt_precio_actividad_categoria` 
(`actividad_id`, `categoria_id`, `monto`, `moneda_iso`, `moneda_simbolo`, `tipo_tramite`) 
VALUES ('20', '5', '300', 'USD', '$', 'I');
-- Desmontadora
INSERT INTO `abt_precio_actividad_categoria` 
(`actividad_id`, `categoria_id`, `monto`, `moneda_iso`, `moneda_simbolo`, `tipo_tramite`) 
VALUES ('8', '5', '300', 'USD', '$', 'I');
-- Barraca
INSERT INTO `abt_precio_actividad_categoria` 
(`actividad_id`, `categoria_id`, `monto`, `moneda_iso`, `moneda_simbolo`, `tipo_tramite`) 
VALUES 
('2', '1', '300', 'USD', '$', 'I'),
('2', '2', '150', 'USD', '$', 'I'),
('2', '3', '75', 'USD', '$', 'I'),
('2', '4', '40', 'USD', '$', 'I'),
('2', '6', '0', 'USD', '$', 'I');
-- Exportadora
INSERT INTO `abt_precio_actividad_categoria` 
(`actividad_id`, `categoria_id`, `monto`, `moneda_iso`, `moneda_simbolo`, `tipo_tramite`) 
VALUES ('10', '5', '300', 'USD', '$', 'I');
-- Importadora
INSERT INTO `abt_precio_actividad_categoria` 
(`actividad_id`, `categoria_id`, `monto`, `moneda_iso`, `moneda_simbolo`, `tipo_tramite`) 
VALUES ('21', '5', '750', 'USD', '$', 'I');
-- Comercializadoras de  productos no maderables de bosque natural
INSERT INTO `abt_precio_actividad_categoria` 
(`actividad_id`, `categoria_id`, `monto`, `moneda_iso`, `moneda_simbolo`, `tipo_tramite`) 
VALUES ('22', '5', '200', 'USD', '$', 'I');
-- Centro de Almacenamiento
INSERT INTO `abt_precio_actividad_categoria` 
(`actividad_id`, `categoria_id`, `monto`, `moneda_iso`, `moneda_simbolo`, `tipo_tramite`) 
VALUES ('23', '5', '200', 'USD', '$', 'I');
-- Comercializadoras de  Carbón
INSERT INTO `abt_precio_actividad_categoria` 
(`actividad_id`, `categoria_id`, `monto`, `moneda_iso`, `moneda_simbolo`, `tipo_tramite`) 
VALUES ('24', '5', '200', 'USD', '$', 'I');
-- Comercializadoras de  productos provenientes de plantaciones
INSERT INTO `abt_precio_actividad_categoria` 
(`actividad_id`, `categoria_id`, `monto`, `moneda_iso`, `moneda_simbolo`, `tipo_tramite`) 
VALUES ('25', '5', '200', 'USD', '$', 'I');
-- Consumidor final de productos forestales
INSERT INTO `abt_precio_actividad_categoria` 
(`actividad_id`, `categoria_id`, `monto`, `moneda_iso`, `moneda_simbolo`, `tipo_tramite`) 
VALUES ('26', '5', '0', 'USD', '$', 'I');

INSERT INTO `abt_precio_actividad_categoria` 
(`actividad_id`, `categoria_id`, `monto`, `moneda_iso`, `moneda_simbolo`, `tipo_tramite`) 
VALUES ('27', '5', '200', 'USD', '$', 'I');

-- *********************** Configuracion Archivos Empresa
truncate `abt_archivo_empresa`;
ALTER TABLE `abt_archivo_empresa` AUTO_INCREMENT=1;
INSERT INTO `abt_archivo_empresa` (`archivo_empresa_id`,`nombre`, `descripcion`) 
VALUES 
 (1,'Carta de solicitud', 'Documento requerido para empresas en proceso de Inscripcion'),
 (2,'Programa de Abastecimiento y Procesamiento de Materia Prima acorde a Norma Técnica 134/97',
 	'Programa de Abastecimiento y Procesamiento de Materia Prima acorde a Norma Técnica 134/97, 
 	elaborado por un profesional inscrito ante la ABT, en un ejemplar que deberá contener la 
 	Declaración jurada de corresponsabilidad del profesional forestal, del propietario y/o Representante Legal'),
 (3,'FO-26, fichas técnicas ', 'FO-26, fichas técnicas para cada actividad'),
 (4,'Boleta de depósito realizado a la cuenta de la ABT Recaudaciones', 'Boleta de depósito realizado a la cuenta de la ABT Recaudaciones'),
 (5,'Croquis de ubicación X_Coord; Y_Coord', 'Croquis de ubicación X_Coord; Y_Coord'),
 (6,'NIT', 'NIT Presentación de fotocopia exhibir los originales'),
 (7,'FUNDEMPRESA', 'FUNDEMPRESA, Presentación de fotocopia exhibir los originales'),
 (8,'Poder de Representación', 'Poder de Representación (De acuerdo al tipo de empresas y en casos que corresponda por actividad) (Exhibición de originales en ABT)'),
 (9,'Constitución legal de la empresa', 'Constitución legal de la empresa, Presentación de fotocopia exhibir los originales'),
 (10,'Contrato de prestación de servicios(Agente Auxiliar y propietario)', 'Contrato de prestación de servicios(Agente Auxiliar y propietario), Presentación de fotocopia exhibir los originales'),
 (11,'Solicitud de Inscripción de la Carpintería y/o Centro Artesanal', 'Solicitud de Inscripción de la Carpintería y/o Centro Artesanal'),
 (12,'Fotocopia de documento de identidad vigente (cedula, pasaporte o libreta de servicio militar)', 'Fotocopia de documento de identidad vigente (cedula, pasaporte o libreta de servicio militar)'),
 (13, 'Presentar un  Programa de Abastecimiento de Materia Prima (PAPMP)', 'Presentar un  Programa de Abastecimiento de Materia Prima (PAPMP) de acuerdo a Norma Técnica 134/97, elaborado por un profesional forestal registrado en la ABT o en su caso por el Responsable de la Unidad Forestal Municipal'),
 (14, 'Solicitud de Inscripción de la Carpintería y/o Centro Artesanal', 'Solicitud de Inscripción de la Carpintería y/o Centro Artesanal'),
 (15,'Fotocopia de documento de identidad vigente del representante legal (cedula, pasaporte o libreta de servicio militar)', 'Fotocopia de documento de identidad vigente del representante legal (cedula, pasaporte o libreta de servicio militar).'),
 (16,'Personería Jurídica de conformación de la Asociación', 'Personería Jurídica de conformación de la Asociación'),
 (17, 'Poder Notariado del representante legal de la asociación', 'Poder Notariado del representante legal de la asociación'),
 (18, 'Lista de afiliados a la asociación', 'Lista de afiliados a la asociación (Nombre Completo y Nº de Documento de Identidad adjuntar fotocopia simple de la misma)'),
 (19, 'Certificación de afiliación a la Asociación de Carpinteros', 'Certificación de afiliación a la Asociación de Carpinteros, un miembro de las carpinterías donde especifique que la carpintería solicitante se encuentra dentro de la Categoría “E”.'),
 (20, 'Solicitud de Inscripción de la Barraca', 'Solicitud de Inscripción de la Barraca'),
 (21, 'Fotocopia de NIT si lo tuviera, o en su caso, el compromiso de presentar en un tiempo determinado', 'Fotocopia de NIT si lo tuviera, o en su caso, el compromiso de presentar en un tiempo determinado'),
 (22, 'Registro de Fundempresa si lo tuviera, o en su caso el compromiso de presentar en un tiempo determinado', 'Registro de Fundempresa si lo tuviera, o en su caso el compromiso de presentar en un tiempo determinado'),
 (23, 'Carta de solicitud del Aserradero', 'Carta de solicitud del Aserradero'),
 (24, 'Solicitud de Inscripción de la Exportadora o Importadora', 'Solicitud de Inscripción de la Exportadora o Importadora'),
 (25, 'Fotocopia de documento de identidad vigente del propietario y/o representante legal (cedula, pasaporte o libreta de servicio militar)', 'Fotocopia de documento de identidad vigente del propietario y/o representante legal (cedula, pasaporte o libreta de servicio militar)'),
 (26, 'Registro SENASAG original o fotocopia legalizada', 'Registro SENASAG original o fotocopia legalizada'),
 (27, 'RUEX- SENAVEX original o fotocopia legalizada', 'RUEX- SENAVEX original o fotocopia legalizada'),
 (28, 'Solicitud de Inscripción', 'Solicitud de Inscripción'),
 (29, 'Documentación que acredite derecho propietario y/o documentación de representación de los propietarios de la maquinaria', 'Documentación que acredite derecho propietario y/o documentación de representación de los propietarios de la maquinaria'),
 (30, 'Formulario donde constan los datos del proyecto con indicación de uso o trabajo a ejecutar', 'Formulario donde constan los datos del proyecto con indicación de uso o trabajo a ejecutar'),
 (31, 'Finalizado el servicio o los servicios realizados, la Empresa en su reinscripción anual del próximo año debe presentar la cantidad de trabajos realizados con su respectiva copia de Autorización de la ABT', 'Finalizado el servicio o los servicios realizados, la Empresa en su reinscripción anual del próximo año debe presentar la cantidad de trabajos realizados con su respectiva copia de Autorización de la ABT'),
 (32, 'Solicitud de inscripción de "Consumidores Finales de Productos Forestales", firmada por la persona natural o representante legal de la persona colectiva', 'Solicitud de inscripción de "Consumidores Finales de Productos Forestales", firmada por la persona natural o representante legal de la persona colectiva'),
 (33, 'Original del Formulario de Registro Empresas Consumidores Finales de Productos Forestales', 'Original del Formulario de Registro Empresas Consumidores Finales de Productos Forestales'),
 (34, 'Fotocopia de Poder Notarial del representante legal', 'Fotocopia de Poder Notarial del representante legal (si correspondiera)')
;

CREATE TABLE `abt_configuracion_archivo_empresa` (
	`configuracion_archivo_empresa_id` INT(11) NOT NULL AUTO_INCREMENT,
	`tipo_tramite` ENUM('I','R') NOT NULL,
	`habilitado` TINYINT(4) NULL DEFAULT '0',
	`archivo_id` INT(11) NOT NULL,
	`documento_legal` TINYINT(4) NULL DEFAULT NULL,
	`por_actualizacion_datos` TINYINT(1) NULL DEFAULT NULL,
	`escaneado_cd` TINYINT(1) NULL DEFAULT NULL,
	PRIMARY KEY (`configuracion_archivo_empresa_id`),
	INDEX `fk_configuracion_archivo_abt_archivo` (`archivo_id`)
)
ENGINE=InnoDB
;

-- Configuracion Archivos y Tipo de Tramite
TRUNCATE `abt_configuracion_archivo_empresa`;

-- ***************** Inscripcion ********************
INSERT INTO `abt_configuracion_archivo_empresa` (`configuracion_archivo_empresa_id`, `tipo_tramite`, `habilitado`, `archivo_id`, `es_digital`, `es_fisico`, `por_actualizacion`) 
-- Carta de solicitud generada por la web
VALUES (1, 'I', '1', '1', NULL, '1', NULL),
-- Presentar un  Programa de Abastecimiento de Materia Prima (PAPMP)
(2, 'I','1', '2', NULL, '1', NULL),
-- Detalle del movimiento de productos forestales y de la maquinaria FO-26
(3, 'I','1', '3', '1', '1', NULL),
-- Boleta de depósito realizado a la cuenta de la ABT
(4, 'I','1', '4', NULL, '1', NULL),
-- Croquis de ubicación X_Coord; Y_Coord
(5, 'I','1', '5', '1', NULL, NULL),
-- Registro biométrico NIT
(6, 'I','1', '6', '1', NULL, NULL),
-- Registro de FUNDEMPRESA
(7, 'I','1', '7', '1', NULL, NULL),
-- Poder de Representación
(8, 'I','1', '8', '1', NULL, NULL),
-- Constitución legal de la empresa
(9, 'I','1', '9', NULL, '1', NULL),
-- Contrato de prestación de servicios(Agente Auxiliar y propietario)
(10, 'I','1', '10', '1', NULL, NULL),
-- Fotocopia de documento de identidad vigente (cedula, pasaporte o libreta de servicio militar)
(11, 'I','1', '11', '1', NULL, NULL),
-- Fotocopia de documento de identidad vigente del representante legal
(12, 'I','1', '12', '1', NULL, NULL),
-- Personería Jurídica de conformación de la Asociación
(13, 'I','1', '13', NULL, '1', NULL),
-- Poder Notariado del representante legal de la asociación
(14, 'I','1', '14', NULL, '1', NULL),
-- Lista de afiliados a la asociación
(15, 'I','1', '15', NULL, '1', NULL),
-- Certificación emitida por una asociación con registro ante la ABT
(16, 'I','1', '16', NULL, '1', NULL),
-- Registro SENASAG original o fotocopia legalizada
(17, 'I','1', '17', '1', NULL, NULL),
-- RUEX- SENAVEX original o fotocopia legalizada
(18, 'I','1', '18', '1', NULL, NULL),
-- Documentación que acredite derecho propietario y/o documentación
(19, 'I','1', '19', NULL, '1', NULL),
-- Fotocopia de Poder Notarial del representante legal
(20, 'I','1', '23', '1', NULL, NULL),
-- Original del Formulario de Registro Empresas Consumidores
(21, 'I','1', '22', '1', '1', NULL)
;

-- ***************** Reinscripcion ********************
INSERT INTO `abt_configuracion_archivo_empresa` (`configuracion_archivo_empresa_id`, `tipo_tramite`, `habilitado`, `archivo_id`, `es_digital`, `es_fisico`, `por_actualizacion`) 
-- Carta de solicitud generada por la web
VALUES (1000, 'R', '1', '20', NULL, '1', NULL),
(1001, 'R', '1', '21', NULL, '1', NULL),
(1002, 'R', '1', '22', NULL, '1', NULL)
;

-- INSCRIPCION: Aserradero 	A, B, C Y D
TRUNCATE `abt_archivo_actividad_categoria`;
INSERT INTO `abt_archivo_actividad_categoria` (`config_archivo_id`, `actividad_id`, `categoria_id`, `clasificacion`)
 VALUES 
 -- Carta de solicitud Web
 ('1', '1', '1', 'E'), 
 ('1', '1', '2', 'E'),
 ('1', '1', '3', 'E'),
 ('1', '1', '4', 'E'),
 -- Programa de Abastecimiento 
 ('2', '1', '1', 'E'), 
 ('2', '1', '2', 'E'),
 ('2', '1', '3', 'E'),
 ('2', '1', '4', 'E'),
 -- F26
 ('3', '1', '1', 'A'), 
 ('3', '1', '2', 'A'),
 ('3', '1', '3', 'A'),
 ('3', '1', '4', 'A'),
  -- Boleta de depósito 
 ('4', '1', '1', 'E'), 
 ('4', '1', '2', 'E'),
 ('4', '1', '3', 'E'),
 ('4', '1', '4', 'E'),
-- Croquis de ubicación 
 ('5', '1', '1', 'S'), 
 ('5', '1', '2', 'S'),
 ('5', '1', '3', 'S'),
 ('5', '1', '4', 'S'),
-- Poder de Representación 
 ('8', '1', '1', 'S'), 
 ('8', '1', '2', 'S'),
 ('8', '1', '3', 'S'),
 ('8', '1', '4', 'S'),
    -- Constitución legal de la empresa
 ('9', '1', '1', 'E'), 
 ('9', '1', '2', 'E'),
 ('9', '1', '3', 'E'),
 ('9', '1', '4', 'E'),
 -- Contrato de prestación de servicios(Agente Auxiliar y propietario) 
 ('10', '1', '1', 'S'), 
 ('10', '1', '2', 'S'),
 ('10', '1', '3', 'S'),
 ('10', '1', '4', 'S'),
 -- Fotocopia de documento de identidad vigente del propietario 
 ('11', '1', '1', 'E'), 
 ('11', '1', '2', 'E'),
 ('11', '1', '3', 'E'),
 ('11', '1', '4', 'E'),
  -- Fotocopia de documento de identidad vigente del representante legal
 ('12', '1', '1', 'S'), 
 ('12', '1', '2', 'S'),
 ('12', '1', '3', 'S'),
 ('12', '1', '4', 'S');

 -- INSCRIPCION: Laminadora 	U
INSERT INTO `abt_archivo_actividad_categoria` (`config_archivo_id`, `actividad_id`, `categoria_id`, `clasificacion`)
 VALUES 
 -- Carta de solicitud 
 ('1', '15', '5', 'E'), 
 -- Programa de Abastecimiento 
 ('2', '15', '5', 'E'), 
 -- F26
 ('3', '15', '5', 'A'), 
  -- Boleta de depósito 
 ('4', '15', '5', 'E'), 
-- Croquis de ubicación 
 ('5', '15', '5', 'S'),
-- Registro biométrico NIT
 ('6', '15', '5', 'E'),
 -- Registro de FUNDEMPRESA
 ('7', '15', '5', 'E'), 
-- Poder de Representación 
 ('8', '15', '5', 'S'),
 -- Constitución legal de la empresa
 ('9', '15', '1', 'E'),
 -- Contrato de prestación de servicios(Agente Auxiliar y propietario) 
 ('10', '15', '5', 'S'),
  -- Fotocopia de documento de identidad vigente del propietario 
 ('11', '15', '1', 'E'),
 -- Fotocopia de documento de identidad vigente del representante legal
 ('12', '15', '1', 'S');

  -- INSCRIPCION: Beneficiadora de castaña 	U
INSERT INTO `abt_archivo_actividad_categoria` (`config_archivo_id`, `actividad_id`, `categoria_id`, `clasificacion`)
 VALUES 
 -- Carta de solicitud 
 ('1', '16', '5', 'E'), 
 -- Programa de Abastecimiento 
 ('2', '16', '5', 'E'), 
 -- F26
 ('3', '16', '5', 'A'), 
  -- Boleta de depósito 
 ('4', '16', '5', 'E'), 
-- Croquis de ubicación 
 ('5', '16', '5', 'S'),
-- Registro biométrico NIT
 ('6', '16', '5', 'E'),
 -- Registro de FUNDEMPRESA
 ('7', '16', '5', 'E'), 
-- Poder de Representación 
 ('8', '16', '5', 'S'),
 -- Constitución legal de la empresa
 ('9', '16', '1', 'E'),
 -- Contrato de prestación de servicios(Agente Auxiliar y propietario) 
 ('10', '16', '5', 'S'),
  -- Fotocopia de documento de identidad vigente del propietario 
 ('11', '16', '1', 'E'),
 -- Fotocopia de documento de identidad vigente del representante legal
 ('12', '16', '1', 'S');
 
  -- INSCRIPCION: Palmiteras 	U
INSERT INTO `abt_archivo_actividad_categoria` (`config_archivo_id`, `actividad_id`, `categoria_id`, `clasificacion`)
 VALUES 
 -- Carta de solicitud 
 ('1', '17', '5', 'E'), 
 -- Programa de Abastecimiento 
 ('2', '17', '5', 'E'), 
 -- F26
 ('3', '17', '5', 'A'), 
  -- Boleta de depósito 
 ('4', '17', '5', 'E'), 
-- Croquis de ubicación 
 ('5', '17', '5', 'S'),
-- Registro biométrico NIT
 ('6', '17', '5', 'E'),
 -- Registro de FUNDEMPRESA
 ('7', '17', '5', 'E'), 
-- Poder de Representación 
 ('8', '17', '5', 'S'),
 -- Constitución legal de la empresa
 ('9', '17', '1', 'E'),
 -- Contrato de prestación de servicios(Agente Auxiliar y propietario) 
 ('10', '17', '5', 'S'),
  -- Fotocopia de documento de identidad vigente del propietario 
 ('11', '17', '1', 'E'),
 -- Fotocopia de documento de identidad vigente del representante legal
 ('12', '17', '1', 'S');

 -- INSCRIPCION: Carpinterías 	A, B, C Y D
INSERT INTO `abt_archivo_actividad_categoria` (`config_archivo_id`, `actividad_id`, `categoria_id`, `clasificacion`)
 VALUES 
 -- Carta de solicitud Web
 ('1', '4', '1', 'E'), 
 ('1', '4', '2', 'E'),
 ('1', '4', '3', 'E'),
 ('1', '4', '4', 'E'),
 -- Programa de Abastecimiento 
 ('2', '4', '1', 'E'), 
 ('2', '4', '2', 'E'),
 ('2', '4', '3', 'E'),
 ('2', '4', '4', 'E'),
 -- F26
 ('3', '4', '1', 'A'), 
 ('3', '4', '2', 'A'),
 ('3', '4', '3', 'A'),
 ('3', '4', '4', 'A'),
  -- Boleta de depósito 
 ('4', '4', '1', 'E'), 
 ('4', '4', '2', 'E'),
 ('4', '4', '3', 'E'),
 ('4', '4', '4', 'E'),
-- Croquis de ubicación 
 ('5', '4', '1', 'S'), 
 ('5', '4', '2', 'S'),
 ('5', '4', '3', 'S'),
 ('5', '4', '4', 'S'),
-- Poder de Representación 
 ('8', '4', '1', 'S'), 
 ('8', '4', '2', 'S'),
 ('8', '4', '3', 'S'),
 ('8', '4', '4', 'S'),
    -- Constitución legal de la empresa
 ('9', '4', '1', 'E'), 
 ('9', '4', '2', 'E'),
 ('9', '4', '3', 'E'),
 ('9', '4', '4', 'E'),
 -- Contrato de prestación de servicios(Agente Auxiliar y propietario) 
 ('10', '4', '1', 'S'), 
 ('10', '4', '2', 'S'),
 ('10', '4', '3', 'S'),
 ('10', '4', '4', 'S'),
 -- Fotocopia de documento de identidad vigente del propietario 
 ('11', '4', '1', 'E'), 
 ('11', '4', '2', 'E'),
 ('11', '4', '3', 'E'),
 ('11', '4', '4', 'E'),
  -- Fotocopia de documento de identidad vigente del representante legal
 ('12', '4', '1', 'S'), 
 ('12', '4', '2', 'S'),
 ('12', '4', '3', 'S'),
 ('12', '4', '4', 'S');

  -- INSCRIPCION: Planta de Tableros 	U
INSERT INTO `abt_archivo_actividad_categoria` (`config_archivo_id`, `actividad_id`, `categoria_id`, `clasificacion`)
 VALUES 
 -- Carta de solicitud 
 ('1', '18', '5', 'E'), 
 -- Programa de Abastecimiento 
 ('2', '18', '5', 'E'), 
 -- F26
 ('3', '18', '5', 'A'), 
  -- Boleta de depósito 
 ('4', '18', '5', 'E'), 
-- Croquis de ubicación 
 ('5', '18', '5', 'S'),
-- Registro biométrico NIT
 ('6', '18', '5', 'E'),
 -- Registro de FUNDEMPRESA
 ('7', '18', '5', 'E'), 
-- Poder de Representación 
 ('8', '18', '5', 'S'),
 -- Constitución legal de la empresa
 ('9', '18', '1', 'E'),
 -- Contrato de prestación de servicios(Agente Auxiliar y propietario) 
 ('10', '18', '5', 'S'),
  -- Fotocopia de documento de identidad vigente del propietario 
 ('11', '18', '1', 'E'),
 -- Fotocopia de documento de identidad vigente del representante legal
 ('12', '18', '1', 'S');

 -- INSCRIPCION: Barraca 	A, B, C Y D
INSERT INTO `abt_archivo_actividad_categoria` (`config_archivo_id`, `actividad_id`, `categoria_id`, `clasificacion`)
 VALUES 
 -- Carta de solicitud Web
 ('1', '2', '1', 'E'), 
 ('1', '2', '2', 'E'),
 ('1', '2', '3', 'E'),
 ('1', '2', '4', 'E'),
 -- Programa de Abastecimiento 
 ('2', '2', '1', 'E'), 
 ('2', '2', '2', 'E'),
 ('2', '2', '3', 'E'),
 ('2', '2', '4', 'E'),
 -- F26
 ('3', '2', '1', 'A'), 
 ('3', '2', '2', 'A'),
 ('3', '2', '3', 'A'),
 ('3', '2', '4', 'A'),
  -- Boleta de depósito 
 ('4', '2', '1', 'E'), 
 ('4', '2', '2', 'E'),
 ('4', '2', '3', 'E'),
 ('4', '2', '4', 'E'),
-- Croquis de ubicación 
 ('5', '2', '1', 'S'), 
 ('5', '2', '2', 'S'),
 ('5', '2', '3', 'S'),
 ('5', '2', '4', 'S'),
-- Poder de Representación 
 ('8', '2', '1', 'S'), 
 ('8', '2', '2', 'S'),
 ('8', '2', '3', 'S'),
 ('8', '2', '4', 'S'),
-- Constitución legal de la empresa
 ('9', '2', '1', 'E'), 
 ('9', '2', '2', 'E'),
 ('9', '2', '3', 'E'),
 ('9', '2', '4', 'E'),
 -- Contrato de prestación de servicios(Agente Auxiliar y propietario) 
 ('10', '2', '1', 'S'), 
 ('10', '2', '2', 'S'),
 ('10', '2', '3', 'S'),
 ('10', '2', '4', 'S'),
 -- Fotocopia de documento de identidad vigente del propietario 
 ('11', '2', '1', 'E'), 
 ('11', '2', '2', 'E'),
 ('11', '2', '3', 'E'),
 ('11', '2', '4', 'E'),
  -- Fotocopia de documento de identidad vigente del representante legal
 ('12', '2', '1', 'S'), 
 ('12', '2', '2', 'S'),
 ('12', '2', '3', 'S'),
 ('12', '2', '4', 'S');

   -- INSCRIPCION: Centro de Almacenamiento 	U
INSERT INTO `abt_archivo_actividad_categoria` (`config_archivo_id`, `actividad_id`, `categoria_id`, `clasificacion`)
 VALUES 
 -- Carta de solicitud 
 ('1', '23', '5', 'E'), 
 -- Programa de Abastecimiento 
 ('2', '23', '5', 'E'), 
 -- F26
 ('3', '23', '5', 'A'), 
  -- Boleta de depósito 
 ('4', '23', '5', 'E'), 
-- Croquis de ubicación 
 ('5', '23', '5', 'S'),
-- Registro biométrico NIT
 ('6', '23', '5', 'E'),
 -- Registro de FUNDEMPRESA
 ('7', '23', '5', 'E'), 
-- Poder de Representación 
 ('8', '23', '5', 'S'),
 -- Constitución legal de la empresa
 ('9', '23', '5', 'E'),
 -- Contrato de prestación de servicios(Agente Auxiliar y propietario) 
 ('10', '23', '5', 'S'),
  -- Fotocopia de documento de identidad vigente del propietario 
 ('11', '23', '5', 'E'),
 -- Fotocopia de documento de identidad vigente del representante legal
 ('12', '23', '5', 'S');

  -- INSCRIPCION: Carpinterías 	E
INSERT INTO `abt_archivo_actividad_categoria` (`config_archivo_id`, `actividad_id`, `categoria_id`, `clasificacion`)
 VALUES 
  -- Carta de solicitud 
 ('1', '4', '6', 'E'),
  -- Programa de Abastecimiento PAPMP
 ('2', '4', '6', 'E'), 
  -- F26
 ('3', '4', '6', 'A'), 
 -- Croquis de ubicación 
 ('5', '4', '6', 'S'),
  -- Contrato de prestación de servicios(Agente Auxiliar y propietario) 
 ('10', '4', '6', 'S'),
   -- Fotocopia de documento de identidad vigente del propietario 
 ('11', '4', '6', 'E'),
 -- Fotocopia de documento de identidad vigente del representante legal
 ('12', '4', '6', 'S'),
 -- Personería Jurídica de conformación de la Asociación
 ('13', '4', '6', 'E'),
  -- Poder Notariado del representante legal de la asociación
 ('14', '4', '6', 'S'),
 -- Lista de afiliados a la asociación
 ('15', '4', '6', 'E'),
 -- Certificación emitida por una asociación con registro ante la ABT
 ('16', '4', '6', 'E');

   -- INSCRIPCION: Barraca 	E
INSERT INTO `abt_archivo_actividad_categoria` (`config_archivo_id`, `actividad_id`, `categoria_id`, `clasificacion`)
 VALUES 
 -- Carta de solicitud 
 ('1', '2', '6', 'E'),
  -- Programa de Abastecimiento PAPMP
 ('2', '2', '6', 'E'),
   -- F26
 ('3', '2', '6', 'A'), 
  -- Croquis de ubicación 
 ('5', '2', '6', 'S'),
 -- Registro biométrico NIT
 ('6', '2', '6', 'E'),
 -- Registro de FUNDEMPRESA
 ('7', '2', '6', 'E'),
 -- Contrato de prestación de servicios(Agente Auxiliar y propietario) 
('10', '2', '6', 'S'),
 -- Fotocopia de documento de identidad vigente del propietario 
 ('11', '2', '6', 'E'),
 -- Fotocopia de documento de identidad vigente del representante legal
 ('12', '2', '6', 'S'),
 -- Personería Jurídica de conformación de la Asociación
 ('13', '2', '6', 'E'),
   -- Poder Notariado del representante legal de la asociación
 ('14', '2', '6', 'S'),
 -- Lista de afiliados a la asociación 
 ('15', '2', '6', 'E');

    -- INSCRIPCION: Aserradero 	E
INSERT INTO `abt_archivo_actividad_categoria` (`config_archivo_id`, `actividad_id`, `categoria_id`, `clasificacion`)
 VALUES 
 -- Carta de solicitud 
 ('1', '1', '6', 'E'),
  -- Programa de Abastecimiento PAPMP
 ('2', '1', '6', 'E'),
   -- F26
 ('3', '1', '6', 'A'), 
  -- Croquis de ubicación 
 ('5', '1', '6', 'S'),
 -- Registro biométrico NIT
 ('6', '1', '6', 'E'),
 -- Registro de FUNDEMPRESA
 ('7', '1', '6', 'E'),
 -- Poder de Representación
 ('8', '1', '6', 'S'),
 -- Contrato de prestación de servicios(Agente Auxiliar y propietario) 
('10', '1', '6', 'S'),
 -- Fotocopia de documento de identidad vigente del propietario 
 ('11', '1', '6', 'E'),
 -- Fotocopia de documento de identidad vigente del representante legal
 ('12', '1', '6', 'S');

    -- INSCRIPCION: Exportadora 	U
INSERT INTO `abt_archivo_actividad_categoria` (`config_archivo_id`, `actividad_id`, `categoria_id`, `clasificacion`)
 VALUES 
 -- Carta de solicitud 
 ('1', '10', '5', 'E'),
  -- Programa de Abastecimiento PAPMP
 ('2', '10', '5', 'E'),
   -- F26
 ('3', '10', '5', 'A'), 
  -- Croquis de ubicación 
 ('5', '10', '5', 'S'),
 -- Boleta de depósito 
 ('4', '10', '5', 'E'), 
 -- Registro biométrico NIT
 ('6', '10', '5', 'E'),
 -- Registro de FUNDEMPRESA
 ('7', '10', '5', 'E'),
 -- Poder de Representación
 ('8', '10', '5', 'S'),
 -- Constitución legal de la empresa
 ('9', '10', '5', 'E'),
 -- Contrato de prestación de servicios(Agente Auxiliar y propietario) 
('10', '10', '5', 'S'),
 -- Fotocopia de documento de identidad vigente del propietario 
 ('11', '10', '5', 'E'),
 -- Fotocopia de documento de identidad vigente del representante legal
 ('12', '10', '5', 'S'),
 -- Registro SENASAG original o fotocopia legalizada
 ('17', '10', '5', 'S'),
 -- RUEX- SENAVEX original o fotocopia legalizada
 ('18', '10', '5', 'S');

     -- INSCRIPCION: Importadora 	U
INSERT INTO `abt_archivo_actividad_categoria` (`config_archivo_id`, `actividad_id`, `categoria_id`, `clasificacion`)
 VALUES 
 -- Carta de solicitud 
 ('1', '21', '5', 'E'),
  -- Programa de Abastecimiento PAPMP
 ('2', '21', '5', 'E'),
   -- F26
 ('3', '21', '5', 'A'), 
  -- Croquis de ubicación 
 ('5', '21', '5', 'S'),
 -- Boleta de depósito 
 ('4', '21', '5', 'E'), 
 -- Registro biométrico NIT
 ('6', '21', '5', 'E'),
 -- Registro de FUNDEMPRESA
 ('7', '21', '5', 'E'),
 -- Poder de Representación
 ('8', '21', '5', 'S'),
 -- Constitución legal de la empresa
 ('9', '21', '5', 'E'),
 -- Contrato de prestación de servicios(Agente Auxiliar y propietario) 
('10', '21', '5', 'S'),
 -- Fotocopia de documento de identidad vigente del propietario 
 ('11', '21', '5', 'E'),
 -- Fotocopia de documento de identidad vigente del representante legal
 ('12', '21', '5', 'S'),
 -- Registro SENASAG original o fotocopia legalizada
 ('17', '21', '5', 'E'),
 -- RUEX- SENAVEX original o fotocopia legalizada
 ('18', '21', '5', 'E');

      -- INSCRIPCION: Carbonera 	A, B, C
INSERT INTO `abt_archivo_actividad_categoria` (`config_archivo_id`, `actividad_id`, `categoria_id`, `clasificacion`)
 VALUES 
 -- Carta de solicitud 
 ('1', '3', '1', 'E'),
 ('1', '3', '2', 'E'),
 ('1', '3', '3', 'E'),
 -- Programa de Abastecimiento PAPMP
 ('2', '3', '1', 'E'),
 ('2', '3', '2', 'E'),
 ('2', '3', '3', 'E'),
   -- F26
 ('3', '3', '1', 'A'), 
 ('3', '3', '2', 'A'), 
 ('3', '3', '3', 'A'), 
  -- Croquis de ubicación 
 ('5', '3', '1', 'S'),
 ('5', '3', '2', 'S'),
 ('5', '3', '3', 'S'),
 -- Boleta de depósito 
 ('4', '3', '1', 'E'), 
 ('4', '3', '2', 'E'), 
 ('4', '3', '3', 'E'), 
 -- Registro biométrico NIT
 ('6', '3', '1', 'E'),
 ('6', '3', '2', 'E'),
 ('6', '3', '3', 'E'),
 -- Registro de FUNDEMPRESA
 ('7', '3', '1', 'E'),
 ('7', '3', '2', 'E'),
 ('7', '3', '3', 'E'),
 -- Poder de Representación
 ('8', '3', '1', 'S'),
 ('8', '3', '2', 'S'),
 ('8', '3', '3', 'S'),
 -- Contrato de prestación de servicios(Agente Auxiliar y propietario) 
('10', '3', '1', 'S'),
('10', '3', '2', 'S'),
('10', '3', '3', 'S'),
 -- Fotocopia de documento de identidad vigente del propietario 
 ('11', '3', '1', 'E'),
 ('11', '3', '2', 'E'),
 ('11', '3', '3', 'E'),
 -- Fotocopia de documento de identidad vigente del representante legal
 ('12', '3', '1', 'S'),
 ('12', '3', '2', 'S'),
 ('12', '3', '3', 'S');

      -- INSCRIPCION: Comercializadoras de  productos no maderables de bosque natural 	U
INSERT INTO `abt_archivo_actividad_categoria` (`config_archivo_id`, `actividad_id`, `categoria_id`, `clasificacion`)
 VALUES 
 -- Carta de solicitud 
 ('1', '22', '5', 'E'),
 -- Programa de Abastecimiento PAPMP
 ('2', '22', '5', 'E'),
   -- F26
 ('3', '22', '5', 'A'), 
  -- Croquis de ubicación 
 ('5', '22', '5', 'S'),
 -- Boleta de depósito 
 ('4', '22', '5', 'E'), 
 -- Registro biométrico NIT
 ('6', '22', '5', 'E'),
 -- Registro de FUNDEMPRESA
 ('7', '22', '5', 'E'),
 -- Poder de Representación
 ('8', '22', '5', 'S'),
 -- Contrato de prestación de servicios(Agente Auxiliar y propietario) 
('10', '22', '5', 'S'),
 -- Fotocopia de documento de identidad vigente del propietario 
 ('11', '22', '5', 'E'),
 -- Fotocopia de documento de identidad vigente del representante legal
 ('12', '22', '5', 'S');

       -- INSCRIPCION: Comercializadoras de  Carbón 	U
INSERT INTO `abt_archivo_actividad_categoria` (`config_archivo_id`, `actividad_id`, `categoria_id`, `clasificacion`)
 VALUES 
 -- Carta de solicitud 
 ('1', '24', '5', 'E'),
 -- Programa de Abastecimiento PAPMP
 ('2', '24', '5', 'E'),
   -- F26
 ('3', '24', '5', 'A'), 
  -- Croquis de ubicación 
 ('5', '24', '5', 'S'),
 -- Boleta de depósito 
 ('4', '24', '5', 'E'), 
 -- Registro biométrico NIT
 ('6', '24', '5', 'E'),
 -- Registro de FUNDEMPRESA
 ('7', '24', '5', 'E'),
 -- Poder de Representación
 ('8', '24', '5', 'S'),
 -- Contrato de prestación de servicios(Agente Auxiliar y propietario) 
('10', '24', '5', 'S'),
 -- Fotocopia de documento de identidad vigente del propietario 
 ('11', '24', '5', 'E'),
 -- Fotocopia de documento de identidad vigente del representante legal
 ('12', '24', '5', 'S');

 -- INSCRIPCION: Comercializadoras de  productos provenientes de plantaciones 	U
INSERT INTO `abt_archivo_actividad_categoria` (`config_archivo_id`, `actividad_id`, `categoria_id`, `clasificacion`)
 VALUES 
 -- Carta de solicitud 
 ('1', '25', '5', 'E'),
 -- Programa de Abastecimiento PAPMP
 ('2', '25', '5', 'E'),
   -- F26
 ('3', '25', '5', 'A'), 
  -- Croquis de ubicación 
 ('5', '25', '5', 'S'),
 -- Boleta de depósito 
 ('4', '25', '5', 'E'), 
 -- Registro biométrico NIT
 ('6', '25', '5', 'E'),
 -- Registro de FUNDEMPRESA
 ('7', '25', '5', 'E'),
 -- Poder de Representación
 ('8', '25', '5', 'S'),
 -- Contrato de prestación de servicios(Agente Auxiliar y propietario) 
('10', '25', '5', 'S'),
 -- Fotocopia de documento de identidad vigente del propietario 
 ('11', '25', '5', 'E'),
 -- Fotocopia de documento de identidad vigente del representante legal
 ('12', '25', '5', 'S')
 ;

  -- INSCRIPCION: Comercializadoras de  productos provenientes de plantaciones 	U
INSERT INTO `abt_archivo_actividad_categoria` (`config_archivo_id`, `actividad_id`, `categoria_id`, `clasificacion`)
 VALUES 
 -- Carta de solicitud 
 ('1', '27', '5', 'E'),
 -- Programa de Abastecimiento PAPMP
 ('2', '27', '5', 'E'),
   -- F26
 ('3', '27', '5', 'A'), 
  -- Croquis de ubicación 
 ('5', '27', '5', 'S'),
 -- Boleta de depósito 
 ('4', '27', '5', 'E'), 
 -- Registro biométrico NIT
 ('6', '27', '5', 'E'),
 -- Registro de FUNDEMPRESA
 ('7', '27', '5', 'E'),
 -- Poder de Representación
 ('8', '27', '5', 'S'),
 -- Contrato de prestación de servicios(Agente Auxiliar y propietario) 
('10', '27', '5', 'S'),
 -- Fotocopia de documento de identidad vigente del propietario 
 ('11', '27', '5', 'E'),
 -- Fotocopia de documento de identidad vigente del representante legal
 ('12', '27', '5', 'S')
 ;

 -- INSCRIPCION: Desmontadoras 	U
INSERT INTO `abt_archivo_actividad_categoria` (`config_archivo_id`, `actividad_id`, `categoria_id`, `clasificacion`)
 VALUES 
 -- Carta de solicitud 
 ('1', '8', '5', 'E'),
 -- Programa de Abastecimiento PAPMP
 ('2', '8', '5', 'E'),
   -- F26
 ('3', '8', '5', 'A'), 
  -- Croquis de ubicación 
 ('5', '8', '5', 'S'),
 -- Boleta de depósito 
 ('4', '8', '5', 'E'), 
 -- Registro biométrico NIT
 ('6', '8', '5', 'E'),
 -- Registro de FUNDEMPRESA
 ('7', '8', '5', 'E'),
 -- Poder de Representación
 ('8', '8', '5', 'S'),
 -- Constitución legal de la empresa
 ('9', '8', '5', 'E'),
 -- Contrato de prestación de servicios(Agente Auxiliar y propietario) 
('10', '8', '5', 'S'),
 -- Fotocopia de documento de identidad vigente del propietario 
 ('11', '8', '5', 'E'),
 -- Fotocopia de documento de identidad vigente del representante legal
 ('12', '8', '5', 'S'),
 -- Documentación que acredite derecho propietario y/o documentación
 ('19', '8', '5', 'S')

 ;


  -- INSCRIPCION: Tratamiento y/o preservación 	U
INSERT INTO `abt_archivo_actividad_categoria` (`config_archivo_id`, `actividad_id`, `categoria_id`, `clasificacion`)
 VALUES 
 -- Carta de solicitud 
 ('1', '19', '5', 'E'),
 -- Programa de Abastecimiento PAPMP
 ('2', '19', '5', 'E'),
   -- F26
 ('3', '19', '5', 'A'), 
  -- Croquis de ubicación 
 ('5', '19', '5', 'S'),
 -- Boleta de depósito 
 ('4', '19', '5', 'E'), 
 -- Registro biométrico NIT
 ('6', '19', '5', 'E'),
 -- Registro de FUNDEMPRESA
 ('7', '19', '5', 'E'),
 -- Poder de Representación
 ('8', '19', '5', 'S'),
 -- Constitución legal de la empresa
 ('9', '19', '5', 'E'),
 -- Contrato de prestación de servicios(Agente Auxiliar y propietario) 
('10', '19', '5', 'S'),
 -- Fotocopia de documento de identidad vigente del propietario 
 ('11', '19', '5', 'E'),
 -- Fotocopia de documento de identidad vigente del representante legal
 ('12', '19', '5', 'S'),
 -- Documentación que acredite derecho propietario y/o documentación
 ('19', '19', '5', 'S')
 ;

   -- INSCRIPCION: Aprovechamiento Integral del bosque 	U
INSERT INTO `abt_archivo_actividad_categoria` (`config_archivo_id`, `actividad_id`, `categoria_id`, `clasificacion`)
 VALUES 
 -- Carta de solicitud 
 ('1', '20', '5', 'E'),
 -- Programa de Abastecimiento PAPMP
 ('2', '20', '5', 'E'),
   -- F26
 ('3', '20', '5', 'A'), 
  -- Croquis de ubicación 
 ('5', '20', '5', 'S'),
 -- Boleta de depósito 
 ('4', '20', '5', 'E'), 
 -- Registro biométrico NIT
 ('6', '20', '5', 'E'),
 -- Registro de FUNDEMPRESA
 ('7', '20', '5', 'E'),
 -- Poder de Representación
 ('8', '20', '5', 'S'),
 -- Constitución legal de la empresa
 ('9', '20', '5', 'E'),
 -- Contrato de prestación de servicios(Agente Auxiliar y propietario) 
('10', '20', '5', 'S'),
 -- Fotocopia de documento de identidad vigente del propietario 
 ('11', '20', '5', 'E'),
 -- Fotocopia de documento de identidad vigente del representante legal
 ('12', '20', '5', 'S'),
 -- Documentación que acredite derecho propietario y/o documentación
 ('19', '20', '5', 'S')
 ;

    -- INSCRIPCION: Consumidor final de productos forestales 	U
INSERT INTO `abt_archivo_actividad_categoria` (`config_archivo_id`, `actividad_id`, `categoria_id`, `clasificacion`)
 VALUES 
  -- Carta de solicitud 
 ('1', '26', '5', 'E'),
 -- PAPMP
 ('2', '26', '5', 'E'),
  -- Croquis de ubicación 
 ('5', '26', '5', 'S'),
  -- Registro biométrico NIT
 ('6', '26', '5', 'E'),
 -- Registro de FUNDEMPRESA
 ('7', '26', '5', 'E'),
 -- Fotocopia de documento de identidad vigente del propietario 
 ('11', '26', '5', 'E'),
   -- Poder Notariado del representante legal de la asociación
 ('14', '26', '5', 'S'),
 -- Original del Formulario de Registro Empresas Consumidores
 ('21', '26', '5', 'E');


 -- ************************** configuracion Volumenes Actividad Categoria  *****************************
TRUNCATE `abt_moviento_volumen`;
-- Aserradero y Categoria A
INSERT INTO `abt_moviento_volumen` (`actividad_id`, `categoria_id`, `operacion`, `valor_inicial`, `unidad_id`, `producto_id`) 
VALUES ('1', '1', '>', '10001', 4, 1024);

-- Aserradero y Categoria B
INSERT INTO `abt_moviento_volumen` (`actividad_id`, `categoria_id`, `operacion`, `valor_inicial`, `valor_final`, `unidad_id`, `producto_id`) 
VALUES ('1', '2', '-', '5001', '10000', 4, 1024),
-- Aserradero y Categoria C
('1', '3', '-', '1001', '5000', 4, 1024);

-- Aserradero y Categoria D
INSERT INTO `abt_moviento_volumen` (`actividad_id`, `categoria_id`, `operacion`, `valor_inicial`, `unidad_id`, `producto_id`) 
VALUES ('1', '4', '<', '1000', 4, 1024);

-- Carbonera y Categoria A
INSERT INTO `abt_moviento_volumen` (`actividad_id`, `categoria_id`, `operacion`, `valor_inicial`, `unidad_id`, `producto_id`) 
VALUES ('3', '1', '>', '1001', 7, 0);
-- Carbonera y Categoria B
INSERT INTO `abt_moviento_volumen` (`actividad_id`, `categoria_id`, `operacion`, `valor_inicial`, `valor_final`, `unidad_id`, `producto_id`) 
VALUES ('3', '2', '-', '501', '1000', 7, 0);
-- Carbonera y Categoria C
INSERT INTO `abt_empresa`.`abt_moviento_volumen` (`actividad_id`, `categoria_id`, `operacion`, `valor_inicial`, `unidad_id`, `producto_id`) 
VALUES ('3', '1', '<', '500', 7, 0);

-- Carpintería y Categoria A
INSERT INTO `abt_moviento_volumen` (`actividad_id`, `categoria_id`, `operacion`, `valor_inicial`, `unidad_id`, `producto_id`) 
VALUES ('4', '1', '>', '1000001', 9, 0);
-- Carpintería y Categoria B
INSERT INTO `abt_moviento_volumen` (`actividad_id`, `categoria_id`, `operacion`, `valor_inicial`, `valor_final`, `unidad_id`, `producto_id`) 
VALUES ('4', '2', '-', '500001', '1000000', 9, 0),
-- Carpintería y Categoria C
('4', '3', '-', '100001', '500000', 9, 0),
-- Carpintería y Categoria D
('4', '4', '-', '50001', '100000', 9, 0);
-- Carpintería y Categoria E
INSERT INTO `abt_moviento_volumen` (`actividad_id`, `categoria_id`, `operacion`, `valor_inicial`, `unidad_id`, `producto_id`) 
VALUES ('4', '6', '<', '50000', 9, 0);

-- Barraca y Categoria A
INSERT INTO `abt_moviento_volumen` (`actividad_id`, `categoria_id`, `operacion`, `valor_inicial`, `unidad_id`, `producto_id`) 
VALUES ('2', '1', '>', '2000001', 9, 0);

INSERT INTO `abt_moviento_volumen` (`actividad_id`, `categoria_id`, `operacion`, `valor_inicial`, `valor_final`, `unidad_id`, `producto_id`) 
-- Barraca y Categoria B
VALUES ('2', '2', '-', '1000001', '2000000', 9, 0),
-- Barraca y Categoria C
('2', '3', '-', '100001', '1000000', 9, 0),
-- Barraca y Categoria D
('2', '4', '-', '50001', '100000', 9, 0);
-- Barraca y Categoria E
INSERT INTO `abt_moviento_volumen` (`actividad_id`, `categoria_id`, `operacion`, `valor_inicial`, `unidad_id`, `producto_id`) 
VALUES ('2', '6', '<', '50000', 9, 0);
