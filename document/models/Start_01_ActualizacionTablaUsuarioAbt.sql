 /************************* Actualizacion de tabla Usuario ABT ****************/
 ALTER TABLE `sys_usuario`
	ADD COLUMN `tipo` ENUM('E','A', 'F') NULL DEFAULT NULL COMMENT 'E : Empresa / Propietario, A: Agente Auxiliar, F: Funcionario ABT' AFTER `codagenteauxiliar`,
	ADD COLUMN `codentidad` INT NULL DEFAULT NULL COMMENT 'Hace referencia a la tabla abt_persona, v_agente_auxiliar, rrhh_funcionario dependiendo del tipo' AFTER `Tipo`;

ALTER TABLE `sys_usuario`
	ADD COLUMN `fecha_reset_pw` DATETIME NULL AFTER `codentidad` COMMENT 'Campo utilizado para registrar la ultima fecha en la que el usuario a reseteado su contraseña';

ALTER TABLE `bf_users`
	ADD COLUMN `user_abt_id` INT NULL DEFAULT '0' AFTER `force_password_reset`;
	
ALTER TABLE `bf_users`
	DROP INDEX `email`,
	ADD UNIQUE INDEX `username` (`username`);
	
	
ALTER TABLE `bf_users`
	ADD UNIQUE INDEX `username` (`username`);