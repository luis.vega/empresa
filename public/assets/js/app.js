
// Inicializar componentes
$(document).ready(function() {

	$('.tool-help').tooltip(); 

	startblockUI();
	$('.popup-message.panel-heading.close')
});


function redirectTo(urlRedirect) {
	if (urlRedirect)
		window.location.href = baseUrl + urlRedirect;
}

function stringRandom(size) {
	var emptyString = "";
	var alphabet = "abcdefghijklmnopqrstuvwxyz012345679";

	while (emptyString.length < size) {
	  emptyString += alphabet[Math.floor(Math.random() * alphabet.length)];
	}
	return emptyString;
}

function startblockUI() {
	var htmlDiv = '<div class="sk-double-bounce">' +
        '<div class="sk-child sk-double-bounce1"></div>' +
        '<div class="sk-child sk-double-bounce2"></div>' +
      '</div>' + '<br> <h3> Por favor espere... </h3>' ;
	$.blockUI.defaults.message= htmlDiv;
}

//*************************** Dialog Error / Success *************************
function showErrors(messages, title) {
	var messageString = "";
	var stringIcon = '<span class="glyphicon glyphicon-chevron-right pull-left"></span>';
	var template = dialogErrorTemplate();
	if (typeof messages === 'string') {
		messageString = '<p class="row text-left">' + stringIcon + ' ' + messages + ' </p>';
	} else {

		if(messages) {
			for (var index = 0; index < messages.length; index++) {
				messageString += '<p class="row text-left">' + stringIcon + ' ' + messages[index] + ' </p>';
			}
			
		} else {
			alert('Error al intentar  mostrar mensaje');
		}

	}
	template = template.replace('{messages}', messageString);
	if (title)
		template = template.replace('{title}', title);
	else 
		template = template.replace('{title}', 'Error');

	$.blockUI({ message: template, css: { width: '42%' } });

	$('.blockUI.blockMsg.blockPage').css('top', '20%');
	$('.blockUI.blockMsg.blockPage').css('max-height', '300px');
	$('.blockUI.blockMsg.blockPage').css('overflow-y', 'scroll');
}

function showSuccess(messages) {
	var messageString = "";
	var stringIcon = '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true">';
	var template = dialogSuccessTemplate();
	if (typeof messages === 'string') {
		messageString = '<p>' + stringIcon + ' ' + messages + ' </p>';
	} else {

		if(messages) {
			for (var index = 0; index < messages.length; index++) {
				messageString += '<p class="row">' + stringIcon + ' ' + messages[index] + ' </span> </p>';
			}
			
			template = template.replace('{messages}', messageString);
			template = template.replace('{title}', 'ABT: Empresa Forestal');

 			$.blockUI({ message: template, css: { width: '370px' } });  
		} else {
			alert('Error al intentar  mostrar mensaje');
		}

		$(document).on("mousedown", $('.close-popup-success'), function(e){
			if(e.button == 0 ) 
				$.unblockUI();
		});
	}

}

function closeMesage(element) {
	$.unblockUI();
}

function dialogErrorTemplate() {
	var template = '<div class="panel panel-danger popup-message" style="cursor: default; padding: 18px;">' +
  '<div class="panel-heading">{title} ' +
  ' <button type="button" class="btn btn-danger btn-circle pull-right close-popup" onclick="closeMesage(this)" style="margin: -5px;"><i class="glyphicon glyphicon-remove"></i></button>' +
  '</div>' +
  '<div class="panel-body"> {messages} </div> </div>' ;
  return template;
}

function dialogSuccessTemplate() {
	var template = '<div class="panel panel-success popup-message" style="cursor: default; padding: 18px;">' +
  '<div class="panel-heading">{title} ' +
  ' <button type="button" class="btn btn-success btn-circle pull-right close-popup-success" style="margin: -5px;"><i class="glyphicon glyphicon-remove"></i></button>' +
  '</div>' +
  '<div class="panel-body"> {messages} </div> </div>' ;
  return template;
}