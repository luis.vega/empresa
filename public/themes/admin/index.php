<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ABT Administrador</title>
    
    <!-- Bootstrap Core CSS -->
	<link href="<?php echo Template::theme_url('/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
	
    <!-- MetisMenu CSS -->
    <link href="<?php echo Template::theme_url('/vendor/metisMenu/metisMenu.min.css'); ?>" rel="stylesheet">

    <!-- SB-Admin-2 CSS -->
    <link href="<?php echo Template::theme_url('/dist/css/sb-admin-2.css'); ?>" rel="stylesheet">
    
     <!-- Custom CSS -->
    <link href="<?php echo Template::theme_url('/css/custom.css'); ?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo Template::theme_url('/vendor/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- jQuery -->
    <script src="<?php echo Template::theme_url('/vendor/jquery/jquery.min.js'); ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo Template::theme_url('/vendor/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo Template::theme_url('/vendor/validator/validator_bootstrap.js'); ?>"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo Template::theme_url('/vendor/metisMenu/metisMenu.min.js'); ?>"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo Template::theme_url('/dist/js/sb-admin-2.js'); ?>"></script>
    <script src="<?php echo Template::theme_url('/vendor/blockui/jquery.blockUI.js'); ?>"></script>
    <script src="<?php echo Template::theme_url('/js/app.js'); ?>"></script>
</head>

<body>
 <script type="text/javascript">
     var baseUrl = "<?php echo base_url(); ?>";
 </script>
    <div id="wrapper">

        <!-- navigation -->
        <?php echo theme_view('navigation'); ?>
		<!-- ./navigation -->

        <!-- page-wrapper -->
        <div id="page-wrapper">
        
         	<!-- page-breadcrumb -->
        	<div id="page-breadcrumb">
					 <?php echo theme_view('breadcrumb'); ?>
            </div>
            <!-- ./page-breadcrumb -->
            
            <div id="page-content">
               <?php
            		echo Template::message();
            		echo isset($content) ? $content : Template::content();
        		?>
            </div>

        </div>
        <!-- ./page-wrapper -->

    </div>
    <!-- /#wrapper -->


</body>

</html>
