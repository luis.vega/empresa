<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">

    <?php echo theme_view('navbar_header'); ?>
    <!-- /.navbar-header -->

    <?php echo theme_view('navbar_top_links'); ?>
    <!-- /.navbar-top-links -->

    <?php echo theme_view('navbar_sidebar_static'); ?>
    <!-- /.navbar-static-side -->
    
</nav>