            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="<?php echo site_url(SITE_AREA . '/index'); ?>">Inicio</a>
                        </li>
                       
                        <li>
                            <a href="#">Empresas<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                            	 <li>
                            		<a href="<?php echo site_url(SITE_AREA . '/solicitudes'); ?>">Solicitudes de Empresas</a>
                        		</li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->