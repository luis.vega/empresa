    <?php if ( ! isset($show) || $show == true) : ?>
    <hr />
    <footer class="footer">
        <div class="container">
            <p>
                Copyright ©  <a href="http://www.abt.gob.bo" target="_blank">&nbsp; 
                ABT - Desarrollado por la Unidad de Sistemas de Información </a> <?php echo APP_VERSION; ?>
            </p>

        </div>
    </footer>
    <?php endif; ?>
	<div id="debug"><!-- Stores the Profiler Results --></div>
    <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
    <script>window.jQuery || document.write('<script src="<?php echo js_path(); ?>jquery-3.2.1.min.js"><\/script>');</script>
    <?php echo Assets::js(); ?>
</body>
</html>