<div class="masthead">
    <img class="logo-header" src="<?php echo site_url(); ?>assets/images/logoAbt.png">
    <ul class="nav nav-pills pull-right">
        <?php if (empty($current_user)) : ?>
        <?php else : ?>
        <li <?php echo check_method('perfil'); ?>><a href="<?php echo 'perfil'; ?>"><?php e('Mi Perfil'); ?></a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Empresa <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li <?php echo check_class('solicitudes'); ?>><a href="<?php echo site_url(); ?>solicitudes"><?php e('Solicitudes'); ?></a></li>
          </ul>
        </li>
        <li><a href="<?php echo site_url('logout'); ?>"> Cerrar Sesión <span class="glyphicon glyphicon-ban-circle"></span></a></li>
        <?php endif; ?>
    </ul>

    <div class="row visible-xs-block">
        <div class="col-md-4">
            <!-- It can be fixed with bootstrap affix http://getbootstrap.com/javascript/#affix-->
            <div id="sidebar" class="well sidebar-nav">
                <h5><i class="glyphicon glyphicon-home"></i>
                    <small><b>Administrar</b></small>
                </h5>
                <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="<?php echo site_url(); ?>"><?php e(lang('bf_home')); ?></a></li>
                  <?php if (!empty($current_user)) : ?>
                    <li><a href="<?php echo site_url(); ?>solicitudes"><?php e('Solicitudes'); ?></a></li>
                  <?php endif; ?>
                </ul>
              <?php if (!empty($current_user)) : ?>
                <h5><i class="glyphicon glyphicon-user"></i>
                    <small><b>Usuario</b></small>
                </h5>
                <ul class="nav nav-pills nav-stacked">
                    <li><a href="<?php echo 'perfil'; ?>"><?php e('Mi Perfil'); ?></a></li>
                    <li><a href="<?php echo site_url('logout'); ?>">Cerrar Sesión <span class="glyphicon glyphicon-ban-circle"></a></li>
                </ul>
              <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<hr />