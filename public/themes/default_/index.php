<?php echo theme_view('header'); ?>

<script type="text/javascript">
	var baseUrl = '<?php echo base_url()?>';
</script>

<div class="container" id="container"><!-- Start of Main Container -->
	<div class="col-md-1"></div>
	<div id="contenedor" class="col-md-10">
	    <?php
		    echo theme_view('_sitenav');

		    echo Template::message();
		    echo isset($content) ? $content : Template::content();

	    ?>

		</div>
	<div class="col-md-1"></div>

</div>
<?php echo theme_view('footer');?>