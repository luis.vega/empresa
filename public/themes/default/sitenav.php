<!-- Navigation
    ==========================================-->

<nav id="menu" class="navbar navbar-default navbar-fixed-top">
  <div class="container"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="/"><i class="fa fa-sun-o"></i> ABT<strong></strong></a> 
    </div>
    
    <?php if (!empty($current_user)) : ?>
    <!-- Collect the nav links, forms, and other content for toggling --> 
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?php echo site_url(); ?>perfil#actual-page" >Mi Perfil</a></li>
        <li><a href="<?php echo site_url(); ?>solicitudes#actual-page">Solicitudes</a></li>
        <li><a href="<?php echo site_url('logout'); ?>">Salir <span class="glyphicon glyphicon-ban-circle"></span></a></li>
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
    <?php endif;?>
  </div>
  <!-- /.container-fluid --> 
</nav>
