<?php
Assets::add_css(array('bootstrap.min.css', 'select2.min.css', 'stepwizard.css', 'app.css'));

Assets::add_js('bootstrap.min.js');
Assets::add_js('jquery.blockUI.js');
Assets::add_js('validator_bootstrap.js');
Assets::add_js('select2.min.js');
Assets::add_js('app.js');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Helios</title>
<meta name="description" content="">
<meta name="author" content="">

<?php echo Assets::css(); ?>

</head>
<body>

  	<div class="container">
		<?php
		    echo isset($content) ? $content : Template::content();
	    ?>
  	</div>
	
    <script>window.jQuery || document.write('<script src="<?php echo js_path(); ?>jquery-3.2.1.min.js"><\/script>');</script>
  	
  	<?php echo Assets::js(); ?>
  	
</body>
</html>