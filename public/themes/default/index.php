<?php
Assets::add_css(array('bootstrap.min.css', 'select2.min.css', 'stepwizard.css', 'app.css'));

Assets::add_js('bootstrap.min.js');
Assets::add_js('jquery.blockUI.js');
Assets::add_js('validator_bootstrap.js');
Assets::add_js('select2.min.js');
Assets::add_js('app.js');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ABT RUEF</title>
<meta name="description" content="">
<meta name="author" content="">


<!-- Stylesheet
    ================================================== -->
  
   	<link href="<?php echo Template::theme_url('fonts/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet">
	<link href="<?php echo Template::theme_url('css/style.css'); ?>" rel="stylesheet">
	<?php /*?>
	  	<link href="<?php echo Template::theme_url('css/bootstrap.min.css'); ?>" rel="stylesheet">
		<link href="<?php echo Template::theme_url('css/prettyPhoto.css'); ?>" rel="stylesheet">
	<?php */?>
	<?php echo Assets::css(); ?>
	<link href='http://fonts.googleapis.com/css?family=Lato:400,700,900,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300' rel='stylesheet' type='text/css'>
	 
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<!-- Javascripts
    ================================================== --> 
<?php /*?>
<script src="<?php echo Template::theme_url('/js/jquery.min.js'); ?>"></script>
<script src="<?php echo Template::theme_url('/js/modernizr.custom.js'); ?>"></script>
<script src="<?php echo Template::theme_url('/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo Template::theme_url('/js/SmoothScroll.js'); ?>"></script>
<script src="<?php echo Template::theme_url('/js/jquery.counterup.js'); ?>"></script>
<script src="<?php echo Template::theme_url('/js/waypoints.js'); ?>"></script>
<script src="<?php echo Template::theme_url('/js/jquery.isotope.js'); ?>"></script>
<script src="<?php echo Template::theme_url('/js/jqBootstrapValidation.js'); ?>"></script>
<script src="<?php //echo Template::theme_url('/js/main.js'); ?>"></script>
<?php */?>

</head>
<body>

<script type="text/javascript">
	var baseUrl = '<?php echo base_url()?>';
</script>

	<?php echo theme_view('sitenav'); ?>

	<?php echo theme_view('header'); ?>
  	<div class="container" id="actual-page">
  		<div class="col-md-1"></div>
    	<div id="contenedor" class="col-md-10">

		<?php
		    echo Template::message();
		    echo isset($content) ? $content : Template::content();
	    ?>
		</div>
  		<div class="col-md-1"></div>
  	</div>
	
	<?php echo theme_view('footer');?>
	
    <script>window.jQuery || document.write('<script src="<?php echo js_path(); ?>jquery-3.2.1.min.js"><\/script>');</script>
  	<?php echo Assets::js(); ?>
  	
</body>
</html>