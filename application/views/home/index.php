<?php 
	// Recursos JS
	Assets::add_module_js('users','js/login_front.js');
?>
<div class="jumbotron" text-align="center">
	<h1>ABT RUEF</h1>

	<p class="lead">REGISTRO ÚNICO DE EMPRESAS FORESTALES</p>

<!--  <div class="jumbotron" text-align="center"> -->
<div text-align="center">
	<?php 

	if (isset($current_user->username) && $es_admin) : ?>
		<a href="<?php echo site_url(SITE_AREA) ?>" class="btn btn-large btn-success">Acceder Admin</a>
	<?php elseif (isset($current_user->username) && !$es_admin) : ?>
		<a href="<?php echo site_url('solicitudes') ?>" class="btn btn-large btn-success col-xl-8">Panel Solicitudes</a>
	<?php else:
		echo $this->load->view('users/login_front');
		
	?>
	<?php endif;?>

</div>

</div>