<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 *
 * @author Victor
 *
 */
class PersonaAdm_model extends BF_Model {

    const TIPO_PERSONA_NATURAL = 'N';
    const TIPO_PERSONA_JUDICIAL = 'J';


    const TIPO_USUARIO_EMPRESA = 'E';
    const TIPO_USUARIO_AGENTE_AUXILIAR = 'A';
    const TIPO_USUARIO_FUNCIONARIO_ABT = 'F';


    const GRUPO_USUARIO_PROPIETARIO = 75;

    const PERSONA_ESTADO_ACTIVO = 'A';

    const ROL_BONFIRE_USER = 4;



    public function __construct()
    {
    	parent::__construct();
    
    	$this->table_name = 'abt_persona';
    	$this->key = 'codpersona';
    }
    
    public function getById($codPersona) 
    {
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
      
        $fields = <<<EOF
    	p.*,
    	u.*,
    	concat(p.nombres,' ' ,p.appaterno, ' ' ,p.apmaterno) AS 'persona_full_name',
        pais.nombre as 'persona_pais',
EOF;
         
        $this->db->select($fields);
        $this->db->from("{$this->table_name} p");
        $this->db->join('sys_usuario u', 'u.codentidad = p.codpersona');
        $this->db->join('abt_pais pais', 'pais.codpais = p.codpaisorigen');
        
        $this->where('codgrupo', self::GRUPO_USUARIO_PROPIETARIO);
        $this->where('tipo', self::TIPO_USUARIO_EMPRESA);
        $this->where('codentidad !=', NULL);
        
        
        $persona = $this->find($codPersona);

        $this->db->dbprefix = $auxPrefix;

        return $persona;
    }

}