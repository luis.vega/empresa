<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * 
 * @author Victor
 *
 */
class SolicitudEmpresaAdm_model extends BF_Model 
{
	public  $SolicitudEstados;
	
	public $SolicitudTipos;
	
	public function __construct()
	{
		parent::__construct();
		$this->table_name = 'abt_solicitud_tramite_empresa';
		$this->key = 'solicitud_tramite_empresa_id';
		
		
		$this->SolicitudEstados = array();
		$this->SolicitudEstados ['P'] = 'Pendiente';
		$this->SolicitudEstados ['PR'] ='Pendiente Revisión';
		$this->SolicitudEstados ['A'] = 'Aprobada';
		$this->SolicitudEstados ['R'] = 'Rechazada';
		
		$this->SolicitudTipos = array();
		$this->SolicitudTipos['I'] = 'Inscripción';
		$this->SolicitudTipos['R'] = 'Reinscripción';
		$this->SolicitudTipos['A'] = 'Addición';
	}
	
	public function getById($id) {
	
		$auxPrefix = $this->db->dbprefix;
		$this->db->dbprefix = '';
	
	
		$solicitud = $this->find($id);
	
	
		$this->db->dbprefix = $auxPrefix;
	
		return $solicitud;
	}
	
	public function getInfoById($id) 
	{
		$auxPrefix = $this->db->dbprefix;
    	$this->db->dbprefix = '';
    	
    	$fields = <<<EOF
    	solicitud.solicitud_tramite_empresa_id,
    	solicitud.fecha_registro,
    	solicitud.empresa_id,
    	solicitud.codigo_usuario_id,
    	solicitud.gestion,
    	solicitud.tipo_solicitud,
    	solicitud.estado,
    	ef.razon_social_nombre AS 'empresa_nombre',
    	ef.numero_fundempresa AS 'empresa_numero_fundempresa',
    	ef.nit AS 'empresa_nit',
    	ef.propietario_codpersona AS 'empresa_propietario_codpersona',
    	concat(p.nombres,' ' ,p.appaterno, ' ' ,p.apmaterno) AS 'empresa_propietario',
        concat(u.Nombres,' ',u.Apellidos) AS 'usuario',
    	concat(p.nrodocid,' ',p.emitido) AS 'empresa_propietario_documento'
EOF;

    	$this->db->select($fields);
    	$this->db->from("{$this->table_name} solicitud");
    	$this->db->join('abt_empresa_forestal ef', 'ef.empresa_forestal_id=solicitud.empresa_id');
    	$this->db->join('abt_persona p', 'p.codpersona=ef.propietario_codpersona' , 'left');
    	$this->db->join('sys_usuario u', 'u.CodUsuario=solicitud.codigo_usuario_id');
    
    	//solicitudes borrador (no mostrar)
    	$this->where_not_in('solicitud.estado' , array('P'));
    	
    	//filtro estado
    	if( $filters && isset($filters->solicitud_estado) && in_array($filters->solicitud_estado, array('PR','A','P','R')))
    	{
    		$this->where('solicitud.estado', $filters->solicitud_estado);
    	}
    	
    	//filtro estado
    	if( $filters && $filters->solicitud_id > 0 )
    	{
    		$this->where('solicitud.solicitud_tramite_empresa_id', "{$filters->solicitud_id}");
    	}
    	
    	//filtro estado
    	if( $filters && !empty($filters->empresa_nombre) )
    	{
    		$this->like('ef.razon_social_nombre', "$filters->empresa_nombre");
    	}
    	
    	$this->order_by('solicitud.solicitud_tramite_empresa_id' , 'desc');

    	if($pagination)
    	{
    		$start = isset($pagination->per_page) ? $pagination->per_page : 1 ;
    		$items = isset($pagination->sho_per_pg) ? $pagination->sho_per_pg : 10 ;
    		$this->limit($items, ($start - 1) * $items );
    	}
    
    	$query = $this->db->get();
    	$list = $query->result();
    
    	$this->db->dbprefix = $auxPrefix;
    
    	return $list;
	}
	
    
    public function getSolicitudes($pagination = null , $filters = null)
    {
    	$auxPrefix = $this->db->dbprefix;
    	$this->db->dbprefix = '';

    	$fields = <<<EOF
    	solicitud.solicitud_tramite_empresa_id,
    	solicitud.fecha_registro,
    	solicitud.empresa_id,
    	solicitud.codigo_usuario_id,
    	solicitud.gestion,
    	solicitud.tipo_solicitud,
    	solicitud.estado,
    	ef.razon_social_nombre AS 'empresa_nombre',
    	ef.numero_fundempresa AS 'empresa_numero_fundempresa',
    	ef.nit AS 'empresa_nit',
    	ef.propietario_codpersona AS 'empresa_propietario_codpersona',
    	concat(p.nombres,' ' ,p.appaterno, ' ' ,p.apmaterno) AS 'empresa_propietario',
    	concat(u.Nombres,' ',u.Apellidos) AS 'usuario'
EOF;

    	$this->db->select($fields);
    	$this->db->from("{$this->table_name} solicitud");
    	$this->db->join('abt_empresa_forestal ef', 'ef.empresa_forestal_id=solicitud.empresa_id');
    	$this->db->join('abt_persona p', 'p.codpersona=ef.propietario_codpersona' , 'left');
    	$this->db->join('sys_usuario u', 'u.CodUsuario=solicitud.codigo_usuario_id');

    	//solicitudes borrador (no mostrar)
    	$this->where_not_in('solicitud.estado' , array('P'));

    	//filtro estado
    	if( $filters && isset($filters->solicitud_estado) && in_array($filters->solicitud_estado, array('PR','A','P','R')))
    	{
    		$this->where('solicitud.estado', $filters->solicitud_estado);
    		//echo "111";
    	}

    	//filtro estado
    	if( $filters && $filters->solicitud_id > 0 )
    	{
    		$this->where('solicitud.solicitud_tramite_empresa_id', "{$filters->solicitud_id}");
    		//echo "2222";
    	}

    	//filtro estado
    	if( $filters && !empty($filters->empresa_nombre) )
    	{
    		$this->like('ef.razon_social_nombre', "$filters->empresa_nombre");
    		//echo "3333";
    	}

    	$this->order_by('solicitud.solicitud_tramite_empresa_id' , 'desc');

    	if($pagination)
    	{
    		$start = isset($pagination->per_page) ? $pagination->per_page : 1 ;
    		$items = isset($pagination->sho_per_pg) ? $pagination->sho_per_pg : 10 ;
    		$this->limit($items, ($start - 1) * $items );
    	}

    	$query = $this->db->get();
    	$list = $query->result();

    	$this->db->dbprefix = $auxPrefix;

    	return $list;
    }

    public function getSolicitudesPorUsuario($pagination = null, $filters = null, $user_id = 0, $status)
    {
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $fields = <<<EOF
    	solicitud.solicitud_tramite_empresa_id,
    	solicitud.fecha_registro,
    	solicitud.empresa_id,
    	solicitud.codigo_usuario_id,
    	solicitud.gestion,
    	solicitud.tipo_solicitud,
    	solicitud.estado,
    	ef.razon_social_nombre AS 'empresa_nombre',
    	ef.numero_fundempresa AS 'empresa_numero_fundempresa',
    	ef.nit AS 'empresa_nit',
    	ef.propietario_codpersona AS 'empresa_propietario_codpersona',
    	concat(p.nombres,' ' ,p.appaterno, ' ' ,p.apmaterno) AS 'empresa_propietario',
    	concat(u.Nombres,' ',u.Apellidos) AS 'usuario'
EOF;

        $this->db->select($fields);
        $this->db->from("{$this->table_name} solicitud");
        $this->db->join('abt_empresa_forestal ef', 'ef.empresa_forestal_id=solicitud.empresa_id');
        $this->db->join('abt_persona p', 'p.codpersona=ef.propietario_codpersona' , 'left');
        $this->db->join('sys_usuario u', 'u.CodUsuario=solicitud.codigo_usuario_id');
        $this->where('solicitud.codigo_usuario_id', $user_id);

        //solicitudes borrador (no mostrar)
//        $this->where_not_in('solicitud.estado' , array('P'));

        //filtro estado
        if( $filters && isset($filters->solicitud_estado) && in_array($filters->solicitud_estado, array('PR','A','P','R')))
        {
            $this->where('solicitud.estado', $filters->solicitud_estado);
            //echo "111";
        } else if(isset($status) && in_array($status, array('PR','A','P','R'))) {
            $this->where('solicitud.estado', $status);
        }

        //filtro estado
        if( $filters && $filters->solicitud_id > 0 )
        {
            $this->where('solicitud.solicitud_tramite_empresa_id', "{$filters->solicitud_id}");
            //echo "2222";
        }

        //filtro estado
        if( $filters && !empty($filters->empresa_nombre) )
        {
            $this->like('ef.razon_social_nombre', "$filters->empresa_nombre");
            //echo "3333";
        }

        $this->order_by('solicitud.solicitud_tramite_empresa_id' , 'desc');

        if($pagination)
        {
            $start = isset($pagination->per_page) ? $pagination->per_page : 1 ;
            $items = isset($pagination->sho_per_pg) ? $pagination->sho_per_pg : 10 ;
            $this->limit($items, ($start - 1) * $items );
        }

        $query = $this->db->get();
        $list = $query->result();

        $this->db->dbprefix = $auxPrefix;

        return $list;
    }
	
}