<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * 
 * @author Victor
 *
 */
class EmpresaAdm_model extends BF_Model {
	
	public $EmpresaEstados;
	
	
	public function __construct()
	{
		parent::__construct();
		
		$this->table_name = 'abt_empresa_forestal';
		$this->key = 'empresa_forestal_id';
		
		$this->EmpresaEstados = array();
		$this->EmpresaEstados['B'] = 'Borrador';
		$this->EmpresaEstados['C'] = 'Completado';
		$this->EmpresaEstados['M'] = 'Modificado';
	}

	public function getById($id) {

    	$auxPrefix = $this->db->dbprefix;
    	$this->db->dbprefix = '';

    	$fields = <<<EOF
    	ef.*,
    	p.*,
    	ef.estado as 'empresa_estado',
    	concat(p.nombres,' ' ,p.appaterno, ' ' ,p.apmaterno) AS 'propietario_nombre_apellido',
    	concat(p.nrodocid,' ',p.emitido) AS 'empresa_propietario_documento',
    	pais.nombre as 'propietario_pais',
EOF;
    	
    	$this->db->select($fields);
    	$this->db->from("{$this->table_name} ef");
    	$this->db->join('abt_persona p', 'p.codpersona=ef.propietario_codpersona');
    	$this->db->join('abt_pais pais', 'pais.codpais = p.codpaisorigen');
    	
        $empresa = $this->find($id);
        

        $this->db->dbprefix = $auxPrefix;

        return $empresa;
	}
	
}