<?php defined('BASEPATH') || exit('No direct script access allowed');

$config['module_config'] = array(
        'name'          => 'Admin',
        'description'   => 'Modulo para administración de información',
        'author'        => 'ABT',
        'version'       => '1.0',
        'menu'          => array(
            'context'   => 'path/to/view'
        ),
        'weights'       => array(
            'context'   => 0
        )
);