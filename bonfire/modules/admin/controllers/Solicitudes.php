<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Solicitudes extends Admin_Controller {

	const KEY_FILTERS_SOLICITUDES = 'admin_key_filters_solicitudes';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/SolicitudEmpresaAdm_model');
		$this->load->model('admin/EmpresaAdm_model');
		$this->load->model('empresas/Empresa_model');
		$this->load->model('empresas/MovimientoProducto_model');
		$this->load->model('empresas/archivo_model');
		$this->load->model('empresas/solicitud_model');
		$this->load->model('empresas/funcionario_model');
		$this->load->library("pagination");
	}

	public function index() 
	{
		$per_page = ($this->input->get('per_page')) ? $this->input->get('per_page') : 1;
		$sho_per_pg = ($this->input->get('sho_per_pg')) ? $this->input->get('sho_per_pg') : 10; 
		
	    $pagination = new stdClass();
	    $pagination->per_page = $per_page;
	    $pagination->sho_per_pg = $sho_per_pg;
	    
	    $Filters = $this->session->userdata(self::KEY_FILTERS_SOLICITUDES);
		
	    $solicitudesAll = $this->SolicitudEmpresaAdm_model->getSolicitudes(null,$Filters);
	   
	    $solicitudesCount = count($solicitudesAll);
	   
		$solicitudesPaginadas = $this->SolicitudEmpresaAdm_model->getSolicitudes($pagination, $Filters);
		
		Template::set('solicitud_estados',  $this->SolicitudEmpresaAdm_model->SolicitudEstados);
		Template::set('search', $Filters);
		Template::set('solicitudes_count', $solicitudesCount);
		Template::set('solicitudes', $solicitudesPaginadas);
		
		$this->pagination->initialize(PaginationConfig::getBootstrapConfig('/admin/solicitudes/', $solicitudesCount, $sho_per_pg));
		 
		Template::set('pagination',  $this->pagination->create_links());
		
		Template::render();
	}
	
	public function search()
	{
		$filters = new stdClass();
		$filters->solicitud_estado 	= $this->input->post('solicitud_estado');
		$filters->solicitud_id 		= $this->input->post('solicitud_id');
		$filters->empresa_nombre 	= $this->input->post('empresa_nombre');

		$this->session->set_userdata(self::KEY_FILTERS_SOLICITUDES, $filters);
	
		Template::redirect('/admin/solicitudes');
	}
	
	public function clearFilters()
	{
		$this->session->set_userdata(self::KEY_FILTERS_SOLICITUDES, null);
	
		Template::redirect('/admin/solicitudes');
	}
	
	public function detalle()
	{
		 $id = ($this->input->get('id')) ? $this->input->get('id') : 0;
		
		 $solicitudInfo = $this->SolicitudEmpresaAdm_model->getById($id);

		 if(empty($solicitudInfo))
		 	Template::redirect('/admin/solicitudes');

		$this->session->set_userdata('codoficina', 6);

		 $solicitud = $this->Empresa_model->Get_SolicitudComplete($id);
		 $solicitud = $solicitud->data;
		 $this->MovimientoProducto_model->LoadInfoFormulario26($solicitud);
		 $this->archivo_model->Read_Config_Documentos($solicitud);
		 $this->archivo_model->Read_Upload_Documentos($solicitud);
		 $archivosModeracion = $this->archivo_model->getModeracion('solicitud',$id);

		 $funcionarios = $this->funcionario_model->Get_List_Responsable_By($this->session->userdata('codoficina'));
		 if (!empty($solicitud->resolucion)) {
		 	$fechaRegistro = strtotime($solicitud->resolucion->fecha_registro);
		 	$resolucion = (object) [
		 		'city' => "Santa Cruz",
		 		'day' => date('d', $fechaRegistro),
		 		'month_literal' => $this->GetMonthLiteral(date('m', $fechaRegistro)),
		 		'year' => date('Y', $fechaRegistro),
		 		'responsable_name' => $solicitud->resolucion->responsable_cargo . '. ' . $solicitud->resolucion->responsable->nombre_apellido,
		 		'resolucion_cargo' => $this->empresa_model->GetTextCargoResolucion(),
		 		'resolucion_codigo' => $solicitud->resolucion->codigo_resolucion
		 	];
		 	Template::set('resolucion', $resolucion);
		 }
		 
		$empresaSucursales =  $this->Empresa_model->GetSucursales($solicitud->empresa_id, $solicitud->solicitud_tramite_empresa_id);
		// print_r('<pre>');
		// print_r($funcionarios);
		// print_r('</pre>');
		// exit;
		Template::set('solicitud_estados',  $this->SolicitudEmpresaAdm_model->SolicitudEstados);
		Template::set('solicitud_tipos',  $this->SolicitudEmpresaAdm_model->SolicitudTipos);
		Template::set('empresa_estados',  $this->EmpresaAdm_model->EmpresaEstados);
		Template::set('funcionarios',  $funcionarios);
		
		Template::set('estado_solicitud_aceptado', Solicitud_Model::ESTADO_ACEPTADO);
		Template::set('estado_solicitud_pendiente', Solicitud_Model::ESTADO_PENDIENTE_REVISION);
		Template::set('solicitud', $solicitud);
		Template::set('solicitud_info', $solicitudInfo);
		Template::set('solicitud_moderacion', $archivosModeracion);
		
		Template::render();
	}

	private function GetMonthLiteral($month) {
		switch ($month) {
			case 1:
				return "Enero";
				break;
			case 2:
				return "Febreo";
				break;
			case 3:
				return "Marzo";
				break;
			case 4:
				return "Abril";
				break;
			case 5:
				return "Mayo";
				break;
			case 6:
				return "Junio";
				break;
			case 7:
				return "Julio";
				break;
			case 8:
				return "Agosto";
				break;
			case 9:
				return "Septiembre";
				break;
			case 10:
				return "Octubre";
				break;
			case 11:
				return "noviembre";
				break;
			case 12:
				return "Diciembre";
				break;

		}
	}
	
}