<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Archivo extends Admin_Controller {

	const KEY_FILTERS_SOLICITUDES = 'admin_key_filters_solicitudes';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('empresas/Archivo_model');
	}
	
	
	public function moderar()
	{
		//if ($this->input->post('submit'))
        {
        	$type = $this->input->post('type');
        	$itemId = $this->input->post('item_id');
         	$documentId = $this->input->post('document_id');
            $estado = $this->input->post('moderacion');
            $observacion  = $this->input->post('observacion');
            
            $this->Archivo_model->moderar($type, $itemId, $documentId, $estado,$observacion);
        }

		die();
	}

	
}