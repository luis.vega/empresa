<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Empresa extends Admin_Controller {

	const KEY_FILTERS_SOLICITUDES = 'admin_key_filters_solicitudes';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('admin/EmpresaAdm_model');
	}
	
	
	public function detalle()
	{
		$id = ($this->input->get('id')) ? $this->input->get('id') : 0;
		
		 $empresa = $this->EmpresaAdm_model->getById($id);
		 
		 /*echo "<pre>";
		 print_r($empresa);
		 echo "</pre>";*/
	
		Template::set('empresa', $empresa);
	
		Template::render();
	}

	
}