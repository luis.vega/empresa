<h1 class="page-header">Detalle de la solicitud</h1>

<div class="row">

  	<div class="col-md-4">
		<div class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-6 control-label">Solicitud ID</label>
				<div class="col-sm-6">
					<p class="form-control-static"><?php echo $solicitud->solicitud_tramite_empresa_id ?></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-6 control-label">Tipo de Solicitud</label>
				<div class="col-sm-6">
					<p class="form-control-static"><?php  echo $solicitud_tipos[$solicitud->tipo_solicitud] ?></p>
				</div>
			</div>
			
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-6 control-label">Gestión</label>
				<div class="col-sm-6">
					<p class="form-control-static"><?php  echo $solicitud->gestion ?></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-6 control-label">Estado</label>
				<div class="col-sm-6">
					<p class="form-control-static"><?php  echo $solicitud_estados[$solicitud->estado] ?></p>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-6 control-label">Fecha de Registro</label>
				<div class="col-sm-6">
					<p class="form-control-static"><?php  echo $solicitud->fecha_registro ?></p>
				</div>
			</div>
			
		</div>
	</div>
	
</div>

<!-- /.panel-heading -->
<div class="panel-body">
    
    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
        <li class="active"><a href="#step1" data-toggle="tab">Datos Generales Empresa</a></li>
        <li><a href="#step2" data-toggle="tab">Formularios - F26</a></li>
        <li><a href="#step3" data-toggle="tab">Archivos Adjuntos Empresa</a></li>
        <li><a href="#step4" data-toggle="tab">Resolución Administrativa</a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane fade in active" id="step1">
            <?php echo $this->load->view('partial/step1');?>
        </div>
        <div class="tab-pane fade" id="step2">
            <?php echo $this->load->view('partial/step2');?>
        </div>
        <div class="tab-pane fade" id="step3">
            <?php echo $this->load->view('partial/step3');?>
        </div>
        <div class="tab-pane fade" id="step4">
             <?php echo $this->load->view('partial/step4');?>
        </div>
    </div>
    
</div>
<!-- /.panel-body -->