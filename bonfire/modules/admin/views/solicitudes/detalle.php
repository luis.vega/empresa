<?php 
	Assets::add_module_js('admin','js/solicitudes/detalle.js');
 ?>
<h1 class="page-header">Información de solicitud</h1>
<div class="row">

  	<div class="col-md-4">
		<div class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-6 control-label">Solicitud ID</label>
				<div class="col-sm-6">
					<p class="form-control-static"><?php echo $solicitud_info->solicitud_tramite_empresa_id ?></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-6 control-label">Tipo de Solicitud</label>
				<div class="col-sm-6">
					<p class="form-control-static"><?php  echo $solicitud_tipos[$solicitud_info->tipo_solicitud] ?></p>
				</div>
			</div>
			
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-6 control-label">Gestión</label>
				<div class="col-sm-6">
					<p class="form-control-static"><?php  echo $solicitud_info->gestion ?></p>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-6 control-label">Estado</label>
				<div class="col-sm-6">
					<p class="form-control-static"><?php  echo $solicitud_estados[$solicitud_info->estado] ?></p>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-md-4">
		<div class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-6 control-label">Fecha de Registro</label>
				<div class="col-sm-6">
					<p class="form-control-static"><?php  echo $solicitud_info->fecha_registro ?></p>
				</div>
			</div>
			
		</div>
	</div>
<?php if ($solicitud_info->estado == $estado_solicitud_pendiente) :?>
	<div class="col-md-4">
		<div class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-6 control-label"> Aprobar Solicitud</label><br>
				<div class="col-sm-6">
					<button class="btn btn-lg btn-block btn-success solicitud-ok">
						<i class="fa fa-check-circle-o fa-3x animated fadeIn"></i>
					</button>
				</div>
			</div>
			
		</div>
	</div>
<?php endif; ?>
</div>

<!-- /.panel-heading -->
<div class="panel-body">
    
    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
        <li class="active"><a href="#step1" data-toggle="tab">Datos Generales Empresa</a></li>
        <li><a href="#step2" data-toggle="tab">Archivos Adjuntos Empresa</a></li>
        <?php if ($solicitud_info->estado == $estado_solicitud_aceptado): ?>
        	<li><a href="#resolucion" data-toggle="tab">Resolución Administrativa</a></li>
        <?php endif; ?>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane fade in active" id="step1">
           <?php echo $this->load->view('partial/solicitud_detalle');?>
        </div>
        <div class="tab-pane fade" id="step2">
            <?php echo $this->load->view('partial/solicitud_archivos');?>
        </div>
        <?php if ($solicitud_info->estado == $estado_solicitud_aceptado) : ?>
	         <div class="tab-pane fade" id="resolucion">
	         	<?php echo $this->load->view('partial/solicitud_resolucion');?>
	         </div>
    	<?php endif; ?>
    </div>
    
</div>
<!-- /.panel-body -->
<?php echo $this->load->view('partial/modal_aprobar_solicitud', array('funcionarios'=>$funcionarios));?>

<script type="text/javascript">
    function printResolution() {
        var contents = $("#box-content-resolucion").html(); //incluido el contenedor
        console.log(contents);
        var frame1 = $('<iframe />');
        frame1[0].name = "frame1";
        frame1.css({ "position": "absolute", "top": "-1000000px" });
        $("body").append(frame1);
        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;

        frameDoc.document.open();
        //Create a new HTML document.
        frameDoc.document.write('<html><head><title>Resolucion</title>');
	     frameDoc.document.write('<link href="/themes/default/css/bootstrap.min.css" rel="stylesheet" type="text/css" />');
	     frameDoc.document.write('<link href="/themes/default/css/bootstrap-theme.css" rel="stylesheet" type="text/css" />');
	     frameDoc.document.write('<link href="/themes/default/css/select2.min.css" rel="stylesheet" type="text/css" />');
	     frameDoc.document.write('<link href="/themes/default/css/stepwizard.css" />');
	     frameDoc.document.write('<link href="/themes/admin/css/custom.css" rel="stylesheet" type="text/css" />');
	     frameDoc.document.write('<link href="/themes/default/screen.css" rel="stylesheet" type="text/css" />');
        //Append the external CSS file.
        frameDoc.document.write('</head><body>');

        //Append the DIV contents.
        frameDoc.document.write(contents);
        frameDoc.document.write('</body></html>');
        frameDoc.document.close();
        setTimeout(function () {
            window.frames["frame1"].focus();
            window.frames["frame1"].print();
            frame1.remove();
        }, 500);

    }

$(document).ready(function() {
	var solicitudId = "<?php echo $solicitud_info->solicitud_tramite_empresa_id ?>";
	// Accion para aprobar Solicitud
	$('.solicitud-ok').click(function () {
		var totalChecks = $('.btn-document-moderation').length;
		var totalAprobados = $('.btn-document-moderation[value="aprobado"]:checked').length;
		var totalRechazados = $('.btn-document-moderation[value="rechazado"]:checked').length;
		if (totalRechazados > 0) 
			showErrors(["No puede Aprobar la solicitud, existen archivos con Observacion."]);
		else {
			if (!totalAprobados || totalAprobados == 0) {
				showErrors(["No puede Aprobar la solicitud, tiene que validar todos los archivos digitales y físicos."]);
			} else {
				if (totalAprobados != (totalChecks / 2)) {
					showErrors(["No puede Aprobar la solicitud, existen archivos no validados."]);
				} else {
					
					$('#modal-aprobar').modal();
				}
			}
		}

	});

	$('#btn-finalizar').click(function () {
		var hasError = $('form', $('#modal-aprobar')).validator('validate').has('.has-error').length;
		if (!hasError) {
			var funcionarioId = $('#funcionario', $('#modal-aprobar')).val();
			var cargoFuncionario = $('#cargo', $('#modal-aprobar')).val();
			var nroResolucion = $('#nro-resolucion', $('#modal-aprobar')).val();
			var datos = {
				solicitud_id: solicitudId, 
				numero_registro: nroResolucion, 
				funcionario_id: funcionarioId, 
				cargo_funcionario: cargoFuncionario
			};
			aprobarSolicitud(datos, reloadSolicitud);
		}
		
	});
});


function reloadSolicitud() {
	location.reload();
}

function aprobarSolicitud(datos, callback) {

	$.blockUI();
	$.ajax({
		url: baseUrl + "api/empresa/aprobar_solicitud_registro/", 
		method: "POST",
		data: datos,
		dataType : 'json',
		success: function(result){
			$.unblockUI();
			if (result) {
				callback();
			}
    	},
    	error: function(datosError) { 
    		$.unblockUI();
    		showErrors(datosError.responseJSON.error);
    	}
    });
}


</script>