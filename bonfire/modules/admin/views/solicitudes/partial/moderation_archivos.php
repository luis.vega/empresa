		<?php 
			/*echo "<pre>"; 
			print_r($documento);
			echo "<pre";*/
		?>

<td>
	<?php if($documento->es_doc_fisico == 0):?>
		<a href="/api/file/download_file_empresa/<?php echo $documento->nombre_file ?>">Descargar Archivo</a>
	<?php endif;?>
</td>
<td>
	<label class="radio-inline"> 
	  <?php $checked = (isset($solicitud_moderacion[$documento->documento_presentado_id]) && $solicitud_moderacion[$documento->documento_presentado_id]->estado=='aprobado' );?>
	  <input class="btn-document-moderation" type="radio" name="<?php echo $documento->documento_presentado_id  ?>" value="aprobado" <?php if($checked):?> checked="checked"<?php endif;?>> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
	</label>
	<label class="radio-inline">
	<?php $checked = (isset($solicitud_moderacion[$documento->documento_presentado_id]) && $solicitud_moderacion[$documento->documento_presentado_id]->estado=='rechazado' );?>					  
	<input class="btn-document-moderation" type="radio" name="<?php echo $documento->documento_presentado_id  ?>" value="rechazado" <?php if($checked):?> checked="checked"<?php endif;?>> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
	</label>
</td>
<td width="300">
	<?php $observacion = (isset($solicitud_moderacion[$documento->documento_presentado_id])) ? $solicitud_moderacion[$documento->documento_presentado_id]->observacion : '';?>
	<b>Observación:</b> <span id="observacion_<?php echo $documento->documento_presentado_id  ?>"><?php echo $observacion;?></span>
</td>