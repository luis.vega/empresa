<!-- line modal -->
<?php 
	$cargos = [
		'lic' => 'Licenciado (a)',
		'ing' => 'Ingeniero (a)',
		'abg' => 'Abogado (a)',
	]
 ?>

<div class="modal fade" id="modal-aprobar" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-keyboard="false" id="modal-aprobar">
  <div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Aprobar Solicitud</h3>
		</div>
		<div class="modal-body">
			<form data-toble="validator" novalidate="true" role="form" id="form-modal-aprobar-solicitud">
				<div class="form-group">
					  <label for="funcionario" class="control-label">Autoridad Firmante</label>
			          <select id="funcionario" name="funcionario" class="form-control" required>
			          	 <option value="">Seleccione</option>
						<?php 
							foreach ($funcionarios as $funcionario) : 
						?>
							<option value="<?php echo $funcionario->codfuncionario; ?>">
								<?php echo $funcionario->nombre_apellido; ?>
							</option>
						<?php endforeach; ?>
			          </select>
					  <div class="help-block with-errors"></div>

				</div>

				<div class="row">
					<div class="form-group col-md-6">
						<label for="cargo" class="control-label">Cargo</label>
				          <select id="cargo" name="cargo" class="form-control" required>
				          	 <option value="">Seleccione</option>
							<?php 
								foreach ($cargos as $abbr => $cargo) : 
							?>
								<option value="<?php echo $abbr; ?>">
									<?php echo $cargo; ?>
								</option>
							<?php endforeach; ?>
				          </select>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group col-md-6">
						<label for="nro-resolucion" class="control-label">Nro Resolución</label>
						<input type="number" name="nro-resolucion" id="nro-resolucion" class="form-control" min="1" required>
						<div class="help-block with-errors"></div>

					</div>
				</div>
			</form>
		</div>
		<div class="modal-footer">
			<div class="btn-group" role="group" aria-label="group button">
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-primary" data-dismiss="modal"  role="button">Cancelar</button>
				</div>
				<div class="col-md-1"></div>
				<div class="btn-group" role="group">
					<button type="button" id="btn-finalizar" class="btn btn-success" role="button">Continuar</button>
				</div>
			</div>
		</div>
	</div>
  </div>
</div>