<h3>Datos Generales Empresa</h3>
<table class="table table-hover">
	<tbody>
		<tr>
			<th scope="row"><label class="control-label">Registro</label></th>
			<td><strong> <?php echo $solicitud->registro_empresa ?> </strong></td>
		</tr>
		<tr>
			<th scope="row" width="300"><label class="control-label">Empresa ID</label></th>
			<td><?php echo $solicitud->empresa_forestal_id ?></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">Razón Social</label></th>
			<td><?php echo $solicitud->razon_social_nombre ?></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">Nit</label></th>
			<td><?php echo $solicitud->nit ?></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">Fundempresa Nro.</label></th>
			<td><?php echo $solicitud->numero_fundempresa ?></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">Registro Antiguo</label></th>
			<td><?php echo $solicitud->registro_antiguo  ?></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">Estado</label></th>
			<td><?php echo $empresa_estados[$solicitud->estado_emp] ?></td>
		</tr>
	<?php if (!empty($solicitud->propietario)) :?>
		<tr>
			<th scope="row"><label class="control-label">Propietario</label></th>
			<td><?php echo $solicitud->propietario->nombre_apellido ?></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">Propietario Pais</label></th>
			<td><?php echo $solicitud->propietario->persona_pais ?></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">Telefonos</label></th>
			<td><?php  echo 'Telf. '. $solicitud->propietario->telefono . ' - Cel. ' . $solicitud->propietario->celular?></td>
		</tr>
	<?php endif; ?>
	</tbody>
</table>
		<?php 
			/*echo "<pre>"; 
			print_r($sucursal);
			echo "<pre";*/
		?>
<?php if(isset($solicitud->sucursales)) foreach ($solicitud->sucursales as $sucursal):?>
		<?php 
			/*echo "<pre>"; 
			print_r($sucursal);
			echo "<pre";*/
		?>
<h3><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span>  Sucursal</h3>
<table class="table table-hover">
<tbody>
	<tr>
		<th scope="row" width="300"><label class="control-label">Direccion</label></th>
		<td><?php echo $sucursal->direccion ?></td>
		<td colspan="4"></td>
	</tr>
	<tr>
		<th scope="row"><label class="control-label">Departamento</label></th>
		<td><?php echo ucwords(strtolower($sucursal->departamento)) ?></td>
		<th><label class="control-label">Provincia</label></th>
		<td><?php echo ucwords(strtolower($sucursal->provincia)) ?></td>
		<th><label class="control-label">Municio</label></th>
		<td><?php echo ucwords(strtolower($sucursal->municipio)) ?></td>
	</tr>
	<tr>
		<th scope="row"><label class="control-label">Ubicación Coordenada(x,y)</label></th>
		<td>(<?php echo $sucursal->coordenada_x .','. $sucursal->coordenada_y ?>)</td>
		<td colspan="4"></td>
	</tr>
	
	<?php if(isset($sucursal->agente)):?>
	<tr>
		<th scope="row"><label class="control-label">Agente</label></th>
		<td><?php echo $sucursal->agente->agente_npm .' - '. $sucursal->agente->registro?></td>
		<td colspan="4"></td>
	</tr>
	<?php endif;?>
</tbody>
</table>
<div class="row">
	<div class="col-md-1">
	</div>
  	<div class="col-md-11">
		
		<?php if(isset($sucursal->actividades)) foreach ($sucursal->actividades as $actividad):?>
		<?php 
			/*echo "<pre>"; 
			print_r($actividad);
			echo "<pre";*/
		?>
		<h3><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Actividad</h3> 
		<table class="table table-hover">
		<tbody>
			<tr>
				<th scope="row" width="300"><label class="control-label">Actividad</label></th>
				<td colspan="3"><?php echo $actividad->actividad->descripcion ?></td>
			</tr>
			<tr>
				<th scope="row"><label class="control-label">Categoria</label></th>
				<td colspan="3"><?php echo $actividad->categoria->descripcion ?></td>
			</tr>
		</tbody>
		</table>
		<?php endforeach;?>
		
		
		<?php if(isset($sucursal->representantes)) foreach ($sucursal->representantes as $representante):?>
		<?php 
			/*echo "<pre>"; 
			print_r($representante);
			echo "<pre";*/
		?>
		<h3><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Representante</h3>
		<table class="table table-hover">
		<tbody>
			<tr >
				<th scope="row" width="300"><label class="control-label">Codigo</label></th>
				<td><?php echo $representante->codpersona?></td>
				<th ><label class="control-label">Nombre</label></th>
				<td><?php echo $representante->nombres . ' ' . $representante->appaterno . ' ' . $representante->apmaterno?></td>
			</tr>
			<tr>
				<td scope="row"><label class="control-label">Documento</label></td>
				<td><?php echo $representante->nrodocid . ' (' . $representante->emitido  . ')' ?></td>
				<th><label class="control-label">Telefonos</label></th>
				<td><?php echo $representante->telefono . ' - ' . $representante->celular  ?></td>
			</tr>
		</tbody>
		</table>
		<?php endforeach;?>
	</div>
</div>
<?php endforeach;?>