<div class="panel panel-default">
    <div class="panel-heading">
        <div style="float: right;">
            <a  href="/admin/solicitudes/clearFilters">Limpiar Filtros</a>	
        </div>
        <h4 class="panel-title">
            <a href="#collapse-filters" data-parent="#accordion" data-toggle="collapse">Filtrar Solicitudes</a>
        </h4>
    </div>
    <div class="panel-collapse collapse" id="collapse-filters">
        <div class="panel-body">
        
            <form role="form" action="/admin/solicitudes/search" method="post">
            
            			<div class="form-group">
            	           <p>Ingrese alguno de los siguientes filtros:</p>
            		    </div>
            		    
            	        <div class="form-group">
            				<label>Estado de Solicitud</label> 
            				<select class="form-control"  name="solicitud_estado">
								<option value="0">Seleccione Estado</option>
			                    <?php foreach ($solicitud_estados as $estado => $nombre) : ?>
				                    <?php if($estado != 'P'):?>
				                    <option value="<?php echo  $estado?>" <?php if(isset($search) && $search->solicitud_estado == $estado):?>selected="selected" <?php endif;?>><?php echo  $nombre?></option>
				                    <?php endif;?>
			                    <?php endforeach; ?>
			                </select>
            			</div>
            	       
                        <div class="form-group">
            				<label>Solicitud ID</label> 
            				<input class="form-control" placeholder="Ingrese ID de Solicictud" name="solicitud_id" value="<?php  if(isset($search)) echo $search->solicitud_id ?>">
            			</div>
            			
            			<div class="form-group">
            				<label>Razón Social</label> 
            				<input class="form-control" placeholder="Ingrese Nombre de la Empresa" name="empresa_nombre" value="<?php  if(isset($search)) echo $search->empresa_nombre ?>">
            			</div>
            
            			<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span> Buscar</button>
            			<a class="btn btn-default" type="button" href="#collapse-filters" data-parent="#accordion" data-toggle="collapse">Cancelar</a>
            </form>
            
        </div>
    </div>
</div>

<div>
	<h4>Solicitudes: <small><?php echo (isset($search) && !empty($search->solicitud_estado)) ? $solicitud_estados[ $search->solicitud_estado] : 'Todas'?> (<b><?php echo $solicitudes_count?></b>)</small></h4> 
</div>