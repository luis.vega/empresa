<h1 class="page-header">Solicitudes de Empresas</h1>

<?php echo $this->load->view('search');?>

<table class="table table-hover">
	<thead>
		<tr>
			<th>#</th>
			<th>Registro</th>
			<th>Gestion</th>
            <th>Raz&oacute;n social</th>
            <th>N° NIT</th>
            <th>N° FUNDEMPRESA</th>
			<th>Propietario</th>
			<!--  <th>Usuario</th> -->
			<th>Estado</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($solicitudes as $solicitud) : ?>
		<tr>
			<th scope="row"><?php echo $solicitud->solicitud_tramite_empresa_id?></th>
			<td><?php echo $solicitud->fecha_registro?></td>
			<td><?php echo $solicitud->gestion?></td>
			<td><?php echo $solicitud->empresa_nombre?></td>
			<td><?php echo $solicitud->empresa_nit?></td>
			<td><?php echo $solicitud->empresa_numero_fundempresa?></td>
			<td><?php echo $solicitud->empresa_propietario?></td>
			<!-- <td><?php echo $solicitud->usuario?></td> -->
			<td><?php echo $solicitud_estados[$solicitud->estado]?></td>
			<td>
			<a class="btn btn-default" target="blank" href="/admin/solicitudes/detalle/?id=<?php echo $solicitud->solicitud_tramite_empresa_id?>" role="button">Ver Solicitud <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span></a>
			<a class="btn btn-default" target="blank" href="/admin/empresa/detalle/?id=<?php echo $solicitud->empresa_id?>" role="button">Ver Empresa <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span></a>
			</td>
		</tr>
        <?php endforeach; ?>
	</tbody>
</table>

<?php echo $pagination; ?>