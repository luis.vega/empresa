<h1 class="page-header">Detalle de Empresa</h1>
<div class="form-horizontal">
	<div class="form-group">
		<label class="col-sm-2 control-label">Empresa ID</label>
		<div class="col-sm-10">
			<p class="form-control-static"><?php echo $empresa->empresa_forestal_id ?></p>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Razón Social</label>
		<div class="col-sm-10">
			<p class="form-control-static"><?php  echo $empresa->razon_social_nombre ?></p>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Nit</label>
		<div class="col-sm-10">
			<p class="form-control-static"><?php  echo $empresa->nit ?></p>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Fundempresa Nro.</label>
		<div class="col-sm-10">
			<p class="form-control-static"><?php  echo $empresa->numero_fundempresa ?></p>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Registro</label>
		<div class="col-sm-10">
			<p class="form-control-static"><?php  echo $empresa->registro_empresa ?></p>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Habilitado</label>
		<div class="col-sm-10">
			<p class="form-control-static"><?php  echo $empresa->habilitado ?></p>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Estado</label>
		<div class="col-sm-10">
			<p class="form-control-static"><?php  echo $empresa->estado ?></p>
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">Propietario Cod.</label>
		<div class="col-sm-10">
			<p class="form-control-static"><?php  echo $empresa->propietario_codpersona ?></p>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Propietario</label>
		<div class="col-sm-10">
			<p class="form-control-static"><?php  echo $empresa->propietario_nombre_apellido ?></p>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label">Telefono</label>
		<div class="col-sm-10">
			<p class="form-control-static"><?php  echo $empresa->telefono . '- ' . $empresa->celular?></p>
		</div>
	</div>
	
</div>

<?php /*?>
<div class="row">
        <div class="col-lg-5">
        <h3>Caracteristicas</h3>

            <table class="table">
                <tr>
                    <th>Caracteristica</th>
                    <th>Valor</th>
                </tr>
                <?php if($this->caracteristicas)foreach ($this->caracteristicas as $caracteristica):?>
                <tr>
                	<td><?php echo $caracteristica->nombre?></td>
                	<td>
                	 <?php foreach ($caracteristica->getValores() as $valor):?>
                	       <?php if(in_array($valor->id, $this->producto_caracteristicas)):  echo $valor->valor; endif;?>
                	 <?php endforeach;?>
                	 </td>
                </tr>
                <?php endforeach;?>
            </table>
            
        </div>
</div>

<div class="row">

        <h3>Imagenes</h3>
        <br />
		<div id="producto-imagenes" class="panel-group">
        
            <?php foreach ($this->imagenes as $imagen):?>
           
            <div class="col-lg-3" style="margin-bottom: 20px;">
                <div class="panel panel-default" style="width: 200px;">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                           &nbsp;
                        </h4>
                    </div>
                    <div class="panel-collapse collapse in" id="collapse-1" style="">
                       <img src="<?php echo $this->basePath(Resource::URL_PRODUCTO_IMG_MEDIANA."{$imagen['nombre_guardado']}")?>" class="img-responsive" alt="Responsive image" style="height: 200px; width: 200px;" >
                    </div>
                </div>
            </div>
            <?php endforeach;?>

        </div>
</div>
<?php */?>