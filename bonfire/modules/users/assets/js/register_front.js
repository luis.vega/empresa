$(document).ready(function() {

	$('#pais-nacionalidad').select2();
	$('#registrar').click(function () {
		
		var hasErrors = $('#form-registro').validator('validate').has('.has-error').length

		if (!hasErrors) {
			registrarPropiertario($("#form-registro").serialize());
		}
	});

	$('#pais-nacionalidad').change(function () {
		var paisSelect = $(this).val();
		if (paisSelect.toString() != paisDefaultId ) {
			$('#departamento-extension').val('').trigger('change');
			$('#departamento-extension').hide();
			$('#departamento-extension').attr('required', false);

		} else {
			$('#departamento-extension').show();
			$('#departamento-extension').attr('required', true);
		} 
	});

});

function registrarPropiertario(formData) {

	$.blockUI();
	$.ajax({
		url: baseUrl + "/api/user/registrar", 
		method: "POST",
		data: formData,
		dataType : 'json',
		success: function(result){
			$.unblockUI();
			if (result && result.message) {
				showSuccess(result.message);

				// Evento Para Cerrar popup
				$(document).on("click", $('.close-popup-success'), function(e){
					redirectTo("/solicitudes");
				});
			}
    	},
    	error: function(datosError) { 
    		$.unblockUI();
    		showErrors(datosError.responseJSON.error);
    	}
    });
}