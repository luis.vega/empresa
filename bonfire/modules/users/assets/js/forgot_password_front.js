$(document).ready(function() {

	$('#send').click(function () {
		
		var hasErrors = $('.form-reset').validator('validate').has('.has-error').length

		if (!hasErrors) {
			var data = new FormData();
			resetPassword({
					email: $('.form-reset #email').val().trim()
				});
		}
	});
});

function resetPassword(data) {
	$.blockUI();
	$.blck
		$.ajax({
		url: baseUrl + "/api/user/reset_password", 
		method: "POST",
		data: data,
		success: function(result){

			$.unblockUI();
			showSuccess(result.message);
			$('.form-reset #email').val('');
			// console.log(result);return;
        	//redirectTo('/');
    	},
    	error: function(datosError) { 
    		$.unblockUI();
			showErrors(datosError.responseJSON.error);
    	}
    });
}