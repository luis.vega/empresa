$(document).ready(function() {

	$('#log-me-in').click(function () {
		
		var hasErrors = $('#login-form').validator('validate').has('.has-error').length

		if (!hasErrors) {
			var data = new FormData();
			iniciarSesion({
					username: $('#login-form #username').val().trim(), 
					password: $('#login-form #password').val().trim(),
					login: true
				});
		}
	});

	$(document).keypress(function(e) {
	  if(e.which == 13) {
		var hasErrors = $('#login-form').validator('validate').has('.has-error').length

		if (!hasErrors) {
			var data = new FormData();
			iniciarSesion({
					username: $('#login-form #username').val().trim(), 
					password: $('#login-form #password').val().trim(),
					login: true
				});
		}
	  }
	});
});

function iniciarSesion(data) {
	$.blockUI();
	$.blck
		$.ajax(
		{
		url: baseUrl + "/api/user/login_front", 
		method: "POST",
		data: data,
		success: function(result){
			$.unblockUI();
        	redirectTo('solicitudes');
    	},
    	error: function(datosError) { 
    		$.unblockUI();
			showErrors(datosError.responseJSON.error);
    	}
    });
}