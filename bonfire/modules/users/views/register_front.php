<?php

$errorClass   = empty($errorClass) ? ' error' : $errorClass;
$controlClass = empty($controlClass) ? 'span6' : $controlClass;
$fieldData = array(
    'errorClass'    => $errorClass,
    'controlClass'  => $controlClass,
);

?>

<?php 
    // Recursos JS
    Assets::add_module_js('users','js/register_front.js');
?>
<script type="text/javascript">
    var paisDefaultId = "<?php echo $paisDefaultId;?>";
</script>
<section id="register">
    <h1 class="page-header">Registrar Cuenta</h1>
    <?php if (validation_errors()) : ?>
    <div class="alert alert-error fade in">
        <?php echo validation_errors(); ?>
    </div>
    <?php endif; ?>
    <div class="alert alert-info fade in">
        <h4 class="alert-heading">Los campos obligatorios están con '*'.</h4>
    </div>
    <div class="row-fluid">
        <div class="col-md-12">
            <form data-toble = "validator" role = "form" class="form-horizontal" id="form-registro">
                <?php echo form_open_multipart('api/file/do_upload');?>
                <fieldset>
                    <div class="form-group">
                      <label class="control-label" for="nombres">Nombres (*)</label>  
                      <input id="nombres" name="nombres" type="text" placeholder="Nombres del Propietario" 
                          class="form-control" required>
                          <div class="help-block with-errors"></div>
                    </div>
                   <div class="form-group">
                      <label class="control-label" for="apellido-paterno">Apellido Paterno (*)</label>  
                      <input id="apellido-paterno" name="apellido-paterno" type="text" placeholder="Apellido Paterno del Propietario" 
                          class="form-control" required>
                          <div class="help-block with-errors"></div>
                   </div>
                   <div class="form-group">
                      <label class="control-label" for="apellido-materno">Apellido Materno</label>  
                      <input id="apellido-materno" name="apellido-materno" type="text" placeholder="Apellido Materno del Propietario" 
                          class="form-control">
                   </div>

                    <div class="form-group">
                      <label class="control-label" for="pais-nacionalidad">Pais Nacionalidad (*)</label>  
                      <select id="pais-nacionalidad" name="pais-nacionalidad" class="form-control" required>
                         <option value="">Seleccione</option>
                        <?php if (isset($paises) && is_array($paises)) :?>
                            <?php foreach ($paises as $pais) : ?>
                                <?php if ($paisDefaultId == $pais->codpais) :?>
                                    <option value="<?php echo $pais->codpais; ?>" selected>
                                        <?php echo $pais->nombre; ?>
                                    </option>
                                <?php else: ?>
                                    <option value="<?php echo $pais->codpais; ?>">
                                        <?php echo $pais->nombre; ?>
                                    </option>
                                <?php endif;?>
                            <?php endforeach; ?>
                          </select>
                        <?php endif;?>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="tipo-documento">Tipo Documento (*)</label>  
                      <select id="tipo-documento" name="tipo-documento" class="form-control" required>
                         <option value="">Seleccione</option>
                        <?php if (isset($tiposDocumento) && is_array($tiposDocumento)) :?>
                            <?php foreach ($tiposDocumento as $tipo) : ?>
                                <?php if ($DocumentoDefaultId == $tipo->codtipodocumento) :?>
                                    <option value="<?php echo $tipo->codtipodocumento; ?>" selected>
                                        <?php echo $tipo->nombre; ?>
                                    </option>
                                <?php else: ?>
                                    <option value="<?php echo $tipo->codtipodocumento; ?>">
                                        <?php echo $tipo->nombre; ?>
                                    </option>
                                <?php endif;?>
                            <?php endforeach; ?>
                          </select>
                        <?php endif;?>
                        <div class="help-block with-errors"></div>
                    </div>

                   <div class="form-group form-inline">
                      <label class="control-label" for="numero-documento">Nro Documento (*)</label> <br>
                      <input id="numero-documento" name="numero-documento" type="text" placeholder="Nro Documento" 
                          class="form-control input-medium" required maxlength="45">
                      <select id="departamento-extension" name="departamento-extension" class="form-control input-medium" required>
                         <option value="">Seleccione Extensi&oacute;n</option>
                        <?php if (isset($departamentos) && is_array($departamentos)) :?>
                            <?php foreach ($departamentos as $departamento) : ?>
                                <option value="<?php echo $departamento->sigla_extension_ci; ?>">
                                    <?php echo $departamento->nombre; ?>
                                </option>
                            <?php endforeach; ?>
                        <?php endif;?>
                      </select>
                   </div>

                <div class="row">
                   <div class="form-group col-md-6">
                      <label class="control-label" for="password">Contraseña (*)</label>  
                      <input id="password" name="password" type="password" placeholder="Contraseña" 
                          class="form-control" required data-minlength="6">
                          <div class="help-block with-errors"></div>
                   </div>
                   <div class="form-group col-md-6 pull-right">
                      <label class="control-label" for="confirm-password">Confirmar Contraseña (*)</label>  
                      <input id="confirm-password" name="confirm-password" type="password" placeholder="Confirmar contraseña" 
                          class="form-control" data-match="#password" data-match-error="No coincide su contraseña" required>
                          <div class="help-block with-errors"></div>
                   </div>
                </div>

                <div class="row panel panel-default">
                  <div class="panel-heading">Otro Datos</div>
                  <div class="panel-body">
                        <div class="form-group col-md-10">
                          <label class="control-label" for="email">Email (*)</label>  
                          <input id="email" name="email" type="email" placeholder="Email del Propietario" 
                              class="form-control" required>
                          <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-10">
                          <label class="control-label" for="telf-celular">Teléfono Celular</label>  
                          <input id="telf-celular" name="telf-celular" type="text" placeholder="Celular del Propietario" 
                              class="form-control">
                          <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-10">
                          <label class="control-label" for="telf-fijo">Teléfono Fijo</label>  
                          <input id="telf-fijo" name="telf-fijo" type="text" placeholder="Telefono Fijo del Propietario" 
                              class="form-control">
                          <div class="help-block with-errors"></div>
                        </div>
                  </div>
                </div>
                    <div class="form-group" style="display: none;">
                      <div class="g-recaptcha" data-sitekey="<?php echo $keyCaptcha;?>"></div>
                    </div>
                </fieldset>
                <div class="form-group col-md-12">
                    <input class="btn btn-success col-md-3 pull-right" type="button" name="register" id="registrar" value="Registrar" />
                </div>
            </form>
            <div class="col-md-12 pull-left">
                ¿Ya registrado?
                <?php echo anchor('', 'Iniciar Sesion'); ?>
            </div>
        </div>
    </div>
</section>