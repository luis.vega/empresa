<?php

$errorClass   = empty($errorClass) ? ' error' : $errorClass;
$controlClass = empty($controlClass) ? 'span6' : $controlClass;
$fieldData = array(
    'errorClass'    => $errorClass,
    'controlClass'  => $controlClass,
);

?>

<script type="text/javascript">
    var paisDefaultId = "<?php echo $paisDefaultId;?>";
</script>
<section id="register">
    <h1 class="page-header">Perfil de Usuario</h1>
    <div class="alert alert-info fade in">
        <h4 class="alert-heading">Los campos obligatorios están con '*'.</h4>
    </div>
    <div class="row-fluid">
        <div class="col-md-12">
            <form data-toble = "validator" role = "form" class="form-horizontal" id="form-registro">
                <?php echo form_open_multipart('api/file/do_upload');?>
                <fieldset>
                    <div class="form-group">
                      <label class="control-label" for="nombres">Nombres (*)</label>  
                      <input id="nombres" name="nombres" type="text" placeholder="Nombres del Propietario" 
                          class="form-control" required value="<?php echo $user->nombres; ?>">
                          <div class="help-block with-errors"></div>
                    </div>
                   <div class="form-group">
                      <label class="control-label" for="apellido-paterno">Apellido Paterno (*)</label>  
                      <input id="apellido-paterno" name="apellido-paterno" type="text" placeholder="Apellido Paterno del Propietario" 
                          class="form-control" required value="<?php echo $user->appaterno; ?>">
                          <div class="help-block with-errors"></div>
                   </div>
                   <div class="form-group">
                      <label class="control-label" for="apellido-materno">Apellido Materno (*)</label>  
                      <input id="apellido-materno" name="apellido-materno" type="text" placeholder="Apellido Materno del Propietario" 
                          class="form-control" value="<?php echo $user->apmaterno; ?>">
                   </div>

                   <div class="form-group">
                      <label class="control-label" for="nombre-usuario">Nombre Usuario</label>
                      <input id="nombre-usuario" name="nombre-usuario" type="text" 
                          class="form-control" value="<?php echo $user->Login; ?>" disabled>
                   </div>

                    <div class="form-group">
                      <label class="control-label" for="pais-nacionalidad">Pais Nacionalidad (*)</label>  
                      <select id="pais-nacionalidad" name="pais-nacionalidad" class="form-control" required>
                         <option value="">Seleccione</option>
                        <?php if (isset($paises) && is_array($paises)) :?>
                            <?php foreach ($paises as $pais) : ?>
                                <?php if ($user->codpaisorigen == $pais->codpais) :?>
                                    <option value="<?php echo $pais->codpais; ?>" selected>
                                        <?php echo $pais->nombre; ?>
                                    </option>
                                <?php else: ?>
                                    <option value="<?php echo $pais->codpais; ?>">
                                        <?php echo $pais->nombre; ?>
                                    </option>
                                <?php endif;?>
                            <?php endforeach; ?>
                          </select>
                        <?php endif;?>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                      <label class="control-label" for="tipo-documento">Tipo Documento (*)</label>  
                      <select id="tipo-documento" name="tipo-documento" class="form-control" required>
                         <option value="">Seleccione</option>
                        <?php if (isset($tiposDocumento) && is_array($tiposDocumento)) :?>
                            <?php foreach ($tiposDocumento as $tipo) : ?>
                                <?php if ($user->codtipodocid == $tipo->codtipodocumento) :?>
                                    <option value="<?php echo $tipo->codtipodocumento; ?>" selected>
                                        <?php echo $tipo->nombre; ?>
                                    </option>
                                <?php else: ?>
                                    <option value="<?php echo $tipo->codtipodocumento; ?>">
                                        <?php echo $tipo->nombre; ?>
                                    </option>
                                <?php endif;?>
                            <?php endforeach; ?>
                          </select>
                        <?php endif;?>
                        <div class="help-block with-errors"></div>
                    </div>

                   <div class="form-group form-inline">
                      <label class="control-label" for="numero-documento">Nro Documento (*)</label> <br>
                      <input id="numero-documento" name="numero-documento" type="text" placeholder="Nro Documento" 
                          class="form-control input-medium" maxlength="45" value="<?php echo $user->nrodocid; ?>">
                    <?php if (isset($user->emitido) ) :?>
                      <select id="departamento-extension" name="departamento-extension" class="form-control input-medium">
                         <option value="">Seleccione Extensi&oacute;n</option>
                          <?php foreach ($departamentos as $departamento) : ?>

                              <?php if ($user->emitido == $departamento->sigla_extension_ci) :?>
                                  <option value="<?php echo $departamento->sigla_extension_ci; ?>" selected>
                                      <?php echo $departamento->nombre; ?>
                                  </option>
                              <?php else: ?>
                                  <option value="<?php echo $departamento->sigla_extension_ci; ?>">
                                      <?php echo $departamento->nombre; ?>
                                  </option>
                              <?php endif;?>
                          <?php endforeach; ?>
                      </select>
                    <?php endif;?>
                   </div>
                <div class="row panel panel-default">
                  <div class="panel-heading">Otro Datos</div>
                  <div class="panel-body">
                        <div class="form-group col-md-10">
                          <label class="control-label" for="email">Email (*)</label>  
                          <input id="email" name="email" type="email" placeholder="Email del Propietario" 
                              class="form-control" required value="<?php echo $user->email; ?>" >
                          <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-10">
                          <label class="control-label" for="telf-celular">Teléfono Celular</label>  
                          <input id="telf-celular" name="telf-celular" type="text" placeholder="Celular del Propietario" 
                              class="form-control" value="<?php echo $user->celular; ?>" >
                          <div class="help-block with-errors"></div>
                        </div>

                        <div class="form-group col-md-10">
                          <label class="control-label" for="telf-fijo">Teléfono Fijo</label>  
                          <input id="telf-fijo" name="telf-fijo" type="text" placeholder="Telefono Fijo del Propietario" 
                              class="form-control"  value="<?php echo $user->telefono; ?>">
                          <div class="help-block with-errors"></div>
                        </div>
                  </div>
                </div>
                </fieldset>
                <div class="form-group col-md-12">
                    <a href="/" class="btn btn-danger col-md-3 pull-right"> Cancelar</a>
                </div>
            </form>
        </div>
    </div>
</section>