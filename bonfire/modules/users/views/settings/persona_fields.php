<fieldset>

	<div class="form-group">
      <label class="control-label" for="nombres">Nombres (*)</label>  
      <input id="nombres" name="nombres" type="text" placeholder="Nombres" 
          class="form-control">
          <div class="help-block with-errors"></div>
    </div>

   <div class="form-group">
      <label class="control-label" for="apellido-paterno">Apellido Paterno (*)</label>  
      <input id="apellido-paterno" name="apellido-paterno" type="text" placeholder="Apellido Paterno" 
          class="form-control">
          <div class="help-block with-errors"></div>
   </div>

   <div class="form-group">
      <label class="control-label" for="apellido-materno">Apellido Materno</label>  
      <input id="apellido-materno" name="apellido-materno" type="text" placeholder="Apellido Materno" 
          class="form-control">
   </div>

    <div class="form-group">
      <label class="control-label" for="pais-nacionalidad">Pais Nacionalidad (*)</label>  
      <select id="pais-nacionalidad" name="pais-nacionalidad" class="form-control">
         <option value="">Seleccione</option>
        <?php if (isset($paises) && is_array($paises)) :?>
            <?php foreach ($paises as $pais) : ?>
                <?php if ($paisDefaultId == $pais->codpais) :?>
                    <option value="<?php echo $pais->codpais; ?>" selected>
                        <?php echo $pais->nombre; ?>
                    </option>
                <?php else: ?>
                    <option value="<?php echo $pais->codpais; ?>">
                        <?php echo $pais->nombre; ?>
                    </option>
                <?php endif;?>
            <?php endforeach; ?>
          </select>
        <?php endif;?>
        <div class="help-block with-errors"></div>
    </div>

    <div class="form-group">
      <label class="control-label" for="tipo-documento">Tipo Documento (*)</label>  
      <select id="tipo-documento" name="tipo-documento" class="form-control">
         <option value="">Seleccione</option>
        <?php if (isset($tiposDocumento) && is_array($tiposDocumento)) :?>
            <?php foreach ($tiposDocumento as $tipo) : ?>
                <?php if ($DocumentoDefaultId == $tipo->codtipodocumento) :?>
                    <option value="<?php echo $tipo->codtipodocumento; ?>" selected>
                        <?php echo $tipo->nombre; ?>
                    </option>
                <?php else: ?>
                    <option value="<?php echo $tipo->codtipodocumento; ?>">
                        <?php echo $tipo->nombre; ?>
                    </option>
                <?php endif;?>
            <?php endforeach; ?>
          </select>
        <?php endif;?>
        <div class="help-block with-errors"></div>
    </div>

   <div class="form-group form-inline">
      <label class="control-label" for="numero-documento">Nro Documento (*)</label> <br>
      <input id="numero-documento" name="numero-documento" type="text" placeholder="Nro Documento" 
          class="form-control input-medium" maxlength="45">
      <select id="departamento-extension" name="departamento-extension" class="form-control input-medium">
         <option value="">Seleccione Extensi&oacute;n</option>
        <?php if (isset($departamentos) && is_array($departamentos)) :?>
            <?php foreach ($departamentos as $departamento) : ?>
                <option value="<?php echo $departamento->sigla_extension_ci; ?>">
                    <?php echo $departamento->nombre; ?>
                </option>
            <?php endforeach; ?>
        <?php endif;?>
      </select>
   </div>

    <div class="form-group">
      <label class="control-label" for="telf-celular">Teléfono Celular</label>  
      <input id="telf-celular" name="telf-celular" type="text" placeholder="Celular" 
          class="form-control">
      <div class="help-block with-errors"></div>
    </div>

    <div class="form-group">
      <label class="control-label" for="telf-fijo">Teléfono Fijo</label>  
      <input id="telf-fijo" name="telf-fijo" type="text" placeholder="Telefono Fijo" 
          class="form-control">
      <div class="help-block with-errors"></div>
    </div>

</fieldset>