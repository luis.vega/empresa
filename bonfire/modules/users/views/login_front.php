
<?php
	$site_open = $this->settings_lib->item('auth.allow_register');
?>

<div class="row">
  <div class="col-md-3"></div>
  <div class="col-md-6">
  
		<div class="panel panel-success" id="login-form" role="form" data-toggle="validator">
				<input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">
			  <div class="panel-heading">
			  	<h3>Iniciar Sesión</h3>
			  </div>
			  <div class="panel-body">
			  	<div class="login">
			        <div class="form-group">
		          		<span class="glyphicon glyphicon-info-sign pull-right tool-help"  data-placement="right"
		          		title="Debe ingresar su cuenta de usuario de acceso, los valores pueden ser: Nombre Usuario asignado al registrarse."></span>
		
			          	<input id="username" name="username" type="text" class="form-control input-md-8" placeholder="Nombre Usuario" required tabindex="1">
			          	<div class="help-block with-errors"></div>
			        </div>
		
			        <div class="form-group">
			          	
		          		<span class="glyphicon glyphicon-info-sign pull-right tool-help"  data-placement="right"
		          		title="Debe ingresar su contraseña se usuario, recuerde qué es la que ingresó al registrarse."></span>
		
			          	<input id="password" name="password" type="password" class="form-control input-md-8" placeholder="Contraseña" required tabindex="2">
			          	<div class="help-block with-errors"></div>
			        </div>
		
				<div class="form-group">
						<input class="btn btn-large btn-primary" type="button" name="login" id="log-me-in" value="Iniciar Sesión" tabindex="3" />
				</div>
		
			
				<p style="text-align: center">
					<?php if ( $site_open ) : ?>
						<?php echo anchor(REGISTER_FRONT_URL, 'Crear Cuenta'); ?>
					<?php endif; ?>
		
					<br/>
					<?php echo anchor('/olvide-mi-cuenta', '¿Olvidaste tus credenciales?'); ?>
				</p>
					
			  	</div>
			  	<div class="col-md-3"></div>
			  </div>
		</div>
		
	</div>
	<div class="col-md-3"></div>
</div>