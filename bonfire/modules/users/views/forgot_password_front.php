

<div class="page-header">
	<h1>Resetear Contraseña</h1>
</div>

<?php 
    // Recursos JS
    Assets::add_module_js('users','js/forgot_password_front.js');
?>

<?php if (validation_errors()) : ?>
	<div class="alert alert-error fade in">
		<?php echo validation_errors(); ?>
	</div>
<?php endif; ?>

<div class="alert alert-info fade in">
	Ingrese su correo electrónico, le enviaremos su nombre de usuario y una contraseña temporal.
</div>

<div class="col-md-12">

<?php echo form_open($this->uri->uri_string(), array('class' => "form-horizontal form-reset", 'autocomplete' => 'off', 'data-toggle' => 'validator', 'role' => 'form')); ?>


	<div class="row">
		<div class="form-group col-md-5">
		  <label class="control-label" for="email">Email</label>  
		  <input id="email" name="email" type="email" placeholder="Email de Usuario" 
		      class="form-control" required>
		  <div class="help-block with-errors"></div>
		</div>
	</div>

	<div class="form-group row">
		<input class="btn btn-primary" type="button" name="send" id="send" value="Enviar Contraseña" />
	</div>

<?php echo form_close(); ?>

</div>
