
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Migration_Empresa_TipoActividad_tables extends Migration
{

    private $table_name = 'atec_actividad';

    private $tiposActividad_data = array();


    /****************************************************************
     * Migration methods
     */
    /**
     * Install this migration
     */
    public function up()
    {
        $this->Load_Data();
        $this->db->update_batch($this->table_name, $this->tiposActividad_data, 'id');
    }

    /**
     * Uninstall this migration
     */
    public function down()
    {
        $update_data = array();
        foreach ($this->tiposActividad_data as $data)
        {
            $update_data[] = array(
                'descripcion' => $data['descripcion'],
                'name' => $data['name'],
            );
        }
        $this->db->update_batch($this->table_name, $update_data, 'id');
    }

    private function Load_Data() {
        $ci =& get_instance();
        $ci->load->model('empresas/tipoactividad_model');

        $this->tiposActividad_data = array(
            array(
                'id' => TipoActividad_model::ASERRADERO,
                'nombre' => 'Aserradero',
                'descripcion' => 'ACTIVIDAD ASERRADERO',
            ),
            // array(
            //     'id' => TipoActividad_model::CARPINTERIAS,
            //     'nombre' => 'Carpinterías',
            //     'descripcion' => 'ACTIVIDAD CARPINTERIA',
            // ),
            // array(
            //     'id' => TipoActividad_model::LAMINADORA,
            //     'nombre' => 'Laminadora',
            //     'descripcion' => 'Actividad Laminadora',
            // ),
            // array(
            //     'id' => TipoActividad_model::ASERRADERO,
            //     'nombre' => '',
            //     'descripcion' => '',
            // ),
        );
    }
}