

$(document).ready(function() {
    $('.print-comprobante1').click(function () {
    	 var contents = $("#box-content-comprobante1")[0].innerHTML; //incluido el contenedor
    	 //var contents = $("#content-comprobante").html(); //sin incluir el contenedor
         var frame1 = $('<iframe />');
         frame1[0].name = "frame1";
         frame1.css({ "position": "absolute", "top": "-10000px" });
         $("body").append(frame1);
         var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
         frameDoc.document.open();
         //Create a new HTML document.
         frameDoc.document.write('<html><head><title>Comprobante de Registro</title>');
         //Append the external CSS file.
         frameDoc.document.write('<link href="/themes/default/css/bootstrap.min.css" rel="stylesheet" type="text/css" />');
         frameDoc.document.write('<link href="/themes/default/css/bootstrap-theme.css" rel="stylesheet" type="text/css" />');
         frameDoc.document.write('<link href="/themes/default/css/app.css" rel="stylesheet" type="text/css" />');
         frameDoc.document.write('<link href="/themes/default/css/print.css" rel="stylesheet" type="text/css" />');
         
         frameDoc.document.write('</head><body>');

         //Append the DIV contents.
         frameDoc.document.write(contents);
         frameDoc.document.write('</body></html>');
         frameDoc.document.close();

         window.frames["frame1"].print();


    });

    $('.volver').click(function () {
        redirectTo('solicitudes');
    });
});