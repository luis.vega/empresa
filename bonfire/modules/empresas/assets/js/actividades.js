

$(document).ready(function() {
    configSteps();

	$('#btn-anterior').click(function () {
		previewAction();
	});
	
	// Enviar Archivos a ser guardados
	$('#btn-siguiente').click(function () {
		saveActividades();
	});

	$('#add-actividad').click(function () {
		addSectionActividad();
	});

	// Avento para Abrir y Cerrar panel de Actividad
	$(document).on('click', '.panel-heading span.clickable', function(e){
	    var $this = $(this);
		if(!$this.hasClass('panel-collapsed')) {
			$this.parents('.panel').find('.panel-body').slideUp();
			$this.addClass('panel-collapsed');
			$this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
		} else {
			$this.parents('.panel').find('.panel-body').slideDown();
			$this.removeClass('panel-collapsed');
			$this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
		}
	});
});

function configSteps() {
	$('#pasos-registro-empresa').find('#paso2').addClass('btn-success');
	$('#pasos-registro-empresa').find('#paso3').attr('disabled', true);
	$('#pasos-registro-empresa').find('#paso4').attr('disabled', true);
}

function addSectionActividad() {
	var div = $('#actividad-principal').clone();
	$('#actividades div.actividad');
	var nroActividades = $('#actividades div.actividad').length +1;
	$(div).find('div.panel-heading h6').text('Actividad ' + nroActividades);
	$(div).attr('id', 'actividad-' + nroActividades);
	$('#actividades').append(div);

	// Cerrar Actividades y Abrir el ultimo
	var actividadesAbiertas = $('.actividad').find('.glyphicon-chevron-up');
	for (var index = 0; index < actividadesAbiertas.length; index++) {
		var actividadSectionOpen = actividadesAbiertas[index];
		$($(actividadSectionOpen).parent()).trigger('click');
	}
	$($(div).find('.clickable')).trigger('click');
	
}

function saveActividades() {
	// Guardar Archivos
	//saveFileFundem(saveFileNit);
	redirectTo("/empresas/archivos-registro");
}

function previewAction() {
	redirectTo("/empresas/registro");
}

function saveDatos(callback) {
	var formData = new FormData(document.getElementById("form-upload-fundem"));
	$.ajax({
      url: baseUrl + "/api/file/upload_archivo_registro_empresa",
      type: "POST",
      data: formData,
      processData: false,
      contentType: false,
      success: function(result){
		if (result && !result.error && callback)
       		callback();
      },
      error: function(datosError) { 
      	console.log('error', datosError)
      }
    }).done(function( data ) {
        console.log('done', data );
    });

} 



