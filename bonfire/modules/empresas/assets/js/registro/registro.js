
$(document).ready(function() {
	esconsumidorFinal = false;
    $('#municipio').select2({ width: '100%' });
    $('#pais-nacionalidad').select2({ width: '100%' });
    setAddRequiredFieldsRepresentante($('.panel-propietario'));

    configSteps();

    $('#btn-siguiente').click(function () {
    	saveRegistroEmpresa();
    });

	$(document).on('click', '#add-sucursal', function () {
		addSectionSucursal();
	});

    	// Avento para Abrir y Cerrar panel de Actividad
	$(document).on('click', '.panel-heading span.clickable', function(e){
	    var $this = $(this);
		if(!$this.hasClass('panel-collapsed')) {
			$($this.parents('.panel')[0]).find('.panel-body').slideUp();
			$this.addClass('panel-collapsed');
			$this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
		} else {
			$($this.parents('.panel')[0]).find('.panel-body').slideDown();
			$this.removeClass('panel-collapsed');
			$this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
		}
	});

	$(document).on('click', '.panel-heading span.removable', function(e){
		$(this).parent().parent().remove();
	});

	actionAddAndRemoveRepresentante();

	$(document).on('change', '#pais-nacionalidad', function(e) {
		var paisSelect = $(this).val();
		var content = $(this).parents()[3];
		if (paisSelect.toString() != paisDefaultId ) {
			$('#departamento-extension', content).val('').trigger('change');
			$('#departamento-extension', content).hide();
			$('#departamento-extension', content).attr('required', false);

		} else {
			$('#departamento-extension', content).show();
			$('#departamento-extension', content).attr('required', true);
		} 
	});

	$('#soy-propietario').click(function () {
		if ($(this).is(':checked')) {
			$('.panel-propietario').hide();
			setNotRequiredRepresentante($('.panel-propietario'));

		} else {
			$('.panel-propietario').show();
			setAddRequiredFieldsRepresentante($('.panel-propietario'));
			$('#departamento-extension', $('.panel-propietario')).attr('required', true);
			$('#numero-documento', $('.panel-propietario')).attr('required', true);

		}
	});

	$('#tengo-registro').click(function () {
		if ($(this).is(':checked')) {
			$('div.registro-antiguo').show();
			$('input#registro-antiguo').attr('required', true);
		} else {
			$('div.registro-antiguo').hide();
			$('input#registro-antiguo').attr('required', false);
		}
	});

	$('#cxb-actividades').click(function () {
		var isChequed = $(this).is(':checked');
		if (isChequed) {
			var checkboxs = $('input#cxb-actividad:not(:checked)');
			for (var i = 0; i < checkboxs.length; i++) {
				$(checkboxs[i]).trigger('click');
			}
		} else {
			var checkeados = $('input#cxb-actividad:checked');
			for (var i = 0; i < checkeados.length; i++) {
				$(checkeados[i]).trigger('click');
			}
		}
	});

	// Eliminar una actividad.
	$(document).on('click', '.remove-actividad', function () {
		var parents = $(this).parents();
		var sucursal = $(parents[7]);
		if ($($(parents).find('#actividades')).select2('val') == consumidorFinalId) {
			$('#numero-nit').attr('required', true);
			$('#numero-fundempresa').attr('required', true);
			$($('#paso2','.stepwizard').parent()).show();
			esconsumidorFinal = false;
		} 

		$($(this).parent().parent()).remove();
		updateNroActividades(sucursal);
	});

	$('#remove-all-actividad').click(function () {
		var checkeados = $('input#cxb-actividad:checked');
		if (checkeados.length > 0) {
			for (var i = 0; i < checkeados.length; i++) {
				$($(checkeados[i]).parent().parent()).remove();
			}
			updateNroActividades($(this).parents()[4]);
		} else {
			showErrors(['Debe seleccionar al menos una actividad para eliminar']);
		}
	});

	// Adicionar Actividad
	$(document).on('click', '#add-actividad', function () {
		addActividad($(this).parents()[4]);
	})

	// Update Actividad de Sucursal
	$(document).on('change', '#actividades', function () {
		var esUnico = $($(this).select2('data')[0].element).data('es-unico');
		var td = $($($(this).parents()[1])).find('td')[2];
		$(td).html('');
		if ($(this).select2('val') == consumidorFinalId) {
			$('#numero-nit').attr('required', false);
			$('#numero-fundempresa').attr('required', false);
			$($('#paso2','.stepwizard').parent()).hide();
			esconsumidorFinal = true;

		} else 
			$($('#paso2','.stepwizard').parent()).show();

		if (esUnico) {
			$(td).text('Categoria Unica');
		} else {
			loadCategorias($(this).select2('val'), td);
		}
	});

	$($('.panel-agente').find('.clickable')).trigger('click');
	$($('#sucursal-principal').find('.clickable')).trigger('click');
	$('#con-agente').trigger('click');

	// Filtro de Axiliares
	$(document).on('click', '#filtrar-auxiliar', function () {
		var content = $(this).parents()[5];
		searchAxiliares(content);
	});
	$(document).on('click', '#filtrar-auxiliar-clean', function () {
		var panel = $(this).parents()[5];
		cleanFilterAgente(panel);
	});
	$(document).on('click', '#filter-form', function () {
		var panel = $(this).parents()[5];
		$(panel).find('#filter-text-complete').val('');
	});

	$(document).on('click', '#con-agente', function () {
		var fields = $(this).parents()[2];
		var panel = $(fields).find('.panel-agente');
		if ($(this).is(':checked')) {
			$(panel).show();
		} else {
			$(panel).hide();
		}
	});
	// En el caso que este en modo edicion
	loadCurrentSolicitud();
});

function loadCurrentSolicitud() {
	if(currentId && currentId != "") {
		getCurrentSolicitud(currentId, loadSolicitud);
	}
}

function loadSolicitud(data) {
	$('#razon-social').val(data.razon_social_nombre);
	$('#numero-nit').val(data.nit);
	$('#numero-fundempresa').val(data.numero_fundempresa);
	if (data.registro_antiguo) {
		$('#tengo-registro').trigger('click');
		$('#registro-antiguo').val(data.registro_antiguo);
	}
	if (data.propietario) {
		cargarDatosPersona($('.panel-propietario'), data.propietario);
		$('#email', $('.panel-propietario')).val(data.propietario.email);
	} else {
		$('#soy-propietario').trigger('click');
	}
	if (data.sucursales &&  data.sucursales.length > 0) {
		cargarSucursales(data.sucursales);
	}
}

function cargarSucursales(collection) {
	for (var indexSuc = 0; indexSuc < collection.length; indexSuc++) {
		var sucursal = collection[indexSuc];
		if (indexSuc > 0) {
			$('#add-sucursal').trigger('click');
		}
		cargarInfoSucursal($('.sucursal')[indexSuc], sucursal);
	}
	
	$('.panel-body', $('div.sucursal')).removeAttr('style');
	var sucursalesAbiertas = $('div.sucursal').find('.panel-body');
	for (var index = 0; index < sucursalesAbiertas.length; index++) {
		var sucursalSectionOpen = sucursalesAbiertas[index];
		$(sucursalSectionOpen).removeAttr('style');
		$(sucursalSectionOpen).css('display', 'none');
	}
}

function cargarInfoSucursal(content, sucursal) {
	$('#direccion', content).val(sucursal.direccion);
	$('#coordenada-x', content).val(sucursal.coordenada_x);
	$('#coordenada-y', content).val(sucursal.coordenada_y);
	$('#municipio', content).val(parseInt(sucursal.municipio_id));
	$('#municipio', content).change();
	$(content).data('data', sucursal);
	if (sucursal.agente_registro) {
		setSelectMunicipioPanel(content);
		addSelectAxiliares([sucursal.agente], content);
		$('#agente', content).val(sucursal.agente_registro);
		$('#agente', content).change();
	}

	if (sucursal.actividades.length > 0) {
		for (var indexAct = 0; indexAct < sucursal.actividades.length; indexAct++) {
			var actividad = sucursal.actividades[indexAct];
			if (actividad.actividad.codtipoactindustrial == consumidorFinalId) {
				esconsumidorFinal = true;
			}
			var key = addActividad(content);
			var tr = $(content).find('.tb-actividades tbody tr.tr-' + key);
			$('#actividades', tr).val(actividad.actividad_id);
			$('#actividades', tr).change();
			$(tr).data('data', actividad);
		}
	}
	if (sucursal.representantes && sucursal.representantes.length > 0) {
		for (var indexRepr = 0; indexRepr < sucursal.representantes.length; indexRepr++) {
			var representante = sucursal.representantes[indexRepr];
			$('#tab-add-representante', content).trigger('click');
			var tabs = $('.representante', content);
			var lastLi = tabs[tabs.length - 1];
			cargarDatosPersona(lastLi, representante);
			$(lastLi).data('data', representante);
		}

		// Marcar Tabs de Representantes el ultimo visible
		var tabs = $('.representante', content);
		var lis = $('.nav-representantes li', content);
		$(tabs[tabs.length - 1]).addClass('active in');
		$('#tab-add-representante', content).add('click');
		// Desmarcado de Tabs
		$('.nav-representantes li', content).attr('class', '');
		$('.nav-representantes li', content).attr('style', '');
		$(lis[lis.length - 3]).addClass('active');

	}
}

function cargarDatosPersona(content, data) {
	$('#nombres', content).val(data.nombres);
	$('#apellido-paterno', content).val(data.appaterno);
	$('#apellido-materno', content).val(data.apmaterno);
	$('#pais-nacionalidad', content).val(data.codpaisorigen);
	$('#pais-nacionalidad', content).change();
	$('#tipo-documento', content).val(data.codtipodocid);
	$('#tipo-documento', content).change();
	$('#numero-documento', content).val(data.nrodocid);

	if (data.emitido) {
		$('#departamento-extension', content).val(data.emitido);
		$('#departamento-extension', content).change();
	}
	$('#telf-celular', content).val(data.celular);
	$('#telf-fijo', content).val(data.telefono);

	$(content).data('data', data);
}

function cargarCategorias(collection, td) {
	var selectTpl = $($('.tpl-actividad select')[1]).clone();
	for (var indexC = 0; indexC < collection.length; indexC++) {
		var categoria = collection[indexC];
		var descripcion = categoria.nombre;
		if (categoria.volumen != "")
			descripcion += "  (" + categoria.volumen +  ")";
		$(selectTpl).append(new Option(descripcion, categoria.id));
	}
	$(selectTpl).attr('required', true);
	$(selectTpl).attr('id', 'categoria');
	var divString = divFormGroup();
	divString = divString.replace('{element}', $(selectTpl).get(0).outerHTML);
	$(td).append(divString);
}

function divFormGroup() {
	var div = '<div class="form-group">' +
	'{element}' + 
	'<div class="help-block with-errors"></div>';
	return div;
}

function addActividad(sucursal) {
	var key = stringRandom(7);
	var trString = '<tr class="tr-{key}"> <td><input type="checkbox" id="cxb-actividad" /></td> <td class="col-md-3">{select}</td> <td></td>';
	trString += '<td class="text-center"> <button class="btn btn-danger btn-xs remove-actividad" type="button" title="Eliminar Fila">';
	trString += '<span class="glyphicon glyphicon-remove"></span> </button> </td> </tr>';

	trString = trString.replace('{select}', $('.tpl-actividad select')[0].outerHTML);
	trString = trString.replace('{key}', key);
	var tbody = $(sucursal).find('.tb-actividades tbody');
	$(tbody).append(trString);
	updateNroActividades(sucursal);
	var select = $(sucursal).find('.tr-' + key + ' select');
	$(select).attr('id', 'actividades');
	$(select).attr('required', true);
	$(select).select2({ width: '100%' });
	return key;
}

function updateNroActividades(sucursal) {
	var trs = $(sucursal).find('.tb-actividades tbody tr');
	$($(sucursal).find('.count-actividades')).text( trs.length + ' Actividad(es)');
}

function actionAddAndRemoveRepresentante() {
	
	$(document).on('click', 'li#tab-add-representante', function(e) {

		var sucursal = $(this).parents()[5];
		var panelSucursales = $(sucursal).find('.representante');
		var keyTag = 'r' + stringRandom(7);
		if ($($(sucursal).find('.nav-representantes li')[0]).is(':visible')) {

			var li = $($($(this).parent()).find('li')[0]).clone();
			var a = $(li).find('a');
			var size = $($(this).parent()).find('li').length;

			var key = (size - 2) + 1;

			var href = $(a).attr('href');
			keyTag = 'r' + stringRandom(7);
			var id = href.split('-')[3];
			href = href.replace(id, keyTag);
			$(a).text('');
			$(a).append(getIconTab() + ' R' + key);
			$(a).attr('href', href);

			var panelTab = $(panelSucursales[0]).clone();

			href = href.replace('#', '');
			$(panelTab).attr('id', href);
			$(panelSucursales[0]).parent().append(panelTab);
			$(this).before(li);

			// Clear campos del panel
			$(panelTab).removeClass('active');
			$(panelTab).find('input').val('');
			setSelectNacionalidadPanel(panelTab);

		} else {
			$($($(this).parent()).find('li')[0]).show();
			var idString = $($($(this).parent()).find('li a')[0]).attr('href');
			var id = idString.split('-')[3];
			idString = idString.replace(id, keyTag);
			$($($(this).parent()).find('li a')[0]).attr('href', idString);
			var panelSucursal = $(sucursal).find('.panel-representantes');
			$($(panelSucursal).find('.representante')[0]).show();
			$($(panelSucursal).find('.representante')[0]).addClass('fade');
			idString = idString.replace('#', '');
			$($(panelSucursal).find('.representante')[0]).removeAttr('disabled');
			$($(panelSucursal).find('.representante')[0]).attr('id', idString);
			$($(panelSucursal).find('.representante')[0]).removeAttr('style');
			setAddRequiredFieldsRepresentante($(panelSucursal).find('.representante')[0]);
			setSelectNacionalidadPanel($(panelSucursal).find('.representante')[0]);
		}

		// Ocultar todos los Tabs
		for (var indexTab = 0; indexTab < panelSucursales.length; indexTab++) {
			$(panelSucursales[indexTab]).removeClass('active');
		}
	});

	$(document).on('click', 'li#tab-remove-representante', function(e) {
		var listTags = $($(this).parent()).find('li');
		var sucursal = $(this).parents()[5];
		var panelSucursal = $(sucursal).find('.panel-representantes'); 

		if (listTags.length > 3 && ($($(sucursal).find('.nav-representantes li')[0]).is(':visible'))) {

			var size = $($(this).parent()).find('li').length;
			var key = (size - 2);
			var liRemove = listTags[key - 1];
			var a = $(liRemove).find('a');
			var panelTab = $($(panelSucursal).find('.representante')[0]).clone();
			href = $(a).attr('href')
			$(a).parent().remove();
			$('' + href).remove();
		} else {
			$($($(this).parent()).find('li')[0]).hide();
			$($(panelSucursal).find('.representante')[0]).hide();
			$($(panelSucursal).find('.representante')[0]).attr('disabled', true);
			setNotRequiredRepresentante($(panelSucursal).find('.representante')[0]);
		}
	});

}

function setSelectNacionalidadPanel(tabPanel) {
	$($($(tabPanel).find('#pais-nacionalidad').parent()).find('span')[0]).remove()
	$(tabPanel).find('#pais-nacionalidad').removeClass('select2-hidden-accessible');
	$(tabPanel).find('#pais-nacionalidad').select2({ width: '100%' });  
}

function setSelectMunicipioPanel(content) {
	$($($(content).find('#municipio').parent()).find('span')[0]).remove()
	$(content).find('#municipio').removeClass('select2-hidden-accessible');
	$(content).find('#municipio').select2({ width: '100%' });  
}

function setNotRequiredRepresentante(panelRepresentante) {
	$($(panelRepresentante).find('input')).removeAttr('required');
	$($(panelRepresentante).find('select')).removeAttr('required');
}

function setAddRequiredFieldsRepresentante(panelRepresentante) {
	$($(panelRepresentante).find('input')).attr('required', true);
	$($(panelRepresentante).find('select')).attr('required', true);
	$($(panelRepresentante).find('#apellido-materno')).attr('required', false);
	$($(panelRepresentante).find('#telf-fijo')).attr('required', false);
}

function getIconTab() {
	return '<span class="glyphicon btn-glyphicon glyphicon-user"></span>';
}

function getIconRemoveSucursal() {
	return '<span class="pull-right removable"><i class="glyphicon glyphicon-trash" title="Eliminar"></i></span>';
}

function configSteps() {
	$('#pasos-registro-empresa').find('#paso1').addClass('btn-success');
	$('#pasos-registro-empresa').find('#paso2').attr('disabled', true);
	$('#pasos-registro-empresa').find('#paso3').attr('disabled', true);
	$('#pasos-registro-empresa').find('#paso4').attr('disabled', true);
}

function saveRegistroEmpresa() {
	// Guardar por Ajax
	// Direccionar despues de Guardar
	var hasErrors = $('#form-registration').validator('validate').has('.has-error').length;

	$('.alert-errors').hide();
	if (!hasErrors) {

		if (currentId) {
			var datos = prepareUpdateData();
			processUpdateEmpresa(datos);
		} else {
			var datos = prepareData();
			processSaveEmpresa(datos);
		}
	} else {
		$('.alert-errors').show();
        $('#contenedor').animate({
            scrollTop: $('.alert-errors').offset().top
        }, 2000);
	}
}

function prepareUpdateData() {
	var dataGeneral = {
		razon_social: $('#razon-social').val().trim(),
		numero_nit: $('#numero-nit').val().trim(),
		numero_fundempresa: $('#numero-fundempresa').val().trim(),
		current_id: currentId
	};
	if ($('#tengo-registro').is(':checked')) {
		dataGeneral.registro_antiguo = $('#registro-antiguo').val().trim();
	}
	
	if ($('#soy-propietario').is(':checked')) {
		dataGeneral.es_propietario = true;
	} else {
		dataGeneral.propietario = getDataPersonal($('.panel-propietario'));
		if ($('.panel-propietario').data('data')) {
			dataGeneral.propietario.codpersona = $('.panel-propietario').data('data').codpersona;
		}
	}

	// Datos Agente Auxiliar
	if ($('#con-agente').is(':checked')) {
		dataGeneral.agente = getDataPersonal($('.panel-agente'));
	}
	dataGeneral.sucursales = getDataSucursales();
	return dataGeneral;
}


function prepareData() {
	var dataGeneral = {
		razon_social: $('#razon-social').val().trim(),
		numero_nit: $('#numero-nit').val().trim(),
		numero_fundempresa: $('#numero-fundempresa').val().trim()
	};
	if ($('#tengo-registro').is(':checked')) {
		dataGeneral.registro_antiguo = $('#registro-antiguo').val().trim();
	}
	
	if ($('#soy-propietario').is(':checked')) {
		dataGeneral.es_propietario = true;
	} else {
		dataGeneral.propietario = getDataPersonal($('.panel-propietario'));
	}

	// Datos Agente Auxiliar
	if ($('#con-agente').is(':checked')) {
		dataGeneral.agente = getDataPersonal($('.panel-agente'));
	}
	dataGeneral.sucursales = getDataSucursales();
	return dataGeneral;
}

function getDataSucursales() {
	var sucursalesData = [];
	var sucursales = $('#sucursales .sucursal');
	for (var indeSuc = 0; indeSuc < sucursales.length; indeSuc++) {
		var sucursal = sucursales[indeSuc];
		var sucursalObject = {
			index_sucursal: indeSuc,
			direccion: $($(sucursal).find('#direccion')).val().trim(),
			coordenada_x: $($(sucursal).find('#coordenada-x')).val().trim(),
			coordenada_y: $($(sucursal).find('#coordenada-y')).val().trim(),
			municipio_id: $($(sucursal).find('#municipio')).select2('val'),
			representantes: [],
			actividades: []
		};
		if ($(sucursal).data('data')) {
			sucursalObject.empresa_sucursal_id = $(sucursal).data('data').empresa_sucursal_id;
		}

		var representantes = $(sucursal).find('div.representante'); 
		for (var indexRepr = 0; indexRepr < representantes.length; indexRepr++) {
			var representante = representantes[indexRepr];
			if (!$(representante).attr('disabled')) {
				var representanteObj = getDataPersonal(representante);
				if ($(representante).data('data')) {
					representanteObj.codpersona = $(representante).data('data').codpersona;
				}
				sucursalObject.representantes.push(representanteObj);
			}
		}

		// Agentes
		if ($($(sucursal).find('#con-agente')).is(':checked')) {
			var agenteIdRegistro = $($(sucursal).find('#div-agente')).find('select#agente').select2('val');
			if (agenteIdRegistro) {
				sucursalObject.agente_registro = agenteIdRegistro;
			} 
		}
		var actividades = $(sucursal).find('.tb-actividades tbody tr'); 
		if (!actividades || actividades.length == 0) {
			showErrors(['Debe añadir al menos una actividad por Sucursal']);
			throw new Error('No se puede continuar con el proceso');
		}
		for (var indexAct = 0; indexAct < actividades.length; indexAct++) {
			var actividadSelect = $(actividades[indexAct]).find('#actividades');
			var actividad = {};
			var esUnico = $($(actividadSelect).select2('data')[0].element).data('es-unico');
			actividad.actividad_id = $(actividadSelect).select2('val');
			if (esUnico) {
				actividad.categoria_id = null;
			} else {
				var categoriaSelect = $(actividades[indexAct]).find('#categoria');
				actividad.categoria_id = $(categoriaSelect).val();
			}
			if ($(actividades[indexAct]).data('data'))
				actividad.sucursal_actividad_id = $(actividades[indexAct]).data('data').sucursal_actividad_id;
			sucursalObject.actividades.push(actividad);
		}
		sucursalesData.push(sucursalObject);
	}
	return sucursalesData;
}

function getDataPersonal(content) {
	var formdata = $(content).find('select, input').serializeArray();
	var data = {};
    $.each(formdata, function () {
        if (data[this.name]) {
            if (!data[this.name].push) {
                data[this.name] = [data[this.name]];
            }
            data[this.name].push(this.value.trim() || '');
        } else {
            data[this.name] = this.value.trim() || '';
        }
    });
	return data;
}

function addSectionSucursal() {
	var div = $('#sucursal-principal').clone();
	$(div).find('input').val('');
	$('#sucursales div.sucursal');
	var nroActividades = $('#sucursales div.sucursal').length;
	$(div).find('div.panel-heading h6').text('Sucursal ' + nroActividades);
	$($(div).find('div.panel-heading')[0]).prepend(getIconRemoveSucursal());
	$(div).attr('id', 'sucursal-' + nroActividades);
	$('#sucursales').append(div);
	setSelectMunicipioPanel(div);

	// Limpiar Div
	var lists = $(div).find('.nav-representantes li');
	for (var i = 1; i < (lists.length - 2); i++) {
		var li = lists[i];
		$(li).remove();
	}
	$(lists[0]).hide();
	var tabs = $(div).find('.representante');
	for (var indexTab = 1; indexTab < tabs.length; indexTab++) {
		var tab = tabs[indexTab];
		$(tab).remove();
	}
	$(tabs[0]).hide();
	$(tabs[0]).attr('disabled', true);
	setNotRequiredRepresentante(tabs[0]);

	// Panel de Actividades
	$($(div).find('.panel-actividades')).find('.count-actividades').text('0 Actividad(es)');
	$($(div).find('.panel-actividades')).find('.tb-actividades tbody tr').remove();
	//tb-actividades

	// Seccion Agente
	var panelAgente = $(div).find('.panel-agente');
	cleanFilterAgente(panelAgente);

	// Cerrar Sucursales y Abrir el ultimo
	var sucursales = $('div.sucursal');
	for (var index = 0; index < sucursales.length; index++) {
		var sucursalSectionOpen = sucursales[index];
		var panel = $('.panel-body', sucursalSectionOpen)[0];
		$(panel).removeAttr('style');
		$(panel).css('display', 'none');
	}
	$($(div).find('.clickable')[0]).trigger('click');
}

function searchAxiliares(content) {
	var filterComplete = $(content).find('#filter-text-complete').val().trim();
	var textFilter = $($(content).find('#form-filter')).find('#filter-text').val().trim();
	var numeroRegistro = $($(content).find('#form-filter')).find('#numero-registro').val().trim();
	if (textFilter === "" && numeroRegistro === "" && filterComplete == "") {
		showErrors(['Debe ingresar valor en los filtros para procesar la búsqueda']);
	} else {
		var filterDocumento = null;
		if (filterComplete != "")
			textFilter = filterComplete;

		var filters =  textFilter.split(',');
		if (filters[1]) 
			filterDocumento = filters[1].trim();

		$(content).find('#filter-text-complete').val(textFilter);

		processFilterAuxiliar({
			filter_nombres: filters[0].trim(),
			filter_documento: filterDocumento,
			numero_registro: numeroRegistro
		}, content);
	}
}

function cleanFilterAgente(panel) {
	$($(panel).find('#form-filter')).find('#filter-text').val('');
	$($(panel).find('#form-filter')).find('#numero-registro').val('');
	$(panel).find('#filter-text-complete').val('');
	$(panel).find('#div-agente').html('');
}

function addSelectAxiliares(collection, panel) {
	$($(panel).find('#div-agente')).empty();
	if (collection) {
		var content = $(panel).find('#div-agente');
		var selectTpl = $($('.tpl-actividad select')[1]).clone();
		for (var indexAgente = 0; indexAgente < collection.length; indexAgente++) {
			var agente = collection[indexAgente];
			var nombreAgente = agente.agente_npm + ' - '+ agente.registro;
			$(selectTpl).append(new Option(nombreAgente, agente.registro));
		}
		$(selectTpl).attr('required', true);
		$(selectTpl).attr('id', 'agente');
		$(selectTpl).addClass('col-md-10');
		$(content).append('<label class="col-md-2 control-label" for="agente">Agente</label>');
		$(content).append(selectTpl);
		$(selectTpl).select2({ width: '100%' });
		$(content).append(templateSelectAgente());
	} else {
		$($(panel).find('#div-agente')).append('<h3>No existen registro</h3>');
	}
}

function templateSelectAgente() {
	return '<div class="help-block with-errors"></div>';
}

/**************** AJax Proceso de Peticiones **************************/
function processFilterAuxiliar(filterData, panel) {
	$.blockUI();
	$.ajax({
		url: baseUrl + "/api/empresa/filter-auxiliares", 
		method: "POST",
		data: filterData,
		dataType : 'json',
		success: function(result){
			$.unblockUI();
			addSelectAxiliares(result.data, panel);
    	},
    	error: function(datosError) { 
    		$.unblockUI();
    		showErrors(datosError.responseJSON.error);
    	}
    });
}


function processSaveEmpresa(formData) {

	$.blockUI();
	$.ajax({
		url: baseUrl + "/api/empresa/save_solicitud_registro", 
		method: "POST",
		data: formData,
		dataType : 'json',
		success: function(result){
			$.unblockUI();
			if (result && result.error) {
				showErrors(result.error);
			} else {
				if (esconsumidorFinal)
					redirectTo("/empresas/archivos-registro");
				else 
					redirectTo("/empresas/formulario26-registro");
			}
    	},
    	error: function(datosError) { 
    		$.unblockUI();
    		if (datosError && datosError.responseJSON)
    			showErrors(datosError.responseJSON.error);
    		else
    			showErrors(['Error en proceso de regisrar los datos, contactese con la USI-ABT.']);
    	}
    });
}

function processUpdateEmpresa(formData) {

	$.blockUI();
	$.ajax({
		url: baseUrl + "api/empresa/update_solicitud_registro", 
		method: "POST",
		data: formData,
		dataType : 'json',
		success: function(result){
			$.unblockUI();
			if (result && result.error) {
				showErrors(result.error);
			} else {
				if (esconsumidorFinal)
					redirectTo("empresas/archivos-registro");
				else 
					redirectTo("empresas/formulario26-registro");
			}
    	},
    	error: function(datosError) { 
    		$.unblockUI();
    		if (datosError && datosError.responseJSON)
    			showErrors(datosError.responseJSON.error);
    		else
    			showErrors(['Error en proceso de actualizar los datos, contactese con la USI-ABT.']);
    	}
    });
}

function loadCategorias(actividadId, td) {

	$.blockUI();
	$.ajax({
		url: baseUrl + "/api/empresa/get_categorias/" + actividadId, 
		method: "GET",
		dataType : 'json',
		success: function(result){
			$.unblockUI();
			if (result && result.data) {
				cargarCategorias(result.data, td);
			}
    	},
    	error: function(datosError) { 
    		$.unblockUI();
    		showErrors(datosError.responseJSON.error);
    	}
    });
}

function getCurrentSolicitud(currentId, callback) {
	$.blockUI();
	$.ajax({
		url: baseUrl + "api/empresa/get_current_registro/" + currentId, 
		method: "GET",
		dataType : 'json',
		success: function(result){
			$.unblockUI();
			if (result && result.data) {
				callback(result.data)
			}
    	},
    	error: function(datosError) { 
    		$.unblockUI();
    		if (datosError && datosError.responseJSON)
    			showErrors(datosError.responseJSON.error);
    		else
    			showErrors(['Error al obtener informacion de la solicitud, contactese con USI-ABT']);
    	}
    });
}