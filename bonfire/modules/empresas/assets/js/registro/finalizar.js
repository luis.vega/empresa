

$(document).ready(function() {

	configSteps();

    $('.print-comprobante').click(function () {
    	 var contents = $("#box-content-comprobante").html(); //incluido el contenedor
    	 //var contents = $("#content-comprobante").html(); //sin incluir el contenedor
         var frame1 = $('<iframe />');
         frame1[0].name = "frame1";
         frame1.css({ "position": "absolute", "top": "-1000000px" });
         $("body").append(frame1);
         var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
         frameDoc.document.open();
         //Create a new HTML document.
         frameDoc.document.write('<html><head><title>Comprobante</title>');
         //Append the external CSS file.
        

         frameDoc.document.write('<script src="/assets/js/modernizr-2.5.3.js" type="text/javascript" /></script>');
         
         frameDoc.document.write('<link href="/themes/default/css/bootstrap.min.css" rel="stylesheet" type="text/css" />');
         frameDoc.document.write('<link href="/themes/default/css/bootstrap-theme.css" rel="stylesheet" type="text/css" />');
         frameDoc.document.write('<link href="/themes/default/css/select2.min.css" rel="stylesheet" type="text/css" />');
         frameDoc.document.write('<link href="/themes/default/css/stepwizard.css" />');
         frameDoc.document.write('<link href="/themes/default/css/app.css" rel="stylesheet" type="text/css" />');
         frameDoc.document.write('<link href="/themes/default/screen.css" rel="stylesheet" type="text/css" />');
         
         frameDoc.document.write('<script src="/assets/js/jquery-3.2.1.min.js" type="text/javascript" /></script>');
         frameDoc.document.write('<script src="/themes/default/js/bootstrap.min.js" type="text/javascript" /></script>');
         frameDoc.document.write('<script src="/assets/js/jquery-3.2.1.min.js" type="text/javascript" /></script>');
         frameDoc.document.write('<script src="/themes/default/js/select2.min.js" type="text/javascript" /></script>');
         frameDoc.document.write('<script src="/themes/default/js/app.js" type="text/javascript" /></script>');
        
         frameDoc.document.write('</head><body>');

         //Append the DIV contents.
         frameDoc.document.write(contents);
         frameDoc.document.write('</body></html>');
         frameDoc.document.close();
         setTimeout(function () {
             window.frames["frame1"].focus();
             window.frames["frame1"].print();
             frame1.remove();
         }, 500);
    });
    
    $('#btn-siguiente').click(function () {
		redirectTo("solicitudes");
    });

    if (esconsumidorFinal) {
    	$($('#paso2','.stepwizard').parent()).hide();
    }
});


function configSteps() {
    $('#pasos-registro-empresa').find('#paso1').addClass('past');
    $('#pasos-registro-empresa').find('#paso2').addClass('past');
    $('#pasos-registro-empresa').find('#paso3').addClass('past');
	$('#pasos-registro-empresa').find('#paso4').addClass('btn-success');
}

