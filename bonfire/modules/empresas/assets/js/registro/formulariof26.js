$(document).ready(function() {
    configSteps();

	$('.tip-file').tooltip()
    // Avento para Abrir y Cerrar panel de Actividad / Sucursales
	$(document).on('click', '.panel-heading span.clickable', function(e){
	    var $this = $(this);
		if(!$this.hasClass('panel-collapsed')) {
			$($this.parents('.panel')[0]).find('.panel-body').slideUp();
			$this.addClass('panel-collapsed');
			$this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
		} else {
			$($this.parents('.panel')[0]).find('.panel-body').slideDown();
			$this.removeClass('panel-collapsed');
			$this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
		}
	});
	$($('.sucursal').find('.clickable')).trigger('click');

	$(document).on('click', '.subtable-panel-actividad', function(e){
		var icon = $(this).find('.icon')
		if ($(icon).hasClass('glyphicon glyphicon-eye-open')) {
 			$(icon).removeClass('glyphicon glyphicon-eye-open');
 			$(icon).addClass('glyphicon glyphicon-eye-close');
		} else {
 			$(icon).removeClass('glyphicon glyphicon-eye-close');
 			$(icon).addClass('glyphicon glyphicon-eye-open');
		}
	    $('.subtable-body', this).toggle(); 
	});

	$('button#add-proyectado').click(function () {
		var sucursalId = $($(this).parents()[5]).attr('id');
		var actividadlId = $($(this).parents()[2]).attr('id');
		$('#producto', $('#form-modal-add-producto')).val('');
		$('#producto', $('#form-modal-add-producto')).data('unidad', null);
		$('#producto', $('#form-modal-add-producto')).attr('disabled', false);
		//$('#producto', $('#form-modal-add-producto')).select2('destroy');
		$('#producto', $('#form-modal-add-producto')).select2({
		    dropdownParent: $('#modal-add-producto'),
		    width: '100%'
		 });
		$('#especie', $('#form-modal-add-producto')).val('');
		$('#especie', $('#form-modal-add-producto')).attr('disabled', false);
		$('#especie', $('#form-modal-add-producto')).select2('destroy');
		$('#especie', $('#form-modal-add-producto')).select2({
		    dropdownParent: $('#modal-add-producto'),
		    width: '100%'
		 });
		$('input', $('#form-modal-add-producto')).val('');
		$('div.unidad', $('#modal-add-producto')).hide();
		$('#observacion', $('#form-modal-add-producto')).val('');
		$('#observacion', $('#form-modal-add-producto')).attr('disabled', false);

		//$('form', $('#form-modal-add-producto').parent()).validator('destroy');
		$('#modal-add-producto').data('seccion', {id_sucursal: sucursalId, id_actividad: actividadlId});

		// Inicializar el modal
		var unidadId = $('#unidad-id', $(this).parent()).data('value'); 
		if (unidadId) {
			var unidadValue = $('#unidad-desc',$(this).parent()).data('value');
			var productoId = $('#producto-id',$(this).parent()).data('value');
			var especieId = $('#especie-id',$(this).parent()).data('value');
			$('#producto', $('#form-modal-add-producto')).data('unidad', {nombre: unidadValue, codunidadcfo: unidadId});
			$('#producto', $('#form-modal-add-producto')).val(productoId);
			$('#producto', $('#form-modal-add-producto')).trigger('change');
			$('#producto', $('#form-modal-add-producto')).attr('disabled', true);
			$('#especie', $('#form-modal-add-producto')).val(especieId);
			$('#especie', $('#form-modal-add-producto')).trigger('change');
			$('#especie', $('#form-modal-add-producto')).attr('disabled', true);

		}
		if ($(this).data('is') == 'proyectado') {
			$('#observacion', $('#form-modal-add-producto')).val(obsProyectadoId);

			var content = $('#' + actividadlId , '#' + sucursalId);
			var table = $($(content).find('.tb-actividades'));
			var data = $('tbody tr#' + obsProyectadoId, table).data('movimiento');
			if (unidadId && data) {
				$('#cantidad', $('#form-modal-add-producto')).val(data.cantidad);
				$('#rendimiento', $('#form-modal-add-producto')).val(data.rendimiento);
			} else {
				if (data) {
					$('#producto', $('#form-modal-add-producto')).val(data.producto_codigo);
					$('#producto', $('#form-modal-add-producto')).trigger('change');
					$('#especie', $('#form-modal-add-producto')).val(data.especiecomun_codigo);
					$('#especie', $('#form-modal-add-producto')).trigger('change');
					$('#cantidad', $('#form-modal-add-producto')).val(data.cantidad);
					$('#rendimiento', $('#form-modal-add-producto')).val(data.rendimiento);
				}
			}
		} else {
			$('#observacion', $('#form-modal-add-producto')).val(obsSaldoStockId);

			var content = $('#' + actividadlId , '#' + sucursalId);
			var table = $($(content).find('.tb-actividades'));
			var data = $('tbody tr#' + obsSaldoStockId, table).data('movimiento');
			if (unidadId && data) {
				$('#cantidad', $('#form-modal-add-producto')).val(data.cantidad);
				$('#rendimiento', $('#form-modal-add-producto')).val(data.rendimiento);
			} else {
				if (data) {
					$('#producto', $('#form-modal-add-producto')).val(data.producto_codigo);
					$('#producto', $('#form-modal-add-producto')).trigger('change');
					$('#especie', $('#form-modal-add-producto')).val(data.especiecomun_codigo);
					$('#especie', $('#form-modal-add-producto')).trigger('change');
					$('#cantidad', $('#form-modal-add-producto')).val(data.cantidad);
					$('#rendimiento', $('#form-modal-add-producto')).val(data.rendimiento);
				}
			}
		}
		$('#observacion', $('#form-modal-add-producto')).trigger('change');
		$('#observacion', $('#form-modal-add-producto')).attr('disabled', true);

		$('#modal-add-producto').modal();
	});

	$(' #add-maquinaria').click(function () {
		var sucursalId = $($(this).parents()[5]).attr('id');
		var actividadlId = $($(this).parents()[2]).attr('id');
		$('input', $('#form-modal-add-detalle-producto')).val('');
		$('form', $('#form-modal-add-detalle-producto').parent()).validator('destroy');
		$('#modal-add-detalle-producto').data('seccion', {id_sucursal: sucursalId, id_actividad: actividadlId});
		$('#modal-add-detalle-producto').modal();
	});

    $('#btn-siguiente').click(function () {
    	saveRegistroFormulario26();
    });

    $('#especie').select2({
	    dropdownParent: $('#modal-add-producto'),
	    width: '100%'
	 });

    $('#producto').select2({
	    dropdownParent: $('#modal-add-producto'),
	     width: '100%'
	});

	$($('#producto'), $('#form-modal-add-producto')).change(function () {
		var productoId = $(this).select2('val');
		$('div.unidad', $('#modal-add-producto')).hide();
		var unidad = $('#producto', $('#form-modal-add-producto')).data('unidad');
		if (unidad) {
			updateUnidadProductoModal(unidad);
		} else 
			getUnidad(productoId, updateUnidadProductoModal);
	});

	$('#save-modal', $('#form-modal-add-producto').parent().parent()).click(function () {
		addDatosModalProducto();
	});

	$(document).on('click', '.row-remove-producto', function(e){
		var productoId = $(this).data('id');
		var rowProducto = $(this).parents()[1];
		removeProducto({id: productoId, empresa_id: empresaId}, rowProducto, removeRowProducto)
	});
	$(document).on('click', '.row-remove-detalle', function(e){
		var productoId = $(this).data('id');
		var rowProducto = $(this).parents()[1];
		removeDetalleMaquinaria({id: productoId, empresa_id: empresaId}, rowProducto, removeRowProducto)
	});
	//

	$('input#file-fo-26-sin-funcionar').change(function () {
		var content = $($(this).parents()[1]).clone();
		var stringForm = '<form method="POST" id="form-file-productos" name="form-upload" enctype="multipart/form-data"> </form>';
		var form = $.parseHTML(stringForm);
		$('.reporte-productos', content).hide();
		$(form).append(content);
		uploadFileProductos(form[0], $(this), $(this).parents()[5], loadProductos);
	});

	$('input#file-fo-26-maquinaria').change(function () {
		var content = $($(this).parents()[1]).clone();
		var stringForm = '<form method="POST" id="form-file-productos" name="form-upload" enctype="multipart/form-data"> </form>';
		var form = $.parseHTML(stringForm);
		$(form).append(content);
		uploadFileProductos(form[0], $(this), $(this).parents()[5], loadMaquinarias);
	});
	loadRegistroFormulario26();

	$('#btn-anterior').click(function () {
		previewAction();
	});
});

function loadRegistroFormulario26() {
	var collection = [];

	// Solicitud de Datos
	var sucursales = solicitudObject.sucursales;
	for (var indexSuc = 0; indexSuc < sucursales.length; indexSuc++) {
		var sucursal = sucursales[indexSuc];
		for (var indexAct = 0; indexAct < sucursal.actividades.length; indexAct++) {
			var actividad = sucursal.actividades[indexAct];
			if (actividad.movimientos) {
				var tabActividad = $('#tab-act-' + indexAct, $('#index-sucursal-' + indexSuc));
				if (actividad.filename) {
					var urlLink = baseUrl + "api/file/download_file_empresa/" + actividad.filename;
					$('.file-download', tabActividad).attr('href', urlLink);
					$($('.file-download', tabActividad).parent()).show();
					$('#file-fo-26', tabActividad).attr('required', false);	
				} else {
					$('.file-download', tabActividad).remove();
				}

				for (var indexMov = 0; indexMov < actividad.movimientos.length; indexMov++) {
					var movimiento = actividad.movimientos[indexMov];
					var datosProducto = {
						producto_codigo: movimiento.producto_codigo,
						especiecomun_codigo: movimiento.especiecomun_codigo,
						unidad_id: movimiento.unidad_id,
						observacion: movimiento.observacion,
						cantidad: movimiento.cantidad,
						rendimiento: movimiento.rendimiento,
						empresa_id: empresaId,
					};
					datosProducto.actividad_id = actividad.sucursal_actividad_id;
					datosProducto.sucursal_id = actividad.sucursal_id;
					var dataRow = [
						movimiento.producto.descripcion,
						movimiento.especie.ncomun + " (" + movimiento.especie.ncientifico + ")",
						movimiento.cantidad,
						movimiento.unidad.nombre,
						movimiento.observacion_desc,
						movimiento.rendimiento
					];
					var collection = [];
					collection['content'] = tabActividad;
					collection['dataRow'] = dataRow;
					collection['data'] = datosProducto;
					collection['key'] = movimiento.observacion;
					addProductoMovimiento(collection);
				}
			}

		}
	}
}

function removeRowProducto(row) {
	$(row).remove();
}

function configSteps() {
	$('#pasos-registro-empresa').find('#paso2').addClass('btn-success');
	$('#pasos-registro-empresa').find('#paso1').addClass('past');
	$('#pasos-registro-empresa').find('#paso3').attr('disabled', true);
	$('#pasos-registro-empresa').find('#paso4').attr('disabled', true);
}

function addDatosModalProducto() {
	var hasErrors = $('form', $('#form-modal-add-producto').parent()).validator('validate').has('.has-error').length;
	if (!hasErrors) {
		var seccionIds = $('#modal-add-producto').data('seccion');
		var datosProducto = {
			producto_codigo: $('#producto', $('#form-modal-add-producto')).select2('val'),
			especiecomun_codigo: $('#especie', $('#form-modal-add-producto')).select2('val'),
			unidad_id: $('#unidad', $('#form-modal-add-producto')).data('unidad').codunidadcfo,
			observacion: $('#observacion', $('#form-modal-add-producto')).val(),
			cantidad: $('#cantidad', $('#form-modal-add-producto')).val().trim(),
			rendimiento: $('#rendimiento', $('#form-modal-add-producto')).val().trim(),
			empresa_id: empresaId,
		};
		datosProducto.actividad_id = $('#' + seccionIds.id_actividad, '#' + seccionIds.id_sucursal).data('actividad-id');
		datosProducto.sucursal_id = $('#' + seccionIds.id_sucursal).data('id');

		var observacionText = $('#observacion', $('#form-modal-add-producto')).find("option:selected").text().trim();
		var dataRow = [
			$('#producto', $('#form-modal-add-producto')).select2('data')[0].text.trim(),
			$('#especie', $('#form-modal-add-producto')).select2('data')[0].text.trim(),
			$('#cantidad', $('#form-modal-add-producto')).val().trim(),
			$('#unidad', $('#form-modal-add-producto')).data('unidad').nombre,
			observacionText,
			$('#rendimiento', $('#form-modal-add-producto')).val().trim()
		];
		var collection = [];
		collection['content'] = $('#' +  seccionIds.id_actividad , '#' +  seccionIds.id_sucursal);
		collection['dataRow'] = dataRow;
		collection['data'] = datosProducto;
		collection['key'] = $('#observacion', $('#form-modal-add-producto')).val();
		addProductoMovimiento(collection);

	}
}

function updateUnidadProductoModal(objectUnidad) {
	$('div.unidad', $('#modal-add-producto')).show();
	var contentUnidad = $('div.unidad', $('#modal-add-producto'));
	$('span', contentUnidad).text(objectUnidad.nombre);
	$('input', contentUnidad).data('unidad', objectUnidad);
}

function loadMaquinarias(collection, content) {
	var table = $($(content).find('.tb-actividades'));
	var tbody = $('tbody', table);
	$('tr', tbody).remove();

	for (var indexProd = 0; indexProd < collection.length; indexProd++) {
		var maquinariaObj = collection[indexProd];
		if (maquinariaObj.modelo == undefined || maquinariaObj.modelo == null)
			maquinariaObj.modelo = "";
		if (maquinariaObj.especificacion_tecnica == undefined || maquinariaObj.especificacion_tecnica == null)
			maquinariaObj.especificacion_tecnica = "";
		if (maquinariaObj.anio_compra == undefined || maquinariaObj.anio_compra == null)
			maquinariaObj.anio_compra = "";
		addMaquinaria(maquinariaObj, content);
	}
}

function loadProductos(data, content) {
	var table = $($(content).find('.tb-actividades'));
	var tbody = $('tbody', table);
	$('tr', tbody).remove();

	var collection = data.productos;
	for (var indexProd = 0; indexProd < collection.length; indexProd++) {
		var productoObj = collection[indexProd];
		productoObj.comentarios = null;

		addProducto(productoObj, content);
	}
	$('.reporte-productos', content).show();
	var divReport = $('.reporte-productos', content);
	if (data.unidad)
		$('.vol-total', divReport).text(data.volumen_total + ' ' + data.unidad.abreviacion.toLowerCase());
	else
		$('.vol-total', divReport).text(data.volumen_total);
	$('.total-filas', divReport).text(data.filas_validas);
	$('.categoria', divReport).text(data.categoria.descripcion);
}

function addProducto(producto, content) {
	var table = $($(content).find('.tb-actividades'));
	var tbody = $('tbody', table);
	var row = rowTableProducto();
	var values = Object.values(producto);
	for (var index = 0; index < values.length; index++) {
		 row = row.replace('{' + index + '}', values[index]);
	}
	$(tbody).append(row);
}

function addProductoMovimiento(result) {
	var content = result['content'];
	var dataRow = result['dataRow'];
	var table = $($(content).find('.tb-actividades'));
	var tbody = $('tbody', table);
	var row = rowTableProductoMovimiento();

	for (var index = 0; index < dataRow.length; index++) {
		 row = row.replace('{' + index + '}', dataRow[index]);
	}
	var rowEdit = $('tr#' + result['key'], tbody);

	row = row.replace('{key}', result['key']);
	if (rowEdit && rowEdit.length > 0) {
		$(rowEdit).replaceWith(row);
	} else 
		$(tbody).append(row);
	$('tr#' + result['key'], tbody).data('movimiento', result['data']);
	$('#modal-add-producto').modal('hide');
	$('#modal-add-producto').data('seccion', null);
	$.unblockUI();

}

function rowTableProductoMovimiento() {
	return '<tr class="row" id="{key}">' + 
		'<td class="col-md-1">{0}</td>' +
		'<td class="col-md-1">{1}</td>' +
		'<td class="col-md-1 text-center">{2}</td>' +
		'<td class="col-md-1">{3}</td>' +
		'<td class="col-md-1">{4}</td>' +
		'<td class="col-md-1 text-center">{5}</td>' +
		'<tr>';
}

function rowTableProducto() {
	return '<tr class="row" id="{key}">' + 
		'<td class="col-md-1">{0}</td>' +
		'<td class="col-md-1">{1}</td>' +
		'<td class="col-md-1">{2}</td>' +
		'<td class="col-md-1 text-center">{3}</td>' +
		'<td class="col-md-1">{4}</td>' +
		'<td class="col-md-1 text-center">{5}</td>' +
		'<tr>';
}

function rowTableMovimiento() {
	return '<tr class="row" id="{key}">' + 
		'<td class="col-md-1">{0}</td>' +
		'<td class="col-md-1 text-center">{1}</td>' +
		'<td class="col-md-1">{2}</td>' +
		'<td class="col-md-1">{3}</td>' +
		'<td class="col-md-1 text-center">{4}</td>' +
		'<tr>';
}

function previewAction() {
	redirectTo("empresas/registro");
}

function validation() {

	var hasErrors = $('.has-error', $('#form-formulario26').validator('validate')).length;
	$('.alert-errors').hide();
	if (!hasErrors) {
		var sucursales = $('.sucursal');
		for (var index = 0; index < sucursales.length; index++) {
			var sucursal = sucursales[index];
			var tbody = $('.actividad table tbody', sucursal);
			for (let indexTable = 0; indexTable < tbody.length; indexTable++) {
				if (!$('tr', tbody).length || $('tr', tbody).length < 1) {
					showErrors(['Se requiere configuracion de Movimiento de Productos para todas las actividades.']);
					throw "No puede continuar con el proceso";
					return false;

				}
			}
			
		}

	} else {
		return false;
		$('.alert-errors').show();
	}
	return true;
}

function saveRegistroFormulario26() {
	if (validation()) {

		var collection = [];
		var sucursales = $('.sucursal');
		for (var index = 0; index < sucursales.length; index++) {
			var sucursal = sucursales[index];
			var tbody = $('.actividad table tbody', sucursal);
			for (var indexTable = 0; indexTable < tbody.length; indexTable++) {
				var movProyectado = $('tr#' + obsProyectadoId, tbody[indexTable]).data('movimiento');
				var movSaldoStock = $('tr#' + obsSaldoStockId, tbody[indexTable]).data('movimiento');

				var file = $('#file-fo-26', $(tbody[indexTable]).parents()[4]);
				var formData = new FormData();
				formData.append('empresa_id', empresaId);

				if (movProyectado) {
					formData.append('movimientos[' + obsProyectadoId + ']', JSON.stringify(movProyectado));
				}
				if (movSaldoStock) {
					formData.append('movimientos[' + obsSaldoStockId + ']', JSON.stringify(movSaldoStock));
				}
				var exitfile = $('.file-download', $(tbody[indexTable]).parents()[4]).length;
				if ($(file)[0].files[0])
					formData.append('file', $(file)[0].files[0]);
				else {
					if (exitfile)
						formData.append('exist_file', true);
				}


				collection.push(formData);

			}
			
		}
		if (collection.length > 0) {
			processRegistroMivimientos(collection);
		}
	}
}

function processRegistroMivimientos(collection) {
	if (collection && collection[0] !== undefined) {
		saveProducto(collection, processRegistroMivimientos);
	} else {
	  redirectTo("empresas/archivos-registro");
	}
}


/**************** AJax Proceso de Peticiones **************************/
function getUnidad(productoId, callback) {
	$.blockUI();
	$.ajax({
		url: baseUrl + "/api/empresa/get_unidad/" + productoId, 
		method: "GET",
		dataType : 'json',
		success: function(result){
			$.unblockUI();
			if (result && result.data)
				callback(result.data);
			else 
    		showErrors(['Error al obtener la unidad del producto seleccionado']);

    	},
    	error: function(datosError) { 
    		$.unblockUI();
    		showErrors(datosError.responseJSON.error);
    	}
    });
}

function saveDesalleMaquinaria(dataProducto, collection,callback) {
	$.blockUI();
	$.ajax({
		url: baseUrl + "/api/empresa/save_detalle_maquinaria", 
		method: "POST",
		data: dataProducto,
		dataType : 'json',
		success: function(result){
			$.unblockUI();
			if (result && result.data) {
				collection['detalle_id'] = result.data.detalle_id;
				callback(collection);
			}
			else 
    		showErrors(['Error al procesar el registro del producto']);

    	},
    	error: function(datosError) { 
    		$.unblockUI();
    		showErrors(datosError.responseJSON.error);
    	}
    });
}

function saveProducto(collection, callback) {
	$.blockUI();
	var dataProducto = collection[0];
	$.ajax({
		url: baseUrl + "/api/empresa/save_producto_registro", 
		method: "POST",
		data: dataProducto,
	    processData: false,
	    contentType: false,
		success: function(result){
			$.unblockUI();
			if (result && result.data) {
				collection.shift();
				callback(collection);
			}
			else 
    			showErrors(['Error al procesar el registro del producto']);

    	},
    	error: function(datosError) { 
    		$.unblockUI();
    		if (datosError && datosError.responseJSON)
    			showErrors(datosError.responseJSON.error);
    		else 
    			showErrors(['Error al procesar el registro de movimientos de producto']);
    	}
    });
}

function removeProducto(dataProducto, element, callback) {
	$.blockUI();
	$.ajax({
		url: baseUrl + "/api/empresa/remove_producto_registro", 
		method: "POST",
		data: $.param(dataProducto),
		success: function(result){
			$.unblockUI();
			if (result) {
				callback(element);
			}
			else 
    			showErrors(['Error al procesar la eliminacion el producto']);

    	},
    	error: function(datosError) { 
			$.unblockUI();
			if (datosError && datosError.responseJSON)
				showErrors(datosError.responseJSON.error);
			else 
				showErrors(['Error al proceso de eliminar el producto']);
    	}
    });
}

function removeDetalleMaquinaria(dataProducto, element, callback) {
	$.blockUI();
	$.ajax({
		url: baseUrl + "/api/empresa/remove_detalle_maquinaria", 
		method: "POST",
		data: dataProducto,
		success: function(result){
			$.unblockUI();
			if (result) {
				callback(element);
			}
			else 
    			showErrors(['Error al procesar la eliminacion el producto']);

    	},
    	error: function(datosError) { 
			$.unblockUI();
			if (datosError && datosError.responseJSON)
				showErrors(datosError.responseJSON.error);
			else 
				showErrors(['Error al proceso de eliminar el producto']);
    	}
    });
}

function uploadFileProductos(dataForm, inputFile, content,callback) {
	var formData = new FormData(dataForm);
	$.blockUI();
	$.ajax({
      url: baseUrl + "/api/empresa/save_productos_registro_empresa",
      type: "POST",
      data: formData,
      processData: false,
      contentType: false,
      success: function(result){
      	$.unblockUI();
      	if (result && result.data && callback)
      		callback(result.data, content);

		$(dataForm).find('.file-remove-upload').show();
      },
      error: function(datosError) { 
		$.unblockUI();
		$(inputFile).val('');
		if (datosError.responseJSON && datosError.responseJSON.error) {
			showErrors(datosError.responseJSON.error, "Error al subir archivo de Producto");
		} else {
			showErrors(['Error al procesar la subida de archivos, contacte a la USI-ABT.'], "Error Fatal archivo de Empresa");
		}
      }
    });

}