
$(document).ready(function() {
	var esconsumidorFinal = false;
    configSteps();
    $('.tip-file').tooltip();
	$('.fileinput-button').on('change', function(){
		$($(this).parent()).find('#bt-reset').show();
	});
	$('#bt-reset').click(function () {
		$(this).hide();
	});

	$('#btn-anterior').click(function () {
		previewAction();
	});

	// Enviar Archivos a ser guardados
	$('#btn-siguiente').click(function () {
		saveFiles();
	});

    	// Avento para Abrir y Cerrar panel de Actividad
	$(document).on('click', '.panel-heading span.clickable', function(e){
	    var $this = $(this);
		if(!$this.hasClass('panel-collapsed')) {
			$($this.parents('.panel')[0]).find('.panel-body').slideUp();
			$this.addClass('panel-collapsed');
			$this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
		} else {
			$($this.parents('.panel')[0]).find('.panel-body').slideDown();
			$this.removeClass('panel-collapsed');
			$this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
		}
	});
	$($('.sucursal').find('.clickable')).trigger('click');
	$('input#file-upload').change(function () {
		var form = $(this).parents()[2];
		uploadFileEmpresa(form);
	});

	$('input#file-upload-sucursal').change(function () {
		var form = $(this).parents()[2];
		uploadFileSucursal(form);
	});

	$('.file-remove-upload').click(function() {
		var form = $(this).parents()[2];
		removeUploadFileSucursal(form);
	});
	loadConfigDataFiles();

	$('#btn-finalizar').click(function () {
		finalizarRegistroEmpresa();
	});
});

function configSteps() {
	$('#pasos-registro-empresa').find('#paso1').addClass('past');
	$('#pasos-registro-empresa').find('#paso2').addClass('past');
	$('#pasos-registro-empresa').find('#paso3').addClass('btn-success');
	$('#pasos-registro-empresa').find('#paso4').attr('disabled', true);
}

function loadConfigDataFiles() {

	if (solicitudObject && solicitudObject.documentos) {
		for (var indexDoc = 0; indexDoc < solicitudObject.documentos.length; indexDoc++) {
			var documento = solicitudObject.documentos[indexDoc];
			var content = $('#file-' + documento.configuracion_archivo_empresa_id , '.files-empresa');
			if (documento.doc_presentado) {
				var urlLink = baseUrl + "api/file/download_file_empresa/" + documento.doc_presentado.nombre_file;
				$('.file-download', content).attr('href', urlLink);
				$('#file-upload', content).attr('required', false);
				$('.download', content).show();
			}
			
		}

		var sucursales = solicitudObject.sucursales;
		for (var indexSuc = 0; indexSuc < sucursales.length; indexSuc++) {
			var sucursal = sucursales[indexSuc];
			var contentSuc = $('#index-' + indexSuc, '.fields-sucursales');

			// Documentos Agente
			if (sucursal.agente && sucursal.agente_documentos) {
				for (var indexDocAg = 0; indexDocAg < sucursal.agente_documentos.length; indexDocAg++) {
					var docAgente = sucursal.agente_documentos[indexDocAg];
					var contentAgente = $('.field-agente', contentSuc);
					var content = $('#file-' + docAgente.configuracion_archivo_empresa_id , contentAgente);
					if (docAgente.doc_presentado) {
						var urlLink = baseUrl + "api/file/download_file_empresa/" + docAgente.doc_presentado.nombre_file;
						$('.file-download', content).attr('href', urlLink);
						$('#file-upload-sucursal', content).attr('required', false);
						$('.download', content).show();
					}

				}
			}

			// Documentos Sucursal
			if (sucursal.documentos) {
				for (var indexS = 0; indexS < sucursal.documentos.length; indexS++) {
					var docSuc = sucursal.documentos[indexS];
					var contentSucursal = $('.fields-sucursal', contentSuc);
					var content = $('#file-' + docSuc.configuracion_archivo_empresa_id , contentSucursal);
					if (docSuc.doc_presentado) {
						var urlLink = baseUrl + "api/file/download_file_empresa/" + docSuc.doc_presentado.nombre_file;
						$('.file-download', content).attr('href', urlLink);
						$('#file-upload-sucursal', content).attr('required', false);
						$('.download', content).show();
					}
				}
			}

			// Documentos reprensentantes
			if (sucursal.representantes && sucursal.representantes_documentos) {
				for (var indexRep = 0; indexRep < sucursal.representantes.length; indexRep++) {
					var representante = sucursal.representantes[indexRep];
					for (var indexDocRe = 0; indexDocRe < sucursal.representantes_documentos.length; indexDocRe++) {
						var docConfig = sucursal.representantes_documentos[indexDocRe];
						var contentRep = $('#tab-rep-' + representante.codpersona, contentSuc);
						var content = $('#file-' + docConfig.configuracion_archivo_empresa_id , contentRep);
						if (representante.doc_presentados && representante.doc_presentados[docConfig.configuracion_archivo_empresa_id]) {
							var doc_presentado = representante.doc_presentados[docConfig.configuracion_archivo_empresa_id];
							var urlLink = baseUrl + "api/file/download_file_empresa/" + doc_presentado.nombre_file;
							$('.file-download', content).attr('href', urlLink);
							$('#file-upload-sucursal', content).attr('required', false);
							$('.download', content).show();
						}
					}
				}
			}

			for (var indexAct = 0; indexAct < sucursal.actividades.length; indexAct++) {
				var actividadSuc = sucursal.actividades[indexAct];
				if (actividadSuc.actividad_id == consumidorFinalId) {
					$($('#paso2','.stepwizard').parent()).hide();
					esconsumidorFinal = true;
				}
			}
		}
	}
}

function saveFiles() {
	// Guardar Archivos
	var hasErrors = $('.has-error', $('#form-archivos').validator('validate')).length;
	var hasErrorsAgentes = $('.has-error', $('#form-file-agente').validator('validate')).length;
	var hasErrorsRepresentantes = $('.has-error', $('form#form-file-representante').validator('validate')).length;
	var hasErrorsActividades = $('.has-error', $('form#form-file-actividad').validator('validate')).length;

	$('.alert-errors').hide();
	if (!hasErrors && !hasErrorsAgentes && !hasErrorsRepresentantes && !hasErrorsActividades) {
		$('#modal-finalizar').modal();
	} else {
		$('.alert-errors').show();
	}
}

function previewAction() {
	if (typeof esconsumidorFinal !== "undefined" && esconsumidorFinal)
		redirectTo("empresas/registro");
	else 
		redirectTo("empresas/formulario26-registro");
}

function finalizar() {
	redirectTo("empresas/finalizar-registro");
}

function finalizarRegistroEmpresa() {
	finalizarSolicitud(finalizar);
}
 

function uploadFileEmpresa(dataForm) {
	var formData = new FormData(dataForm);
	$.blockUI();
	$.ajax({
      url: baseUrl + "/api/file/upload_archivo_empresa",
      type: "POST",
      data: formData,
      processData: false,
      contentType: false,
      success: function(result){
		$.unblockUI();
		if (result && !result.error) {
			$(dataForm).find('.file-remove-upload').show();
		}

      },
      error: function(datosError) { 
		$.unblockUI();
		if (datosError.responseJSON && datosError.responseJSON.error) {
			var errors = ['Error en el archivo ' + datosError.responseJSON.filename, datosError.responseJSON.error];
			showErrors(errors, "Error al subir archivo de Empresa");
		} else
			showErrors(['Error al procesar la subida de archivos, contacte a la USI-ABT.'], "Error Fatal archivo de Empresa");
      }
    });

}

function uploadFileSucursal(dataForm) {
	var formData = new FormData(dataForm);
	$.blockUI();
	$.ajax({
      url: baseUrl + "/api/file/upload_archivo_sucursal",
      type: "POST",
      data: formData,
      processData: false,
      contentType: false,
      success: function(result){
		$.unblockUI();
		$(dataForm).find('.file-remove-upload').show();
      },
      error: function(datosError) { 
		$.unblockUI();
		$($(dataForm).find('input[type="file"]')).val('');
		if (datosError.responseJSON && datosError.responseJSON.error) {
			var errors = ['Error en el archivo ' + datosError.responseJSON.filename, datosError.responseJSON.error];
			showErrors(errors, "Error al subir archivo de Empresa");
		} else {
			showErrors(['Error al procesar la subida de archivos, contacte a la USI-ABT.'], "Error Fatal archivo de Empresa");
		}
      }
    });

}

function removeUploadFileSucursal(dataForm) {
	var formData = new FormData(dataForm);
	$.blockUI();
	$.ajax({
      url: baseUrl + "/api/file/remove_upload_archivo_sucursal",
      type: "POST",
      data: formData,
      processData: false,
      contentType: false,
      success: function(result){
      	$(dataForm).find('.file-remove-upload').hide();
      	$($(dataForm).find('input[type="file"]')).val('');
		$.unblockUI();
      },
      error: function(datosError) { 
		$.unblockUI();
		
		if (datosError.responseJSON && datosError.responseJSON.error) {
			var errors = ['Archivo ' + datosError.responseJSON.filename, datosError.responseJSON.error];
			showErrors(errors, "Error al eliminar archivo de Empresa");
		} else {
			showErrors(['Error al procesar la eliminacion de archivos, contacte a la USI-ABT.'], "Error Fatal archivo de Empresa");
		}
      }
    });

}

function finalizarSolicitud(callback) {

	$.blockUI();
	$.ajax({
		url: baseUrl + "/api/empresa/finalizar_solicitud_registro/", 
		method: "POST",
		success: function(result){
			$.unblockUI();
			if (result && result.message) {
				callback();
			}
    	},
    	error: function(datosError) { 
    		$.unblockUI();

    		if (datosError.responseJSON && datosError.responseJSON.error) {
	    		showErrors(datosError.responseJSON.error);
			} else {
				showErrors(['Error al procesar la finalizacion de solicitud, contacte a la USI-ABT.']);
			}
    	}
    });
}


