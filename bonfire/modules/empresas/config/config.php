<?php defined('BASEPATH') || exit('No direct script access allowed');

$config['module_config'] = array(
        'name'          => 'Empresa',
        'description'   => 'Modulo para el registro de empresas',
        'author'        => 'ABT',
        'version'       => '1.0',
        'menu'          => array(
            'context'   => 'path/to/view'
        ),
        'weights'       => array(
            'context'   => 0
        )
);