<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class ConfiguracionEmpresa extends Admin_Controller
{
	
	public function __construct()
	{
		parent::__construct();

		Template::set('toolbar_title', 'Empresas Forestales');

	}
		

	public function Registro() {
		Template::set('page_title', 'Configuracion Archivos Empresa');


		Template::render('config');
	}



}