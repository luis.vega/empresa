<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Solicitudes extends Authenticated_Controller {

    const KEY_FILTERS_SOLICITUDES = 'admin_key_filters_solicitudes';

	public function __construct()
	{
		parent::__construct();

		Template::set('toolbar_title', 'Solicitudes Empresa');

		$this->load->model('empresas/trimestre_model');
        $this->load->model('admin/SolicitudEmpresaAdm_model');
        $this->load->model('admin/EmpresaAdm_model');
        $this->load->model('empresas/Empresa_model');
        $this->load->model('empresas/MovimientoProducto_model');
        $this->load->model('empresas/archivo_model');
        $this->load->library("pagination");

		$this->load->helper(array('form', 'url'));
		$this->closeSessionValues();
	}

	public function index() {
        Template::set('solicitud_estados',  $this->SolicitudEmpresaAdm_model->SolicitudEstados);

        Template::set('solicitudesPendientes', $this->getSolicitudesPorEstado('P'));
        Template::set('solicitudesPendientesRevision', $this->getSolicitudesPorEstado('PR'));
        Template::set('solicitudesAprobados', $this->getSolicitudesPorEstado('A'));
        Template::set('solicitudesRechazados', $this->getSolicitudesPorEstado('R'));

        Template::set('page_title', 'Panel Solicitudes');
        Template::set_view('solicitudes/index');
        Template::render();
     }

	public function show() {
		Template::set('page_title', 'Información Solicitud');
		Template::set_view('solicitudes/show');
		Template::render();
	}

	private function closeSessionValues() {
		// Variable de sesion para la solicitud en proceso de registro
        $this->session->set_userdata('solicitud_inscripcion', null);
	}

    public function ver_comprobante($solicitudId) {

        $datosResolucion = array();

        if (!$this->auth->is_logged_in()) {
            Template::redirect('/');
        }

        if (!isset($solicitudId) || empty($solicitudId)) {
            Template::redirect('/solicitudes');
        }

        $solicitud = $this->empresa_model->Get_SolicitudComplete($solicitudId);
        if ($solicitud->has_errors || empty($solicitud->data->es_pendiente_revision)) {
            Template::redirect('/solicitudes');
        }

        $solicitud = $solicitud->data;
        $this->archivo_model->Read_Config_All_Documentos($solicitud);

        Template::set('consumidor_final_id', Actividad_model::CONSUMIDOR_FINAL);
        Template::set('solicitud', $solicitud);
        Template::set('page_title', 'Finalizar - Registro Empresa');
        Template::set('datos_resolucion', $datosResolucion);
        Template::set_view('solicitudes/ver_comprobante');
        Template::render();
    }

    public function ver_solicitud($solicitudId) {
        $datosResolucion = array();

        if (!$this->auth->is_logged_in()) {
            Template::redirect('/');
        }

        if (!isset($solicitudId) || empty($solicitudId)) {
            Template::redirect('/solicitudes');
        }

        $solicitudInfo = $this->SolicitudEmpresaAdm_model->getById($solicitudId);

        if(empty($solicitudInfo))
            Template::redirect('/solicitudes');

        $solicitud = $this->Empresa_model->Get_SolicitudComplete($solicitudId);
        $solicitud = $solicitud->data;
        $this->MovimientoProducto_model->LoadInfoFormulario26($solicitud);
        $this->archivo_model->Read_Config_Documentos($solicitud);
        $this->archivo_model->Read_Upload_Documentos($solicitud);
        $archivosModeracion = $this->archivo_model->getModeracion('solicitud',$solicitudId);

        if (!empty($solicitud->resolucion)) {
            $fechaRegistro = strtotime($solicitud->resolucion->fecha_registro);
            $resolucion = (object) [
                'city' => "Santa Cruz",
                'day' => date('d', $fechaRegistro),
                'month_literal' => $this->GetMonthLiteral(date('m', $fechaRegistro)),
                'year' => date('Y', $fechaRegistro),

            ];
            Template::set('resolucion', $resolucion);
        }

        $empresaInfo = $this->EmpresaAdm_model->getById($solicitud->empresa_forestal_id);
        $empresaSucursales =  $this->Empresa_model->GetSucursales($solicitud->empresa_id, $solicitud->solicitud_tramite_empresa_id);

        Template::set('solicitud_estados',  $this->SolicitudEmpresaAdm_model->SolicitudEstados);
        Template::set('solicitud_tipos',  $this->SolicitudEmpresaAdm_model->SolicitudTipos);
        Template::set('empresa_estados',  $this->EmpresaAdm_model->EmpresaEstados);

        Template::set('estado_solicitud_aceptado', Solicitud_Model::ESTADO_ACEPTADO);
        Template::set('estado_solicitud_pendiente', Solicitud_Model::ESTADO_PENDIENTE_REVISION);
        Template::set('solicitud', $solicitud);
        Template::set('solicitud_info', $solicitudInfo);
        Template::set('solicitud_moderacion', $archivosModeracion);

        Template::render();
    }

	private function getSolicitudesPorEstado($status) {
        $user_id = $this->auth->user()->user_abt_id;

        $Filters = $this->session->userdata(self::KEY_FILTERS_SOLICITUDES);

        $solicitudes = $this->SolicitudEmpresaAdm_model->getSolicitudesPorUsuario(null, $Filters, $user_id, $status);

        return $solicitudes;
    }

    private function GetMonthLiteral($month) {
        switch ($month) {
            case 1:
                return "Enero";
                break;
            case 2:
                return "Febreo";
                break;
            case 3:
                return "Marzo";
                break;
            case 4:
                return "Abril";
                break;
            case 5:
                return "Mayo";
                break;
            case 6:
                return "Junio";
                break;
            case 7:
                return "Julio";
                break;
            case 8:
                return "Agosto";
                break;
            case 9:
                return "Septiembre";
                break;
            case 10:
                return "Octubre";
                break;
            case 11:
                return "noviembre";
                break;
            case 12:
                return "Diciembre";
                break;

        }
    }
}