<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Empresas extends Front_Controller
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->library('users/auth');
		Template::set('toolbar_title', 'Empresas Forestales');
		$this->load->model('empresas/trimestre_model');
		$this->load->model('empresas/actividad_model');
		$this->load->model('empresas/categoria_model');
		$this->load->model('empresas/archivo_model');
		$this->load->model('empresas/empresa_model');
		$this->load->model('empresas/especie_model');
		$this->load->model('empresas/movimientoproducto_model');
		$this->load->model('empresas/productoforestal_model');
		$this->load->model('api/municipio_model');
        $this->load->model('api/departamento_model');
        $this->load->model('api/tipodocumento_model');
		$this->load->model('api/pais_model');
		
		$this->load->helper(array('form', 'url'));

	}
		
	public function index() {
		$this->set_current_user();
		Template::set('page_title', 'Panel Empresas');
		Template::render();
	}

	public function update_registro() {
		$solicitudId =  $this->uri->segment(3);

        if (!$this->auth->is_logged_in() || empty($solicitudId)) {
            Template::redirect('/solicitudes');
        }

        $solicitud = $this->empresa_model->Get_Solicitud($solicitudId);

        if ($solicitud->has_errors || isset($solicitud->data->es_pendiente_revision) || isset($solicitud->data->es_aceptado)) {
        	Template::redirect('/solicitudes');
		}

		$userLogin = $this->auth->user();
        if ($solicitud->data->codigo_usuario_id != $userLogin->user_abt_id)
        	Template::redirect('/solicitudes/NoAccesible');
        		
		$this->session->set_userdata('solicitud_inscripcion', $solicitudId);
		Template::redirect('/empresas/registro');

	}

	public function Registro() {
		//$_SESSION['solicitud_inscripcion'] = 122;

		$this->set_current_user();

        if (!$this->auth->is_logged_in()) {
            Template::redirect('/');
        }

        $solicitudId = $this->session->userdata('solicitud_inscripcion');
        if (isset($solicitudId)) {
        	$solicitud = $this->empresa_model->Get_Solicitud($solicitudId);
        	if (!empty($solicitud->has_errors))
            	Template::redirect('solicitudes');
            else
				Template::set('solicitud_id', md5($solicitudId));

        }
		Template::set('page_title', 'Registro Empresa');
		$listTrimestres = $this->trimestre_model->Get_List();
		$listMunicipios = $this->municipio_model->Get_List_Ordenada_By_Departamentos();
		$listPaises = $this->pais_model->Get_List();
        $listDepartamentos = $this->departamento_model->Get_List();
        $listTiposDocumento = $this->tipodocumento_model->Get_List();
        $listActividades = $this->actividad_model->Get_List();

		Template::set('consumidor_final_id', Actividad_model::CONSUMIDOR_FINAL);
		Template::set('actividades', $listActividades);
		Template::set('paises', $listPaises);
        Template::set('tiposDocumento', $listTiposDocumento);
        Template::set('departamentos', $listDepartamentos);
        
        Template::set('paisDefaultId', Pais_model::PAIS_BOLIVIA_ID);
        Template::set('DocumentoDefaultId', TipoDocumento_model::TIPO_DOC_CI_ID);
		Template::set('municipios', $listMunicipios);
		Template::set('trimestres', $listTrimestres);

		Template::set_view('registro/registro');
		Template::render();
	}

	public function Archivos_Registro() {
		//$_SESSION['solicitud_inscripcion'] = 128;
        if (!$this->auth->is_logged_in()) {
            Template::redirect('/');
        }

        $solicitudId = $this->session->userdata('solicitud_inscripcion');
        if (!isset($solicitudId) || empty($solicitudId)) {
            Template::redirect('/solicitudes');
        }
        $solicitud = $this->empresa_model->Get_Solicitud($solicitudId);
        if ($solicitud->has_errors || empty($solicitud->data->es_pendiente)) {
        	Template::redirect('/solicitudes');
		}
		$this->set_current_user();

        $solicitud = $solicitud->data;
		$this->archivo_model->Read_Config_Documentos($solicitud);
		$this->archivo_model->Read_Upload_Documentos($solicitud);
		$typeFiles = $this->archivo_model->Get_Mime_Type_File_Upload();

		Template::set('consumidor_final_id', Actividad_model::CONSUMIDOR_FINAL);
		Template::set('mimetypes_file', implode(',', $typeFiles));
		Template::set('solicitud', $solicitud);
		Template::set('page_title', 'Archivos - Registro Empresa');
		Template::set_view('registro/archivos');
		Template::render();
	}

	public function Formulario26_Registro() {
		//$_SESSION['solicitud_inscripcion'] = 123;
        if (!$this->auth->is_logged_in()) {
            Template::redirect('/');
        }

        $solicitudId = $this->session->userdata('solicitud_inscripcion');
        if (!isset($solicitudId) || empty($solicitudId)) {
            Template::redirect('/solicitudes');
        }
        $this->set_current_user();

        $solicitud = $this->empresa_model->Get_Solicitud($solicitudId);
        if ($solicitud->has_errors || empty($solicitud->data->es_pendiente)) {
        	Template::redirect('/solicitudes');
		}
        $solicitud = $solicitud->data;
        $productos = $this->productoforestal_model->Get_List();
        $especies = $this->especie_model->Get_List();
        $observaciones = $this->movimientoproducto_model->Get_List_Observaciones();
        $this->movimientoproducto_model->LoadInfoFormulario26($solicitud);

		Template::set('obs_proyectado_id', MovimientoProducto_Model::OBSER_PROYECTADO);
		Template::set('obs_saldostock_id', MovimientoProducto_Model::OBSER_SALDO_STOCK);
		Template::set('observaciones', $observaciones);
		Template::set('especies', $especies);
		Template::set('productos', $productos);
		Template::set('solicitud', $solicitud);
		Template::set('page_title', 'Formularios FO-26 - Registro Empresa');
		Template::set_view('registro/formulario_f26');
		Template::render();
	}

	public function Finalizar_Registro() {

		$datosResolucion = array();

        if (!$this->auth->is_logged_in()) {
            Template::redirect('/');
        }

        $solicitudId = $this->session->userdata('solicitud_inscripcion');
        if (!isset($solicitudId) || empty($solicitudId)) {
            Template::redirect('/solicitudes');
        }
        $this->set_current_user();

        $solicitud = $this->empresa_model->Get_SolicitudComplete($solicitudId);
        if ($solicitud->has_errors || empty($solicitud->data->es_pendiente_revision)) {
        	Template::redirect('/solicitudes');
		}

		$solicitud = $solicitud->data;
		$this->archivo_model->Read_Config_All_Documentos($solicitud);

		Template::set('consumidor_final_id', Actividad_model::CONSUMIDOR_FINAL);
		Template::set('solicitud', $solicitud);
		Template::set('page_title', 'Finalizar - Registro Empresa');
		Template::set('datos_resolucion', $datosResolucion);
		Template::set_view('registro/finalizar_registro');
		Template::render();
	}
}