<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * User Model.
 *
 * The central way to access and perform CRUD on users.
 *
 * @package Bonfire\Modules\Empresas\Models\trimestre_model
 * @author  Luis Vega
 */
class Trimestre_model extends BF_Model {

	const TRIMESTRE_1 = 1; 
	const TRIMESTRE_2 = 2; 
	const TRIMESTRE_3 = 3; 
	const TRIMESTRE_4 = 4; 

	public function Get_List() {

		$listTrimestres = array();
		$trimestre1 = new stdClass();
		$trimestre1->id = self::TRIMESTRE_1; 
		$trimestre1->nombre = '1'; 

		$trimestre2 = new stdClass();
		$trimestre2->id = self::TRIMESTRE_2; 
		$trimestre2->nombre = '2'; 

		$trimestre3 = new stdClass();
		$trimestre3->id = self::TRIMESTRE_3; 
		$trimestre3->nombre = '3';

		$trimestre4 = new stdClass();
		$trimestre4->id = self::TRIMESTRE_4; 
		$trimestre4->nombre = '4'; 

		array_push($listTrimestres,  $trimestre1, $trimestre2, $trimestre3, $trimestre4);

		return $listTrimestres;
	}
	
}