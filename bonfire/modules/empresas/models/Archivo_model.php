<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * User Model.
 *
 * The central way to access and perform CRUD on users.
 *
 * @package Bonfire\Modules\Empresas\Models\archivo_model
 * @author  Luis Vega
 */
class Archivo_model extends BF_Model {

    const ARCHIVO_ID_NIT = 6; 
    const ARCHIVO_ID_FUNDEMPRESA = 7;
    const ARCHIVO_ID_CONSTITUCION_LEGAL = 9; 
    const ARCHIVO_ID_PERSONERIA_JURIDICA = 13; 
    const ARCHIVO_ID_FORMULARIO_CONSUMIDOR_FINAL = 22; 
    const ARCHIVO_ID_SENASAG = 17; 
    const ARCHIVO_ID_RUEX_SENAVEX = 18; 
    const ARCHIVO_ID_DOC_DERECHO_PROP = 19;
    
    //  Documentos Agente Auxiliar
    const CONFIG_ID_CONTRATO_PRESTACION_SERVICIO = 10;
    const ID_CONTRATO_PRESTACION_SERVICIO = 10;

    //  Representante Legal
    const CONFIG_ID_FOTOCOPIA_DOC_REPRESENTANTE = 12;
    const ID_FOTOCOPIA_DOC_REPRESENTANTE = 12;
    const CONFIG_ID_PODER_NOTARIAL_REPRESENTANTE = 14;
    const ID_FOTOCOPIA_PODER_NOTARIAL_REPRESENTANTE = 14;
    const CONFIG_ID_PODER_REPRESENTACION_REPRESENTANTE = 8;
    const ID_PODER_REPRESENTACION_REPRESENTANTE = 8;

    // Propietario
    const ID_FOTOCOPIA_DOCUMENTO_PROP = 11;
    const CONFIG_ID_FOTOCOPIA_DOCUMENTO_PROP= 11;
    const ID_FOTOCOPIA_DOC_ACREDITE_DERECHO_PROP = 19;
    const CONFIG_ID_FOTOCOPIA_DOC_ACREDITE_DERECHO_PROP = 19;

    const CONFIG_ID_FICHERO_FO26 = 3;
    const CONFIG_ID_NIT = 6;
    const CONFIG_ID_FUNDEMPRESA = 7;

    const TIPO_DOC_PRESTADO_ACTIVIDAD = 'A';
    const TIPO_DOC_SUCURSAL = 'S';
    const TIPO_DOC_EMPRESA = 'E';
    const TIPO_DOC_PRESTADO_AGENTE = 'AG';
    const TIPO_DOC_PRESTADO_REPRESENTANTE = 'R';

    const CLASIF_EMPRESA = "E";
    const CLASIF_SUCURSAL = "S";
    const CLASIF_ACTIVIDAD = "A";

    protected $table_name = 'abt_archivo_empresa';
    protected $table_archivo = 'abt_archivo_empresa';
    protected $table_documento_presentado = 'abt_documento_presentado';
	protected $table_config_archivo = 'abt_configuracion_archivo_empresa';
	protected $table_config_actividad_categoria = 'abt_archivo_actividad_categoria';
    protected $table_empresa = 'abt_empresa_forestal';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('empresas/actividad_model');
        $this->load->model('empresas/empresa_model');
        $this->load->model('empresas/categoria_model');
        $this->load->model('empresas/historialempresa_model');
    }

    public function Get_Mime_Type_File_Upload() {
        return [
            'image/png',
            'image/jpeg',
            'image/jpg',
            'application/pdf'
        ];
    }

    public function Get_Type_File_Upload() {
        return [
            'png',
            'jpeg',
            'jpg',
            'pdf'
        ];
    }


    private function GetConfigIdsArchivosPropietario() {
        return [
            self::CONFIG_ID_FOTOCOPIA_DOCUMENTO_PROP,
            self::CONFIG_ID_FOTOCOPIA_DOC_ACREDITE_DERECHO_PROP
        ];
    }

    private function GetConfigIdsArchivosAgentes() {
        return [
            self::CONFIG_ID_CONTRATO_PRESTACION_SERVICIO
        ];
    }

    private function GetConfigIdsArchivosRepresentantes() {
        return [
            self::CONFIG_ID_FOTOCOPIA_DOC_REPRESENTANTE,
            self::CONFIG_ID_PODER_NOTARIAL_REPRESENTANTE,
            self::CONFIG_ID_PODER_REPRESENTACION_REPRESENTANTE
        ];
    }

    public function Saveupdate_File_Empresa($empresaId, $fileNameSave, $filename, $configArchivoId, $userId) {

        $response = ResponseApp::Validate();
        $this->db->trans_begin();

        $datos = [
            'espresentado' => false,
            'nombre_file' => $fileNameSave,
            'config_archivo_id' => $configArchivoId,
            'empresa_id' => $empresaId
        ];
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $this->table_name = $this->table_documento_presentado;

        $documentPresentado = $this->find_by([
                'config_archivo_id' => $configArchivoId, 
                'empresa_id' => $empresaId,
                'entidad_id' => NULL,
                'tipo' => null,
                'empresa_sucursal_id' => NULL
            ]);
        if (!isset($documentPresentado) || empty($documentPresentado)) {
            $saveId = $this->insert_entity($datos);
            $this->table_name = $this->table_empresa;
            $this->historialempresa_model->Save("Se ha agreado archivo {$filename} a la empresa", 'add_file', $empresaId, $userId);
        } else {
            $this->db->update($this->table_name, $datos, ['documento_presentado_id' => $documentPresentado->documento_presentado_id]);
            $this->table_name = $this->table_empresa;
            $this->historialempresa_model->Save("Se ha actualizado archivo {$filename} a la empresa", 'update_file', $empresaId, $userId);
        }
        $this->db->dbprefix = $auxPrefix;

        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            $response->is_valid = true;
        }
    }

    public function Saveupdate_File_Sucursal($datosFile, $userId) {

        $response = ResponseApp::Validate();
        $this->db->trans_begin();

        $sucursal = $this->empresa_model->Get_Sucursal_By($datosFile->empresa_sucursal_id);
        $datos = [
            'espresentado' => false,
            'nombre_file' => $datosFile->filename_save,
            'config_archivo_id' => $datosFile->config_archivo_Id,
            'empresa_sucursal_id' => $datosFile->empresa_sucursal_id
        ];
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $this->table_name = $this->table_documento_presentado;

        $documentPresentado = null;
        if ($datosFile->tipo == self::TIPO_DOC_PRESTADO_AGENTE) {
            $documentPresentado = $this->find_by([
                'config_archivo_id' => $datosFile->config_archivo_Id,
                'tipo' => self::TIPO_DOC_PRESTADO_AGENTE,
                'empresa_sucursal_id' => $datosFile->empresa_sucursal_id
            ]);
        } elseif ($datosFile->tipo == self::TIPO_DOC_PRESTADO_ACTIVIDAD) {
            $documentPresentado = $this->find_by([
                'config_archivo_id' => $datosFile->config_archivo_Id,
                'entidad_id' => $datosFile->entidad_id,
                'tipo' => self::TIPO_DOC_PRESTADO_ACTIVIDAD,
                'empresa_sucursal_id' => $datosFile->empresa_sucursal_id
            ]);
        } elseif ($datosFile->tipo == self::TIPO_DOC_PRESTADO_REPRESENTANTE) {
            $documentPresentado = $this->find_by([
                'config_archivo_id' => $datosFile->config_archivo_Id,
                'entidad_id' => $datosFile->entidad_id,
                'tipo' => self::TIPO_DOC_PRESTADO_REPRESENTANTE,
                'empresa_sucursal_id' => $datosFile->empresa_sucursal_id
            ]);
        } else {
            if ($datosFile->tipo == self::TIPO_DOC_SUCURSAL) { 
                $documentPresentado = $this->find_by([
                    'config_archivo_id' => $datosFile->config_archivo_Id,
                    'entidad_id' => NULL,
                    'tipo' => NULL,
                    'empresa_sucursal_id' => $datosFile->empresa_sucursal_id
                ]);
            }
        }

        if (!isset($documentPresentado) || empty($documentPresentado)) {
            if ($datosFile->tipo == self::TIPO_DOC_PRESTADO_AGENTE) {
                $datos['tipo'] = self::TIPO_DOC_PRESTADO_AGENTE;
            } elseif ($datosFile->tipo == self::TIPO_DOC_PRESTADO_ACTIVIDAD) {
                $datos['tipo'] = self::TIPO_DOC_PRESTADO_ACTIVIDAD;
                $datos['entidad_id'] = $datosFile->entidad_id;

            } elseif ($datosFile->tipo == self::TIPO_DOC_PRESTADO_REPRESENTANTE) {
                $datos['tipo'] = self::TIPO_DOC_PRESTADO_REPRESENTANTE;
                $datos['entidad_id'] = $datosFile->entidad_id;
            }
            $saveId = $this->insert_entity($datos);
            $this->table_name = $this->table_empresa;

            $this->historialempresa_model->Save("Se ha agreado archivo {$datosFile->filename} a la empresa", 'add_file', $sucursal->empresa_id, $userId);
        } else {
            $this->db->update($this->table_name, $datos, ['documento_presentado_id' => $documentPresentado->documento_presentado_id]);
            $this->table_name = $this->table_empresa;
            $this->historialempresa_model->Save("Se ha actualizado archivo {$datosFile->filename} a la empresa", 'update_file', $sucursal->empresa_id, $userId);
        }
        $this->db->dbprefix = $auxPrefix;

        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            $response->is_valid = true;
        }
    }

    public function Remove_File_Sucursal($datosFile, $userId) {

        $response = ResponseApp::Validate();
        $this->db->trans_begin();

        $sucursal = $this->empresa_model->Get_Sucursal_By($datosFile->empresa_sucursal_id);

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $this->table_name = $this->table_documento_presentado;

        $documentPresentado = null;
        if ($datosFile->tipo == self::TIPO_DOC_PRESTADO_AGENTE) {
            $documentPresentado = $this->find_by([
                'config_archivo_id' => $datosFile->config_archivo_Id,
                'tipo' => self::TIPO_DOC_PRESTADO_AGENTE,
                'empresa_sucursal_id' => $datosFile->empresa_sucursal_id
            ]);
        } elseif ($datosFile->tipo == self::TIPO_DOC_PRESTADO_ACTIVIDAD) {
            $documentPresentado = $this->find_by([
                'config_archivo_id' => $datosFile->config_archivo_Id,
                'entidad_id' => $datosFile->entidad_id,
                'tipo' => self::TIPO_DOC_PRESTADO_ACTIVIDAD,
                'empresa_sucursal_id' => $datosFile->empresa_sucursal_id
            ]);
        } elseif ($datosFile->tipo == self::TIPO_DOC_PRESTADO_REPRESENTANTE) {
            $documentPresentado = $this->find_by([
                'config_archivo_id' => $datosFile->config_archivo_Id,
                'entidad_id' => $datosFile->entidad_id,
                'tipo' => self::TIPO_DOC_PRESTADO_REPRESENTANTE,
                'empresa_sucursal_id' => $datosFile->empresa_sucursal_id
            ]);
        } else {

            if ($datosFile->tipo == self::TIPO_DOC_SUCURSAL) {
                $documentPresentado = $this->find_by([
                    'config_archivo_id' => $datosFile->config_archivo_Id,
                    'entidad_id' => NULL,
                    'tipo' => NULL,
                    'empresa_id' => NULL,
                    'empresa_sucursal_id' => $datosFile->empresa_sucursal_id
                ]);
            } else {
                $documentPresentado = $this->find_by([
                    'config_archivo_id' => $datosFile->config_archivo_Id,
                    'entidad_id' => NULL,
                    'tipo' => NULL,
                    'empresa_id' => $sucursal->empresa_id,
                    'empresa_sucursal_id' => NULL
                ]);
            }
        }

        if (!isset($documentPresentado) || empty($documentPresentado)) {
            array_push($response->errors , "No se ha encontrado archivo relacionado para eliminar");
            $response->has_errors = true;
        } else {
            $this->db->delete($this->table_name, ['documento_presentado_id' => $documentPresentado->documento_presentado_id]);
            $this->historialempresa_model->Save("Se ha actualizado archivo {$datosFile->filename} a la empresa", 'remove_file', $sucursal->empresa_id, $userId);
            // Eliminacion
            unlink(config_item('path-upload-empresa') . $documentPresentado->nombre_file);
        }
        $this->db->dbprefix = $auxPrefix;

        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            $response->is_valid = true;
        }
        return $response;
    }

    public function Remove_File_Empresa($datosFile, $userId) {

        $response = ResponseApp::Validate();
        $this->db->trans_begin();
        $sucursal = $this->empresa_model->Get_Sucursal_By($datosFile->empresa_sucursal_id);

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $this->table_name = $this->table_documento_presentado;

        $documentPresentado = $this->find_by([
                    'config_archivo_id' => $datosFile->config_archivo_Id,
                    'entidad_id' => NULL,
                    'tipo' => NULL,
                    'empresa_id' => $sucursal['empresa_id'],
                    'empresa_sucursal_id' => NULL
        ]);

        if (!isset($documentPresentado) || empty($documentPresentado)) {
            array_push($response->errors , "No se ha encontrado archivo relacionado para eliminar");
            $response->has_errors = true;
        } else {
            $this->db->delete($this->table_name, ['documento_presentado_id' => $documentPresentado->documento_presentado_id]);
            $this->historialempresa_model->Save("Se ha actualizado archivo {$datosFile->filename} a la empresa", 'remove_file', $sucursal['empresa_id'], $userId);
            // Eliminacion
            unlink(config_item('path-upload-empresa') . $documentPresentado->nombre_file);
        }
        $this->db->dbprefix = $auxPrefix;

        
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            $response->is_valid = true;
        }
        return $response;
    }


	public function Get_Documentos_Empresa($tipo) {

        $documentosValidosIds = [
            self::ARCHIVO_ID_NIT, 
            self::ARCHIVO_ID_FUNDEMPRESA,
            self::ARCHIVO_ID_CONSTITUCION_LEGAL
        ];
    	$auxPrefix = $this->db->dbprefix;
    	$this->db->dbprefix = '';

        $this->select([
            "{$this->table_config_archivo}.configuracion_archivo_empresa_id",
            "{$this->table_name}.nombre",
            "{$this->table_name}.descripcion"
        ]);
        $this->join(
            $this->table_config_archivo,
            "{{$this->table_config_archivo}.archivo_id=archivo_empresa_id"
        );
        $this->where_in('archivo_empresa_id', $documentosValidosIds);
        $this->where('habilitado', true);
        $this->where('tipo_tramite', $tipo);
        $list = parent::find_all();

        $this->db->dbprefix = $auxPrefix;
        return $list;
	}

    public function Save_Update_Documentos_Fisico($empresaObject) {
        $this->Read_Config_Documentos_Fisicos($empresaObject);

        $this->db->trans_begin();
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        // Documentos de Empresa
        foreach ($empresaObject->documentos_fisico  as $documentoEmp) {
            $this->SaveDocumentoEmpresaFisico($documentoEmp->configuracion_archivo_empresa_id, $empresaObject->empresa_forestal_id);
        }

        foreach ($empresaObject->sucursales as $sucursal) {
            foreach ($sucursal->documentos_fisico as $documentoSuc) {
                $this->SaveDocumentoSucursalFisico($documentoSuc->configuracion_archivo_empresa_id, $sucursal->empresa_sucursal_id);
            }

            if (!empty($sucursal->agente)) {
                foreach ($sucursal->agente_documentos_fisico as $documentoAgente) {
                    $this->SaveDocumentoAgenteFisico($documentoAgente->configuracion_archivo_empresa_id, $sucursal->empresa_sucursal_id);
                }
            }

            if (!empty($sucursal->representantes) && sizeof($sucursal->representantes) > 0 && sizeof($sucursal->representantes_documentos_fisico) > 0) {
                foreach ($sucursal->representantes as $representante) {
                    foreach ($sucursal->representantes_documentos_fisico as $documentoRep) {
                        $this->SaveDocumentoRepresentanteFisico($documentoRep->configuracion_archivo_empresa_id, $sucursal->empresa_sucursal_id, $representante->codpersona);
                    }
                }
            }

            foreach ($sucursal->actividades as $actividad) {
               if (!empty($actividad->documentos_fisico) && sizeof($actividad->documentos_fisico) > 0) {
                    foreach ($actividad->documentos_fisico as $docActividad) {
                        $this->SaveDocumentoActividadFisico($docActividad->configuracion_archivo_empresa_id, $sucursal->empresa_sucursal_id, $actividad->sucursal_actividad_id);
                    }
               }
               if (!empty($actividad->documentos_fisico_f26) && sizeof($actividad->documentos_fisico_f26) > 0) {
                    foreach ($actividad->documentos_fisico_f26 as $docf26) {
                        $this->SaveDocumentoActividadFisico($docf26->configuracion_archivo_empresa_id, $sucursal->empresa_sucursal_id, $actividad->sucursal_actividad_id);
                    }
               }
            }
        }
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            $this->db->dbprefix = $auxPrefix;
        }
    }

    private function SaveDocumentoEmpresaFisico($configArchivoId, $empresaId) {

        $datos = [
            'espresentado' => false,
            'nombre_file' => "",
            'config_archivo_id' => $configArchivoId,
            'empresa_id' => $empresaId,
            'es_doc_fisico' => true
        ];

        $this->table_name = $this->table_documento_presentado;

        $documentPresentado = $this->find_by([
                'config_archivo_id' => $configArchivoId, 
                'empresa_id' => $empresaId,
                'entidad_id' => NULL,
                'tipo' => null,
                'empresa_sucursal_id' => NULL,
                'es_doc_fisico' => true
        ]);
        if (!isset($documentPresentado) || empty($documentPresentado)) {
            $this->insert_entity($datos);
        } else {
            $this->db->update($this->table_name, $datos, ['documento_presentado_id' => $documentPresentado->documento_presentado_id]);
        }
    }

    private function SaveDocumentoSucursalFisico($configArchivoId, $sucursalId) {

        $datos = [
            'espresentado' => false,
            'nombre_file' => "",
            'config_archivo_id' => $configArchivoId,
            'empresa_sucursal_id' => $sucursalId,
            'es_doc_fisico' => true
        ];

        $this->table_name = $this->table_documento_presentado;

        $documentPresentado = $this->find_by([
                'config_archivo_id' => $configArchivoId, 
                'empresa_sucursal_id' => $sucursalId,
                'entidad_id' => NULL,
                'tipo' => null,
                'es_doc_fisico' => true
        ]);
        if (!isset($documentPresentado) || empty($documentPresentado)) {
            $this->insert_entity($datos);
        } else {
            $this->db->update($this->table_name, $datos, ['documento_presentado_id' => $documentPresentado->documento_presentado_id]);
        }
    }

    private function SaveDocumentoAgenteFisico($configArchivoId, $sucursalId) {

        $datos = [
            'espresentado' => false,
            'nombre_file' => "",
            'config_archivo_id' => $configArchivoId,
            'empresa_sucursal_id' => $sucursalId,
            'es_doc_fisico' => true,
            'tipo' => 'AG'
        ];

        $this->table_name = $this->table_documento_presentado;

        $documentPresentado = $this->find_by([
                'config_archivo_id' => $configArchivoId, 
                'empresa_sucursal_id' => $sucursalId,
                'entidad_id' => NULL,
                'tipo' => 'AG',
                'es_doc_fisico' => true
        ]);
        if (!isset($documentPresentado) || empty($documentPresentado)) {
            $this->insert_entity($datos);
        } else {
            $this->db->update($this->table_name, $datos, ['documento_presentado_id' => $documentPresentado->documento_presentado_id]);
        }
    }

    private function SaveDocumentoRepresentanteFisico($configArchivoId, $sucursalId, $representanteId) {

        $datos = [
            'espresentado' => false,
            'nombre_file' => "",
            'config_archivo_id' => $configArchivoId,
            'empresa_sucursal_id' => $sucursalId,
            'es_doc_fisico' => true,
            'tipo' => 'R',
            'entidad_id' => $representanteId
        ];
        $this->table_name = $this->table_documento_presentado;

        $documentPresentado = $this->find_by([
                'config_archivo_id' => $configArchivoId, 
                'empresa_sucursal_id' => $sucursalId,
                'entidad_id' => NULL,
                'tipo' => 'R',
                'es_doc_fisico' => true,
                'entidad_id' => $representanteId
        ]);
        if (!isset($documentPresentado) || empty($documentPresentado)) {
            $this->insert_entity($datos);
        } else {
            $this->db->update($this->table_name, $datos, ['documento_presentado_id' => $documentPresentado->documento_presentado_id]);
        }
    }

    private function SaveDocumentoActividadFisico($configArchivoId, $sucursalId, $actividadId) {

        $datos = [
            'espresentado' => false,
            'nombre_file' => "",
            'config_archivo_id' => $configArchivoId,
            'empresa_sucursal_id' => $sucursalId,
            'es_doc_fisico' => true,
            'tipo' => 'A',
            'entidad_id' => $actividadId
        ];
        $this->table_name = $this->table_documento_presentado;

        $documentPresentado = $this->find_by([
                'config_archivo_id' => $configArchivoId, 
                'empresa_sucursal_id' => $sucursalId,
                'entidad_id' => NULL,
                'tipo' => 'A',
                'es_doc_fisico' => true,
                'entidad_id' => $actividadId
        ]);
        if (!isset($documentPresentado) || empty($documentPresentado)) {
            $this->insert_entity($datos);
        } else {
            $this->db->update($this->table_name, $datos, ['documento_presentado_id' => $documentPresentado->documento_presentado_id]);
        }
    }

    public function Read_Config_All_Documentos(&$empresaObject) {
        $this->Read_Config_Documentos($empresaObject);
        $this->Read_Config_Documentos_Fisicos($empresaObject);
    }

    public function Read_Config_Documentos(&$empresaObject) {

        $documentosValidosIds = [];
        $representantesIds = $this->GetConfigIdsArchivosRepresentantes();
        $agenteIds = $this->GetConfigIdsArchivosAgentes();
        $propIds = $this->GetConfigIdsArchivosPropietario();
        foreach ($empresaObject->sucursales as $sucursal) {
            $sucursal->documentos = [];
            $sucursal->agente_documentos = [];
            $sucursal->representantes_documentos = [];
            $sucursal->con_documentos_actividades = false;
            foreach ($sucursal->actividades as $actividad) {
                $filters = [
                    'actividad_id' => $actividad->actividad_id,
                    'categoria_id' => $actividad->categoria_id,
                    'tipo' => $empresaObject->tipo_solicitud,
                    'es_digital' => true
                ];

                $configDocuments = $this->Get_Config_Archivo($filters);
                $actividad->documentos = [];
                $actividad->documento_f26 = [];
                foreach ($configDocuments as $configDoc) {
                    switch ($configDoc->clasificacion) {
                        case self::CLASIF_EMPRESA:
                            if ($configDoc->configuracion_archivo_empresa_id == self::CONFIG_ID_NIT) {
                                if (!empty($empresaObject->nit))
                                    array_push($documentosValidosIds, $configDoc->configuracion_archivo_empresa_id);
                            } else {
                                if ($configDoc->configuracion_archivo_empresa_id == self::CONFIG_ID_FUNDEMPRESA) {
                                    if (!empty($empresaObject->numero_fundempresa))
                                        array_push($documentosValidosIds, $configDoc->configuracion_archivo_empresa_id);
                                } else 
                                    array_push($documentosValidosIds, $configDoc->configuracion_archivo_empresa_id);
                            }
                            break;
                        case self::CLASIF_SUCURSAL:
                            if (in_array($configDoc->configuracion_archivo_empresa_id, $representantesIds) 
                                && !empty($sucursal->representantes) && sizeof($sucursal->representantes) > 0) {
                                array_push($sucursal->representantes_documentos, 
                                    $configDoc->configuracion_archivo_empresa_id);
                            } else {
                                if (in_array($configDoc->configuracion_archivo_empresa_id, $agenteIds) &&
                                !empty($sucursal->agente_registro)) {
                                    array_push($sucursal->agente_documentos, $configDoc->configuracion_archivo_empresa_id);
                                } else {
                                    array_push($sucursal->documentos, $configDoc->configuracion_archivo_empresa_id);
                                }
                            }
                        break;
                        case self::CLASIF_ACTIVIDAD:
                            if ($configDoc->configuracion_archivo_empresa_id != self::CONFIG_ID_FICHERO_FO26)
                                array_push($actividad->documentos, $configDoc->configuracion_archivo_empresa_id);
                            else  {
                                if ($configDoc->configuracion_archivo_empresa_id == self::CONFIG_ID_FICHERO_FO26)
                                    array_push($actividad->documento_f26, $configDoc->configuracion_archivo_empresa_id);
                            }
                        break;
                    }
                }
                if (sizeof($actividad->documentos) > 0) {
                    array_unique($actividad->documentos);
                    $configs = $this->GetConfigDocumentsBy($actividad->documentos, $empresaObject->tipo_solicitud);
                    if (sizeof($configs) > 0)
                        $actividad->documentos = $configs;
                    $sucursal->con_documentos_actividades = true;
                }

                if (sizeof($actividad->documento_f26) > 0) {
                    array_unique($actividad->documento_f26);
                    $configs = $this->GetConfigDocumentsBy($actividad->documento_f26, $empresaObject->tipo_solicitud);
                    if (sizeof($configs) > 0)
                        $actividad->documento_f26 = $configs;
                }
            }

            // Agentes
            if (sizeof($sucursal->agente_documentos) > 0) {
                array_unique($sucursal->agente_documentos);
                $configs = $this->GetConfigDocumentsBy($sucursal->agente_documentos, $empresaObject->tipo_solicitud);
                if (sizeof($configs) > 0)
                    $sucursal->agente_documentos = $configs;
            }

            // representantes
            if (sizeof($sucursal->representantes_documentos) > 0) {
                array_unique($sucursal->representantes_documentos);
                $configs = $this->GetConfigDocumentsBy($sucursal->representantes_documentos, $empresaObject->tipo_solicitud);
                if (sizeof($configs) > 0)
                    $sucursal->representantes_documentos = $configs;
            }
            // Sucursal
            if (sizeof($sucursal->documentos) > 0) {
                array_unique($sucursal->documentos);
                $configs = $this->GetConfigDocumentsBy($sucursal->documentos, $empresaObject->tipo_solicitud);
                if (sizeof($configs) > 0)
                    $sucursal->documentos = $configs;
            }
        }
        // Documentos de Empresa
        if (sizeof($documentosValidosIds) > 0) {
            array_unique($documentosValidosIds);
            $configs = $this->GetConfigDocumentsBy($documentosValidosIds, $empresaObject->tipo_solicitud);
            if (sizeof($configs) > 0)
                $empresaObject->documentos = $configs;
        }
    }

    public function Read_Config_Documentos_Fisicos(&$empresaObject) {

        $documentosValidosIds = [];
        $representantesIds = $this->GetConfigIdsArchivosRepresentantes();
        $agenteIds = $this->GetConfigIdsArchivosAgentes();
        $propIds = $this->GetConfigIdsArchivosPropietario();
        foreach ($empresaObject->sucursales as $sucursal) {
            $sucursal->documentos_fisico = [];
            $sucursal->agente_documentos_fisico = [];
            $sucursal->representantes_documentos_fisico = [];
            $sucursal->con_documentos_actividades = false;
            foreach ($sucursal->actividades as $actividad) {
                $filters = [
                    'actividad_id' => $actividad->actividad_id,
                    'categoria_id' => $actividad->categoria_id,
                    'tipo' => $empresaObject->tipo_solicitud,
                    'es_fisico' => true
                ];
                $configDocuments = $this->Get_Config_Archivo($filters);
                $actividad->documentos_fisico = [];
                $actividad->documentos_fisico_f26 = [];
                foreach ($configDocuments as $configDoc) {
                    switch ($configDoc->clasificacion) {
                        case self::CLASIF_EMPRESA:
                            array_push($documentosValidosIds, $configDoc->configuracion_archivo_empresa_id);
                            break;
                        case self::CLASIF_SUCURSAL:
                            if (in_array($configDoc->configuracion_archivo_empresa_id, $representantesIds) 
                                && !empty($sucursal->representantes) && sizeof($sucursal->representantes) > 0) {
                                array_push($sucursal->representantes_documentos_fisico, 
                                    $configDoc->configuracion_archivo_empresa_id);
                            } else {
                                if (in_array($configDoc->configuracion_archivo_empresa_id, $agenteIds) &&
                                !empty($sucursal->agente_registro)) {
                                    array_push($sucursal->agente_documentos_fisico, $configDoc->configuracion_archivo_empresa_id);
                                } else {
                                    array_push($sucursal->documentos_fisico, $configDoc->configuracion_archivo_empresa_id);
                                }
                            }
                        break;
                        case self::CLASIF_ACTIVIDAD:
                            if ($configDoc->configuracion_archivo_empresa_id != self::CONFIG_ID_FICHERO_FO26)
                                array_push($actividad->documentos_fisico, $configDoc->configuracion_archivo_empresa_id);
                            else   {
                                if ($configDoc->configuracion_archivo_empresa_id == self::CONFIG_ID_FICHERO_FO26)
                                    array_push($actividad->documentos_fisico_f26, $configDoc->configuracion_archivo_empresa_id);
                            }
                        break;
                    }

                }

                if (sizeof($actividad->documentos_fisico) > 0) {
                    array_unique($actividad->documentos_fisico);
                    $configs = $this->GetConfigDocumentsBy($actividad->documentos_fisico, $empresaObject->tipo_solicitud);
                    if (sizeof($configs) > 0)
                        $actividad->documentos_fisico = $configs;
                    $sucursal->con_documentos_actividades = true;
                }
                if (sizeof($actividad->documentos_fisico_f26) > 0) {
                    array_unique($actividad->documentos_fisico_f26);
                    $configs = $this->GetConfigDocumentsBy($actividad->documentos_fisico_f26, $empresaObject->tipo_solicitud);
                    if (sizeof($configs) > 0)
                        $actividad->documentos_fisico_f26 = $configs;
                }
            }

            // Agentes
            if (sizeof($sucursal->agente_documentos_fisico) > 0) {
                array_unique($sucursal->agente_documentos_fisico);
                $configs = $this->GetConfigDocumentsBy($sucursal->agente_documentos_fisico, $empresaObject->tipo_solicitud);
                if (sizeof($configs) > 0)
                    $sucursal->agente_documentos_fisico = $configs;
            }

            // representantes
            if (sizeof($sucursal->representantes_documentos_fisico) > 0) {
                array_unique($sucursal->representantes_documentos_fisico);
                $configs = $this->GetConfigDocumentsBy($sucursal->representantes_documentos_fisico, $empresaObject->tipo_solicitud);
                if (sizeof($configs) > 0)
                    $sucursal->representantes_documentos_fisico = $configs;
            }
            // Sucursal
            if (sizeof($sucursal->documentos_fisico) > 0) {
                array_unique($sucursal->documentos_fisico);
                $configs = $this->GetConfigDocumentsBy($sucursal->documentos_fisico, $empresaObject->tipo_solicitud);
                if (sizeof($configs) > 0)
                    $sucursal->documentos_fisico = $configs;
            }
        }
        // Documentos de Empresa
        if (sizeof($documentosValidosIds) > 0) {
            array_unique($documentosValidosIds);
            $configs = $this->GetConfigDocumentsBy($documentosValidosIds, $empresaObject->tipo_solicitud);
            if (sizeof($configs) > 0)
                $empresaObject->documentos_fisico = $configs;
        }
    }

    public function Read_Upload_Documentos(&$empresaObject) {

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $auxTableName = $this->table_name;

        $this->table_name = $this->table_documento_presentado;
        $documentosValidosIds = [];
        $representantesIds = $this->GetConfigIdsArchivosRepresentantes();
        $agenteIds = $this->GetConfigIdsArchivosAgentes();
        $propIds = $this->GetConfigIdsArchivosPropietario();
        foreach ($empresaObject->sucursales as $sucursal) {

            foreach ($sucursal->actividades as $actividad) {
                $actividad->documentos = [];

            }

            // Agentes
            if (sizeof($sucursal->agente_documentos) > 0) {
                foreach ($sucursal->agente_documentos as $documentConfig) {
                    $documentPresentado = $this->find_by([
                        'config_archivo_id' => $documentConfig->configuracion_archivo_empresa_id,
                        'tipo' => self::TIPO_DOC_PRESTADO_AGENTE,
                        'empresa_sucursal_id' => $sucursal->empresa_sucursal_id
                    ]);

                    if (isset($documentPresentado) && !empty($documentPresentado))
                        $documentConfig->doc_presentado = $documentPresentado;
                }
            }

            // representantes
            if (sizeof($sucursal->representantes_documentos) > 0) {
                foreach ($sucursal->representantes as $representante) {
                    $representante->doc_presentados = [];
                    foreach ($sucursal->representantes_documentos as $documentConfig) {
                        $documentPresentado = $this->find_by([
                            'config_archivo_id' => $documentConfig->configuracion_archivo_empresa_id,
                            'entidad_id' => $representante->codpersona,
                            'tipo' => self::TIPO_DOC_PRESTADO_REPRESENTANTE,
                            'empresa_sucursal_id' => $sucursal->empresa_sucursal_id
                        ]);
                        if (isset($documentPresentado) && !empty($documentPresentado)) {
                            $representante->doc_presentados[$documentConfig->configuracion_archivo_empresa_id] = $documentPresentado;
                        }
                    }
                }
            }
            // Sucursal
            if (sizeof($sucursal->documentos) > 0) {
                foreach ($sucursal->documentos as $documentConfig) {
                    $documentPresentado = $this->find_by([
                        'config_archivo_id' => $documentConfig->configuracion_archivo_empresa_id,
                        'entidad_id' => NULL,
                        'tipo' => NULL,
                        'empresa_id' => NULL,
                        'empresa_sucursal_id' => $sucursal->empresa_sucursal_id
                    ]);

                    if (isset($documentPresentado) && !empty($documentPresentado))
                        $documentConfig->doc_presentado = $documentPresentado;
                }
            }
        }
        // Documentos de Empresa
        if (sizeof($empresaObject->documentos) > 0) {
            foreach ($empresaObject->documentos as $documentConfig) {
                $documentPresentado = $this->find_by([
                    'config_archivo_id' => $documentConfig->configuracion_archivo_empresa_id,
                    'entidad_id' => NULL,
                    'tipo' => NULL,
                    'empresa_sucursal_id' => NULL,
                    'empresa_id' => $empresaObject->empresa_forestal_id
                ]);

                if (isset($documentPresentado) && !empty($documentPresentado))
                    $documentConfig->doc_presentado = $documentPresentado;
            }
        }
        $this->table_name = $auxTableName;
        $this->db->dbprefix = $auxPrefix;
    }

    private function Get_Config_Archivo($filters) {
        $filters = (object) $filters;
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $this->select([
            "{$this->table_config_archivo}.configuracion_archivo_empresa_id",
            "{$this->table_name}.nombre",
            "{$this->table_name}.descripcion",
            "{$this->table_config_archivo}.es_fisico",
            "{$this->table_config_archivo}.es_digital",
            "{$this->table_config_archivo}.por_actualizacion",
            "{$this->table_config_actividad_categoria}.clasificacion"
        ]);
        $this->join(
            $this->table_config_archivo,
            "{{$this->table_config_archivo}.archivo_id=archivo_empresa_id"
        );
        $this->join(
            $this->table_config_actividad_categoria,
            "{{$this->table_config_actividad_categoria}.config_archivo_id=configuracion_archivo_empresa_id"
        );
        if (!empty($filters->actividad_id))
            $this->where("{$this->table_config_actividad_categoria}.actividad_id", $filters->actividad_id);
        if (!empty($filters->categoria_id))
            $this->where("{$this->table_config_actividad_categoria}.categoria_id", $filters->categoria_id);

        if (isset($filters->es_digital))
            $this->where("{$this->table_config_archivo}.es_digital", $filters->es_digital);

        if (isset($filters->es_fisico))
            $this->where("{$this->table_config_archivo}.es_fisico", $filters->es_fisico);

        if (!empty($filters->tipo))
            $this->where('tipo_tramite', $filters->tipo);

        $this->where('habilitado', true);
        $list = parent::find_all();
        $this->db->dbprefix = $auxPrefix;

        return $list;
    }

    public function Get_Documentos_Empresa_By($empresaObject, $tipo) {

        $documentosValidosIds = [
            self::ARCHIVO_ID_NIT, 
            self::ARCHIVO_ID_FUNDEMPRESA,
        ];
        foreach ($empresaObject->sucursales as $key => $sucursal) {
            foreach ($sucursal->actividades as $actividad) {
                if ($this->isPersoneriaJuridica($actividad->actividad_id, $actividad->categoria_id))
                    array_push($documentosValidosIds, self::ARCHIVO_ID_PERSONERIA_JURIDICA);
                if ($this->isConstitucionLegal($actividad->actividad_id, $actividad->categoria_id))
                    array_push($documentosValidosIds, self::ARCHIVO_ID_CONSTITUCION_LEGAL);
                if ($this->isDocumentoAcreditacionDerechoPropietario($actividad->actividad_id, $actividad->categoria_id))
                    array_push($documentosValidosIds, self::ARCHIVO_ID_DOC_DERECHO_PROP);
                if ($this->isFormularioConsumidorFinal($actividad->actividad_id, $actividad->categoria_id)) {
                    array_push($documentosValidosIds, self::ARCHIVO_ID_FORMULARIO_CONSUMIDOR_FINAL);
                    $empresaObject->es_consumidor_final = true;
                }
                if ($this->isSenasag($actividad->actividad_id, $actividad->categoria_id))
                    array_push($documentosValidosIds, self::ARCHIVO_ID_SENASAG);
                if ($this->isRuexSenavex($actividad->actividad_id, $actividad->categoria_id))
                    array_push($documentosValidosIds, self::ARCHIVO_ID_RUEX_SENAVEX);
            }
        }
        $documentosValidosIds = array_unique($documentosValidosIds);
        print_r($documentosValidosIds);exit;
        return $this->GetConfigDocumentsBy($documentosValidosIds, $tipo);
    }

    private function GetConfigDocumentsBy($archivoIds, $tipo) {
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $this->select([
            "{$this->table_config_archivo}.configuracion_archivo_empresa_id",
            "{$this->table_name}.nombre",
            "{$this->table_name}.descripcion",
            "{$this->table_config_archivo}.es_digital",
            "{$this->table_config_archivo}.es_fisico",
            "{$this->table_config_archivo}.por_actualizacion"

        ]);
        $this->join(
            $this->table_config_archivo,
            "{{$this->table_config_archivo}.archivo_id=archivo_empresa_id"
        );
        $this->where_in('configuracion_archivo_empresa_id', $archivoIds);
        $this->where('habilitado', true);
        $this->where('tipo_tramite', $tipo);
        $list = parent::find_all();
        
        $this->db->dbprefix = $auxPrefix;

        return $list;
    }

    private function isPersoneriaJuridica($actividadId, $categoriaId) {
        if ($actividadId == Actividad_model::BARRACAS && $categoriaId == Categoria_model::CATEGORIA_E )
            return true;
        return false;
    }

    private function isConstitucionLegal($actividadId, $categoriaId) {
        if ($actividadId == Actividad_model::ASERRADERO || 
            $actividadId == Actividad_model::LAMINADORA || 
            $actividadId == Actividad_model::BENEFICIADORA_CASTANIA || 
            $actividadId == Actividad_model::PALMITERA || 
            $actividadId == Actividad_model::PLANTA_TABLEROS || 
            $actividadId == Actividad_model::CENTRO_ALMACENAMIENTO || 
            $actividadId == Actividad_model::BARRACAS && $categoriaId != Categoria_model::CATEGORIA_E || 
            $actividadId == Actividad_model::CARPINTERIAS && $categoriaId != Categoria_model::CATEGORIA_E || 
            $actividadId == Actividad_model::COMER_PROD_PROV_PLANTACIONES || 
            $actividadId == Actividad_model::COMER_PROD_NO_MADERABLES || 
            $actividadId == Actividad_model::DESMONTADORA || 
            $actividadId == Actividad_model::TRATAMIENTO_Y_O_PRESERVACION || 
            $actividadId == Actividad_model::APROVECHAMIENTO_INTEGRAL_BOSQUE || 
            $actividadId == Actividad_model::EXPORTADORA || 
            $actividadId == Actividad_model::IMPORTADORA || 
            $actividadId == Actividad_model::CARBONERAS)
            return true;
        return false;
    }

    private function isDocumentoAcreditacionDerechoPropietario($actividadId, $categoriaId) {
        if ($actividadId == Actividad_model::DESMONTADORA ||
            $actividadId == Actividad_model::TRATAMIENTO_Y_O_PRESERVACION ||
            $actividadId == Actividad_model::APROVECHAMIENTO_INTEGRAL_BOSQUE)
            return true;
        return false;
    }

    private function isFormularioConsumidorFinal($actividadId) {
        if ($actividadId == Actividad_model::CONSUMIDOR_FINAL)
            return true;
        return false;
    }

    private function isSenasag($actividadId) {
        if ($actividadId == Actividad_model::EXPORTADORA || 
            $actividadId == Actividad_model::IMPORTADORA)
            return true;
        return false;
    }

    private function isRuexSenavex($actividadId) {
        if ($actividadId == Actividad_model::EXPORTADORA || 
            $actividadId == Actividad_model::IMPORTADORA )
            return true;
        return false;
    }

    /**
     * Set the select and join portions of the SQL query for the find* methods.
     *
     * @todo Set this in the before_find observer?
     *
     * @return void
     */
    protected function preFind()
    {
        if (empty($this->selects)) {
            $this->select(["{$this->table_name}.*", 'nombre']);
        }
    }
    
    public function moderar($type, $itemId, $documentId, $estado,$observacion)
    {
    	//$userLogin = $this->auth->user();
    	
    	$auxPrefix = $this->db->dbprefix;
    	$auxTableName = $this->table_name;
    	$this->db->dbprefix = '';
    	$this->table_name = 'abt_documento_moderacion';

    	$data = array();
    	$data['estado'] = $estado;
    	$data['observacion'] = "$observacion";
    	$data['fecha_moderacion'] = date('Y-m-d H:i:s');
    	$data['usuario_codigo'] = 1;//$userLogin->user_abt_id;
    	
    	$this->where('type',$type);
    	$this->where('item_id',$itemId);
    	$this->where('documento_id',$documentId);
    	
    	$result = false;
    	
    	if(!$this->find_all())
    	{
    		$data['type'] = $type;
    		$data['item_id'] = $itemId;
    		$data['documento_id'] = $documentId;
    		
    		$result = $this->insert_entity($data);
    	}
    	else
    	{
    		$where = array();
    		$where['type'] = $type;
    		$where['item_id'] = $itemId;
    		$where['documento_id'] = $documentId;
    		
    		$result = $this->db->update($this->table_name, $data, $where);
    	}
    	
    	if($result && $estado == 'rechazado')
    	{
    		$where = array();
    		$where['solicitud_tramite_empresa_id'] = $itemId;
    		
    		$data = array();
    		$data['estado'] = 'R';
    		
    		$result = $this->db->update('abt_solicitud_tramite_empresa', $data, $where);
    	}

    	$this->db->dbprefix = $auxPrefix;
    	$this->table_name = $auxTableName;
    }
    
    public function getModeracion($type, $itemId, $documentId = null)
    {
    	$auxPrefix = $this->db->dbprefix;
    	$auxTableName = $this->table_name;
    	$this->db->dbprefix = '';
    	$this->table_name = 'abt_documento_moderacion';
    	 
    	$this->where('type',$type);
    	$this->where('item_id', $itemId);
    	
    	if(!is_null($documentId))
    		$this->where('documento_id',$documentId);
    	
    	$results = $this->find_all();
    	
    	$resultFormated = [];
        if ($results) {
            foreach ($results as $key => $moderation) 
            {
                //no se repetira la clave por la condicion en el where (type, item_id)
                $resultFormated[$moderation->documento_id] = $moderation;
            }
        }

    	$this->db->dbprefix = $auxPrefix;
    	$this->table_name = $auxTableName;
    	
    	return $resultFormated;
    }
	
}