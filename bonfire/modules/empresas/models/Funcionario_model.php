<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * User Model.
 *
 * The central way to access and perform CRUD on users.
 *
 * @package Bonfire\Modules\Empresas\Models\especie_model
 * @author  Luis Vega
 */
class Funcionario_model extends BF_Model {

	
    protected $table_name = 'rrhh_funcionario';
    protected $table_funcionario_of = 'rrhh_funcionariooficina';
	protected $table_oficina = 'abt_oficina';

    public function Get_List_Responsable_By($codigoOficina) {
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        
        $this->select([
            "{$this->table_name}.*, concat({$this->table_name}.nombres,' ', {$this->table_name}.appaterno, ' ', {$this->table_name}.apmaterno) as nombre_apellido",
        ]);
        $this->join(
            $this->table_funcionario_of,
            "{$this->table_funcionario_of}.codfuncionario={$this->table_name}.codfuncionario"
        );
        $this->join(
            $this->table_oficina,
            "{$this->table_oficina}.Codoficina={$this->table_funcionario_of}.codoficina"
        );
        $this->where('firmaresolucion', 'S');
        $this->order_by('nombre_apellido');
        $this->where("{$this->table_oficina}.Codoficina", $codigoOficina);
        $list = $this->find_all();

        $this->db->dbprefix = $auxPrefix;

        return $list;
    }

    public function Get($id) {

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $this->select([
            "{$this->table_name}.*, concat({$this->table_name}.nombres,' ', {$this->table_name}.appaterno, ' ', {$this->table_name}.apmaterno) as nombre_apellido",
        ]);
        $categoria = parent::find_by('codfuncionario', $id);
        $this->db->dbprefix = $auxPrefix;

        return $categoria;
    }
	
}