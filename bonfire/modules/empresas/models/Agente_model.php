<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * User Model.
 *
 * The central way to access and perform CRUD on users.
 *
 * @package Bonfire\Modules\Empresas\Models\agente_model
 * @author  Luis Vega
 */
class Agente_model extends BF_Model {

    const ESTADO_HABILITADO = 'H'; 
    const ESTADO_P = 'P'; 

    protected $table_name = 'v_agentesauxiliares';

	public function Get_List() {

    	$auxPrefix = $this->db->dbprefix;
    	$this->db->dbprefix = '';

        $this->preFind();
        $list = parent::find_all();

        $this->db->dbprefix = $auxPrefix;
        return $list;
	}

    public function Filter_By($filters) {

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        if (isset($filters->filter_nombres) && !empty($filters->filter_nombres))
            $this->like('agente_npm', "{$filters->filter_nombres}");

        if (isset($filters->filter_nombres) && !empty($filters->filter_nombres))
            $this->like('agente_pmn', "{$filters->filter_nombres}");
        if (isset($filters->filter_documento) && !empty($filters->filter_documento))
            $this->like('ci', "{$filters->filter_documento}");

        if (isset($filters->numero_registro) && !empty($filters->numero_registro))
            $this->like('registro', "{$filters->numero_registro}");

        $this->where('estado', self::ESTADO_HABILITADO);
        $list = $this->find_all();

        $this->db->dbprefix = $auxPrefix;

        return $list;
    }

	    /**
     * Set the select and join portions of the SQL query for the find* methods.
     *
     * @todo Set this in the before_find observer?
     *
     * @return void
     */
    protected function preFind()
    {
        if (empty($this->selects)) {
            $this->select(["{$this->table_name}.*", 'nombre']);
        }
    }
	
}