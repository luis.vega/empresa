<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * User Model.
 *
 *
 * @package Bonfire\Modules\Empresas\Models\actividad_model
 * @author  Luis Vega
 */
class Unidad_model extends BF_Model {


	protected $table_name = 'tec_unidadcfo';
    protected $table_producto = 'tec_tipoproductoforestal';
    protected $table_movimiento_volumen = 'abt_moviento_volumen';

	public function Get_List() {

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $this->preFind();
        $list = parent::find_all();

        $this->db->dbprefix = $auxPrefix;
        return $list;
	}

	public function Get_By($productoId) {

    	$auxPrefix = $this->db->dbprefix;
    	$this->db->dbprefix = '';
        $this->select([
            "{$this->table_name}.*"
        ]);
        $this->join(
            $this->table_producto,
            "{$this->table_producto}.codunidadcfo={$this->table_name}.codunidadcfo"
        );
        $unidad = parent::find_by(['codtipoproductoforestal' => $productoId]);
        $this->db->dbprefix = $auxPrefix;

        return $unidad;
	}

    public function Get_By_Actividad($actividadId) {

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $this->select([
            "{$this->table_name}.*"
        ]);
        $this->join(
            $this->table_movimiento_volumen,
            "{$this->table_movimiento_volumen}.unidad_id={$this->table_name}.codunidadcfo"
        );
        $unidad = parent::find_by(['actividad_id' => $actividadId]);
        $this->db->dbprefix = $auxPrefix;

        return $unidad;
    }

    public function Get_By_Abbr($abbr) {

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $this->select([
            "{$this->table_name}.*"
        ]);
        $abbr = strtoupper($abbr);
        $unidad = parent::find_by(['UPPER(abreviacion)=' => $abbr]);
        $this->db->dbprefix = $auxPrefix;

        return $unidad;
    }

    public function Get($id) {

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        
        $unidad = parent::find_by('codunidadcfo', $id);
        $this->db->dbprefix = $auxPrefix;

        return $unidad;
    }

	/**
     * Set the select and join portions of the SQL query for the find* methods.
     *
     * @todo Set this in the before_find observer?
     *
     * @return void
     */
    protected function preFind()
    {
        if (empty($this->selects)) {
            $this->select(["{$this->table_name}.*", 'nombre']);
        }
    }
	
}