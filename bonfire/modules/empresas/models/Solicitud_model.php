<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * User Model.
 *
 * The central way to access and perform CRUD on users.
 *
 * @package Bonfire\Modules\Empresas\Models\solicitudTramite_model
 * @author  Luis Vega
 */
class Solicitud_model extends BF_Model {

    const TIPO_INSCRIPCION = 'I'; 
    const TIPO_REINSCRIPCION = 'R'; 
    const TIPO_ADICION = 'A';

    const ESTADO_PENDIENTE = 'P'; 
    const ESTADO_PENDIENTE_REVISION = 'PR'; 
    const ESTADO_ACEPTADO = 'A'; 
    const ESTADO_RECHAZADO = 'R'; 

    const TIPO_PERS_PROPIETARIO = 'P'; 
    const TIPO_PERS_REPRESENTANTE = 'R'; 
	const TIPO_PERS_AGENTE = 'A'; 
	
    protected $table_name = 'abt_solicitud_tramite_empresa';
    protected $table_empresa = 'abt_empresa';

	public function Get_List() {

    	$auxPrefix = $this->db->dbprefix;
    	$this->db->dbprefix = '';

        $this->preFind();
        $list = parent::find_all();

        $this->db->dbprefix = $auxPrefix;
        return $list;
	}

    /**
     * Set the select and join portions of the SQL query for the find* methods.
     *
     * @todo Set this in the before_find observer?
     *
     * @return void
     */
    protected function preFind()
    {
        if (empty($this->selects)) {
            $this->select(["{$this->table_name}.*", 'nombre']);
        }
    }
	
}