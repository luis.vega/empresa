<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * User Model.
 *
 * The central way to access and perform CRUD on users.
 *
 * @package Bonfire\Modules\Empresas\Models\categoria_model
 * @author  Luis Vega
 */
class Categoria_model extends BF_Model {

	const CATEGORIA_A = 1; 
	const CATEGORIA_B = 2; 
	const CATEGORIA_C = 3; 
	const CATEGORIA_D = 4; 
	const CATEGORIA_U = 5; 
	const CATEGORIA_E = 6; 
	
    protected $table_name = 'atec_categoria';
	protected $table_categoria_actividad = 'atec_categoria_actividad';

	public function Get_List() {

    	$auxPrefix = $this->db->dbprefix;
    	$this->db->dbprefix = '';

        $this->preFind();
        $list = parent::find_all();

        $this->db->dbprefix = $auxPrefix;
        return $list;
	}

    public function Get_List_By_Actividad($actividadId) {
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        
        $this->join(
            $this->table_categoria_actividad,
            "{{$this->table_categoria_actividad}.id_categoria={$this->table_name}.id"
        );
        $this->select(["{$this->table_name}.*", "{$this->table_categoria_actividad}.descripcion volumen"]);
        $this->where('id_actividad', $actividadId);
        $list = $this->find_all();

        $this->db->dbprefix = $auxPrefix;

        return $list;
    }

    public function Get($id) {

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $categoria = parent::find_by('id', $id);
        $this->db->dbprefix = $auxPrefix;

        return $categoria;
    }
	    /**
     * Set the select and join portions of the SQL query for the find* methods.
     *
     * @todo Set this in the before_find observer?
     *
     * @return void
     */
    protected function preFind()
    {
        if (empty($this->selects)) {
            $this->select(["{$this->table_name}.*", 'nombre']);
        }
    }
	
}