<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * User Model.
 *
 * The central way to access and perform CRUD on users.
 *
 * @package Bonfire\Modules\Empresas\Models\historialsolicitud_model
 * @author  Luis Vega
 */
class HistorialSolicitud_model extends BF_Model {

    const ACTION_CREAR = 'crear'; 
	
    protected $table_name = 'abt_historico_solicitud';

	public function Get_List() {

    	$auxPrefix = $this->db->dbprefix;
    	$this->db->dbprefix = '';

        $this->preFind();
        $list = parent::find_all();

        $this->db->dbprefix = $auxPrefix;
        return $list;
	}

    /**
     * [Save description]
     * @param [type] $descripcion  [description]
     * @param [type] $accion       [description]
     * @param [type] $solicitudId    [description]
     * @param [type] $usuarioEbtId [description]
     */
    public function Save($descripcion, $accion, $solicitudId, $usuarioEbtId) {
            $datosHistorico = array(
                'descripcion' => $descripcion,
                'accion' => $accion,
                'solicitud_id' => $solicitudId,
                'usuario_codigo' => $usuarioEbtId,
                'fecha_registro' => date('Y-m-j H:i:s', strtotime("now"))
            );

            $auxPrefix = $this->db->dbprefix;
            $this->db->dbprefix = '';
            $saveId = $this->insert_entity($datosHistorico);
            $this->db->dbprefix = $auxPrefix;
            
            return $saveId;
    }

	    /**
     * Set the select and join portions of the SQL query for the find* methods.
     *
     * @todo Set this in the before_find observer?
     *
     * @return void
     */
    protected function preFind()
    {
        if (empty($this->selects)) {
            $this->select(["{$this->table_name}.*", 'nombre']);
        }
    }
	
}