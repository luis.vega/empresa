<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * User Model.
 *
 * The central way to access and perform CRUD on users.
 *
 * @package Bonfire\Modules\Empresas\Models\actividad_model
 * @author  Luis Vega
 */
class Actividad_model extends BF_Model {

	const ASERRADERO = 1; 
	const BARRACAS = 2; 
	const CARPINTERIAS = 4; 
	const LAMINADORA = 15; 
	const BENEFICIADORA_CASTANIA = 16; 
	const PALMITERA = 17; 
	const CARBONERAS = 3; 
	const PLANTA_TABLEROS = 18; 
	const DESMONTADORA = 8; 
	const TRATAMIENTO_Y_O_PRESERVACION = 19; 
	const APROVECHAMIENTO_INTEGRAL_BOSQUE = 20; 
	const EXPORTADORA = 10; 
	const IMPORTADORA = 21; 
	const COMER_PROD_NO_MADERABLES = 22;
	const CENTRO_ALMACENAMIENTO = 23;
	const COMER_DE_CARBON = 24;
	const COMER_PROD_PROV_PLANTACIONES = 25;
    const CONSUMIDOR_FINAL = 26;
	const COMER_TABLEROS_AGLOMERADOS = 27;

	protected $table_name = 'tec_tipoactindustrial';
	protected $table_categoria_actividad = 'atec_categoria_actividad';

	public function Get_List() {

    	$auxPrefix = $this->db->dbprefix;
    	$this->db->dbprefix = '';
        $list = parent::find_all_by('habilitado', true);
        $this->table_name = $this->table_categoria_actividad;
        foreach ($list as $value) {
        	$count = parent::count_by('id_actividad', $value->codtipoactindustrial);
        	if ($count == 1) 
        		$value->es_unico = true;
        }
        $this->db->dbprefix = $auxPrefix;

        return $list;
	}

	public function Get($id) {

    	$auxPrefix = $this->db->dbprefix;
    	$this->db->dbprefix = '';
        $actividad = parent::find_by(['habilitado' => true, 'codtipoactindustrial' => $id]);
        $this->db->dbprefix = $auxPrefix;

        return $actividad;
	}

	/**
     * Set the select and join portions of the SQL query for the find* methods.
     *
     * @todo Set this in the before_find observer?
     *
     * @return void
     */
    protected function preFind()
    {
        if (empty($this->selects)) {
            $this->select(["{$this->table_name}.*", 'descripcion']);
        }
    }
	
}