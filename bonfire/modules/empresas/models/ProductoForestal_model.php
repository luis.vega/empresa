<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * User Model.
 *
 * The central way to access and perform CRUD on users.
 *
 * @package Bonfire\Modules\Empresas\Models\productoforestal_model
 * @author  Luis Vega
 */
class ProductoForestal_model extends BF_Model {


	protected $table_name = 'tec_tipoproductoforestal';
	protected $table_clasificacion = 'abt_clasificacion_productos';

    const CLASI_ESTADO_ACTIVO = 'A';
    const CLASI_MIN_ID = 1000;

	public function Get_List() {

    	$auxPrefix = $this->db->dbprefix;
    	$this->db->dbprefix = '';
        $this->select([
            "{$this->table_name}.codtipoproductoforestal",
            "{$this->table_name}.descripcion"

        ]);
        $this->join(
            $this->table_clasificacion,
            "{$this->table_clasificacion}.cod_clasificacion_producto={$this->table_name}.cod_clasificacion_producto"
        );
        $filters = [
            "{$this->table_clasificacion}.estado" => self::CLASI_ESTADO_ACTIVO,
            "codtipoproductoforestal >" => self::CLASI_MIN_ID
        ];
        $list = parent::find_all_by($filters);
        $this->db->dbprefix = $auxPrefix;

        return $list;
	}

    public function Get_Like($descripcion) {

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $this->select([
            "{$this->table_name}.codtipoproductoforestal",
            "{$this->table_name}.descripcion"
        ]);
        $this->join(
            $this->table_clasificacion,
            "{$this->table_clasificacion}.cod_clasificacion_producto={$this->table_name}.cod_clasificacion_producto", 'INNER'
        );
        $descripcion = strtoupper($descripcion);
        $filters = [
            "{$this->table_clasificacion}.estado" => self::CLASI_ESTADO_ACTIVO,
            "codtipoproductoforestal >" => self::CLASI_MIN_ID,
            "UPPER({$this->table_name}.descripcion)=" => $descripcion
        ];
        $list = parent::find_all_by($filters);
        $this->db->dbprefix = $auxPrefix;

        if (!empty($list))
            return $list[0];

        return false;
    }

	public function Get($id) {

    	$auxPrefix = $this->db->dbprefix;
    	$this->db->dbprefix = '';
        $actividad = parent::find_by(['codtipoproductoforestal' => $id]);
        $this->db->dbprefix = $auxPrefix;

        return $actividad;
	}

	/**
     * Set the select and join portions of the SQL query for the find* methods.
     *
     * @todo Set this in the before_find observer?
     *
     * @return void
     */
    protected function preFind()
    {
        if (empty($this->selects)) {
            $this->select(["{$this->table_name}.*", 'descripcion']);
        }
    }
	
}