<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * User Model.
 *
 * The central way to access and perform CRUD on users.
 *
 * @package Bonfire\Modules\Empresas\Models\historialempresa_model
 * @author  Luis Vega
 */
class HistorialEmpresa_model extends BF_Model {

    const ACTION_CREAR = 'crear'; 
    const ACTION_ADD_SUCURSAL = 'add_sucursal'; 
	
    protected $table_name = 'abt_historial_empresa';

	public function Get_List() {

    	$auxPrefix = $this->db->dbprefix;
    	$this->db->dbprefix = '';

        $this->preFind();
        $list = parent::find_all();

        $this->db->dbprefix = $auxPrefix;
        return $list;
	}

    /**
     * [Save description]
     * @param [type] $descripcion  [description]
     * @param [type] $accion       [description]
     * @param [type] $empresaId    [description]
     * @param [type] $usuarioEbtId [description]
     */
    public function Save($descripcion, $accion, $empresaId, $usuarioEbtId) {
            $datosHistorico = array(
                'descripcion' => $descripcion,
                'accion' => $accion,
                'empresa_id' => $empresaId,
                'usuario_codigo' => $usuarioEbtId,
                'fecha_registro' => date('Y-m-j H:i:s', strtotime("now"))
            );

            $auxPrefix = $this->db->dbprefix;
            $this->db->dbprefix = '';
            $saveId = $this->insert_entity($datosHistorico);
            $this->db->dbprefix = $auxPrefix;
            
            return $saveId;
    }

	    /**
     * Set the select and join portions of the SQL query for the find* methods.
     *
     * @todo Set this in the before_find observer?
     *
     * @return void
     */
    protected function preFind()
    {
        if (empty($this->selects)) {
            $this->select(["{$this->table_name}.*", 'nombre']);
        }
    }
	
}