<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * User Model.
 *
 * The central way to access and perform CRUD on users.
 *
 * @package Bonfire\Modules\Empresas\Models\movimientoproducto_model
 * @author  Luis Vega
 */
class MovimientoProducto_model extends BF_Model {

    const OBSER_PROYECTADO = 'P'; 
	const OBSER_PROYECTADO_DESC = 'Proyectado'; 
    const OBSER_SALDO_STOCK = 'ST'; 
	const OBSER_SALDO_STOCK_DESC = 'Saldo en Stock';
    const MOV_VOL_OPERACION_MAYOR = ">";
    const MOV_VOL_OPERACION_MENOR = "<";
    const MOV_VOL_OPERACION_RANGO = "-";

	protected $table_name = 'abt_movimiento_producto';
	protected $table_movimiento = 'abt_movimiento_producto';
	protected $table_detalle_maquinaria = 'abt_movimiento_detalle_maquinaria';
	
	public function __construct()
    {
        parent::__construct();
        $this->load->model('empresas/historialempresa_model');
        $this->load->model('empresas/productoforestal_model');
        $this->load->model('empresas/especie_model');
        $this->load->model('empresas/actividad_model');
        $this->load->model('empresas/categoria_model');
        $this->load->model('empresas/unidad_model');
        $this->load->model('empresas/archivo_model');
    }

	public function Get_List_Observaciones() {
		return [
			(object)['key' => self::OBSER_PROYECTADO, 'descripcion' => self::OBSER_PROYECTADO_DESC],
			(object)['key' => self::OBSER_SALDO_STOCK, 'descripcion' => self::OBSER_SALDO_STOCK_DESC]
		];
	}

    public function LoadInfoFormulario26(&$solicitudObject) {
        foreach ($solicitudObject->sucursales as $key => $sucursal) {
            foreach ($sucursal->actividades as $actividadSuc) {
                $actividadSuc->movimientos = $this->GetMovimientos($actividadSuc->sucursal_actividad_id);
                $actividadSuc->filename = $this->GetFilenameFO26($sucursal->empresa_sucursal_id, $actividadSuc->sucursal_actividad_id);
                $actividadSuc->documento = $this->GetDocumentoFO26($sucursal->empresa_sucursal_id, $actividadSuc->sucursal_actividad_id);
            }
        }
    }

    private function GetMovimientos($actividadSucId) {
        $movimientos = $this->GetMovimientosProductoBy($actividadSucId);
        if (!empty($movimientos)) {
            foreach ($movimientos as $movimiento) {
                $movimiento->unidad = $this->unidad_model->Get($movimiento->unidad_id);
                $movimiento->especie = $this->especie_model->Get($movimiento->especiecomun_codigo);
                $movimiento->producto = $this->productoforestal_model->Get($movimiento->producto_codigo);
                if ($movimiento->observacion == self::OBSER_SALDO_STOCK)
                    $movimiento->observacion_desc = self::OBSER_SALDO_STOCK_DESC;
                else
                    $movimiento->observacion_desc = self::OBSER_PROYECTADO_DESC;
            }
        }

        return $movimientos;
    }

    private function GetFilenameFO26($sucursalId, $actividadId) {
        $auxPrefix = $this->db->dbprefix;
        $auxTableName = $this->table_name;
        $this->db->dbprefix = '';

        $this->table_name = 'abt_documento_presentado';
        $filters = [
            'esPresentado' => true,
            'empresa_sucursal_id' => $sucursalId,
            'entidad_id' => $actividadId,
            'config_archivo_id' => Archivo_Model::CONFIG_ID_FICHERO_FO26,
            'tipo' => Archivo_Model::TIPO_DOC_PRESTADO_ACTIVIDAD
        ];
        $filePresentado = parent::find_by($filters);
        $this->table_name = $auxTableName;
        $this->db->dbprefix = $auxPrefix;

        if (!empty($filePresentado))
            return $filePresentado->nombre_file;

        return false;
    }
    
    private function GetDocumentoFO26($sucursalId, $actividadId) {
    	$auxPrefix = $this->db->dbprefix;
    	$auxTableName = $this->table_name;
    	$this->db->dbprefix = '';
    
    	$this->table_name = 'abt_documento_presentado';
    	$filters = [
    			'esPresentado' => true,
    			'empresa_sucursal_id' => $sucursalId,
    			'entidad_id' => $actividadId,
    			'config_archivo_id' => Archivo_Model::CONFIG_ID_FICHERO_FO26,
    			'tipo' => Archivo_Model::TIPO_DOC_PRESTADO_ACTIVIDAD
    			];
    	$filePresentado = parent::find_by($filters);
    	$this->table_name = $auxTableName;
    	$this->db->dbprefix = $auxPrefix;
    
    	return $filePresentado;
    }
    

    public function SaveProductos($collection, $actividadId, $empresaId, $userId) {
        $response = ResponseApp::Validate();

        $productos = [];
        $actividadSuc = $this->GetSucursalActividad($actividadId);
        $movimientoVolumen = $this->GetMovimientoVolumenBy($actividadSuc->actividad_id);
        $totalmovimientos = 0;
        $filasValidas = 0;
        $this->db->trans_begin();
        
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $this->db->delete('abt_movimiento_producto', ['actividad_id' => $actividadId]);
        foreach ($collection as $key => $productoCell) {
            $productoObject = $this->productoforestal_model->Get_Like($productoCell->producto);
            $index = $key + 1;
            if (!$productoObject) {
                array_push($response->errors, 
                    "El producto con valor='{$productoCell->producto}', no es válido en la fila {$index}.");
                $response->has_errors = true;
            }

            $especieObject = $this->especie_model->Get_Like($productoCell->especie);
            if (!$especieObject) {
                array_push($response->errors, 
                    "La especie con valor='{$productoCell->especie}', no es válida para la fila {$index}");
                $response->has_errors = true;
            }

            $observacionEnum = "";
            if (strtoupper($productoCell->observaciones) == strtoupper(self::OBSER_PROYECTADO_DESC) || 
                strtoupper($productoCell->observaciones) == strtoupper(self::OBSER_SALDO_STOCK_DESC)) {

                $observacionEnum = strtoupper($productoCell->observaciones) == strtoupper(self::OBSER_SALDO_STOCK_DESC) ? 
                    self::OBSER_SALDO_STOCK : self::OBSER_PROYECTADO;
            } else {
                array_push($response->errors, "La Observacion con valor='{$productoCell->observaciones}', no es válido en la fila {$index}.");
                $response->has_errors = true;
            }

            $unidadObject = $this->unidad_model->Get_By_Abbr($productoCell->unidad);
            if (empty($unidadObject)) {
                array_push($response->errors, "La Unidad con valor='{$productoCell->unidad}', no es válido en la fila {$index}.");
                $response->has_errors = true;
            }

            if (!$response->has_errors) {
                $productoCell->cantidad = round(floatval($productoCell->cantidad), 2);
                $datos = new stdClass();
                $datos->actividad_id = $actividadId;
                $datos->unidad_id = $unidadObject->codunidadcfo;
                $datos->producto_codigo = $productoObject->codtipoproductoforestal;
                $datos->observacion = $observacionEnum;
                $datos->especiecomun_codigo = $especieObject->codespeciencomun;
                $datos->cantidad = $productoCell->cantidad;
                $datos->rendimiento = intval($productoCell->rendimiento);
                if (!empty($movimientoVolumen) && $movimientoVolumen->unidad_id != $unidadObject->codunidadcfo) {
                    $datos->comentarios = "La unidad del registro no corresponde con la unidad de la actividad.";
                    $productoCell->comentarios = $datos->comentarios;
                } else {
                    $totalmovimientos += $datos->cantidad;
                    $filasValidas++;
                }
                array_push($productos, $productoCell);
                $this->SaveTransaction($datos, $userId, $empresaId);

            }

        }
        if ($response->has_errors)
            return $response;
        else {
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else
            {
                $this->historialempresa_model->Save("Se ha exportado Productos en la actividad={$actividadId}", 'add_producto', $empresaId, $userId);
                $this->UpdateCategoria($totalmovimientos, $actividadSuc);
                $this->db->trans_commit();
                $this->db->dbprefix = $auxPrefix;

                // Preparar Datos para enviar
                $actividadSuc = $this->GetSucursalActividad($actividadId);
                $categoria = $this->categoria_model->Get($actividadSuc->categoria_id);
                $response->is_valid = true;
                $dataReport = [
                    'productos' => $productos, 
                    'categoria' => $categoria, 
                    'volumen_total' => $totalmovimientos,
                    'filas_validas' => $filasValidas
                ];

                if (!empty($movimientoVolumen)) {
                    $unidad = $this->unidad_model->Get($movimientoVolumen->unidad_id);
                    if (!empty($unidad))
                        $dataReport['unidad'] = $unidad;
                }

                $response->data = (object) $dataReport;
            }
        }

        return $response;
    }

    private function UpdateCategoria($totalMovimientos, $actividadSucursalObject) {
 
        $actividadSucursalObject->categoria_id = 0;
        $movimientos = $this->GetAllMovimientosVolumenBy($actividadSucursalObject->actividad_id);
        if (!empty($movimientos) && sizeof($movimientos)) {
            foreach ($movimientos as $movimientoVol) {
                switch ($movimientoVol->operacion) {
                    case self::MOV_VOL_OPERACION_RANGO:

                        if ($totalMovimientos > $movimientoVol->valor_inicial && 
                        $totalMovimientos < $movimientoVol->valor_final)
                            $actividadSucursalObject->categoria_id = $movimientoVol->categoria_id;
                        break;
                    case self::MOV_VOL_OPERACION_MENOR:
                        if ($totalMovimientos < $movimientoVol->valor_inicial)
                            $actividadSucursalObject->categoria_id = $movimientoVol->categoria_id;
                        break;
                    case self::MOV_VOL_OPERACION_MAYOR:
                        if ($totalMovimientos > $movimientoVol->valor_inicial)
                            $actividadSucursalObject->categoria_id = $movimientoVol->categoria_id;
                        break;
                }

            }
        }

        if ($actividadSucursalObject->categoria_id == 0) 
            $actividadSucursalObject->categoria_id = $this->GetDefaultActividad($actividadSucursalObject->actividad_id);

        $actividadSucId = $actividadSucursalObject->sucursal_actividad_id;
        $actividadSucursalObject = (array) $actividadSucursalObject;
        unset($actividadSucursalObject['sucursal_actividad_id']);
        $this->UpdateSucursalActividad($actividadSucursalObject, $actividadSucId);

    }

    private function GetDefaultActividad($actividadId) {
        switch ($actividadId) {
            case actividad_Model::ASERRADERO :
                return Categoria_model::CATEGORIA_E;
                break;
            case actividad_Model::LAMINADORA :
                return Categoria_model::CATEGORIA_U;
                break;
            case actividad_Model::BENEFICIADORA_CASTANIA :
                return Categoria_model::CATEGORIA_U;
                break;
            case actividad_Model::PALMITERA :
                return Categoria_model::CATEGORIA_U;
                break;
            case actividad_Model::CARBONERAS :
                throw new Exception("Error en la configuracion o calculo para la actividad Carbonera");
                break;
            case actividad_Model::CARPINTERIAS :
                throw new Exception("Error en la configuracion o calculo para la actividad Carpintería");
                break;
            case actividad_Model::PLANTA_TABLEROS :
                return Categoria_model::CATEGORIA_U;
                break;
            case actividad_Model::TRATAMIENTO_Y_O_PRESERVACION :
                return Categoria_model::CATEGORIA_U;
                break;
            case actividad_Model::DESMONTADORA :
                return Categoria_model::CATEGORIA_U;
                break;
            case actividad_Model::APROVECHAMIENTO_INTEGRAL_BOSQUE :
                return Categoria_model::CATEGORIA_U;
                break;
            case actividad_Model::BARRACAS :
                throw new Exception("Error en la configuracion o calculo para la actividad Barraca");
                break;
            case actividad_Model::EXPORTADORA :
                return Categoria_model::CATEGORIA_U;
                break;
            case actividad_Model::IMPORTADORA :
                return Categoria_model::CATEGORIA_U;
                break;
            case actividad_Model::COMER_PROD_NO_MADERABLES :
                return Categoria_model::CATEGORIA_U;
                break;
            case actividad_Model::CENTRO_ALMACENAMIENTO :
                return Categoria_model::CATEGORIA_U;
                break;
            case actividad_Model::COMER_DE_CARBON :
                return Categoria_model::CATEGORIA_U;
                break;
            case actividad_Model::COMER_PROD_PROV_PLANTACIONES :
                return Categoria_model::CATEGORIA_U;
                break;
            case actividad_Model::COMER_TABLEROS_AGLOMERADOS :
                return Categoria_model::CATEGORIA_U;
                break;
            case actividad_Model::CONSUMIDOR_FINAL :
                return Categoria_model::CATEGORIA_U;
                break;
            default:
                throw new Exception("Error la actividad es desconocida.");
            break;
        }
    }

    private function UpdateSucursalActividad($sucursalActividad, $actividadSucId) {
        $auxPrefix = $this->db->dbprefix;
        $auxTableName = $this->table_name;
        $this->db->dbprefix = '';

        $this->table_name = 'abt_sucursal_has_actividad';
        $this->db->update($this->table_name, $sucursalActividad, 
            array('sucursal_actividad_id' => $actividadSucId));
        $this->db->dbprefix = $auxPrefix;
        $this->table_name = $auxTableName;
    }

    private function GetSucursalActividad($actividadSucId) {
        $auxPrefix = $this->db->dbprefix;
        $auxTableName = $this->table_name;
        $this->db->dbprefix = '';

        $this->table_name = 'abt_sucursal_has_actividad';
        $actividad = parent::find_by('sucursal_actividad_id', $actividadSucId);
        $this->db->dbprefix = $auxPrefix;
        $this->table_name = $auxTableName;

        return $actividad;
    }

    private function GetMovimientosProductoBy($actividadId) {
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $movimientos = parent::find_all_by('actividad_id', $actividadId);
        $this->db->dbprefix = $auxPrefix;
        if (!empty($movimientos))
            return $movimientos;

        return false;
    }

    private function GetAllMovimientosVolumenBy($actividadId) {
        $auxPrefix = $this->db->dbprefix;
        $auxTableName = $this->table_name;
        $this->db->dbprefix = '';

        $this->table_name = 'abt_moviento_volumen';
        $movimientos = parent::find_all_by('actividad_id', $actividadId);
        $this->db->dbprefix = $auxPrefix;
        $this->table_name = $auxTableName;
        if (!empty($movimientos))
            return $movimientos;

        return false;
    }

    private function GetMovimientoVolumenBy($actividadId) {
        $auxPrefix = $this->db->dbprefix;
        $auxTableName = $this->table_name;
        $this->db->dbprefix = '';

        $this->table_name = 'abt_moviento_volumen';
        $movimiento = parent::find_by('actividad_id', $actividadId);
        $this->db->dbprefix = $auxPrefix;
        $this->table_name = $auxTableName;
        if (!empty($movimiento))
            return $movimiento;

        return false;
    }

    private function SaveTransaction($datos, $userId, $empresaId) {
        $datosArray = (array)$datos;

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $saveId = $this->insert_entity($datosArray);
        $this->db->dbprefix = $auxPrefix;

    }

    public function SaveMovimientos($data) {
        $response = ResponseApp::Validate();
        $sucursalId = 0;
        $empresaId = 0;
        $this->db->trans_begin();

        $totalmovimientos = 0;
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $actividadId = json_decode($data['movimientos'][self::OBSER_PROYECTADO])->actividad_id;
        $this->db->delete('abt_movimiento_producto', ['actividad_id' => $actividadId]);

        foreach ($data['movimientos'] as $key => $informcion) {
            $movimiento = (array)json_decode($informcion);
            $sucursalId = $movimiento['sucursal_id'];
            $actividadId = $movimiento['actividad_id'];
            $empresaId = $movimiento['empresa_id'];
            unset($movimiento['sucursal_id']);
            $this->Save($movimiento, $data['user_id']);
            $totalmovimientos += $movimiento['cantidad'];
        }

        if (empty($data['exist_file'])) {
            $dataFile = [
                'espresentado' => true,
                'nombre_file' => $data['filename'],
                'empresa_sucursal_id' => $sucursalId,
                'config_archivo_id' => Archivo_Model::CONFIG_ID_FICHERO_FO26,
                'empresa_id' => $empresaId,
                'entidad_id' => $actividadId,
                'tipo' => Archivo_Model::TIPO_DOC_PRESTADO_ACTIVIDAD
            ];
            $this->SaveFileF26($dataFile);
        }

        $actividadSuc = $this->GetSucursalActividad($actividadId);
        if ($actividadSuc->actividad_id == actividad_Model::ASERRADERO && $actividadSuc->categoria_id != Categoria_model::CATEGORIA_E)
            $this->UpdateCategoria($totalmovimientos, $actividadSuc);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            $response->is_valid = true;
        }
        $this->db->dbprefix = $auxPrefix;

        return $response;
    }

    public function SaveFileF26($datos) {
        $auxTable = $this->table_name;
        $auxPrefix = $this->db->dbprefix;

        $this->db->dbprefix = '';
        $this->table_name = 'abt_documento_presentado';

        $this->db->delete($this->table_name, 
                [
                    'empresa_sucursal_id' => $datos['empresa_sucursal_id'],
                    'config_archivo_id' => $datos['config_archivo_id'],
                    'entidad_id' => $datos['entidad_id'],
                    'tipo' => $datos['tipo'],
                ]);

        $this->insert_entity($datos);
        $this->db->dbprefix = $auxPrefix;
        $this->table_name = $auxTable;
    }

	public function Save($datos, $userId) {
		$response = ResponseApp::Validate();
		$datosArray = (array)$datos;
		$empresaId = $datosArray['empresa_id'];
		unset($datosArray['empresa_id']);

        $this->db->trans_begin();

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $saveId = $this->insert_entity($datosArray);
        $this->db->dbprefix = $auxPrefix;

        $this->historialempresa_model->Save("Se ha agregado Producto en la actividad={$datosArray['actividad_id']}", 'add_producto', $empresaId, $userId);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            $response->is_valid = true;
            $response->data = $saveId;
        }
        return $response;
	}

    public function SaveMaquinarias($collection, $actividadId, $empresaId, $userId) {
        $response = ResponseApp::Validate();

        $maquinarias = [];
        $this->db->trans_begin();

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $this->db->delete('abt_movimiento_detalle_maquinaria', ['actividad_id' => $actividadId]);
        foreach ($collection as $key => $maquinariaCell) {
            $maquinariaCell->empresa_id = $empresaId;
            $maquinariaCell->actividad_id = $actividadId;
            if (empty($maquinariaCell->anio_compra))
                $maquinariaCell->anio_compra = null;
            if (empty($maquinariaCell->especificacion_tecnica))
                $maquinariaCell->especificacion_tecnica = null;
            if (empty($maquinariaCell->modelo))
                $maquinariaCell->modelo = null;
            $this->SaveDetalleMaquinariaTransac($maquinariaCell);
            array_push($maquinarias, $maquinariaCell);
        }

        if ($response->has_errors)
            return $response;
        else {
            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else
            {
                $this->historialempresa_model->Save("Se ha exportado Maquinarias en la actividad={$actividadId}", 'add_producto', $empresaId, $userId);
                $this->db->trans_commit();
                $this->db->dbprefix = $auxPrefix;

                $response->is_valid = true;
                $response->data = $maquinarias;
            }
        }

        return $response;
    }

    public function SaveDetalleMaquinariaTransac($datos) {
        $datosArray = (array)$datos;
        $empresaId = $datosArray['empresa_id'];
        unset($datosArray['empresa_id']);

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $this->table_name = $this->table_detalle_maquinaria;

        $this->insert_entity($datosArray);
        $this->db->dbprefix = $auxPrefix;
    }

	public function SaveDetalleMaquinaria($datos, $userId) {
		$response = ResponseApp::Validate();
		$datosArray = (array)$datos;
		$empresaId = $datosArray['empresa_id'];
		unset($datosArray['empresa_id']);

        $this->db->trans_begin();

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $this->table_name = $this->table_detalle_maquinaria;

        $saveId = $this->insert_entity($datosArray);
        $this->db->dbprefix = $auxPrefix;

        $this->historialempresa_model->Save("Se ha agregado Detalle Maquinaria en la actividad={$datosArray['actividad_id']}", 'add_producto', $empresaId, $userId);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            $response->is_valid = true;
            $response->data = $saveId;
        }
        $this->table_name = $this->table_movimiento;

        return $response;
	}

	public function Remove($producto, $userId) {
		$response = ResponseApp::Validate();

        $producto = (object) $producto;
        $this->db->trans_begin();
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $productoObject =  $this->find_by('movimiento_producto_id', $producto->id);
        if ($productoObject) {
            $this->db->delete($this->table_name, ['movimiento_producto_id' => $producto->id]);

            $this->historialempresa_model->Save("Se ha eliminado Movimiento de Producto de la empresa", 'remove_producto', $producto->empresa_id, $userId);

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else
            {
                $this->db->trans_commit();
                $response->is_valid = true;
            }
        } 
        $this->db->dbprefix = $auxPrefix;

        return $response;
	}

	public function RemoveDetalleMaquinaria($maquinaria, $userId) {
		$response = ResponseApp::Validate();

        $maquinaria = (object) $maquinaria;
        $this->db->trans_begin();

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $this->table_name = $this->table_detalle_maquinaria;
        $maquinariaObject =  $this->find_by('movimiento_detalle_maquinaria_id', $maquinaria->id);
        if ($maquinariaObject) {
            $this->db->delete($this->table_name, ['movimiento_detalle_maquinaria_id' => $maquinaria->id]);
            $this->historialempresa_model->Save("Se ha eliminado Detalle Maquinaria de Producto de la empresa", 'remove_producto', $maquinaria->empresa_id, $userId);

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else
            {
                $this->db->trans_commit();
                $response->is_valid = true;
            }
        }
        $this->db->dbprefix = $auxPrefix;

        return $response;
	}
}