<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * User Model.
 *
 * The central way to access and perform CRUD on users.
 *
 * @package Bonfire\Modules\Empresas\Models\empresa_model
 * @author  Luis Vega
 */
class Empresa_model extends BF_Model {

    const ESTADO_BORRADOR = 'B'; 
    const ESTADO_COMPLETADO = 'C'; 
    const ESTADO_MODIFICADO = 'M'; 
	const ESPECIE_ID_VARIOS = 3436;

    // ABT 
    const TIPO_OFICINA_DD = 2;
    const TIPO_OFICINA_UOBT = 3;
    const CODIGO_UNIDAD_RD = 'RD';
    const CODIGO_UNIDAD_RU = 'RU';
	
    protected $table_name = 'abt_empresa_forestal';
    protected $table_empresa = 'abt_empresa_forestal';
    protected $table_sucursal_has_actividad = 'abt_sucursal_has_actividad';
    protected $table_solicitud = 'abt_solicitud_tramite_empresa';
    protected $table_persona_tramite = 'abt_persona_tramite';
    protected $table_sucursal = 'abt_sucursal_empresa';
    protected $table_oficina = 'abt_oficina';
	protected $table_precio_actividad_categoria = 'abt_precio_actividad_categoria';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('empresas/historialempresa_model');
        $this->load->model('empresas/historialsolicitud_model');
        $this->load->model('empresas/agente_model');
        $this->load->model('empresas/actividad_model');
        $this->load->model('empresas/categoria_model');
        $this->load->model('empresas/solicitud_model');
        $this->load->model('empresas/tipotramite_model');
        $this->load->model('empresas/categoria_model');
        $this->load->model('empresas/unidad_model');
        $this->load->model('empresas/funcionario_model');
        $this->load->model('api/personaabt_model');
    }

	public function Get_List() {

    	$auxPrefix = $this->db->dbprefix;
    	$this->db->dbprefix = '';

        $this->preFind();
        $list = parent::find_all();

        $this->db->dbprefix = $auxPrefix;
        return $list;
	}

    public function Finalizar_Solicitud($solicitudId, $usuarioAbtId) {
        $response = ResponseApp::Validate();
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $this->db->trans_begin();
        $solicitud = $this->Get_Solicitud($solicitudId);

        if ($solicitud->has_errors) {
            $response->errors = $solicitud->errors;
            $this->db->dbprefix = $auxPrefix;

            return $response;
        }

        if (!empty($solicitud->data->es_pendiente_revision)) {
            array_push($response->errors, "La solicitud ya fue finalizada.");
            $response->has_errors = true;
            $this->db->dbprefix = $auxPrefix;

            return $response;
        }

        $persisted = $solicitud->data;
        $actividades = [];
        $actividadMayor = null;
        foreach ($persisted->sucursales as $sucursal) {
            foreach ($sucursal->actividades as $actividad) {
                $precio = $this->GetPrecio($actividad->actividad_id, $actividad->categoria_id);
                if (empty($precio)) 
                    throw new Exception("No existe configuracion de precio para la ActividadId={$actividad['actividad_id']}, Categoria={$actividad['categoria_id']}");

                $actividad->precio = $precio; 
                if (empty($actividadMayor) || $precio->monto > $actividadMayor->precio->monto) {
                    $actividadMayor = $actividad;
                }

                $this->SaveUpdateActividad(['esprincipal' => false], $actividad->sucursal_actividad_id);
            }
        }

        if (!empty($actividadMayor)) {
            $this->SaveUpdateActividad(['esprincipal' => true], $actividadMayor->sucursal_actividad_id);
            $datosPago = [
                'monto' => $actividadMayor->precio->monto,
                'pagoComprobado' => false,
                'monto_moneda' => $actividadMayor->precio->moneda_iso,
                'solicitud_id' => $solicitudId
            ];
            $this->SaveUpdatePagoSolicitud($datosPago);

        }
        // Update Solicitud
        $datos = [
            'estado' => Solicitud_model::ESTADO_PENDIENTE_REVISION,
            'fecha_registro' => CommonApp::DateNow()
        ];
        $this->db->update($this->table_solicitud, $datos, ['solicitud_tramite_empresa_id' => $solicitudId]);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->historialsolicitud_model->Save("Se ha finalizado el proceso de solicitud", 'update', $solicitudId, $usuarioAbtId);

            $this->db->trans_commit();
            $this->db->dbprefix = $auxPrefix;

            $response->is_valid = true;
        }
        return $response;
    }

    public function Aprobar_Solicitud($data, $usuarioAbtId) {
        $response = ResponseApp::Validate();
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $solicitudId = $data->solicitud_id;
        $numeroRegistro = $data->numero_registro;
        $this->db->trans_begin();
        $solicitud = $this->Get_SolicitudComplete($solicitudId);

        if ($solicitud->has_errors) {
            $response->errors = $solicitud->errors;
            $this->db->dbprefix = $auxPrefix;

            return $response;
        }

        if (!empty($solicitud->data->es_aceptado)) {
            array_push($response->errors, "La solicitud ya fue Aceptada.");
            $response->has_errors = true;
            $this->db->dbprefix = $auxPrefix;

            return $response;
        }

        $persisted = $solicitud->data;
        $datosUpdateEmpresa = [
            'estado' => self::ESTADO_COMPLETADO,
            'habilitado' => true,
            'registro_empresa' => $this->GetNextRegistro()
        ];
        $codeEmpresa = $this->GetNextRegistro();
        if (empty($persisted->propietario_codpersona)) {
            $datos = $persisted->propietario;
            $datosProp = (object) array(
                'nombres' => $datos->nombres,
                'apellido_paterno' => $datos->appaterno,
                'apellido_materno' => $datos->apmaterno,
                'telefono_celular' => $datos->celular,
                'telefono_fijo' => $datos->telefono,
                'pais_nacionalidad' => $datos->codpaisorigen,
                'tipo_documento' => $datos->codtipodocid,
                'password' => $datos->codtipodocid,
                'numero_documento' => $datos->nrodocid,
                'sigla_departamento_extension' => $datos->emitido,
                'email' => $datos->email,
                'es_propietario' => true
            );
            $savepropietario = $this->personaabt_model->Save_Update_Persona($datosProp);
            $datosUpdateEmpresa['propietario_codpersona'] = $savepropietario->data;
        }

        $actividades = [];
        $actividadMayor = null;
        foreach ($persisted->sucursales as $sucursal) {
            foreach ($sucursal->representantes as $representante) {
                $datosRep = (object) array(
                    'nombres' => $representante->nombres,
                    'apellido_paterno' => $representante->appaterno,
                    'apellido_materno' => $representante->apmaterno,
                    'telefono_celular' => $representante->celular,
                    'telefono_fijo' => $representante->telefono,
                    'pais_nacionalidad' => $representante->codpaisorigen,
                    'tipo_documento' => $representante->codtipodocid,
                    'password' => $representante->codtipodocid,
                    'numero_documento' => $representante->nrodocid,
                    'sigla_departamento_extension' => $representante->emitido,
                    'email' => $representante->email
                );
                $saverepresentante = $this->personaabt_model->Save_Update_Persona($datosRep);
                $query = $this->db->get_where('abt_representante_legal', 
                    [
                        'abt_sucursal_id' => $sucursal->empresa_sucursal_id, 
                        'representante_codpersona' => $saverepresentante->data,
                        'habilitado' => true
                    ]);
                $repSucursal = $query->result_array();

                if (empty($repSucursal) || sizeof($repSucursal) == 0) {
                    $this->db->insert('abt_representante_legal', 
                        [
                            'abt_sucursal_id' => $sucursal->empresa_sucursal_id, 
                            'representante_codpersona' => $saverepresentante->data,
                            'habilitado' => true
                        ]);
                }

                $this->db->update('abt_persona_tramite', 
                        ['codpersona_id' => $saverepresentante->data], ['codpersona' => $representante->codpersona]);

            }
        }

        //  Update Empresa
        $this->db->update($this->table_name, $datosUpdateEmpresa, ['empresa_forestal_id' => $persisted->empresa_forestal_id]);

        $codigoResolucion = $this->GetCodigoResolucion($numeroRegistro, $persisted->gestion);
        // Update Solicitud
        $datos = [
            'estado' => Solicitud_model::ESTADO_ACEPTADO,
            'fecha_aprobacion' => CommonApp::DateNow()
        ];
        $this->db->update($this->table_solicitud, $datos, ['solicitud_tramite_empresa_id' => $solicitudId]);

        // Codigo Resolucion
        $this->db->insert('abt_resolucion_empresa', 
                    [
                        'solicitud_id' => $solicitudId, 
                        'fecha_registro' => CommonApp::DateNow(),
                        'codigo_resolucion' => $codigoResolucion,
                        'usuario_id' => $usuarioAbtId,
                        'responsable_id' => $data->responsable_id,
                        'responsable_cargo' => $data->responsable_cargo
                    ]);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->historialsolicitud_model->Save("Se ha Aprobado la solicitud", 'update', $solicitudId, $usuarioAbtId);

            $this->db->trans_commit();
            $this->db->dbprefix = $auxPrefix;

            $response->is_valid = true;
        }
        return $response;
    }

    private function GetCodigosUnidad() {
        return [
                self::TIPO_OFICINA_DD => self::CODIGO_UNIDAD_RD,
                self::TIPO_OFICINA_UOBT => self::CODIGO_UNIDAD_RU
            ];
    }

    private function GetCodigoResolucion($numeroRegistro, $gestion) {
        $codTipoOficina = $this->session->userdata('codtipoficina');
        $codOficina = $this->session->userdata('codoficina');
        $codigoResolucion = $this->GetCodigosUnidad()[$codTipoOficina];
        $codigoResolucion .= '-' . config_item('sigla-abt');
        $oficina = $this->GetOficina($codOficina);

        if (empty($oficina))
            throw new Exception("No existe oficina asociada al funcionario");
        $codigoResolucion .= '-' . $oficina->Abreviatura;
        $codigoResolucion .= '-' . config_item('sigla-resolucion-ref');


        if (strlen($numeroRegistro) < 4) {
            $valueIntString = "0000" . $numeroRegistro;
            $numeroRegistro = substr($valueIntString, -4);
        }
        $codigoResolucion .= '-' . $numeroRegistro;
        $codigoResolucion .= '-' . $gestion;
        return $codigoResolucion;
    }

    public function GetTextCargoResolucion() {
        $codTipoOficina = $this->session->userdata('codtipoficina');
        if (self::TIPO_OFICINA_DD == $codTipoOficina)
            return "Director Departamental";
        else 
            return "Responsable de UOBT";

    }

    private function GetOficina($codigoOficina) {
        $auxTableName = $this->table_name;
        
        $this->table_name = $this->table_oficina;

        $this->select(["*"]);
        $this->where('estado', 'A');
        $oficina = $this->find_by('codoficina', $codigoOficina);
        return $oficina;
    }

    private function GetNextRegistro() {
        $this->db->select("max(registro_empresa) as last_code");
        $this->db->from("{$this->table_name} m");
        $this->db->where('registro_empresa !=', null);
        $query = $this->db->get();
        $list = $query->result();
        $lastCode = $list[0];
        $code = config_item('prefix-code-empresa');
        if (!empty($lastCode->last_code)) {
            $lastCode = explode('-', $lastCode->last_code)[2];
            $valueInt = intval($lastCode);
            $valueInt += 1;
            $valueIntString = "0000" . $valueInt;
            $code = $code . '-' . substr($valueIntString, -5);
        } else 
            $code = $code . '-00001';

        return $code;

    }

    private function SaveUpdatePagoSolicitud($datosPago) {
        $auxTableName = $this->table_name;
        $this->table_name = 'abt_pago_solicitud';

        $persisted = parent::find_by('solicitud_id', $datosPago['solicitud_id']);
        if ($persisted)
            $this->db->update($this->table_name, $datosPago, ['pago_solicitud_id' => $persisted->pago_solicitud_id]);
        else 
            $saveId = $this->insert_entity($datosPago);

        $this->table_name = $auxTableName;

    }

    public function Save_Solicitud_Inscripcion($entityData) {
        $response = ResponseApp::Validate();

        $this->db->trans_begin();

        // Save Empresa
        $empresaId = $this->SaveEmpresa($entityData);

        // Save Solicitud 
        $solicitudId = $this->SaveSolicitudInscripcion($empresaId, $entityData->codigo_usuario_id);

        // Save Sucursales
        $this->SaveSucursalesEmpresa($empresaId, $solicitudId, $entityData->codigo_usuario_id,$entityData->sucursales);

        // Save Propietario
        if (!isset($entityData->propietario_codpersona)) {
            $propietario = $entityData->propietario;
            $propietario->tipo = Solicitud_model::TIPO_PERS_PROPIETARIO;
            $this->SavePersonaSolicitud($solicitudId, $empresaId, $propietario);
        }

       // Save Agente Auxiliar
        if (isset($entityData->agente)) {
            $agente = $entityData->agente;
            $agente->tipo = Solicitud_model::TIPO_PERS_AGENTE;
            $this->SavePersonaSolicitud($solicitudId, $empresaId, $agente);
        }
        $this->historialempresa_model->Save("La empresa fue asociada a una solicitud {$solicitudId}", 'update', $empresaId, $entityData->codigo_usuario_id);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
            $response->is_valid = true;
            $response->data = $solicitudId;
        }
        return $response;
    }

    private function SaveSolicitudInscripcion($empresaId, $usuarioAbtId) {
        $datosSolicitud= array(
            'fecha_registro' => CommonApp::DateNow(),
            'empresa_id' => $empresaId,
            'codigo_usuario_id' => $usuarioAbtId,
            'gestion' => date('Y'),
            'tipo_solicitud' => Solicitud_model::TIPO_INSCRIPCION,
            'estado' => Solicitud_model::ESTADO_PENDIENTE,
        );
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $this->table_name = $this->table_solicitud;

        $saveId = $this->insert_entity($datosSolicitud);
        $this->db->dbprefix = $auxPrefix;
        $this->table_name = $this->table_empresa;

        return $saveId;
    }

    private function UpdatePersonaSolicitud($personId, $entidadId, $dataObject) {
        $data = (array)$dataObject;
        $data['entidad_id'] = $entidadId;

        $auxPrefix = $this->db->dbprefix;
        $auxTableName = $this->table_name;
        $this->db->dbprefix = '';
        $this->table_name = $this->table_persona_tramite;

        $this->db->update($this->table_name, $data, ['codpersona' => $personId]);
        $this->db->dbprefix = $auxPrefix;
        $this->table_name = $auxTableName;
    }

    private function SaveUpdatePersonaSolicitud($solicitudId, $entidadId, $dataObject) {
        $data = (array)$dataObject;
        $data['entidad_id'] = $entidadId;
        $data['solicitud_id'] = $solicitudId;

        $auxPrefix = $this->db->dbprefix;
        $auxTableName = $this->table_name;
        $this->db->dbprefix = '';
        $this->table_name = $this->table_persona_tramite;
        if (isset($data['codpersona']) && !empty($data['codpersona'])) {
            $id = $data['codpersona'];
            unset($data['codpersona']);
            $this->db->update($this->table_name, $data, ['codpersona' => $id]);
        } else
            $this->insert_entity($data);

        $this->db->dbprefix = $auxPrefix;
        $this->table_name = $auxTableName;
    }

    private function SavePersonaSolicitud($solicitudId, $entidadId, $dataObject) {
        $data = (array)$dataObject;
        $data['entidad_id'] = $entidadId;
        $data['solicitud_id'] = $solicitudId;

        $auxPrefix = $this->db->dbprefix;
        $auxTableName = $this->table_name;
        $this->db->dbprefix = '';
        $this->table_name = $this->table_persona_tramite;

        $saveId = $this->insert_entity($data);
        $this->db->dbprefix = $auxPrefix;
        $this->table_name = $auxTableName;
    }

    private function SaveEmpresa($data) {
            $datosEmpresa = array(
                'razon_social_nombre' => $data->razon_social_nombre,
                'numero_fundempresa' => $data->numero_fundempresa,
                'nit' => $data->nit,
                'estado' => self::ESTADO_BORRADOR
            );
            // En el caso que exista codigo usuario
            if (isset($data->propietario_codpersona)) {
                $datosEmpresa['propietario_codpersona'] = $data->propietario_codpersona;
            }

            if (isset($data->registro_antiguo))
                $datosEmpresa['registro_antiguo'] = $data->registro_antiguo;

            $auxPrefix = $this->db->dbprefix;
            $this->db->dbprefix = '';
            $saveEmpresaId = $this->insert_entity($datosEmpresa);
            $this->db->dbprefix = $auxPrefix;

            $this->historialempresa_model->Save("Se ha registrado la empresa", 
                'create', $saveEmpresaId, $data->codigo_usuario_id);

            return $saveEmpresaId;
    }

    private function SaveSucursalesEmpresa($empresaId, $solicitudId, $usuarioId,$collection) {

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        
        foreach ($collection as $sucursal) {
            $this->table_name = $this->table_sucursal;
            $datosSucursal = array(
                'empresa_id' => $empresaId,
                'index_sucursal' => $sucursal->index_sucursal,
                'direccion' => $sucursal->direccion,
                'coordenada_x' => $sucursal->coordenada_x,
                'coordenada_y' => $sucursal->coordenada_y,
                'municipio_id' => $sucursal->municipio_id
            );
            if (!empty($sucursal->agente_registro))
                $datosSucursal['agente_registro'] = $sucursal->agente_registro;

            $saveId = $this->insert_entity($datosSucursal);
            foreach ($sucursal->representantes as $representante) {
                $representante->tipo = Solicitud_model::TIPO_PERS_REPRESENTANTE;
                $this->SavePersonaSolicitud($solicitudId, $saveId, $representante);
            }
            if (!isset($sucursal->actividades) || !empty($sucursal->actividades) && sizeof($sucursal->actividades) < 1)
                throw new Exception("Existe sucursal sin actividad, se requiere al menos una actividad para este proceso");

            $collectionActividades = $this->SaveActividadesInscripcion($saveId, $sucursal->actividades);
            $this->historialempresa_model->Save("Se ha agregado sucursal {$saveId}", 'add_sucursal', $empresaId, $usuarioId);
        }
        $this->db->dbprefix = $auxPrefix;
        $this->table_name = $this->table_empresa;
    }

    private function SaveActividadesInscripcion($sucursalId, $collection) {
        $actividades = $this->PrepararActividades($collection);
        foreach ($actividades as $actividad) {
            $data = array(
                'categoria_id' => $actividad['categoria_id'],
                'actividad_id' => $actividad['actividad_id'],
                'sucursal_id' => $sucursalId
            );
            if (isset($actividad['es_principal']))
                $data['esprincipal'] =  true;

            $this->SaveActividad($data);
        }
    }

    private function SaveActividad($data) {
        $auxPrefix = $this->db->dbprefix;
        $auxTableName = $this->table_name;
        $this->db->dbprefix = '';
        $this->table_name = $this->table_sucursal_has_actividad;

        $saveId = $this->insert_entity($data);
        $this->db->dbprefix = $auxPrefix;
        $this->table_name = $auxTableName;
    }

    /**
     * Metodo que ordena y obtiene la actividad de mayor ingreso 
     * de acuerdo a la configuracion entre las actividades y categoria
     * @param [type] $collection [description]
     */
    private function PrepararActividades($collection) {
        $actividades = array();
        $actividadMayor = new stdClass();
        foreach ($collection as $actividad) {
            $categoriaId = Categoria_Model::CATEGORIA_U;
            if (empty($actividad['categoria_id']))
                $actividad['categoria_id'] = $categoriaId;
                
            $precio = $this->GetPrecio($actividad['actividad_id'], $actividad['categoria_id']);
            if (empty($precio)) 
                throw new Exception("No existe configuracion de precio para la ActividadId={$actividad['actividad_id']}, Categoria={$actividad['categoria_id']}");
            $actividad['mont'] = $precio->monto;
            $actividad['key'] = Random::GenerarString(25, 'alpha-numeric');
            if (!isset($actividad->monto) || $actividad['monto'] > $actividad->monto ) {
                $actividadMayor = (object)$actividad;
            }
            array_push($actividades, $actividad);
        }
        $collectionOrdenado = array();
        foreach ($actividades as $object) {
            if ($object['key'] != $actividadMayor->key) {
                unset($object['key']);
                array_push($collectionOrdenado, $object);
            }
        }
        $actividadMayor->es_principal = true;
        unset($actividadMayor->key);
        array_push($collectionOrdenado, (array)$actividadMayor);
        return $collectionOrdenado;
    }

    private function GetPrecio($actividadId, $categoriaId) {
        $auxTableName = $this->table_name;
        
        $this->table_name = $this->table_precio_actividad_categoria;
        //TipoTramite_model
        $this->select(["*"]);
        $this->where('tipo_tramite', TipoTramite_model::TIPO_INSCRIPCION);
        $this->where('actividad_id', $actividadId);
        $this->where('categoria_id', $categoriaId);
        $precios = $this->find_all();
        if (isset($precios))
            return $precios[0];
        return null;
    }

    public function Update_Solicitud_Inscripcion($entityData) {
        $response = ResponseApp::Validate();

        $this->db->trans_begin();

        $persisted = $this->Get_SolicitudComplete($entityData->solicitud_id);

        if (empty($persisted) || $persisted->has_errors) {
            array_push($response->errors, $persisted->errors[0]);
            $response->has_errors = true;
        } else {

            // Save Empresa
            $persisted = $persisted->data;
            $entityData->empresa_id = $persisted->empresa_forestal_id;
            $solicitudId = $entityData->solicitud_id;
            $this->UpdateEmpresa($entityData, $persisted);

            // Save Solicitud 
            $this->UpdateSolicitudInscripcion($entityData->solicitud_id, $entityData->codigo_usuario_id);

            // Save Sucursales
            $this->SaveUpdateSucursalesEmpresa($entityData->empresa_id, $solicitudId, $entityData->codigo_usuario_id,$entityData->sucursales);

            // Save Propietario
            if (!isset($entityData->propietario_codpersona)) {
                $propietario = $entityData->propietario;
                $propietario->tipo = Solicitud_model::TIPO_PERS_PROPIETARIO;
                $this->SaveUpdatePersonaSolicitud($solicitudId, $entityData->empresa_id, $propietario);
            }

            $this->historialempresa_model->Save("La empresa fue modificada en la solicitud {$solicitudId}", 'update', $entityData->empresa_id, $entityData->codigo_usuario_id);
            $dataJson = json_encode($persisted);
            $this->historialempresa_model->Save("Las Datos Anteriores: {$dataJson}", 'json', $entityData->empresa_id, $entityData->codigo_usuario_id);

            if ($this->db->trans_status() === FALSE)
            {
                $this->db->trans_rollback();
            }
            else
            {
                $this->db->trans_commit();
                $response->is_valid = true;
            }

        }

        return $response;
    }

    private function UpdateEmpresa($data, $persisted) {

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $datosEmpresa = array(
            'razon_social_nombre' => $data->razon_social_nombre,
            'numero_fundempresa' => $data->numero_fundempresa,
            'nit' => $data->nit,
            'estado' => self::ESTADO_BORRADOR
        );
        // En el caso que exista codigo usuario
        if (isset($data->propietario_codpersona)) {
            $datosEmpresa['propietario_codpersona'] = $data->propietario_codpersona;
            if (!empty($persisted->propietario)) {
                $this->db->delete('abt_persona_tramite', 
                        ['codpersona' => $persisted->propietario->codpersona]);
            }
        }

        if (isset($data->registro_antiguo))
            $datosEmpresa['registro_antiguo'] = $data->registro_antiguo;

        $this->db->update($this->table_name, $datosEmpresa, ['empresa_forestal_id' => $data->empresa_id]);
        $saveEmpresaId = $this->insert_entity($datosEmpresa);
        $this->db->dbprefix = $auxPrefix;

        $this->historialempresa_model->Save("Se ha actualizado la empresa", 
            'update', $data->empresa_id, $data->codigo_usuario_id);

        return $saveEmpresaId;
    }

    private function UpdateSolicitudInscripcion($solicitudId, $usuarioAbtId) {
        $datosSolicitud= array(
            'fecha_registro' => CommonApp::DateNow(),
            'gestion' => date('Y'),
            'estado' => Solicitud_model::ESTADO_PENDIENTE,
        );
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $this->table_name = $this->table_solicitud;

        $this->db->update($this->table_name, $datosSolicitud, ['solicitud_tramite_empresa_id' => $solicitudId]);

        $this->historialsolicitud_model->Save("Se ha actualizado la solicitud", 'update', $solicitudId, $usuarioAbtId);
        $this->db->dbprefix = $auxPrefix;
        $this->table_name = $this->table_empresa;

    }

    private function SaveUpdateSucursalesEmpresa($empresaId, $solicitudId, $usuarioId, $collection) {

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $sucursalEditIds = [];
        foreach ($collection as $sucursal) {
            $this->table_name = $this->table_sucursal;
            $datosSucursal = array(
                'empresa_id' => $empresaId,
                'index_sucursal' => $sucursal->index_sucursal,
                'direccion' => $sucursal->direccion,
                'coordenada_x' => $sucursal->coordenada_x,
                'coordenada_y' => $sucursal->coordenada_y,
                'municipio_id' => $sucursal->municipio_id
            );
            if (!empty($sucursal->agente_registro))
                $datosSucursal['agente_registro'] = $sucursal->agente_registro;

            if (!empty($sucursal->empresa_sucursal_id)) {
                $saveId = $sucursal->empresa_sucursal_id;
                $this->db->update($this->table_name, $datosSucursal, ['empresa_sucursal_id' => $sucursal->empresa_sucursal_id]);
            } else 
                $saveId = $this->insert_entity($datosSucursal);
            array_push($sucursalEditIds, $saveId);

            foreach ($sucursal->representantes as $representante) {
                $representante->tipo = Solicitud_model::TIPO_PERS_REPRESENTANTE;
                if (empty($representante->codpersona))
                    $this->SavePersonaSolicitud($solicitudId, $saveId, $representante);
                else
                    $this->UpdatePersonaSolicitud($representante->codpersona, $saveId, $representante);
            }
            if (!isset($sucursal->actividades) || !empty($sucursal->actividades) && sizeof($sucursal->actividades) < 1)
                throw new Exception("Existe sucursal sin actividad, se requiere al menos una actividad para este proceso");

            $editIds = $this->SaveUpdateActividadesInscripcion($saveId, $sucursal->actividades);
            $this->historialempresa_model->Save("Se ha agregado/actualizado sucursal {$saveId}", 'add_sucursal', $empresaId, $usuarioId);
            if (sizeof($editIds) > 0) {
                $this->RemoveActividadesNoUsadas($editIds, $saveId);
            }
        }

        if (sizeof($sucursalEditIds) > 0) {
            $this->RemoveSucursalesNoUsadas($sucursalEditIds, $empresaId, $usuarioId);
        }

        $this->db->dbprefix = $auxPrefix;
        $this->table_name = $this->table_empresa;
    }

    private function SaveUpdateActividadesInscripcion($sucursalId, $collection) {
        $actividades = $this->PrepararActividades($collection);
        $actividadEditIds = [];
        foreach ($actividades as $actividad) {
            $data = array(
                'categoria_id' => $actividad['categoria_id'],
                'actividad_id' => $actividad['actividad_id'],
                'sucursal_id' => $sucursalId
            );
            if (isset($actividad['es_principal']))
                $data['esprincipal'] = true;

            $actividadSucId = null;
            if (!empty($actividad['sucursal_actividad_id'])) {
                $actividadSucId = $actividad['sucursal_actividad_id'];
            }
            $saveId = $this->SaveUpdateActividad($data, $actividadSucId);
            array_push($actividadEditIds, $saveId);
        }
        return $actividadEditIds;
    }

    private function RemoveSucursalesNoUsadas($ids, $empresaId, $usuarioId) {
        $auxPrefix = $this->db->dbprefix;
        $auxTableName = $this->table_name;
        $this->db->dbprefix = '';
        $this->table_name = $this->table_sucursal;

        $this->select([
            "{$this->table_name}.*"
        ]);
        $this->where_not_in('empresa_sucursal_id', $ids);
        $this->where('empresa_id', $empresaId);
        $list = parent::find_all();
        if (!empty($list)) {
            foreach ($list as $sucursalEmpresa) {
                // Obtener documento
                $this->RemoveActividadesNoUsadas([0], $sucursalEmpresa->empresa_sucursal_id);
                $this->db->delete($this->table_name, 
                    ['empresa_sucursal_id' => $sucursalEmpresa->empresa_sucursal_id]);

                $sucursalString = json_encode($sucursalEmpresa);
                $this->historialempresa_model->Save("Se ha eliminado Sucursal = {$sucursalString}", 
                    'remove', $data->empresa_id, $data->codigo_usuario_id);
            }
        }
        $this->db->dbprefix = $auxPrefix;
        $this->table_name = $auxTableName;
    }

    private function RemoveActividadesNoUsadas($ids, $sucursalId) {
        $auxPrefix = $this->db->dbprefix;
        $auxTableName = $this->table_name;
        $this->db->dbprefix = '';
        $this->table_name = $this->table_sucursal_has_actividad;

        $this->select([
            "{$this->table_name}.*"
        ]);
        $this->where_not_in('sucursal_actividad_id', $ids);
        $this->where('sucursal_id', $sucursalId);

        $list = parent::find_all();
        if (!empty($list)) {
            foreach ($list as $actividadSuc) {
                // Obtener documento
                $this->RemovetAllMovimientosByActividad($actividadSuc->sucursal_actividad_id);
                $this->RemovetAllDocumentsByActividad($actividadSuc->sucursal_actividad_id);
                $this->db->delete($this->table_name, ['sucursal_actividad_id' => $actividadSuc->sucursal_actividad_id]);
            }
        } 
        

        $this->db->dbprefix = $auxPrefix;
        $this->table_name = $auxTableName;
    }

    public function RemovetAllDocumentsByActividad($actividadSucId) {
        $auxTableName = $this->table_name;

        $this->table_name = 'abt_documento_presentado';
        $documentos = $this->find_all_by([
            'entidad_id' => $actividadSucId,
            'tipo' => 'A'
        ]);
        foreach ($documentos as $documento) {
            $this->db->delete($this->table_name, ['documento_presentado_id' => $documento->documento_presentado_id]);
            $pathFilename = config_item('path-upload-empresa') . $documento->nombre_file;
            if(file_exists($pathFilename))
                unlink($pathFilename);
        }
        $this->table_name = $auxTableName;
    }

    private function RemovetAllMovimientosByActividad($actividadSucId) {
        $auxTableName = $this->table_name;

        $this->table_name = 'abt_movimiento_producto';
        $this->db->delete($this->table_name, ['actividad_id' => $actividadSucId]);
        $this->table_name = $auxTableName;
    }

    private function SaveUpdateActividad($data, $actividadSucId) {
        $auxPrefix = $this->db->dbprefix;
        $auxTableName = $this->table_name;
        $this->db->dbprefix = '';
        $this->table_name = $this->table_sucursal_has_actividad;
        if (!empty($actividadSucId)) {
            $this->db->update($this->table_name, $data, ['sucursal_actividad_id' => $actividadSucId]);
            $saveId = $actividadSucId;
        } else 
            $saveId = $this->insert_entity($data);
        $this->db->dbprefix = $auxPrefix;
        $this->table_name = $auxTableName;
        return $saveId;
    }

    public function Get_Solicitud($solicitudId) {
        $response = ResponseApp::Entity();
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $this->select($this->GetFiedlsSolicitud());
        $this->join(
            $this->table_solicitud,
            "{{$this->table_solicitud}.empresa_id=empresa_forestal_id"
        );
        $solicitud = $this->find_by('solicitud_tramite_empresa_id', $solicitudId);
        $this->db->dbprefix = $auxPrefix;

        if (!isset($solicitud) || empty($solicitud)) {
            array_push($response->errors, "No existe solicitud con este ID={$solicitudId}");
            $response->has_errors = true;
        } else {
            $this->GetEstadoEmpresa($solicitud);
            $this->GetEstadoSolicitud($solicitud);
            $sucursales = $this->GetSucursales($solicitud->empresa_id, $solicitudId);
            if ($sucursales)
                $solicitud->sucursales = $sucursales;
            $response->data = $solicitud;
        }

        return $response;
    }

    public function Get_SolicitudComplete($solicitudId) {
        $response = ResponseApp::Entity();
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $this->select($this->GetFiedlsSolicitud());
        $this->join(
            $this->table_solicitud,
            "{{$this->table_solicitud}.empresa_id=empresa_forestal_id"
        );
        $solicitud = $this->find_by('solicitud_tramite_empresa_id', $solicitudId);
        $this->db->dbprefix = $auxPrefix;

        if (!isset($solicitud) || empty($solicitud)) {
            array_push($response->errors, "No existe solicitud con este ID={$solicitudId}");
            $response->has_errors = true;
        } else {
            $this->GetEstadoEmpresa($solicitud);
            $this->GetEstadoSolicitud($solicitud);
            // propietario 
            if (empty($solicitud->propietario_codpersona)) {
                $persona = $this->GetPropietarioSucursal($solicitudId, $solicitud->empresa_forestal_id);
                if (!empty($persona)) {
                    $solicitud->propietario = $persona;
                }
            } else {
                $peronaAbt = $this->personaabt_model->GetPersonaById($solicitud->propietario_codpersona);
                if (!empty($peronaAbt))
                    $solicitud->propietario = $peronaAbt;  
            }
            $sucursales = $this->GetSucursales($solicitud->empresa_id, $solicitudId);
            if ($solicitud->estado_sol == Solicitud_model::ESTADO_PENDIENTE_REVISION) {
                $solicitud->pago = $this->Get_Pago_Solicitud_By($solicitudId);
            }

            // Personas ABT representante
            if ($solicitud->estado_sol == Solicitud_model::ESTADO_ACEPTADO) {
                $this->LoadPersonaAbtRepresentantes($sucursales);
            }
            if ($sucursales)
                $solicitud->sucursales = $sucursales;

            if (isset($solicitud->es_aceptado)) {
                $resolucion = $this->Get_Resolucion($solicitudId);
                if ($resolucion)
                    $solicitud->resolucion = $resolucion;
            }

            $response->data = $solicitud;
        }

        return $response;
    }

    private function LoadPersonaAbtRepresentantes(&$sucursales) {
        foreach ($sucursales as $key => $sucursal) {
           $representantesAbt = $this->personaabt_model->GetPersonasRepresentanteById($sucursal->empresa_sucursal_id);
           if ($representantesAbt && sizeof($representantesAbt) > 0)
            $sucursal->representantes_abt = $representantesAbt;
        }
    }

    public function Get_Pago_Solicitud_By($solicitudId) {
        $auxTableName = $this->table_name;
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $this->table_name = 'abt_pago_solicitud';
        $pasoSolicitud = $this->find_by('solicitud_id', $solicitudId);
        $this->table_name = $auxTableName;
        $this->db->dbprefix = $auxPrefix;

        return $pasoSolicitud;
    }

    public function Get_Resolucion($solicitudId) {
        $auxTableName = $this->table_name;
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $this->table_name = 'abt_resolucion_empresa';
        $resolucion = $this->find_by('solicitud_id', $solicitudId);
        $this->table_name = $auxTableName;
        $this->db->dbprefix = $auxPrefix;
        if (!empty($resolucion)) {
            $resolucion->responsable = $this->funcionario_model->Get($resolucion->responsable_id);
        }

        return $resolucion;
    }

    public function Get_Sucursal_By($sucursalId) {
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $this->table_name = $this->table_sucursal;
        $sucursal = $this->find_by('empresa_sucursal_id', $sucursalId);
        $this->table_name = $this->table_empresa;
        $this->db->dbprefix = $auxPrefix;

        return $sucursal;
    }

    public function GetSucursales($empresaId, $solicitudId) {
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $this->table_name = $this->table_sucursal;
        
        $fields = <<<EOF
    	s.*,
        m.nombre as 'municipio',
        p.nombre as 'provincia',
        d.Nombre as 'departamento'	
EOF;
        $this->db->select($fields);
        
        $this->db->from("abt_sucursal_empresa s");
        $this->db->join('tec_municipio m', 'm.codmunicipio = s.municipio_id');
        $this->db->join('tec_provincia p', 'p.codprovincia = m.codprovincia');
        $this->db->join('departamento d', 'd.codDepartamento= p.codDepartamento');
        
        $this->group_by('s.empresa_id');
        $this->order_by('s.index_sucursal');
        $sucursales = $this->find_all_by('s.empresa_id', $empresaId); 
        
        $this->table_name = $this->table_empresa;
        $this->db->dbprefix = $auxPrefix;

        foreach ($sucursales as $sucursal) {
            $actividades = $this->GetActividadesSucursal($sucursal->empresa_sucursal_id);
            $representantes = $this->GetRepresentantesSucursal($sucursal->empresa_sucursal_id, $solicitudId);
            if (!empty($actividades))
                $sucursal->actividades = $actividades;
            if (!empty($representantes))
                $sucursal->representantes = $representantes;
            if (!empty($sucursal->agente_registro)) {
                $filter = new stdClass();
                $filter->numero_registro = $sucursal->agente_registro;
                $agentes = $this->agente_model->Filter_By($filter);
                if (!empty($agentes))
                    $sucursal->agente = $agentes[0];
            }
        }

        return $sucursales;
    }

    private function GetActividadesSucursal($sucursalId) {
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $actividadesArray = array();
        $this->table_name = $this->table_sucursal_has_actividad;
        $actividades = $this->find_all_by('sucursal_id', $sucursalId);

        if (!empty($actividades)) {
            foreach ($actividades as $actividadSucursal) {
                $actividad = $this->actividad_model->Get($actividadSucursal->actividad_id);
                $categoria = $this->categoria_model->Get($actividadSucursal->categoria_id);
                $unidad = $this->unidad_model->Get_By_Actividad($actividadSucursal->actividad_id);
                if ($actividad)
                    $actividadSucursal->actividad = $actividad;
                if ($categoria)
                    $actividadSucursal->categoria = $categoria;
                if ($unidad) {
                    $actividadSucursal->unidad = $unidad;
                    $movimiento = $this->GetMovimientoVolumen($actividadSucursal->actividad_id);
                    if (!empty($movimiento)) {
                        $actividadSucursal->producto_id = $movimiento->producto_id;
                    }
                    $actividadSucursal->especie_id = self::ESPECIE_ID_VARIOS;
                }
            }
        }

        $this->table_name = $this->table_empresa;
        $this->db->dbprefix = $auxPrefix;

        return $actividades;
    }

    private function GetMovimientoVolumen($actividadId) {
        $auxPrefix = $this->db->dbprefix;
        $tableName = $this->table_name;
        $this->db->dbprefix = '';
        $this->table_name = 'abt_moviento_volumen';
        $movimientoVolumen = $this->find_by(['actividad_id' => $actividadId]);

        $this->table_name = $tableName;
        $this->db->dbprefix = $auxPrefix;

        return $movimientoVolumen;
    }

    private function GetRepresentantesSucursal($sucursalId, $solicitudId) {

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $this->table_name = $this->table_persona_tramite;
        $representantes = $this->find_all_by(['entidad_id' => $sucursalId, 'solicitud_id' => $solicitudId, 'tipo' => 'R']);

        $this->table_name = $this->table_empresa;
        $this->db->dbprefix = $auxPrefix;

        return $representantes;
    }

    private function GetPropietarioSucursal($solicitudId, $empresaId) {
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $this->table_name = $this->table_persona_tramite;
        $this->select(["{$this->table_name}.*, concat(
            {$this->table_name}.nombres, ' ', 
            {$this->table_name}.appaterno, ' ',
            {$this->table_name}.apmaterno
        ) as nombre_apellido, pais.nombre as persona_pais"]);

        $this->join(
            "abt_pais pais",
            "pais.codpais={$this->table_name}.codpaisorigen"
        );
        $propietario = $this->find_by(['entidad_id' => $empresaId, 'solicitud_id' => $solicitudId, 'tipo' => 'P']);

        $this->table_name = $this->table_empresa;
        $this->db->dbprefix = $auxPrefix;

        return $propietario;
    }

    private function GetEstadoEmpresa(&$entidad) {
        if ($entidad->estado_emp == self::ESTADO_BORRADOR)
            $entidad->emp_es_borrador = true;
        if ($entidad->estado_emp == self::ESTADO_COMPLETADO)
            $entidad->emp_es_completado = true;
        if ($entidad->estado_emp == self::ESTADO_MODIFICADO)
            $entidad->emp_es_modificado = true;
    }

    private function GetEstadoSolicitud(&$solicitud) {
        if ($solicitud->estado_sol == Solicitud_model::ESTADO_PENDIENTE)
            $solicitud->es_pendiente = true;
        if ($solicitud->estado_sol == Solicitud_model::ESTADO_PENDIENTE_REVISION)
            $solicitud->es_pendiente_revision = true;
        if ($solicitud->estado_sol == Solicitud_model::ESTADO_ACEPTADO)
            $solicitud->es_aceptado = true;
        if ($solicitud->estado_sol == Solicitud_model::ESTADO_RECHAZADO)
            $solicitud->es_rechazado = true;
    }

    private function GetFiedlsSolicitud() {
        $fields = [
            "{$this->table_name}.*",
            "{$this->table_name}.estado estado_emp",
            "{$this->table_solicitud}.estado estado_sol",
            "{$this->table_solicitud}.*"
        ];
        return $fields;
    }


	/**
     * Set the select and join portions of the SQL query for the find* methods.
     *
     * @todo Set this in the before_find observer?
     *
     * @return void
     */
    protected function preFind()
    {
        if (empty($this->selects)) {
            $this->select(["{$this->table_name}.*", 'nombre']);
        }
    }
	
}