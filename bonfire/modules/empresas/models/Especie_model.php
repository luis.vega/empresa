<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * User Model.
 *
 * The central way to access and perform CRUD on users.
 *
 * @package Bonfire\Modules\Empresas\Models\especie_model
 * @author  Luis Vega
 */
class Especie_model extends BF_Model {

	
    protected $table_name = 'tec_especiencomun';
	protected $table_especie = 'tec_especie';


    public function Get_List() {
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        
        $this->join(
            $this->table_especie,
            "{$this->table_especie}.codespecie={$this->table_name}.codespecie"
        );
        $this->select([
            "{$this->table_name}.codespeciencomun",
            "{$this->table_name}.ncomun",
            "{$this->table_especie}.ncientifico "
        ]);
        $list = $this->find_all();

        $this->db->dbprefix = $auxPrefix;

        return $list;
    }

    public function Get_Like($descripcion) {
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        
        $this->join(
            $this->table_especie,
            "{$this->table_especie}.codespecie={$this->table_name}.codespecie"
        );
        $this->select([
            "{$this->table_name}.codespeciencomun",
            "{$this->table_name}.ncomun",
            "{$this->table_especie}.ncientifico "
        ]);
        $descripcion = strtoupper($descripcion);
        $filters = [
            "UPPER(CONCAT({$this->table_name}.ncomun, ' (', {$this->table_especie}.ncientifico,')'))=" => $descripcion
        ];
        $list = parent::find_all_by($filters);
        $this->db->dbprefix = $auxPrefix;
        if (!empty($list) && sizeof($list) > 0)
            return $list[0];

        return false;
    }

    public function Get($id) {

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $this->join(
            $this->table_especie,
            "{$this->table_especie}.codespecie={$this->table_name}.codespecie"
        );
        $this->select([
            "{$this->table_name}.codespeciencomun",
            "{$this->table_name}.ncomun",
            "{$this->table_especie}.ncientifico "
        ]);
        $categoria = parent::find_by('codespeciencomun', $id);
        $this->db->dbprefix = $auxPrefix;

        return $categoria;
    }
	
}