<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * User Model.
 *
 * The central way to access and perform CRUD on users.
 *
 * @package Bonfire\Modules\Empresas\Models\tipoActividad_model
 * @author  Luis Vega
 */
class Clasificacion_model extends BF_Model {

	const CARBON_VEGETAL = 1; 
	const CASTANIA = 2; 
	const DURMIENTES = 3; 
	const FLITCH = 4; 
	const LAMINAS = 5; 
	const LENIA = 6; 
	const MADERA_ASERRADA = 7; 
	const MADERA_EN_TROZA = 8; 

	public function Get_List() {

		$listTrimestres = array();
		$tipoCarbonVegetal = new stdClass();
		$tipoCarbonVegetal->id = self::CARBON_VEGETAL; 
		$tipoCarbonVegetal->nombre = 'CARBON_VEGETAL'; 

		$tipoCastania = new stdClass();
		$tipoCastania->id = self::CASTANIA; 
		$tipoCastania->nombre = 'CASTAÑA'; 

		$tipoDurmientes = new stdClass();
		$tipoDurmientes->id = self::DURMIENTES; 
		$tipoDurmientes->nombre = 'DURMIENTES'; 

		$tipoFlitch = new stdClass();
		$tipoFlitch->id = self::FLITCH; 
		$tipoFlitch->nombre = 'FLITCH'; 

		$tipoLaminas = new stdClass();
		$tipoLaminas->id = self::LAMINAS; 
		$tipoLaminas->nombre = 'LAMINAS'; 

		$tipoLenia = new stdClass();
		$tipoLenia->id = self::LENIA; 
		$tipoLenia->nombre = 'LEÑA'; 

		$tipoMaderaAserrada = new stdClass();
		$tipoMaderaAserrada->id = self::MADERA_ASERRADA; 
		$tipoMaderaAserrada->nombre = 'MADERA_ASERRADA'; 

		$tipoMaderaTroza = new stdClass();
		$tipoMaderaTroza->id = self::MADERA_EN_TROZA; 
		$tipoMaderaTroza->nombre = 'MADERA_EN_TROZA'; 

		array_push($tipoCarbonVegetal,  $tipoCastania, 
			$tipoDurmientes, $tipoFlitch, $tipoLaminas, $tipoLenia, $tipoMaderaAserrada, $tipoMaderaTroza);

		return $listTrimestres;
	}
	
}