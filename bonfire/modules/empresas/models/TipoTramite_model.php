<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * User Model.
 *
 * The central way to access and perform CRUD on users.
 *
 * @package Bonfire\Modules\Empresas\Models\tipotramite_model
 * @author  Luis Vega
 */
class TipoTramite_model extends BF_Model {

	const TIPO_INSCRIPCION = 'I'; 
	const TIPO_REINSCRIPCION = 'R';	
}