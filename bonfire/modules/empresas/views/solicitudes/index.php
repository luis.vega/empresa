<?php

$errorClass   = empty($errorClass) ? ' error' : $errorClass;
$controlClass = empty($controlClass) ? 'span6' : $controlClass;
$fieldData = array(
    'errorClass'    => $errorClass,
    'controlClass'  => $controlClass,
);

// Recursos JS
Assets::add_module_js('empresas', 'js/solicitudes/index.js');

?>

<div class="col-md-">
	<h1 class="page-header">Panel Solicitudes</h1>
	<form class="form">
  		<div class="form-group">
            <a href="<?php echo site_url(); ?>empresas/registro" class="btn btn-primary">
				<span class="glyphicon glyphicon-plus"></span>
            	Nueva Solicitud
	        </a>
  		</div>
		<div class="panel panel-default">
			<div class="panel-heading panel-tabs">

                    <div class="col-md-3">
                    	<h3 class="panel-title hidden-xs">Lista de Solicitudes</h3>
                    	<h5 class="panel-title visible-xs">Lista de Solicitudes</h5>
                    </div>
                    <div class="col-md-8">
						<ul class="nav nav-tabs pull-right hidden-xs">
                            <li class="active">
                                <a href="#tab-pendientes" data-toggle="tab">
                                    <span class="glyphicon btn-glyphicon glyphicon-warning-sign img-circle"></span>
                                    Pendiente
                                </a>
                            </li>
                            <li>
                                <a href="#tab-en-revision" data-toggle="tab">
                                    <span class="glyphicon btn-glyphicon glyphicon-edit img-circle"></span>
                                    En Revision
                                </a>
                            </li>
                            <li>
	                        	<a href="#tab-aceptados" data-toggle="tab">
	                        	 	<span class="glyphicon btn-glyphicon glyphicon-thumbs-up img-circle"></span>
	                        		Aceptado
	                        	</a>
	                        </li>
                            <li>
	                        	<a href="#tab-rechazados" data-toggle="tab">
	                        		<span class="glyphicon btn-glyphicon glyphicon-thumbs-down"></span>
	                        		 Rechazado
	                        	</a>
	                        </li>
	                    </ul>
                    </div>
			</div>
			 <div class="panel-body">
	            <div class="tab-content">
	                <div class="tab-pane fade in active" id="tab-pendientes">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Registro</th>
                                <th>Gestion</th>
                                <th>Raz&oacute;n social</th>
                                <th>N° NIT</th>
                                <th>N° FUNDEMPRESA</th>
                                <th>Propietario</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($solicitudesPendientes as $solicitud) : ?>
                                <tr>
                                    <th scope="row"><?php echo $solicitud->solicitud_tramite_empresa_id?></th>
                                    <td><?php echo $solicitud->fecha_registro?></td>
                                    <td><?php echo $solicitud->gestion?></td>
                                    <td><?php echo $solicitud->empresa_nombre?></td>
                                    <td><?php echo $solicitud->empresa_nit?></td>
                                    <td><?php echo $solicitud->empresa_numero_fundempresa?></td>
                                    <td><?php echo $solicitud->empresa_propietario?></td>
                                    <td>
                                        <a class="btn btn-default" href="/empresas/update_registro/<?php echo $solicitud->solicitud_tramite_empresa_id?>" role="button">Completar Solicitud <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
	                </div>
                    <div class="tab-pane fade" id="tab-en-revision">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Registro</th>
                                <th>Gestion</th>
                                <th>Raz&oacute;n social</th>
                                <th>N° NIT</th>
                                <th>N° FUNDEMPRESA</th>
                                <th>Propietario</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($solicitudesPendientesRevision as $solicitud) : ?>
                                <tr>
                                    <th scope="row"><?php echo $solicitud->solicitud_tramite_empresa_id?></th>
                                    <td><?php echo $solicitud->fecha_registro?></td>
                                    <td><?php echo $solicitud->gestion?></td>
                                    <td><?php echo $solicitud->empresa_nombre?></td>
                                    <td><?php echo $solicitud->empresa_nit?></td>
                                    <td><?php echo $solicitud->empresa_numero_fundempresa?></td>
                                    <td><?php echo $solicitud->empresa_propietario?></td>
                                    <td>
                                        <a class="btn btn-default" href="/solicitudes/ver-comprobante/<?php echo $solicitud->solicitud_tramite_empresa_id?>" role="button">Ver Comprobante <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="tab-aceptados">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Registro</th>
                                <th>Gestion</th>
                                <th>Raz&oacute;n social</th>
                                <th>N° NIT</th>
                                <th>N° FUNDEMPRESA</th>
                                <th>Propietario</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($solicitudesAprobados as $solicitud) : ?>
                                <tr>
                                    <th scope="row"><?php echo $solicitud->solicitud_tramite_empresa_id?></th>
                                    <td><?php echo $solicitud->fecha_registro?></td>
                                    <td><?php echo $solicitud->gestion?></td>
                                    <td><?php echo $solicitud->empresa_nombre?></td>
                                    <td><?php echo $solicitud->empresa_nit?></td>
                                    <td><?php echo $solicitud->empresa_numero_fundempresa?></td>
                                    <td><?php echo $solicitud->empresa_propietario?></td>
                                    <td>
                                        <a class="btn btn-default" href="/solicitudes/ver-solicitud/<?php echo $solicitud->solicitud_tramite_empresa_id?>" role="button">Ver Solicitud <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="tab-rechazados">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Registro</th>
                                <th>Gestion</th>
                                <th>Raz&oacute;n social</th>
                                <th>N° NIT</th>
                                <th>N° FUNDEMPRESA</th>
                                <th>Propietario</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($solicitudesRechazados as $solicitud) : ?>
                                <tr>
                                    <th scope="row"><?php echo $solicitud->solicitud_tramite_empresa_id?></th>
                                    <td><?php echo $solicitud->fecha_registro?></td>
                                    <td><?php echo $solicitud->gestion?></td>
                                    <td><?php echo $solicitud->empresa_nombre?></td>
                                    <td><?php echo $solicitud->empresa_nit?></td>
                                    <td><?php echo $solicitud->empresa_numero_fundempresa?></td>
                                    <td><?php echo $solicitud->empresa_propietario?></td>
                                    <td>
                                        <a class="btn btn-default" href="/empresas/update_registro/<?php echo $solicitud->solicitud_tramite_empresa_id?>" role="button">Ver Solicitud <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
	            </div>
			</div>
		</div>
	</form>
</div>
