<table class="table table-hover">
	<thead>
		<tr>
			<th></th>
			<th></th>
			<th>Aprobar / Rechazar</th>
			<th>Observación</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th scope="row"><label class="control-label">Empresa ID</label></th>
			<td><?php echo $empresa->empresa_forestal_id ?></td>
			<td>
				<label class="radio-inline">
				  <input type="radio" name="inlineRadioOptions_a_1" id="inlineRadio1" value="option1"> Aprobado
				</label>
				<label class="radio-inline">
				  <input type="radio" name="inlineRadioOptions_a_1" id="inlineRadio2" value="option2"> Rechazado
				</label>
			</td>
			<td></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">Razón Social</label></th>
			<td><?php echo $empresa->razon_social_nombre ?></td>
			<td>
				<label class="radio-inline">
				  <input type="radio" name="inlineRadioOptions_a_2" id="inlineRadio1" value="option1"> Aprobado
				</label>
				<label class="radio-inline">
				  <input type="radio" name="inlineRadioOptions_a_2" id="inlineRadio2" value="option2"> Rechazado
				</label>
			</td>
			<td></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">Nit</label></th>
			<td><?php echo $empresa->nit ?></td>
			<td>
				<label class="radio-inline">
				  <input type="radio" name="inlineRadioOptions_a_3" id="inlineRadio1" value="option1"> Aprobado
				</label>
				<label class="radio-inline">
				  <input type="radio" name="inlineRadioOptions_a_3" id="inlineRadio2" value="option2"> Rechazado
				</label>
			</td>
			<td></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">Fundempresa Nro.</label></th>
			<td><?php echo $empresa->numero_fundempresa ?></td>
			<td>
				<label class="radio-inline">
				  <input type="radio" name="inlineRadioOptions_a_4" id="inlineRadio1" value="option1"> Aprobado
				</label>
				<label class="radio-inline">
				  <input type="radio" name="inlineRadioOptions_a_4" id="inlineRadio2" value="option2"> Rechazado
				</label>
			</td>
			<td>El numero de registro no corresponde a la empresa</td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">Registro</label></th>
			<td><?php echo $empresa->registro_empresa ?></td>
			<td>
				<label class="radio-inline">
				  <input type="radio" name="inlineRadioOptions_a_5" id="inlineRadio1" value="option1"> Aprobado
				</label>
				<label class="radio-inline">
				  <input type="radio" name="inlineRadioOptions_a_5" id="inlineRadio2" value="option2"> Rechazado
				</label>
			</td>
			<td></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">Habilitado</label></th>
			<td><?php echo $empresa->habilitado ?></td>
			<td>
				<label class="radio-inline">
				  <input type="radio" name="inlineRadioOptions_a_6" id="inlineRadio1" value="option1"> Aprobado
				</label>
				<label class="radio-inline">
				  <input type="radio" name="inlineRadioOptions_a_6" id="inlineRadio2" value="option2"> Rechazado
				</label>
			</td>
			<td></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">Estado</label></th>
			<td><?php echo $empresa->estado ?></td>
			<td>
				<label class="radio-inline">
				  <input type="radio" name="inlineRadioOptions_a_7" id="inlineRadio1" value="option1"> Aprobado
				</label>
				<label class="radio-inline">
				  <input type="radio" name="inlineRadioOptions_a_7" id="inlineRadio2" value="option2"> Rechazado
				</label>
			</td>
			<td></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">Propietario Cod.</label></th>
			<td><?php echo $empresa->propietario_codpersona ?></td>
			<td>
				<label class="radio-inline">
				  <input type="radio" name="inlineRadioOptions_a_8" id="inlineRadio1" value="option1"> Aprobado
				</label>
				<label class="radio-inline">
				  <input type="radio" name="inlineRadioOptions_a_8" id="inlineRadio2" value="option2"> Rechazado
				</label>
			</td>
			<td></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">Propietario</label></th>
			<td><?php echo $empresa->empresa_propietario ?></td>
			<td>
				<label class="radio-inline">
				  <input type="radio" name="inlineRadioOptions_a_9" id="inlineRadio1" value="option1"> Aprobado
				</label>
				<label class="radio-inline">
				  <input type="radio" name="inlineRadioOptions_a_9" id="inlineRadio2" value="option2"> Rechazado
				</label>
			</td>
			<td></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">Telefonos</label></th>
			<td><?php  echo $empresa->telefono . '- ' . $empresa->celular?></td>
			<td>
				<label class="radio-inline">
				  <input type="radio" name="inlineRadioOptions_a_10" id="inlineRadio1" value="option1"> Aprobado
				</label>
				<label class="radio-inline">
				  <input type="radio" name="inlineRadioOptions_a_10" id="inlineRadio2" value="option2"> Rechazado
				</label>
			</td>
			<td></td>
		</tr>
	</tbody>
</table>