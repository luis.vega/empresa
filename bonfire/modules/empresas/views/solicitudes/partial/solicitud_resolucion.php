<br><br>
<div class="row">
	<div class="col-md-1"></div>
	<label>Imprimir:</label>
	<button class="btn print-comprobante">
		<span class="glyphicon glyphicon-print"></span>
	</button>
</div><br>
<div class="row">
	<div class="col-md-1"></div>
	<div class="col-md-10 resolucion-admin">
		<p class="bold">RESOLUCIÓN ADMINISTRATIVA</p>
		<p class="bold">AUTORIDAD DE FISCALIZACIÓN Y CONTROL SOCIAL DE BOSQUES Y TIERRA</p>
		<p class="bold">DIRECCIÓN DEPARTAMENTAL SANTA CRUZ</p>
		<p class="bold">RD-ABT-DDSC-REF-00102-2018</p>
		<p class="bold">Santa Cruz, <?php echo $resolucion->day; ?> de <?php echo $resolucion->month_literal; ?> de <?php echo $resolucion->year; ?></p>

		<hr>

		<p class="bold">VISTOS Y CONSIDERANDO:</p>
		<div class="text-content">
			<p>Que, la Ley Forestal No. 1700 del 12 de julio de 1996 establece: Para el otorgamiento y vigencia de la </p>
			<p>autorización de funcionamiento de centros de procesamiento primario de productos forestales se deberá </p>
			<p>presentar y actualizar anualmente un Programa de Abastecimiento y Procesamiento de Materia Prima</p>
			<p><strong>(PAPMP)</strong>, en el que se especifiquen las fuentes y cantidades a utilizar, las que necesariamente deberán </p>
			<p>proceder de bosques manejados, salvo los casos de desmonte debidamente autorizados. Dicha autorización </p>
			<p>constituye una licencia administrativa cuya contravención da lugar a la suspensión temporal o cancelación </p>
			<p>definitiva de las actividades, sin perjuicio de las sanciones civiles y penales a que hubiese lugar. </p>
		</div>
		<br>
		<div class="text-content">
			<p>Que, visto el <strong>PAPMP</strong>, se sugiere la aprobación del programa de abastecimiento y procesamiento de materia </p>
			<p>2018 (PAPMP-2018). El interesado cancela el valor de la reinscripción mediante boleta de pago Nº71845094 </p>
			<p>de fecha 09/02/2018</p>
		</div>
		<br>
		<div class="text-content">
			<p>Que la Resolución Ministerial N° 134/97, resuelve aprobar la Norma Técnica sobre PAPMP, en la cual se </p>
			<p>detallan los Requisitos técnicos mínimos para la presentación de los mismos.</p>
		</div>
		<br>
		<div class="text-content">
			<p>La Información presentada en su Programa de Abastecimiento y Procesamiento de Materia Prima por el</p>
			<p>Profesional Responsable y Propietario y/o Rep. Legal de la empresa se considera declaración jurada que se </p>
			<p>detallas a continuación:</p>

			<?php 
				$sucursalMatriz = $solicitud->sucursales[0];
			?>
			<table width="100%" border="1" class="table-info">
				<tbody>
					<tr>
						<td colspan="5"><p> <strong>RAZON SOCIAL:</strong> <?php echo strtoupper($solicitud->razon_social_nombre); ?></p> </td>
						<td><p><strong>REGISTRO:</strong> <br> <br><?php echo $solicitud->registro_empresa; ?></p></td>
					</tr>
					<tr>
						<td colspan="5"><p> <strong>TRAMITE:</strong> 
							<?php echo strtoupper($solicitud_tipos[$solicitud->tipo_solicitud]); ?></p> 
						</td>
						<td><p><strong>GESTIÓN:</strong> <?php echo $solicitud->gestion; ?></p></td>
					</tr>
					<tr>
						<td colspan="5"><p> <strong>PROPIETARIO:</strong> 
							<?php echo strtoupper($solicitud->propietario->nombre_apellido); ?></p> 
						</td>
						<td><p><strong>C.I.: </strong> <?php echo $solicitud->propietario->nrodocid . ' ' . $solicitud->propietario->emitido; ?></p></td>
					</tr>
					<tr class="text-center">
						<td><strong>ACTIVIDAD</strong></td>
						<td><strong>CATEGORIA</strong></td>
						<td><strong>UBICACION</strong></td>
						<td><strong>MUNICIPIO</strong></td>
						<td><strong>REP. LEGAL</strong></td>
						<td><strong>AGENTE AUXILIAR</strong></td>
					</tr>
					<?php 
						foreach ($solicitud->sucursales as $keySuc => $sucursal) : 
							$sucName = "Sucursal " . ($keySuc + 1);
					?>
						
						<?php 
							$RepresentantesStrng = "";
							foreach ($sucursal->representantes_abt as $representanteAbt) {
								$RepresentantesStrng .= $representanteAbt->nombre_apellido . '; C.I. ' . $representanteAbt->nrodocid . ' ' . $representanteAbt->emitido;
							}
							$agenteName = "-";
							if (!empty($sucursal->agente)) 
								$agenteName = strtoupper($sucursal->agente->agente_npm);
						?>
						<?php foreach($sucursal->actividades as $actividadSuc) :?>
							<tr>
								<td><p><?php echo $actividadSuc->actividad->descripcion ?></p></td>
								<td class="text-center"><p>"<?php echo $actividadSuc->categoria->nombre; ?>"</p></td>
								<td class="text-center"><p><?php echo $sucursal->coordenada_x ?>;<?php echo $sucursal->coordenada_y ?></p></td>
								<td class="text-center"><p><?php echo strtolower($sucursal->municipio); ?></p></td>
								<td style="font-size: 13px;"><p><?php echo strtoupper($RepresentantesStrng); ?></p></td>
								<td style="font-size: 14px;"><p><?php echo $agenteName; ?></p></td>
							</tr>
						<?php endforeach; ?>
					<?php endforeach; ?>
				</tbody>
			</table><br>
			<p class="bold">POR TANTO:</p>
			<p>El suscrito Director/ Responsable de UOBT/Responsable de …………………….., de la Autoridad de Fiscalización y </p>
			<p>Control Social de Bosques y Tierra (ABT) en uso de las facultades que le confiere el Régimen Forestal de La </p>
			<p>Nación, y al amparo de lo que establece el D.S. 071/2009 Art. 31 concordante D.S. 29894/2009 Art. 144.</p>
			<p class="bold">RESUELVE:</p>
			<p><strong>PRIMERO</strong>: Aprobar en todos sus términos; la <strong>
				<?php echo strtolower($solicitud_tipos[$solicitud_info->tipo_solicitud]); ?>
			</strong>, del Programa de Abastecimiento y </p>
			<p>Procesamiento de Materia prima gestión 2018 (PAPMP-2018), conforme lo establecido en los </p>
			<p>de la ficha técnica (FO-26) descrito anteriormente, aplicando y reconociendo para ello, la Fe pública que </p>
			<p>confiere el artículo 27° de la Ley Forestal Nº 1700, estableciendo responsabilidad civil y penal por la veracidad </p>
			<p>y cabalidad de la información contenida.</p>
			<p><strong>SEGUNDO</strong>: Aprobar la ficha técnica (FO-26) de cada actividad del documento objeto de la presente </p>
			<p>Resolución Administrativa, que sin insertarse forma parte indisoluble de la misma. La licencia de </p>
			<p>funcionamiento es válida hasta el 31 de diciembre de 2018; vencida la mencionada licencia se tendrá por </p>
			<p>caducada la misma de pleno derecho sin necesidad de manifestación expresa de la Autoridad Administrativa.</p>
			<p><strong>TERCERO</strong>: El Propietario, Representante Legal, y el Profesional Responsable obligatoriamente deben </p>
			<p>presentar, los Informes Trimestrales establecidos (R.M. 134/97). Conminar a los titulares de la empresa </p>
			<p>forestal  efectivizar el control de ingreso y egreso del producto forestal con los documentos de respaldo </p>
			<p>(CFO_D), en caso de incumplimiento se procederá al decomiso, según Ley Forestal 1700 y la Norma Técnica</p>
			<p>Nº 134/97.</p>
			<p>Es dado en el despacho del Director/ Responsable de UOBT/Responsable de ………ABT.</p>
			<p><strong>Regístrese, Comuníquese y archívese.</strong></p>
		</div>
	</div>
	<div class="col-md-1"></div>
</div>