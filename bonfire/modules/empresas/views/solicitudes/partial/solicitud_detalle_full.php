<h3>Datos Generales Empresa</h3>
<table class="table table-hover">
	<tbody>
		<tr>
			<th scope="row"><label class="control-label">Razón Social</label></th>
			<td><?php echo $empresa_info->razon_social_nombre ?></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">N° NIT</label></th>
			<td><?php echo $empresa_info->nit ?></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">N° FUNDEMPRESA</label></th>
			<td><?php echo $empresa_info->numero_fundempresa ?></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">Registro</label></th>
			<td><?php echo $empresa_info->registro_empresa ?></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">Registro Antiguo</label></th>
			<td><?php echo $empresa_info->registro_antiguo  ?></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">Habilitado</label></th>
			<td><?php echo $empresa_info->habilitado ?></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">Propietario Cod.</label></th>
			<td><?php echo $empresa_info->propietario_codpersona ?></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">Propietario</label></th>
			<td><?php echo $empresa_info->propietario_nombre_apellido ?></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">Propietario Pais</label></th>
			<td><?php echo $empresa_info->propietario_pais ?></td>
		</tr>
		<tr>
			<th scope="row"><label class="control-label">Telefonos</label></th>
			<td><?php  echo $empresa_info->telefono . '- ' . $empresa_info->celular?></td>
		</tr>
		
		<?php /*Se muestran los documentos por fila, para que se ven los registros vacios*/?>
			
		<tr>
			<th scope="row"><label class="control-label">Registro biométrico NIT</label></th>
			<?php if(!isset($solicitud->documentos[0]->doc_presentado)):?>
			<td colspan="3">No hay documento presentado</td>
			<?php else:?>
			<td><a href="/api/file/download_file_empresa/<?php echo $solicitud->documentos[0]->doc_presentado->nombre_file ?>">Descargar Archivo</a></td>
			<td>
				<label class="radio-inline"> 
				  <input class="btn-document-moderation" type="radio" name="<?php echo $solicitud->documentos[0]->doc_presentado->documento_presentado_id  ?>" value="aprobado"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
				</label>
				<label class="radio-inline" >
				  <input class="btn-document-moderation" type="radio" name="<?php echo $solicitud->documentos[0]->doc_presentado->documento_presentado_id  ?>" value="rechazado"> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				</label>
			</td>
			<td><b>Observación:</b> -</td>
			<?php endif;?>
		</tr>
		
		<tr>
			<th scope="row"><label class="control-label">Registro de FUNDEMPRESA</label></th>
			<?php if(!isset($solicitud->documentos[1]->doc_presentado)):?>
			<td colspan="3">No hay documento presentado</td>
			<?php else:?>
			<td><a href="/api/file/download_file_empresa/<?php echo $solicitud->documentos[1]->doc_presentado->nombre_file ?>">Descargar Archivo</a></td>
			<td>
				<label class="radio-inline">
				  <input class="btn-document-moderation" type="radio" name="<?php echo $solicitud->documentos[1]->doc_presentado->documento_presentado_id  ?>" value="aprobado"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
				</label>
				<label class="radio-inline">
				  <input class="btn-document-moderation" type="radio" name="<?php echo $solicitud->documentos[1]->doc_presentado->documento_presentado_id  ?>" value="rechazado"> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				</label>
			</td>
			<td><b>Observación:</b> -</td>
			<?php endif;?>
		</tr>
		
				<tr>
			<th scope="row"><label class="control-label">Fotocopia de documento de identidad vigente propietario</label></th>
			<?php if(!isset($solicitud->documentos[2]->doc_presentado)):?>
			<td colspan="3">No hay documento presentado</td>
			<?php else:?>
			<td><a href="/api/file/download_file_empresa/<?php echo $solicitud->documentos[2]->doc_presentado->nombre_file ?>">Descargar Archivo</a></td>
			<td>
				<label class="radio-inline">
				  <input class="btn-document-moderation" type="radio" name="<?php echo $solicitud->documentos[2]->doc_presentado->documento_presentado_id  ?>" value="aprobado"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
				</label>
				<label class="radio-inline">
				  <input class="btn-document-moderation" type="radio" name="<?php echo $solicitud->documentos[2]->doc_presentado->documento_presentado_id  ?>" value="rechazado"> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				</label>
			</td>
			<td><b>Observación:</b> -</td>
			<?php endif;?>
		</tr>
		
		<?php /*Si no estan los documentos no se muestran las 3 filas*/?>
		<?php /*if(isset($solicitud->documentos)) foreach ($solicitud->documentos as $documento):?>
		<tr>
			<th scope="row"><label class="control-label"><?php echo $documento->nombre?></label></th>
			<?php if(!isset($documento->doc_presentado)):?>
			<td colspan="3">No hay documento presentado</td>
			<?php else:?>
			<td><a href="/api/file/download_file_empresa/<?php echo $documento->doc_presentado->nombre_file ?>">Descargar Archivo</a></td>
			<td>
				<label class="radio-inline">
				  <input class="btn-document-moderation" type="radio" name="<?php echo $documento->doc_presentado->documento_presentado_id  ?>" value="aprobado"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
				</label>
				<label class="radio-inline">
				  <input class="btn-document-moderation" type="radio" name="<?php echo $documento->doc_presentado->documento_presentado_id  ?>" value="rechazado"> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				</label>
			</td>
			<td><b>Observación:</b> -</td>
			<?php endif;?>
		</tr>
		<?php endforeach;*/?>
		
	</tbody>
</table>
		<?php 
			/*echo "<pre>"; 
			print_r($sucursal);
			echo "<pre";*/
		?>
<?php if(isset($solicitud->sucursales)) foreach ($solicitud->sucursales as $sucursal):?>
		<?php 
			/*echo "<pre>"; 
			print_r($sucursal);
			echo "<pre";*/
		?>
<h3><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span>  Sucursal</h3>
<table class="table table-hover">
<tbody>
	<tr>
		<th scope="row" width="300"><label class="control-label">Direccion</label></th>
		<td><?php echo $sucursal->direccion ?></td>
		<td colspan="4"></td>
	</tr>
	<tr>
		<th scope="row"><label class="control-label">Departamento</label></th>
		<td><?php echo ucwords(strtolower($sucursal->departamento)) ?></td>
		<th><label class="control-label">Provincia</label></th>
		<td><?php echo ucwords(strtolower($sucursal->provincia)) ?></td>
		<th><label class="control-label">Municio</label></th>
		<td><?php echo ucwords(strtolower($sucursal->municipio)) ?></td>
	</tr>
	<tr>
		<th scope="row"><label class="control-label">Ubicación X_Coord; Y_Coord</label></th>
		<td>(<?php echo $sucursal->coordenada_x .','. $sucursal->coordenada_y ?>)</td>
		<td colspan="4"></td>
	</tr>
	<tr>
		<th scope="row"><label class="control-label">Croquis de ubicación X_Coord; Y_Coord</label></th>
		<?php if(!isset($sucursal->documentos[0]->doc_presentado)):?>
		<td>No hay documento presentado</td>
		<?php else:?>
		<td><a href="/api/file/download_file_empresa/<?php echo $sucursal->documentos[0]->doc_presentado->nombre_file ?>">Descargar Archivo</a></td>
		<td>
			<label class="radio-inline">
			  <input class="btn-document-moderation" type="radio" name="<?php echo $sucursal->documentos[0]->doc_presentado->documento_presentado_id  ?>" value="aprobado"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
			</label>
			<label class="radio-inline">
			  <input class="btn-document-moderation" type="radio" name="<?php echo $sucursal->documentos[0]->doc_presentado->documento_presentado_id  ?>" value="rechazado"> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
			</label>
		</td>
		<td><b>Observación:</b> -</td>
		<?php endif;?>
	</tr>
	
	<?php if(isset($sucursal->agente)):?>
	<tr>
		<th scope="row"><label class="control-label">Agente</label></th>
		<td><?php echo $sucursal->agente->agente_npm .' - '. $sucursal->agente->registro?></td>
		<td colspan="4"></td>
	</tr>
	<tr>
		<th scope="row"><label class="control-label">Contrato de prestación de servicios(Agente Auxiliar y propietario)</label></th>
		<?php if(!isset($sucursal->agente_documentos[0]->doc_presentado)):?>
		<td>No hay documento presentado</td>
		<?php else:?>
		<td><a href="/api/file/download_file_empresa/<?php echo $sucursal->agente_documentos[0]->doc_presentado->nombre_file ?>">Descargar Archivo</a></td>
		<td>
			<label class="radio-inline">
			  <input class="btn-document-moderation" type="radio" name="<?php echo $sucursal->agente_documentos[0]->doc_presentado->documento_presentado_id  ?>" value="aprobado"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
			</label>
			<label class="radio-inline">
			  <input class="btn-document-moderation" type="radio" name="<?php echo $sucursal->agente_documentos[0]->doc_presentado->documento_presentado_id  ?>" value="rechazado"> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
			</label>
		</td>
		<td><b>Observación:</b> -</td>
		<?php endif;?>
	</tr>
	<?php endif;?>
</tbody>
</table>
<div class="row">
	<div class="col-md-1">
	</div>
  	<div class="col-md-11">
		
		<?php if(isset($sucursal->actividades)) foreach ($sucursal->actividades as $actividad):?>
		<?php 
			/*echo "<pre>"; 
			print_r($actividad);
			echo "<pre";*/
		?>
		<h3><span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> Actividad</h3> 
		<table class="table table-hover">
		<tbody>
			<tr>
				<th scope="row" width="300"><label class="control-label">Actividad</label></th>
				<td colspan="3"><?php echo $actividad->actividad->descripcion ?></td>
			</tr>
			<tr>
				<th scope="row"><label class="control-label">Categoria</label></th>
				<td colspan="3"><?php echo $actividad->categoria->descripcion ?></td>
			</tr>
			<tr>
				<th scope="row"><label class="control-label">Detalle del movimiento de productos forestales y de la maquinaria FO-26</label></th>
				<?php if(!$actividad->documento):?>
				<td>No hay documento presentado</td>
				<?php else:?>
				<td><a href="/api/file/download_file_empresa/<?php echo $actividad->documento->nombre_file ?>">Descargar Archivo</a></td>
				<td>
					<label class="radio-inline">
					  <input class="btn-document-moderation" type="radio" name="<?php echo $actividad->documento->documento_presentado_id  ?>" value="aprobado"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
					</label>
					<label class="radio-inline">
					  <input class="btn-document-moderation" type="radio" name="<?php echo $actividad->documento->documento_presentado_id  ?>" value="rechazado"> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
					</label>
				</td>
				<td><b>Observación:</b> -</td>
				<?php endif;?>
			</tr>
		</tbody>
		</table>
		<?php endforeach;?>
		
		
		<?php if(isset($sucursal->representantes)) foreach ($sucursal->representantes as $representante):?>
		<?php 
			/*echo "<pre>"; 
			print_r($representante);
			echo "<pre";*/
		?>
		<h3><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Representante</h3>
		<table class="table table-hover">
		<tbody>
			<tr >
				<th scope="row" width="300"><label class="control-label">Codigo</label></th>
				<td><?php echo $representante->codpersona?></td>
				<th ><label class="control-label">Nombre</label></th>
				<td><?php echo $representante->nombres . ' ' . $representante->appaterno . ' ' . $representante->apmaterno?></td>
			</tr>
			<tr>
				<td scope="row"><label class="control-label">Documento</label></td>
				<td><?php echo $representante->nrodocid . ' (' . $representante->emitido  . ')' ?></td>
				<th><label class="control-label">Telefonos</label></th>
				<td><?php echo $representante->telefono . ' - ' . $representante->celular  ?></td>
			</tr>
			<tr >
				<th scope="row"><label class="control-label">Poder de Representación</label></th>
				<?php if(!isset($representante->doc_presentados[8]) ):?>
				<td>No se presento documento</td>
				<?php else:?>
				<td><a href="/api/file/download_file_empresa/<?php echo $representante->doc_presentados[8]->nombre_file ?>">Descargar Archivo</a></td>
				<td>
					<label class="radio-inline">
					  <input class="btn-document-moderation" type="radio" name="<?php echo $representante->doc_presentados[8]->documento_presentado_id  ?>" value="aprobado"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
					</label>
					<label class="radio-inline">
					  <input class="btn-document-moderation" type="radio" name="<?php echo $representante->doc_presentados[8]->documento_presentado_id  ?>" value="rechazado"> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
					</label>
				</td>
				<td><b>Observación:</b> -</td>
				<?php endif;?>
			</tr>
			<tr >
				<th scope="row"><label class="control-label">Fotocopia de documento de identidad vigente del representante legal</label></th>
				<?php if(!isset($representante->doc_presentados[12]) ):?>
				<td>No se presento documento</td>
				<?php else:?>
				<td><a href="/api/file/download_file_empresa/<?php echo $representante->doc_presentados[12]->nombre_file ?>">Descargar Archivo</a></td>
				<td>
					<label class="radio-inline">
					  <input class="btn-document-moderation" type="radio" name="<?php echo $representante->doc_presentados[12]->documento_presentado_id  ?>" value="aprobado"> <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
					</label>
					<label class="radio-inline">
					  <input class="btn-document-moderation" type="radio" name="<?php echo $representante->doc_presentados[12]->documento_presentado_id  ?>" value="rechazado"> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
					</label>
				</td>
				<td><b>Observación:</b> -</td>
				<?php endif;?>
			</tr>
		</tbody>
		</table>
		<?php endforeach;?>
	</div>
</div>
<?php endforeach;?>

<!-- Modal -->
<div role="dialog" class="modal fade" id="modal-document-moderation" tabindex="-1" document-id="">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title">Aprobar Documento</h4>
			</div>
			<div class="modal-body">
				<div class="moderation-aproval" class="form-group">
					<p>Esta seguro que desea aprobar el documento?</p>
				</div>
				<div class="moderation-reject" class="form-group">
					<p>Esta seguro que desea rechazar el documento?</p>
					<label >Observación:</label> <input class="form-control" placeholder="Ingrese motivo de rechazo del documento" name="document_reject" />
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="action-save-moderation">Aceptar</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				
			</div>
		</div>
	</div>
</div>
<!-- ./Modal -->

<script type="text/javascript">
$(document).ready(function(){
	
	$('.btn-document-moderation').click(function(event){
		var moderation = $(this).val();
		var document_id =  $(this).attr("name");
		var aprobar = (moderation == 'aprobado');
		var title = (aprobar) ? "Aprobar Documento" : "Rechazar Documento";

		if(aprobar)
		{
			$('#modal-document-moderation .moderation-aproval').show();
			$('#modal-document-moderation .moderation-reject').hide();	
			
		}else{
			$('#modal-document-moderation .moderation-aproval').hide();
			$('#modal-document-moderation .moderation-reject').show();	
		}

		$('#modal-document-moderation .modal-title').html(title);
		$('#modal-document-moderation').attr('document-id',document_id);
		$('#modal-document-moderation').attr('document-moderation',moderation);
		$('#modal-document-moderation').modal('show');
	});
	
	$('#action-save-moderation').click(function(event){
		
		$('#modal-document-moderation').modal('hide');
		var document_id = $('#modal-document-moderation').attr('document-id');
		var document_moderation = $('#modal-document-moderation').attr('document-moderation');
		var document_reject = $('#modal-document-moderation input[name="document_reject"]').val();

		console.log(document_id +"-"+document_moderation+"-"+document_reject);

		$.ajax({
			  type: "POST",
			  dataType: "json",
			  url: '/archivo/moderar',
			  data: {'id': document_id,'moderacion': document_moderation, 'observacion': document_reject},
			}).done(function( response ) {
				alert("done!");
			});
		
	});

});

</script>