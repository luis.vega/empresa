<?php

$errorClass   = empty($errorClass) ? ' error' : $errorClass;
$controlClass = empty($controlClass) ? 'span6' : $controlClass;
$fieldData = array(
    'errorClass'    => $errorClass,
    'controlClass'  => $controlClass,
);

// Recursos JS
Assets::add_module_js('empresas','js/solicitudes/solicitud.js');
?>

<section id="register">
    <h1 class="page-header">Comprobante de Registro</h1>

    <div class="row">
        <div class="col-md-1"></div>
        <label>Imprimir:</label>
        <button class="btn print-comprobante1">
            <span class="glyphicon glyphicon-print"></span>
        </button>
    </div><br>

    <div id="box-content-comprobante1">
        <div class="panel panel-primary content-comprobante" id="content-comprobante">
        <div class="panel-heading">
            <h3 class="panel-title"> Solicitud Inscripción</h3>
        </div>
        <div class="panel-body">
            <div class="row">

                <p class="navbar-text pull-right">
                    <label>REG. Nro: </label>
                    <?php echo $solicitud->solicitud_tramite_empresa_id ?>
                </p>

                <div class="col-md-12">
                    <p class="navbar-text">
                        <label>Razon Social: </label>
                        <?php echo $solicitud->razon_social_nombre; ?>
                    </p>
                    <p class="navbar-text">
                        <label>Gestion: </label>
                        <?php echo $solicitud->gestion; ?>
                    </p>

                    <p class="navbar-text pull-right">
                        <label>Fecha: </label>
                        <?php echo $solicitud->fecha_registro; ?>
                    </p>
                </div>
                <div class="col-md-10">
                    <p class="navbar-text">
                        <label>N° NIT: </label>
                        <?php echo empty($solicitud->nit) ? '-' : $solicitud->nit; ?>
                    </p>
                    <p class="navbar-text">
                        <label>N° FUNDEMPRESA: </label>
                        <?php echo empty($solicitud->numero_fundempresa) ? '-' : $solicitud->numero_fundempresa; ?>
                    </p>
                    <p class="navbar-text">
                        <label>Monto Pago Inscripción : </label>
                        <?php echo $solicitud->pago->monto . ' ' . $solicitud->pago->monto_moneda; ?> (Dólares Americanos)
                    </p>
                </div>
            </div>
            <hr class="panel">
            <label>Documentos General de Empresa</label>

            <div class="row comprobante-documents">
                <div class="col-md-1"></div>
                <div class="list-group col-md-5 panel-default">
                    <a href="#" class="list-group-item list-group-item-info">
                        Documentos Digital Presentados
                    </a>
                    <a href="#" class="list-group-item">
                        <ul>
                            <?php foreach ($solicitud->documentos as $documentoEmp) :?>
                                <li> <?php echo $documentoEmp->nombre; ?> </li>
                            <?php endforeach; ?>
                        </ul>
                    </a>
                </div>
                <div class="list-group col-md-5 panel-default">
                    <a href="#" class="list-group-item list-group-item-info">
                        Documentos Fisicos a Presentar
                    </a>
                    <a href="#" class="list-group-item">
                        <ul>
                            <?php foreach ($solicitud->documentos_fisico as $documentoEmp) :?>
                                <li> <?php echo $documentoEmp->nombre; ?> </li>
                            <?php endforeach; ?>
                        </ul>
                    </a>
                </div>
            </div>

            <hr class="panel">
            <?php foreach ($solicitud->sucursales as $key => $sucursal) :?>

                <div class="row">
                    <div class="col-md-12">
                        <p class="navbar-text">
                            <label>Sucursal: </label>
                            <?php echo ($key + 1); ?>
                        </p><br>
                        <p class="navbar-text">
                            <label>Direccion: </label>
                            <?php echo $sucursal->direccion; ?>
                        </p>
                        <p class="navbar-text">
                            <label>Municipio: </label>
                            <?php echo $sucursal->departamento . ' - ' . $sucursal->provincia . ' - ' . $sucursal->municipio; ?>
                        </p>
                    </div>
                    <div class="col-md-12 comprobante-documents">
                        <div class="col-md-1"></div>
                        <div class="list-group col-md-5 panel-default">
                            <a href="#" class="list-group-item list-group-item-info">
                                Documentos Presentados
                            </a>
                            <a href="#" class="list-group-item">
                                <label>Documentos Sucursal</label>:
                                <ul>
                                    <?php foreach ($sucursal->documentos as $docConfigSuc):?>
                                        <li> <?php echo $docConfigSuc->nombre; ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </a>
                            <?php if (sizeof($sucursal->agente_documentos)) : ?>
                                <a href="#" class="list-group-item">
                                    <label>Agente :</label> <?php echo $sucursal->agente->agente_npm; ?>
                                    <ul>
                                        <?php foreach ($sucursal->agente_documentos as $docConfigSuc):?>
                                            <li> <?php echo $docConfigSuc->nombre; ?></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </a>
                            <?php endif; ?>
                            <?php if (sizeof($sucursal->representantes_documentos) > 0) :?>
                                <?php
                                foreach ($sucursal->representantes as $indexRep => $representante):
                                    $fullName = $representante->nombres . ' ' . $representante->appaterno;
                                    if (empty($representante->apmaterno))
                                        $fullName .=  ' ' . $representante->apmaterno;
                                    ?>
                                    <a href="#" class="list-group-item">
                                        <label>Representante <?php echo ($indexRep + 1) ?></label>: <?php echo $fullName; ?>
                                        <ul>
                                            <?php foreach ($sucursal->representantes_documentos as $docConfigRep):?>
                                                <li> <?php echo $docConfigRep->nombre; ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </a>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <?php if (sizeof($sucursal->actividades) > 0) :?>
                                <?php
                                foreach ($sucursal->actividades as $indexAct => $actividadSuc):
                                    $fullNameAct = $actividadSuc->actividad->descripcion . ' ,' . $actividadSuc->categoria->descripcion;
                                    ?>
                                    <a href="#" class="list-group-item">

                                        <?php if ($actividadSuc->actividad_id == $consumidor_final_id): ?>
                                            <script type="text/javascript">
                                                var esconsumidorFinal = "<?php echo true;?>";
                                            </script>
                                        <?php endif; ?>
                                        <label>Actividad <?php echo ($indexAct + 1) ?></label>: <?php echo $fullNameAct; ?>
                                        <ul>
                                            <?php foreach ($actividadSuc->documento_f26 as $docConfigRep):?>
                                                <li> <?php echo $docConfigRep->nombre; ?></li>
                                            <?php endforeach; ?>
                                            <?php foreach ($actividadSuc->documentos as $docConfigRep):?>
                                                <li> <?php echo $docConfigRep->nombre; ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </a>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>

                        <div class="list-group col-md-5 panel-default">
                            <a href="#" class="list-group-item list-group-item-info">
                                Documentos Fisicos a Presentar
                            </a>
                            <?php if (sizeof($sucursal->documentos_fisico) > 0) : ?>
                                <a href="#" class="list-group-item">
                                    <label>Documentos Sucursal</label>:
                                    <ul>
                                        <?php foreach ($sucursal->documentos_fisico as $docConfigSuc):?>
                                            <li> <?php echo $docConfigSuc->nombre; ?></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </a>
                            <?php endif; ?>
                            <?php if (sizeof($sucursal->agente_documentos_fisico)) : ?>
                                <a href="#" class="list-group-item">
                                    <label>Agente :</label> <?php echo $sucursal->agente->agente_npm; ?>
                                    <ul>
                                        <?php foreach ($sucursal->agente_documentos_fisico as $docConfigSuc):?>
                                            <li> <?php echo $docConfigSuc->nombre; ?></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </a>
                            <?php endif; ?>
                            <?php if (sizeof($sucursal->representantes_documentos_fisico) > 0) :?>
                                <?php
                                foreach ($sucursal->representantes as $indexRepFis => $representante):
                                    $fullName = $representante->nombres . ' ' . $representante->appaterno;
                                    if (empty($representante->apmaterno))
                                        $fullName .=  ' ' . $representante->apmaterno;
                                    ?>
                                    <a href="#" class="list-group-item">
                                        <label>Representante <?php echo ($indexRepFis + 1) ?></label>: <?php echo $fullName; ?>
                                        <ul>
                                            <?php foreach ($sucursal->representantes_documentos_fisico as $docConfigSuc):?>
                                                <li> <?php echo $docConfigSuc->nombre; ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </a>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <?php if (sizeof($sucursal->actividades)) :?>
                                <?php
                                foreach ($sucursal->actividades as $indexAct => $actividadSuc):
                                    $fullNameAct = $actividadSuc->actividad->descripcion . ' ,' . $actividadSuc->categoria->descripcion;
                                    ?>
                                    <a href="#" class="list-group-item">
                                        <label>Actividad <?php echo ($indexAct + 1) ?></label>: <?php echo $fullNameAct; ?>
                                        <ul>
                                            <?php foreach ($actividadSuc->documentos_fisico_f26 as $docConfigRep):?>
                                                <li> <?php echo $docConfigRep->nombre; ?></li>
                                            <?php endforeach; ?>

                                            <?php foreach ($actividadSuc->documentos_fisico as $docConfigRep):?>
                                                <li> <?php echo $docConfigRep->nombre; ?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </a>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>


            <?php endforeach; ?>
            <label>NOTA</label>: Este documento es la carta generada por la Web, requisito principal para presentar en ABT y validar los datos registrados.
        </div>
    </div>
    </div>

    <div id="hidden-print-div"></div>
    <div class="row box-action pull-right">
            <button class="btn btn-success btn-lg volver">
                Salir
            </button>
    </div>
</section>