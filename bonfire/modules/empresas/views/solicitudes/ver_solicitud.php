<?php
Assets::add_module_js('admin','detalle.js');
?>
<h1 class="page-header">Información de solicitud</h1>
<div class="row">

    <div class="col-md-4">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-6 control-label">Solicitud ID</label>
                <div class="col-sm-6">
                    <p class="form-control-static"><?php echo $solicitud_info->solicitud_tramite_empresa_id ?></p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-6 control-label">Tipo de Solicitud</label>
                <div class="col-sm-6">
                    <p class="form-control-static"><?php  echo $solicitud_tipos[$solicitud_info->tipo_solicitud] ?></p>
                </div>
            </div>

        </div>
    </div>

    <div class="col-md-4">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-6 control-label">Gestión</label>
                <div class="col-sm-6">
                    <p class="form-control-static"><?php  echo $solicitud_info->gestion ?></p>
                </div>
            </div>
            <div class="form-group hidden">
                <label class="col-sm-6 control-label">Estado</label>
                <div class="col-sm-6">
                    <p class="form-control-static"><?php  echo $solicitud_estados[$solicitud_info->estado] ?></p>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-6 control-label">Fecha de Registro</label>
                <div class="col-sm-6">
                    <p class="form-control-static"><?php  echo $solicitud_info->fecha_registro ?></p>
                </div>
            </div>

        </div>
    </div>
    <?php if ($solicitud_info->estado == $estado_solicitud_pendiente) :?>
        <div class="col-md-4 hidden">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-6 control-label"> Aprobar Solicitud</label><br>
                    <div class="col-sm-6">
                        <button class="btn btn-lg btn-block btn-success solicitud-ok">
                            <i class="fa fa-check-circle-o fa-3x animated fadeIn"></i>
                        </button>
                    </div>
                </div>

            </div>
        </div>
    <?php endif; ?>
</div>

<!-- /.panel-heading -->
<div class="panel-body">

    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
        <li class="active"><a href="#step1" data-toggle="tab">Datos Generales Empresa</a></li>
        <li class="hidden"><a href="#step2" data-toggle="tab">Archivos Adjuntos Empresa</a></li>
        <?php if ($solicitud_info->estado == $estado_solicitud_aceptado): ?>
            <li class="hidden"><a href="#resolucion" data-toggle="tab">Resolución Administrativa</a></li>
        <?php endif; ?>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane fade in active" id="step1">
            <?php echo $this->load->view('partial/solicitud_detalle');?>
        </div>
        <div class="tab-pane fade" id="step2">
            <?php echo $this->load->view('partial/solicitud_archivos');?>
        </div>
        <?php if ($solicitud_info->estado == $estado_solicitud_aceptado) : ?>
            <div class="tab-pane fade hidden" id="resolucion">
                <?php echo $this->load->view('partial/solicitud_resolucion');?>
            </div>
        <?php endif; ?>
    </div>

</div>
<!-- /.panel-body -->

<script type="text/javascript">
    $(document).ready(function() {

        var solicitudId = "<?php echo $solicitud_info->solicitud_tramite_empresa_id ?>";
        // Accion para aprobar Solicitud
        $('.solicitud-ok').click(function () {
            $.blockUI();
            var totalChecks = $('.btn-document-moderation').length;
            var totalAprobados = $('.btn-document-moderation[value="aprobado"]:checked').length;
            var totalRechazados = $('.btn-document-moderation[value="rechazado"]:checked').length;
            if (totalRechazados > 0)
                showErrors(["No puede Aprobar la solicitud, existen archivos con Observacion."]);
            else {
                if (!totalAprobados || totalAprobados == 0) {
                    showErrors(["No puede Aprobar la solicitud, tiene que validar todos los archivos digitales y físicos."]);
                } else {
                    if (totalAprobados != (totalChecks / 2)) {
                        showErrors(["No puede Aprobar la solicitud, existen archivos no validados."]);
                    } else {
                        // Aprobar Solicitud
                        aprobarSolicitud(solicitudId, reloadSolicitud);
                    }
                }
            }

        });
    });

    function reloadSolicitud() {
        location.reload();
    }

    function aprobarSolicitud(solicitudId, callback) {

        $.blockUI();
        $.ajax({
            url: baseUrl + "api/empresa/aprobar_solicitud_registro/" + solicitudId,
            method: "GET",
            dataType : 'json',
            success: function(result){
                $.unblockUI();
                if (result) {
                    callback();
                }
            },
            error: function(datosError) {
                $.unblockUI();
                showErrors(datosError.responseJSON.error);
            }
        });
    }


</script>