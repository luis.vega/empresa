<br> <br>
<div class="stepwizard row">
    <div class="stepwizard-row" id="pasos-registro-empresa">
        <div class="stepwizard-step">
            <button type="button" id="paso1" class="btn btn-circle">1</button>
            <p>Datos Generales Empresa</p>
        </div>
        <div class="stepwizard-step">
            <button type="button" id="paso2" class="btn btn-circle">2</button>
            <p>Formularios - F26</p>
        </div>
        <div class="stepwizard-step">
            <button type="button" id="paso3"  class="btn btn-circle">3</button>
            <p>Archivos Adjuntos Empresa</p>
        </div>
        <div class="stepwizard-step">
            <button type="button" id="paso4"  class="btn btn-circle">4</button>
            <p>Comprobante</p>
        </div> 
    </div>
</div>
