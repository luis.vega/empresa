<?php

$errorClass   = empty($errorClass) ? ' error' : $errorClass;
$controlClass = empty($controlClass) ? 'span6' : $controlClass;
$fieldData = array(
    'errorClass'    => $errorClass,
    'controlClass'  => $controlClass,
);

// Recursos JS
Assets::add_module_js('empresas','js/finalizar.js');

?>

<?php 
	echo $this->load->view('pasosRegistro');
?>

<section id="register">
	<input type="hidden" id="abt-token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
	<h1 class="page-header">Finalización de Registro</h1>
		
		<fieldset>
		  
				<div class="panel panel-default">
				  <div class="panel-heading">Resolución Administrativa</div>
				  <div class="panel-body">
				        <div class="form-group" id="div-resolucion-admin">
							<?php 
								$this->load->view('ResolucionAdministrativa', $datos_resolucion);
							?>
				        </div>
				        <button class="btn btn-primary hidden-print pull-right" id="print-resolucion">
				        	<span class="glyphicon glyphicon-print" aria-hidden="true">Imprimir</span>
				        </button>
				  </div>
				</div>

		<div class="row box-action">
		        <div class="col-md-6">
		        	<div id="btn-anterior" class="pull-left btn-anterior">
			             <a href="#">Anterior</a>
			        </div>
		        </div>
		</div>
</section>