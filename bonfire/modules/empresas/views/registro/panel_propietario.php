<div class="panel panel-info panel-propietario">
	<div class="panel-heading">
        <h3 class="panel-title hidden-xs">Datos Propietario</h3>
        <h6 class="panel-title visible-xs">Datos Propietario</h6>
        <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>

	</div>
	 <div class="panel-body">
        <div class="tab-content">
            <?php echo $template; ?>
            <div class="form-group">
              <label class="control-label" for="email">Email (*)</label>  
              <input id="email" name="email" type="email" placeholder="Email del Propietario" 
                  class="form-control" required>
              <div class="help-block with-errors"></div>
            </div>
        </div>
	</div>
</div>