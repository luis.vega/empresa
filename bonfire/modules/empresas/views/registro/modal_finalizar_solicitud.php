
<!-- line modal -->
<div class="modal fade" id="modal-finalizar" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-keyboard="false" id="modal-finalizar">
  <div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Proceso de Inscripción</h3>
		</div>
		<div class="modal-body">
			
            <!-- content goes here -->
            <div class="alert alert-success alert-autocloseable-success">
        			¿Desea finalizar el proceso de inscripción? <br>
        			<label>Una vez finalizado el proceso, no podrá editar la información.</label>
			</div>
		</div>
		<div class="modal-footer">
			<div class="btn-group" role="group" aria-label="group button">
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-primary" data-dismiss="modal"  role="button">Cancelar</button>
				</div>
				<div class="col-md-1"></div>
				<div class="btn-group" role="group">
					<button type="button" id="btn-finalizar" class="btn btn-success" role="button">Continuar</button>
				</div>
			</div>
		</div>
	</div>
  </div>
</div>