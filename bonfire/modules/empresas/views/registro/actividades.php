<script type="text/javascript">
	var baseUrl = '<?php base_url()?>';
</script>

<?php

$errorClass   = empty($errorClass) ? ' error' : $errorClass;
$controlClass = empty($controlClass) ? 'span6' : $controlClass;
$fieldData = array(
    'errorClass'    => $errorClass,
    'controlClass'  => $controlClass,
);

// Recursos JS
Assets::add_module_js('empresas','js/registro/actividades.js');

?>

<?php 
	echo $this->load->view('pasosRegistro');
?>
<section id="register">
	<input type="hidden" id="abt-token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
	<h1 class="page-header">Registro de Actividades</h1>
		
		<fieldset id="actividades">
		  		<div class="form-group">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button" id="add-actividad">
                        	<span class="glyphicon glyphicon-plus"></span>
                        	Agregar Actividad
                        </button>
                    </span>
		  		</div>
				<div class="panel panel-default actividad" id="actividad-principal">
				  <div class="panel-heading">
				  	<h6>Actividad Principal</h6>
				  	<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
				  </div>
				  <div class="panel-body">
			        <div class="form-group">
			          <label class="col-md-4 control-label" for="tipo-actividad">Tipo de Actividad</label>  
			          <select id="tipo-actividad" name="tipo-actividad" class="form-control" required="">
			          	 <option value="">Seleccione</option>
						<?php foreach ($tiposActividad as $tipo) : ?>
	 							<option value="<?php echo $tipo->id; ?>">
	 								<?php echo $tipo->nombre; ?>
	 							</option>
							<?php endforeach; ?>
			          </select>
			        </div>

			        <div class="form-group">
			          <label class="col-md-4 control-label" for="categoria-actividad">Categoría</label>  
			          <select id="categoria-actividad" name="categoria-actividad" class="form-control" required="">
			          	 <option value="">Seleccione</option>
						<?php foreach ($categorias as $categoria) : ?>
	 							<option value="<?php echo $categoria->id; ?>">
	 								<?php echo $categoria->nombre; ?>
	 							</option>
							<?php endforeach; ?>
			          </select>
			        </div>


			        <div class="form-group row">
			          <label class="col-md-12 control-label" for="coordenadas">Coordenadas</label>  
			          <div class="col-md-12">
			          	  <div class="col-md-6">
			          		  <input id="coordenada-x" name="coordenada-x" type="text" placeholder="Coordenada X" 
					            class="form-control" required="">
			          	  </div>
						  <div class="col-md-6">
					          <input id="coordenada-y" name="coordenada-y" type="text" placeholder="Coordenada Y" 
						          class="form-control" required="">
					      </div>
			          </div>

			        </div>
			        <div class="form-group">
			          <label class="col-md-4 control-label" for="municipio">Municipio</label>  
			          <select id="municipio" name="municipio" class="form-control" required="">
			          	 <option value="">Seleccione</option>
						<?php foreach ($municipios as $municipio) : ?>
	 							<option value="<?php echo $municipio->codmunicipio; ?>">
	 								<?php echo $municipio->nombre; ?>
	 							</option>
							<?php endforeach; ?>
			          </select>
			        </div>

			        <div class="form-group">
			          <label class="col-md-4 control-label" for="direccion">Dirección</label>  
			          <input id="direccion" name="direccion" type="text" placeholder="Dirección / Ubicación" 
				          class="form-control" required="">
			        </div>

				  </div>
				</div>
		</fieldset>		
		<div class="row box-action">
		        <div class="col-md-6">
		        	<div id="btn-anterior" class="pull-left btn-anterior">
			             <a href="#">Anterior</a>
			        </div>
		        </div>
		        <div class="col-md-6">
			        <div id="btn-siguiente" class="pull-right btn-siguiente">
			             <a href="#">Siguiente</a>
			        </div>
		        </div>
		</div>
</section>