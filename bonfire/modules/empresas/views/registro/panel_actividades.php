<div class="panel panel-info panel-actividades">
	<div class="panel-heading">
          <h4 class="panel-title hidden-xs">Actividades</h4>
          <h5 class="visible-xs">Actividades</h5>
          <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
	</div>
	 <div class="panel-body">
        <div class="form-group">
                <button class="btn btn-primary" type="button" id="add-actividad" title="Agregar Actividad">
                    <span class="glyphicon glyphicon-plus"></span>
                    Agregar
                </button>
                <button class="btn btn-danger" type="button" id="remove-all-actividad" title="Elimina todas las Actividades marcadas">
                    <span class="glyphicon glyphicon-trash"></span>
                </button>


        </div>
        <hr class="panel">
        <span class="label label-primary pull-left count-actividades">0 Actividad(es)</span> 
        <table class="table table-striped tb-actividades">
            <thead>
                <tr>
                    <th> <input type="checkbox" id="cxb-actividades" /></th>
                    <th width="30%">Actividad</th>
                    <th width="30%">Categoria</th>
                    <th class="text-center">Accion</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
	</div>
</div>