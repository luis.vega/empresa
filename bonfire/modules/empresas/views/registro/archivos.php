<?php

$errorClass   = empty($errorClass) ? ' error' : $errorClass;
$controlClass = empty($controlClass) ? 'span6' : $controlClass;
$fieldData = array(
    'errorClass'    => $errorClass,
    'controlClass'  => $controlClass,
);

// Recursos JS
Assets::add_module_js('empresas','js/registro/archivos.js');

?>

<?php 
	echo $this->load->view('pasosRegistro');
?>

<script type="text/javascript">
    var solicitudObject = JSON.parse('<?php echo json_encode($solicitud);?>');
    var consumidorFinalId = "<?php echo $consumidor_final_id;?>";
    
</script>

<section id="register">
	<h1 class="page-header">Archivos Adjuntos</h1>
	<blockquote class="alert-danger alert-errors" style="display: none;">
	  <p>Existen campos obligatorios, por favor revise todo el formulario y las secciones.</p>
	</blockquote>
	<form class="form" data-toggle="validator" role="form" id="form-archivos">
		<fieldset class="files-empresa">
			<div class="panel panel-primary empresa">
			  <div class="panel-heading">
			  	<h6>Archivos Empresa</h6>
			  	<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
			  </div>
			  <div class="panel-body">
			  	<form action="template"></form>
			  	<?php foreach ($solicitud->documentos as $configDocument) : ;?>
						<form method="POST" action="" id="form-file-empresas" name="form-upload" 
								 enctype="multipart/form-data" action="<?php echo 'api/file/do_upload';?>">
							<div class="form-group row" id="file-<?php echo $configDocument->configuracion_archivo_empresa_id;?>">
								<input type="hidden" name="empresa-id" value="<?php echo $solicitud->empresa_forestal_id; ?>">
								<input type="hidden" name="type-empresa" value="1">
								<input type="hidden" name="config-file-name" value="<?php echo $configDocument->nombre; ?>">
								<input type="hidden" name="config-file-id" value="<?php echo $configDocument->configuracion_archivo_empresa_id; ?>">
								<label class="col-xs-12 col-md-12 control-label">
									<?php echo $configDocument->nombre; ?>
									<span class="glyphicon glyphicon-exclamation-sign tip-file" aria-hidden="true" 
													data-original-title="<?php echo $configDocument->descripcion; ?>"></span>
								</label>
								<div class="col-xs-10 col-md-6">
									<input type="file" accept="<?php echo $mimetypes_file; ?>" class="form-control col-md-10" name="file" id="file-upload" required />
									<span class="glyphicon glyphicon-remove file-remove-upload" style="display: none;"></span>
								</div>
								<div class="col-md-2 download" style="display: none;">
									<label >Descargar:
										<a href="#" class="file-download" target="_blank"> <span class="glyphicon glyphicon-cloud-download"></span></a> 
									</label>
								</div>
							</div>
						</form>
				<?php endforeach; ?>
			  </div>
			</div>
		</fieldset>
		<fieldset class="fields-sucursales">
			<blockquote class="alert-info">
				<p>Archivos de Sucursales</p>
			</blockquote> 
			<?php foreach($solicitud->sucursales as $sucursal): ?>

			<div class="panel panel-primary sucursal" id="index-<?php echo $sucursal->index_sucursal?>">
				<div class="panel-heading">
					<?php if ($sucursal->index_sucursal == 0) : ?>
					<h6>Casa Matriz</h6>
					<?php else : ?>
						<h6>Sucursal <?php echo $sucursal->index_sucursal;?></h6>
					<?php endif; ?>
					<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
				</div>
				<div class="panel-body">

				<div class="list-group panel-default">
				  <a href="#" class="list-group-item list-group-item-info">
				    Actividades con Categoria Correspondiente
				  </a>
				  <a href="#" class="list-group-item">
				  	 <ul>
				  	 	<?php foreach ($sucursal->actividades as $actividadSuc) :?>
				  			<li> <?php echo $actividadSuc->actividad->descripcion; ?>, 
				  				<strong><?php echo $actividadSuc->categoria->descripcion; ?></strong> </li>
				  		<?php endforeach; ?>
				  	 </ul>
				  </a>
				</div>
				<?php if (sizeof($sucursal->documentos) > 0): ?>
				<fieldset class="fields-representante">
					<h3>Documentos Sucursal</h3>
					<hr>
					<fieldset class="fields-sucursal">
					  <?php foreach($sucursal->documentos as $documentoSuc): ?>
						<form method="POST" action="" id="form-file-agente" name="form-upload" 
								 enctype="multipart/form-data" action="<?php echo 'api/file/do_upload';?>">
							<div class="row" id="file-<?php echo  $documentoSuc->configuracion_archivo_empresa_id;?>">
								<input type="hidden" name="sucursal-id" value="<?php echo $sucursal->empresa_sucursal_id; ?>">
								<input type="hidden" name="type-sucursal" value="1">
								<input type="hidden" name="config-file-name" value="<?php echo $documentoSuc->nombre; ?>">
								<input type="hidden" name="config-file-id" value="<?php echo $documentoSuc->configuracion_archivo_empresa_id; ?>">
								<label class="col-xs-12 col-md-12 control-label">
									<?php echo $documentoSuc->nombre; ?>
									<span class="glyphicon glyphicon-exclamation-sign tip-file" aria-hidden="true" 
													data-original-title="<?php echo $documentoSuc->descripcion; ?>"></span>
								</label>
								<div class="form-group col-xs-10 col-md-6">
									<input type="file" accept="<?php echo $mimetypes_file; ?>" 
									class="form-control" name="file" id="file-upload-sucursal" required />
									<span class="glyphicon glyphicon-remove file-remove-upload" style="display: none;"></span>
									<div class="help-block with-errors"></div>
								</div>
								<div class="col-md-2 download" style="display: none;">
									<label >Descargar:
										<a href="#" class="file-download" target="_blank"> <span class="glyphicon glyphicon-cloud-download"></span></a> 
									</label>
								</div>
							</div>
						</form>
					  <?php endforeach; ?>
					</fieldset>
					<hr class="panel">
				</fieldset>
				<?php endif; ?>
				 <?php 
					// Cargado de Agente 
					if (!empty($sucursal->agente)) :
				 ?>
				 <fieldset class="field-agente">
					<h3> Documentos Agente</h3>
					<hr>
					<label class="col-xs-12 col-md-12 control-label">
						Agente: 
						<span class="badge badge-secondary"><?php echo $sucursal->agente->agente_npm; ?></span>
					</label>
					<?php foreach($sucursal->agente_documentos as $docAgente): ?>
						<form method="POST" action="" id="form-file-agente" name="form-upload" 
								 enctype="multipart/form-data" action="<?php echo 'api/file/do_upload';?>">
							<div class="row" id="file-<?php echo $docAgente->configuracion_archivo_empresa_id; ?>">
								<input type="hidden" name="sucursal-id" value="<?php echo $sucursal->empresa_sucursal_id; ?>">
								<input type="hidden" name="type-agente" value="1">
								<input type="hidden" name="config-file-name" value="<?php echo $docAgente->nombre; ?>">
								<input type="hidden" name="config-file-id" value="<?php echo $docAgente->configuracion_archivo_empresa_id; ?>">
								<label class="col-xs-12 col-md-12 control-label">
									<?php echo $docAgente->nombre; ?>
									<span class="glyphicon glyphicon-exclamation-sign tip-file" aria-hidden="true" 
													data-original-title="<?php echo $docAgente->descripcion; ?>"></span>
								</label>
								<div class="form-group col-xs-10 col-md-6">
									<input type="file" accept="<?php echo $mimetypes_file; ?>" 
									class="form-control" name="file" id="file-upload-sucursal" required />
									<span class="glyphicon glyphicon-remove file-remove-upload" style="display: none;"></span>
									<div class="help-block with-errors"></div>
								</div>

								<div class="col-md-2 download" style="display: none;">
									<label >Descargar:
										<a href="#" class="file-download" target="_blank"> <span class="glyphicon glyphicon-cloud-download"></span></a> 
									</label>
								</div>
							</div>
						</form>
					<?php endforeach; ?>
				</fieldset>
					<hr class="panel">
				 <?php endif; ?>
				 <?php if (!empty($sucursal->representantes) && sizeof($sucursal->representantes) > 0) : ?>
					<div class="panel panel-info representante" id="representantes">
						<div class="panel-heading">
							<h6>Archivos Representantes Legales</h6>
							<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
						</div>
						<div class="panel-body">
						<ul class="nav nav-tabs row">
						<?php 
							$repText = sizeof($sucursal->representantes) > 3 ? "R" : "Representante";
							foreach($sucursal->representantes as $key => $representante):
						 ?>
								<?php if ($key == 0): ?>
									<li class="active"><a href="#tab-rep-<?php echo $representante->codpersona?>"
										data-toggle="tab"><?php echo $repText ?> 1</a>
									</li>
								<?php else : ?>
									<li>
										<a href="#tab-rep-<?php echo $representante->codpersona?>" 
										data-toggle="tab"> <?php echo $repText .  ' ' . ($key + 1);?> </a>
									</li>
								<?php endif; ?>
							<?php endforeach; ?>
						</ul>
						<div class="tab-content">
						<?php 
							foreach($sucursal->representantes as $key => $representante): 
								$fullName = $representante->nombres . ' ' . $representante->appaterno;
								if (!empty($representante->apmaterno))
									$fullName .= ' ' . $representante->apmaterno;
						?>
								<?php if ($key == 0): ?>
									<div class="tab-pane fade in active" id="tab-rep-<?php echo $representante->codpersona?>">
								<?php else : ?>
									<div class="tab-pane fade" id="tab-rep-<?php echo $representante->codpersona?>">
								<?php endif; ?>
								
									<ul class="list-group row">
										<li class="list-group-item col-md-12">
											<label class="col-xs-12 col-md-12">
												Representante: 
												<span class="badge badge-secondary"><?php echo $fullName; ?></span>
											</label>
											<br>
										</li>
							<?php foreach($sucursal->representantes_documentos as $configDocument): ?>
								<li class="list-group-item col-md-12" id="file-<?php echo $configDocument->configuracion_archivo_empresa_id ?>">
									<form method="POST" action="" id="form-file-representante" name="form-upload" 
											 enctype="multipart/form-data" action="<?php echo 'api/file/do_upload';?>">
										<div class="form-group">
											<input type="hidden" name="sucursal-id" value="<?php echo $sucursal->empresa_sucursal_id; ?>">
											<input type="hidden" name="type-representante" value="1">
											<input type="hidden" name="config-file-name" value="<?php echo $configDocument->nombre; ?>">
											<input type="hidden" name="entidad-id" value="<?php echo $representante->codpersona;?>">
											<input type="hidden" name="config-file-id" value="<?php echo $configDocument->configuracion_archivo_empresa_id; ?>">
											<label class="col-xs-12 col-md-12 control-label">
												<?php echo $configDocument->nombre; ?>
												<span class="glyphicon glyphicon-exclamation-sign tip-file" aria-hidden="true" 
																data-original-title="<?php echo $configDocument->descripcion; ?>"></span>
											</label>
											<div class="col-xs-10 col-md-6">
												<input type="file" accept="<?php echo $mimetypes_file; ?>" class="form-control col-md-10" name="file" id="file-upload-sucursal" required />
												<span class="glyphicon glyphicon-remove file-remove-upload" style="display: none;"></span>
											</div>
											<div class="col-md-2 download" style="display: none;">
												<label >Descargar:
													<a href="#" class="file-download" target="_blank"> <span class="glyphicon glyphicon-cloud-download"></span></a> 
												</label>
											</div>
										</div>
									</form>
								</li>
							<?php endforeach; ?>
								</ul>
								</div>

					<?php endforeach; ?>
						</div>
					  </div>
					</div>
				<?php endif; ?>
				<?php if (!empty($sucursal->actividades) && sizeof($sucursal->actividades) > 0
				&& $sucursal->con_documentos_actividades) : ?>
					<!-- Actividades -->
					<hr class="panel">
					<div class="panel panel-info actividad" id="actividades">
						<div class="panel-heading">
							<h6>Archivos Actividades</h6>
							<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
						</div>
						<div class="panel-body">

						<ul class="nav nav-tabs">
							<?php 
								foreach($sucursal->actividades as $index => $actividad): 
							?>
							<?php if ($index == 0): ?>
								<li class="active"><a href="#tab-act-0" data-toggle="tab">A1</a></li>
							<?php else: ?> 
								<li>
									<a href="#tab-act-<?php echo $index++;?>" data-toggle="tab"> <?php echo "A " . $index; ?></a>
								</li>
							<?php endif; ?>
							<?php endforeach; ?>
						</ul>
						<div class="tab-content">
							<?php 
								foreach($sucursal->actividades as $index => $actividadSucursal): 
							?>
									<?php if ($index == 0): ?>
										<div class="tab-pane fade in active" id="tab-act-0">
									<?php else: ?> 
										<div class="tab-pane fade" id="tab-act-<?php echo $index++;?>">
									<?php endif; ?>

									<ul class="list-group">
										<li class="list-group-item">
											<label class="col-xs-12 col-md-12">
												Actividad: 
												<span class="badge badge-secondary">
													<?php echo $actividadSucursal->actividad->descripcion; ?> 
												</span>
												| <?php echo $actividadSucursal->categoria->descripcion; ?> 
											</label>
											<br>
										</li>
										<?php foreach($actividadSucursal->archivos_config as $configDocument): ?>
											<li class="list-group-item">
												
									<form method="POST" action="" id="form-file-actividad" name="form-upload" 
											 enctype="multipart/form-data" action="<?php echo 'api/file/do_upload';?>" class="row">
										<div class="form-group">
											<input type="hidden" name="sucursal-id" value="<?php echo $sucursal->empresa_sucursal_id; ?>">
											<input type="hidden" name="type-actividad" value="1">
											<input type="hidden" name="config-file-name" value="<?php echo $configDocument->nombre; ?>">
											<input type="hidden" name="entidad-id" value="<?php echo $actividadSucursal->sucursal_actividad_id;?>">
											<input type="hidden" name="config-file-id" value="<?php echo $configDocument->configuracion_archivo_empresa_id; ?>">
											<label class="col-xs-12 col-md-12 control-label">
												<?php echo $configDocument->nombre; ?>
												<span class="glyphicon glyphicon-exclamation-sign tip-file" aria-hidden="true" 
																data-original-title="<?php echo $configDocument->descripcion; ?>"></span>
											</label>
											<div class="col-xs-10 col-md-6">
												<input type="file" accept="<?php echo $mimetypes_file; ?>" class="form-control col-md-10" name="file" id="file-upload-sucursal" required />
												<span class="glyphicon glyphicon-remove file-remove-upload" style="display: none;"></span>
											</div>
											<?php if (isset($configDocument->documento_legal) && $configDocument->documento_legal) : ?>
												<span class="glyphicon glyphicon-briefcase"></span> |
											<?php endif; ?>
											
											<?php if (isset($configDocument->escaneado_cd) && $configDocument->escaneado_cd) : ?>
												<span class="glyphicon glyphicon-cd"></span> |
											<?php elseif (isset($configDocument->por_actualizacion_datos) && $configDocument->por_actualizacion_datos) : ?>
												<span class="glyphicon glyphicon-retweet"></span>
											<?php endif; ?>
										</div>
									</form>
											</li>
										<?php endforeach; ?>
									</ul>
								</div>
							<?php endforeach; ?>
						</div>
 
						</div>
					</div>
				<?php endif; ?>
				</div>
			</div>

			<?php endforeach; ?>
		</fieldset>
		<div class="row box-action">
		        <div class="col-md-6">
		        	<div id="btn-anterior" class="pull-left btn-anterior">
			             <a href="#">Anterior</a>
			        </div>
		        </div>
		        <div class="col-md-6">
			        <div id="btn-siguiente" class="pull-right btn-siguiente">
			             <a href="#">Finalizar</a>
			        </div>
		        </div>
		</div>
	</form>

<?php 
	$this->load->view('modal_finalizar_solicitud');
?>
</section>