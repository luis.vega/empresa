<?php

$errorClass   = empty($errorClass) ? ' error' : $errorClass;
$controlClass = empty($controlClass) ? 'span6' : $controlClass;
$currentId = empty($solicitud_id) ? '' : $solicitud_id;
$fieldData = array(
    'errorClass'    => $errorClass,
    'controlClass'  => $controlClass,
);

// Recursos JS
Assets::add_module_js('empresas','js/registro/registro.js');

?>

<?php 
	echo $this->load->view('pasosRegistro');
?>
<script type="text/javascript">
    var paisDefaultId = "<?php echo $paisDefaultId;?>";
    var consumidorFinalId = "<?php echo $consumidor_final_id;?>";
    var currentId = "<?php echo $currentId;?>";
</script>
<section id="register">
	<input type="hidden" id="abt-token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
	<h1 class="page-header">Datos Nueva Empresa</h1>

	<blockquote class="alert-info">
	  <p>Los campos obligatorios están con '*'.</p>
	</blockquote>

	<blockquote class="alert-warning alert-errors" style="display: none;">
	  <p>Existen campos obligatorios, por favor revise todo el formulario y las secciones.</p>
	</blockquote>
	<form class="form" data-toble="validator" role="form" id="form-registration">
		<fieldset>
		        <div class="form-group">
		          <label class="col-md-10 control-label" for="razon-social">Nombre / Razón social / Nombre de FUNDEMPRESA (*) </label>  
		          <input id="razon-social" name="razon-social" type="text" placeholder="Razón social" 
		          class="form-control input-md" required>
		          <div class="help-block with-errors"></div>
		        </div>
		        <div class="form-group">
		          <label class="col-md-4 control-label" for="gestion">Gestión</label>  
		          <input id="gestion" name="gestion" type="text"
		          class="form-control input-md" disabled="" value="<?php echo date('Y')?>">
		        </div>

		        <div class="row">
		        	<div class="col-md-6">
				        <div class="form-group">
				          <label class="col-md-12 control-label" for="numero-nit">N° NIT</label>  
				          <input id="numero-nit" name="numero-nit" type="text" placeholder="N° NIT" 
					          class="form-control" required>
				        </div>
		        	</div>		        
		        	<div class="col-md-6">
				        <div class="form-group">
				          <label class="col-md-12 control-label" for="numero-fundempresa">N° FUNDEMPRESA</label>  
				          <input id="numero-fundempresa" name="numero-fundempresa" type="text" placeholder="N° FUNDEMPRESA" 
					          class="form-control" required>
				        </div>
		        	</div>
		        </div>

		        <div class="row">
		        	<label for="tengo-registro" class="btn btn-info" style="margin: 15px;">Tengo Registro Antiguo 
		        		<input type="checkbox" id="tengo-registro" class="badgebox" name="tengo-registro"><span class="badge">&check;</span></label><br>
				        <div class="form-group registro-antiguo col-md-6" style="display: none;">
				          <label class="col-md-12 control-label" for="registro-antiguo">N° Registro Antiguo</label>  
				          <input id="registro-antiguo" name="registro-antiguo" type="text" placeholder="N° Reg. Antiguo" 
					          class="form-control">
				        </div>
		        </div>

		        <div class="form-group">
		        	<label for="soy-propietario" class="btn btn-info">Soy Propietario <input type="checkbox" id="soy-propietario" class="badgebox" name="soy-propietario"><span class="badge">&check;</span></label><br>
						<?php 
						 	$template = $this->load->view('users/settings/persona_fields', array(), true);
							$this->load->view('panel_propietario', array('template' => $template));
						?>
		        </div>

			<!-- ************************  Seccion de SUCURSALES ***************** -->
				<div class="form-group tpl-actividad" hidden="">
                      <select id="tpl-actividades" name="actividad" class="form-control">
                     	<option value="">Seleccione</option>
                        <?php foreach ($actividades as $actividad) : ?>
	                        <option value="<?php echo $actividad->codtipoactindustrial; ?>" data-es-unico="<?php echo isset($actividad->es_unico) ? 1 : 0; ?>">
	                            <?php echo $actividad->descripcion; ?>
	                        </option>
                        <?php endforeach; ?>
                      </select>

                      <select id="tpl-categorias" name="categoria" class="form-control">
                      	<option value="">Seleccione</option>
                      </select>
				</div>
		        <br>
				<blockquote class="alert-info">
				  <p>Información de Sucursales</p>
				</blockquote>
<fieldset id="sucursales">
	  			<div class="form-group">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button" id="add-sucursal">
                        	<span class="glyphicon glyphicon-plus"></span>
                        	Agregar Sucursal
                        </button>
                    </span>
		  		</div>
				<div class="panel panel-primary sucursal" id="sucursal-principal">
					  <div class="panel-heading">
					  	<h6>Casa Matriz</h6>
					  	<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
					  </div>
					  <div class="panel-body">

				        <div class="form-group">
				          <label class="col-md-4 control-label" for="direccion">Dirección</label>  
				          <input id="direccion" name="direccion" type="text" placeholder="Dirección / Ubicación" 
					          class="form-control" required>
					          <div class="help-block with-errors"></div>
				        </div>

				        <div class="form-group form-inline">
				          <label class="col-md-12 control-label" for="coordenadas">Coordenadas</label>
				          <input id="coordenada-x" name="coordenada-x" type="text" placeholder="Coordenada X" 
						            class="form-control input-medium" data-fv-field="number" required>
						  <input id="coordenada-y" name="coordenada-y" type="text" placeholder="Coordenada Y" 
							          class="form-control input-medium" data-fv-field="number" required>
							<div class="help-block with-errors"></div>
				        </div>

				        <div class="form-group">
				          <label class="col-md-4 control-label" for="municipio">Municipio</label>
				          <select id="municipio" name="municipio" class="form-control" required="">
				          	 <option value="">Seleccione</option>
							<?php foreach ($municipios as $municipio) : ?>
		 							<option value="<?php echo $municipio->codmunicipio; ?>">
		 								<?php echo $municipio->nombre; ?>
		 							</option>
								<?php endforeach; ?>
				          </select>
				          <div class="help-block with-errors"></div>
				        </div>
				        <fieldset id="agente-field">
					        <div class="form-group">
					        	<label for="con-agente" class="btn btn-info">Registrar Agente <input type="checkbox" id="con-agente" class="badgebox" name="con-agente"><span class="badge">&check;</span></label><br>
									<?php 
										$this->load->view('panel_agente');
									?>
					        </div>
				        </fieldset>
				        <fieldset id="actividades-field">
						<?php 
							$this->load->view('panel_actividades');
						?>
						</fieldset>
						<?php 
						 	$template = $this->load->view('users/settings/persona_fields', array(), true);
							$this->load->view('panel_representantes_legales', array('template' => $template));
						?>
					  </div>
				</div>
</fieldset>
<!-- ************************  Seccion de SUCURSALES ***************** -->

<!-- Modal Representante Legal -->

		</fieldset>
		<div class="row box-action">
			<div class="col-md-10">
		        <div id="btn-siguiente" class="pull-right btn-siguiente">
		             <a href="#">Siguiente</a>
		        </div>
		    </div>
		</div>
	</form>
</section>