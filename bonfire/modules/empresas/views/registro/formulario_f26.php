<?php

$errorClass   = empty($errorClass) ? ' error' : $errorClass;
$controlClass = empty($controlClass) ? 'span6' : $controlClass;
$fieldData = array(
    'errorClass'    => $errorClass,
    'controlClass'  => $controlClass,
);

// Recursos JS
Assets::add_module_js('empresas','js/registro/formulariof26.js');

?>
<?php 
	echo $this->load->view('pasosRegistro');
?>
<script type="text/javascript">
    var empresaId = "<?php echo $solicitud->empresa_forestal_id;?>";
    var obsProyectadoId = "<?php echo $obs_proyectado_id;?>";
    var obsSaldoStockId = "<?php echo $obs_saldostock_id;?>";
    var solicitudObject = JSON.parse('<?php echo json_encode($solicitud);?>');
</script>

<section id="register">
	<input type="hidden" id="abt-token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
	<h1 class="page-header">Datos Formularios FO-26</h1>


	<blockquote class="alert-warning alert-errors" style="display: none;">
	  <p>Existen campos obligatorios, por favor revise todo el formulario y las secciones.</p>
	</blockquote>
	<form class="form" data-toble="validator" novalidate="true" role="form" id="form-formulario26">
		<blockquote class="alert-info">
		  <p>Información de Sucursales</p>
		</blockquote>
		<fieldset class="fields-sucursales">
			<?php foreach($solicitud->sucursales as $sucursal): ?>
			<div class="panel panel-primary sucursal" id="index-sucursal-<?php echo $sucursal->index_sucursal?>" data-id="<?php echo $sucursal->empresa_sucursal_id; ?>">
				<div class="panel-heading">
					<?php if ($sucursal->index_sucursal == 0) : ?>
					<h6>Casa Matriz</h6>
					<?php else : ?>
						<h6>Sucursal <?php echo $sucursal->index_sucursal;?></h6>
					<?php endif; ?>
					<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
				</div>
				<div class="panel-body">
					<h3 class="page-header">Actividades FO-26</h3>

					<ul class="nav nav-tabs">
						<?php 
							foreach($sucursal->actividades as $index => $actividad):
								$tabName = sizeof($sucursal->actividades) > 3 ? "A" : "Actividad"; 
						?>
						<?php if ($index == 0): ?>
							<li class="active"><a href="#tab-act-0" data-toggle="tab"><?php echo $tabName; ?> 1</a></li>
						<?php else: ?> 
							<li>
								<a href="#tab-act-<?php echo $index++;?>" data-toggle="tab"> <?php echo $tabName . $index; ?></a>
							</li>
						<?php endif; ?>
						<?php endforeach; ?>
					</ul>
					<div class="tab-content">

							<?php 
								foreach($sucursal->actividades as $index => $actividadSucursal): 
							?>
									<?php if ($index == 0): ?>
										<div class="tab-pane fade in active actividad" id="tab-act-0" data-actividad-id="<?php echo $actividadSucursal->sucursal_actividad_id; ?>">
									<?php else: ?> 
										<div class="tab-pane fade actividad" id="tab-act-<?php echo $index++;?>" data-actividad-id="<?php echo $actividadSucursal->sucursal_actividad_id; ?>">
									<?php endif; ?>

									<ul class="list-group">
										<li class="list-group-item">
											<label class="col-xs-12 col-md-12">
												Actividad: 
												<span class="badge badge-secondary">
													<?php echo $actividadSucursal->actividad->descripcion; ?> 
												</span> 
													|
													Unidad:
												<?php if (!empty($actividadSucursal->unidad)) : ?> 
													<span class="badge badge-secondary">
														<?php echo $actividadSucursal->unidad->nombre; ?> 
													</span>
													(<?php echo $actividadSucursal->unidad->abreviacion; ?> )
												<?php else: ?>
													No definida
												<?php endif; ?>
											</label>
											<br>
										</li>
										<li class="list-group-item">
						<div class="row">
									<div class="col-md-6">
										
										<input type="hidden" name="empresa-id" value="<?php echo $solicitud->empresa_forestal_id; ?>">
										<input type="hidden" name="type-productos" value="1">
										<input type="hidden" name="actividad-id" value="<?php echo $actividadSucursal->sucursal_actividad_id; ?>">
										<label class="col-xs-12 col-md-12 control-label">
											Importar Fichero FO-26
										</label>
										<div class="form-group">
											<input type="file" accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" 
											class="form-control" name="file" id="file-fo-26" required /><br>
											<label style="display: none;">Descargar:
												<a href="#" class="file-download" target="_blank"> <span class="glyphicon btn-lg glyphicon-cloud-download"></span></a> 
												<a href="#" class="file-download" target="_blank"> <span class="glyphicon btn-lg glyphicon-cloud-download"></span></a> 
											</label>
										
											<div class="help-block with-errors"></div>
										</div>
									</div>
						</div>

									    </li>
									    <li class="list-group-item">
						                     <button class="btn btn-primary" type="button" id="add-proyectado" data-is="proyectado">
					                         	<span class="glyphicon glyphicon-plus"></span> Proyectado
					                        </button>
					                        <span class="glyphicon glyphicon-question-sign tip-file" aria-hidden="true" 
													data-original-title="Permite Agregar Movimiento Total para los Productos Proyectados"></span>

						                     <button class="btn btn-primary" type="button" id="add-proyectado" data-is="saldo-stock">
					                         	<span class="glyphicon glyphicon-plus"></span> Saldo en Stock
					                        </button>
					                        <span class="glyphicon glyphicon-question-sign tip-file" aria-hidden="true" 
													data-original-title="Permite Agregar Moviemiento Total para los Productos con Saldo en Stock"></span>
											<?php if (!empty($actividadSucursal->unidad)) : ?>
												<input type="hidden" name="unidad-id" id="unidad-id" data-value="<?php echo $actividadSucursal->unidad->codunidadcfo; ?>" >
												<input type="hidden" id="unidad-desc" data-value="<?php echo $actividadSucursal->unidad->nombre;?>" >
												<input type="hidden" id="especie-id" data-value="<?php echo $actividadSucursal->especie_id ?>">
												<input type="hidden" id="producto-id" data-value="<?php echo $actividadSucursal->producto_id ?>">
											<?php endif; ?>
									    </li>
										<li class="list-group-item content-table-producto">
											<fieldset class="table-productos">
									        <table class="table table-striped tb-actividades">
									            <thead>
									            	<tr >
									            		<th colspan="9" class="text-center">
									            			Resumen Total de Movimiento de Productos
									            		</th>
									            	</tr>
									                <tr class="row">
									                    <th class="col-md-3">
									                    	<label> Productos </label>
									                    </th>
									                    <th class="col-md-2">
									                    	<label> Especie </label>
									                    </th>
									                    <th class="col-md-2">
									                    	<label class="hidden-xs"> Cant. Vol. Total </label>
									                    	<label class="visible-xs"> 
									                    		<span class="glyphicon glyphicon-question-sign tip-file" aria-hidden="true" data-original-title="Cantidad"></span>
									                    		C
									                    	</label>
									                    </th>
									                    <th class="col-md-1">
									                    	<label> Unidad </label>
									                    </th>
									                    <th class="col-md-2">
									                    	<label> Observaciones </label>
									                    </th>
									                    <th class="col-md-2">
									                    	<label> Rendimiento (%)  </label>
									                    </th>
									                </tr>
									            </thead>
									            <tbody>

									            </tbody>
									        </table>
									        <ul class="list-group reporte-productos" style="display: none;">
									        	<li class="list-group-item">
									        		<label >
									        			Volumen Total:
									        			<span class="badge badge-secondary vol-total">
														</span>
									        		</label>
									        		| 
									        		<label>
									        			Total Filas Válidas:
									        			<span class="badge badge-secondary total-filas">
														</span> 
									        		</label>
									        		| <label class="categoria"></label>
									        	</li>
									        </ul>
											</fieldset>
										</li>
									</ul>
								</div>
							<?php endforeach; ?>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
		</fieldset>
		<div class="row box-action">
		        <div class="col-md-6">
		        	<div id="btn-anterior" class="pull-left btn-anterior">
			             <a href="#">Anterior</a>
			        </div>
		        </div>
		        <div class="col-md-6">
			        <div id="btn-siguiente" class="pull-right btn-siguiente">
			             <a href="#">Siguiente</a>
			        </div>
		        </div>
		</div>
	</form>
<?php 
	$this->load->view('modal_producto_f26', array('productos' => $productos, 'especies' => $especies, 'observaciones' => $observaciones));
?>
</section>