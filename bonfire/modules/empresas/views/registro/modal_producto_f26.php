
<!-- line modal -->
<div class="modal fade" id="modal-add-producto" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true" data-keyboard="false">
  <div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			<h3 class="modal-title" id="lineModalLabel">Producto FO-26</h3>
		</div>
		<div class="modal-body">
			
            <!-- content goes here -->
			<form data-toble="validator" novalidate="true" role="form" id="form-modal-add-producto">
				<div class="form-group">
					  <label for="producto" class="control-label">Producto</label>
			          <select id="producto" name="producto" class="form-control" required>
			          	 <option value="">Seleccione</option>
						<?php foreach ($productos as $producto) : ?>
							<option value="<?php echo $producto->codtipoproductoforestal; ?>">
								<?php echo $producto->descripcion; ?>
							</option>
						<?php endforeach; ?>
			          </select>
					  <div class="help-block with-errors"></div>

				</div>
				<div class="form-group">
					  <label for="especie" class="control-label">Especie</label>
			          <select id="especie" name="especie" class="form-control" required>
			          	 <option value="">Seleccione</option>
						<?php foreach ($especies as $especie) : ?>
							<option value="<?php echo $especie->codespeciencomun; ?>">
								<?php echo $especie->ncomun . ' (' . $especie->ncientifico . ')'; ?>
							</option>
						<?php endforeach; ?>
			          </select>
					  <div class="help-block with-errors"></div>
				</div>
				<div class="row">
					<div class="form-group col-md-6">
						  <label for="observacion" class="control-label">Observaciones</label>
				          <select id="observacion" name="especie" class="form-control" required>
				          	 <option value="">Seleccione</option>
							<?php foreach ($observaciones as $observacion) : ?>
								<option value="<?php echo $observacion->key; ?>">
									<?php echo $observacion->descripcion;?>
								</option>
							<?php endforeach; ?>
				          </select>
						  <div class="help-block with-errors"></div>
					</div>
					<div class="form-group unidad col-md-6" style="display: none;">
						<input type="hidden" name="unidad" id="unidad">
						<label for="unidad" class="control-label">Unidad</label>
						<span class="badge badge-secondary">
							Mi unidad
						</span>
					</div>
				</div>

				<div class="row">
					<div class="form-group col-md-6">
						<label for="cantidad" class="control-label">Cantidad - Volumen Total</label>
						<input type="number" name="cantidad" id="cantidad" class="form-control" step="0.01" required>
						<div class="help-block with-errors"></div>

					</div>
					<div class="form-group col-md-6">
						<label for="rendimiento" class="control-label">Rendimiento (%)</label>
						<input type="number" name="rendimiento" id="rendimiento" class="form-control" min="1" max="100" step="0.01" required>
						<div class="help-block with-errors"></div>

					</div>
				</div>
            </form>

		</div>
		<div class="modal-footer">
			<div class="btn-group" role="group" aria-label="group button">
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-primary" data-dismiss="modal"  role="button">Salir</button>
				</div>
				<div class="col-md-1"></div>
				<div class="btn-group" role="group">
					<button type="button" id="save-modal" class="btn btn-success" data-action="save" role="button">Agregar</button>
				</div>
			</div>
		</div>
	</div>
  </div>
</div>