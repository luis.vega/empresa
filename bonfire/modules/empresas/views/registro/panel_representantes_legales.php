<div class="panel panel-info panel-representantes">
	<div class="panel-heading panel-tabs">

            <div class="col-md-3">
            	<h3 class="panel-title hidden-xs">Representantes Legales</h3>
            	<h5 class="panel-title visible-xs">Rep. Legales</h5>
            </div>
            <div class="col-md-8">
				<ul class="nav nav-tabs nav-representantes">
                    <li class="active" style="display: none;" >
                    	<a href="#tab-rep-legal-r1" data-toggle="tab">
                    		<span class="glyphicon btn-glyphicon glyphicon-user"></span>
                    		R1
                    	</a>
                    </li>
                    <li id="tab-add-representante">
                    	<a href="#tab-rep-nuevo" data-toggle="tab" title="Adicionar">
                    		<span class="glyphicon glyphicon-plus"></span>
                    	</a>
                    </li>
                    <li id="tab-remove-representante">
                    	<a href="#tab-rep-nuevo" data-toggle="tab" title="Eliminar">
                    		<span class="glyphicon glyphicon-trash"></span>
                    	</a>
                    </li>
                </ul>
            </div>
          <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
	</div>
	 <div class="panel-body">
        <div class="tab-content">
            <div class="tab-pane active representante" id="tab-rep-legal-r1" style="display: none;" disabled>
            	<?php echo $template; ?>
            </div>
        </div>
	</div>
</div>