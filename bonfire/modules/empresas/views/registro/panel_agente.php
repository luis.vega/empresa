<div class="panel panel-info panel-agente">
	<div class="panel-heading">
        <h3 class="panel-title hidden-xs">Datos Agente Auxiliar</h3>
        <h6 class="panel-title visible-xs">Datos Agente Auxiliar</h6>
        <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>

	</div>
	 <div class="panel-body">
        <div class="tab-content">
            <div class="input-group" id="adv-search">
                <input type="text" class="form-control" placeholder="Buscar por ..." id="filter-text-complete">
                <div class="input-group-btn">
                    <div class="btn-group" role="group">
                        <div class="dropdown dropdown-lg">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false" id="filter-form"><span class="caret"></span></button>
                            <div class="dropdown-menu dropdown-menu-right form-horizontal" role="menu" id="form-filter">
                                  <label for="filter">Filtros de Búsqueda:</label>
                                <div class="form-group">
                                  <input id="filter-text" name="filter-text" type="text" placeholder="Nombres / Apellidos, Nro CI " 
                                      class="form-control">
                                </div>
                                <div class="form-group">
                                  <input id="numero-registro" name="numero-registro" type="text" placeholder="N° REGISTRO" 
                                      class="form-control">
                                </div>
                            </div>
                        </div>
                        <button id="filtrar-auxiliar-clean" type="button" class="btn btn-default"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
                        <button id="filtrar-auxiliar" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                    </div>
                </div>
            </div>

            <div class="form-group text-center col-md-7 col-xs-12" id="div-agente">
            </div>
        </div>
	</div>
</div>