<?php

$errorClass   = empty($errorClass) ? ' error' : $errorClass;
$controlClass = empty($controlClass) ? 'span6' : $controlClass;
$fieldData = array(
    'errorClass'    => $errorClass,
    'controlClass'  => $controlClass,
);

// Recursos JS
Assets::add_module_js('empresas','js/registro.js');

?>

<?php 
	echo $this->load->view('pasosRegistro');
?>
<section id="register">
	<input type="hidden" id="abt-token" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
	<h1 class="page-header">Datos Nueva Empresa</h1>
	<form class="form">
		<fieldset>

		        <div class="form-group">
		          <label class="col-md-12 control-label" for="numero-registro">Número de Registro</label>  
		          <input id="numero-registro" name="numero-registro" type="text" placeholder="Número de Registro" 
			          class="form-control" required="">
		        </div>

		        <div class="row">
		        	<div class="col-md-6">
				        <div class="form-group">
				          <label class="col-md-4 control-label" for="gestion">Gestión</label>  
				          <input id="gestion" name="gestion" type="text"
				          class="form-control input-md" disabled="" value="<?php echo date('Y')?>">
				        </div>
		        	</div>
		        	<div class="col-md-6">
				        <div class="form-group">
				          <label class="col-md-4 control-label" for="numero-trimestre">N° Trimestre</label>  
				          <select id="numero-trimestre" name="numero-trimestre" class="form-control" required="">
				          	 <option value="">Seleccione</option>
							<?php if (isset($trimestres) && is_array($trimestres)) :?>
								<?php foreach ($trimestres as $trimestre) : ?>
		 							<option value="<?php echo $trimestre->id; ?>">
		 								<?php echo $trimestre->nombre; ?>
		 							</option>
								<?php endforeach; ?>
					          </select>
					        <?php endif;?>
				        </div>
		        	</div>
		        </div>
		        <div class="form-group">
		          <label class="col-md-4 control-label" for="razon-social">Razón social</label>  
		          <input id="razon-social" name="razon-social" type="text" placeholder="Razón social" 
		          class="form-control input-md" required="">
		        </div>

		        <div class="row">
		        	<div class="col-md-6">
				        <div class="form-group">
				          <label class="col-md-12 control-label" for="numero-nit">N° NIT</label>  
				          <input id="numero-nit" name="numero-nit" type="text" placeholder="N° NIT" 
					          class="form-control" required="">
				        </div>
		        	</div>		        
		        	<div class="col-md-6">
				        <div class="form-group">
				          <label class="col-md-12 control-label" for="numero-fundempresa">N° FUNDEMPRESA</label>  
				          <input id="numero-fundempresa" name="numero-fundempresa" type="text" placeholder="N° FUNDEMPRESA" 
					          class="form-control" required="">
				        </div>
		        	</div>
		        </div>

				<div class="panel panel-default">
				  <div class="panel-heading">Propietario / Representante legal</div>
				  <div class="panel-body">
				        <div class="form-group">
				          <label class="col-md-12 control-label" for="prop-rep-nombre">Nombre del Propietario o Representante legal</label>  
				          <input id="prop-rep-nombre" name="prop-rep-nombre" type="text" placeholder="Nombre del Propietario o Representante legal" 
					          class="form-control" required="">
				        </div>

				        <div class="row">
				        	<div class="col-md-6">
						        <div class="form-group">
						          <label class="col-md-4 control-label" for="prop-rep-telefono-fax">Teléfono / Fax </label>  
						          <input id="prop-rep-telefono-fax" name="prop-rep-telefono-fax" type="text" placeholder="Teléfono / Fax" 
							          class="form-control" required="">
						        </div>
				        	</div>
				        	<div class="col-md-6">
						        <div class="form-group">
						          <label class="col-md-6 control-label" for="prop-rep-correo">Correo electrónico </label>  
						          <input id="prop-rep-correo" name="prop-rep-correo" type="email" placeholder="Correo electrónico" 
							          class="form-control" required>
						        </div>
				        	</div>
				        </div>
				
				  </div>
				</div>

				<div class="panel panel-default">
				  <div class="panel-heading">Agente Auxiliar</div>
				  <div class="panel-body">
				        <div class="form-group">
				          <label class="col-md-4 control-label" for="agente-nombre">Nombre del Agente Auxiliar</label>  
				          <input id="prop-rep-nombre" name="prop-rep-nombre" type="text" placeholder="Nombre del Agente Auxiliar
" 
					          class="form-control" required="">
				        </div>

				        <div class="row">
				        	<div class="col-md-6">
						        <div class="form-group">
						          <label class="col-md-4 control-label" for="agente-telefono-fax">Teléfono / Fax </label>  
						          <input id="agente-telefono-fax" name="agente-telefono-fax" type="text" placeholder="Teléfono / Fax" 
							          class="form-control" required="">
						        </div>
				        	</div>
				        	<div class="col-md-6">
						        <div class="form-group">
						          <label class="col-md-6 control-label" for="agente-correo">Correo electrónico </label>  
						          <input id="agente-correo" name="agente-correo" type="email" placeholder="Correo electrónico" 
							          class="form-control" required>
						        </div>
				        	</div>
				        </div>

				  </div>
				</div>
		</fieldset>
		<div class="row box-action">
			<div class="col-md-10">
		        <div id="btn-siguiente" class="pull-right btn-siguiente">
		             <a href="#">Siguiente</a>
		        </div>
		    </div>
		</div>
	</form>
</section>