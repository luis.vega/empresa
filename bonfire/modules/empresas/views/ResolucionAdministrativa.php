<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 15">
<meta name=Originator content="Microsoft Word 15">

<style>
<!--
 /* Font Definitions */
 @font-face
  {font-family:Wingdings;
  panose-1:5 0 0 0 0 0 0 0 0 0;
  mso-font-charset:2;
  mso-generic-font-family:auto;
  mso-font-pitch:variable;
  mso-font-signature:0 268435456 0 0 -2147483648 0;}
@font-face
  {font-family:"Cambria Math";
  panose-1:2 4 5 3 5 4 6 3 2 4;
  mso-font-charset:0;
  mso-generic-font-family:roman;
  mso-font-pitch:variable;
  mso-font-signature:-536870145 1107305727 0 0 415 0;}
@font-face
  {font-family:Calibri;
  panose-1:2 15 5 2 2 2 4 3 2 4;
  mso-font-charset:0;
  mso-generic-font-family:swiss;
  mso-font-pitch:variable;
  mso-font-signature:-536870145 1073786111 1 0 415 0;}
@font-face
  {font-family:"Arial Narrow";
  panose-1:2 11 6 6 2 2 2 3 2 4;
  mso-font-charset:0;
  mso-generic-font-family:swiss;
  mso-font-pitch:variable;
  mso-font-signature:647 2048 0 0 159 0;}
@font-face
  {font-family:Tahoma;
  panose-1:2 11 6 4 3 5 4 4 2 4;
  mso-font-charset:0;
  mso-generic-font-family:swiss;
  mso-font-pitch:variable;
  mso-font-signature:-520081665 -1073717157 41 0 66047 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
  {mso-style-unhide:no;
  mso-style-qformat:yes;
  mso-style-parent:"";
  margin-top:0cm;
  margin-right:0cm;
  margin-bottom:10.0pt;
  margin-left:0cm;
  line-height:115%;
  mso-pagination:widow-orphan;
  font-size:11.0pt;
  font-family:"Calibri",sans-serif;
  mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
  {mso-style-priority:34;
  mso-style-unhide:no;
  mso-style-qformat:yes;
  margin-top:0cm;
  margin-right:0cm;
  margin-bottom:10.0pt;
  margin-left:35.4pt;
  line-height:115%;
  mso-pagination:widow-orphan;
  font-size:11.0pt;
  font-family:"Calibri",sans-serif;
  mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Times New Roman";}
.MsoChpDefault
  {mso-style-type:export-only;
  mso-default-props:yes;
  font-family:"Calibri",sans-serif;
  mso-ascii-font-family:Calibri;
  mso-ascii-theme-font:minor-latin;
  mso-fareast-font-family:Calibri;
  mso-fareast-theme-font:minor-latin;
  mso-hansi-font-family:Calibri;
  mso-hansi-theme-font:minor-latin;
  mso-bidi-font-family:"Times New Roman";
  mso-bidi-theme-font:minor-bidi;
  mso-fareast-language:EN-US;}
.MsoPapDefault
  {mso-style-type:export-only;
  margin-bottom:8.0pt;
  line-height:107%;}
@page WordSection1
  {size:613.0pt 793.0pt;
  margin:74.0pt 54.0pt 14.0pt 69.0pt;
  mso-header-margin:0cm;
  mso-footer-margin:47.35pt;
  mso-paper-source:0;}
div.WordSection1
  {page:WordSection1;}
@page WordSection2
  {size:613.0pt 793.0pt;
  margin:74.0pt 65.0pt 14.0pt 80.0pt;
  mso-header-margin:0cm;
  mso-footer-margin:47.35pt;
  mso-paper-source:0;}
div.WordSection2
  {page:WordSection2;}
 /* List Definitions */
 @list l0
  {mso-list-id:32191747;
  mso-list-type:hybrid;
  mso-list-template-ids:-317711900 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189;}
@list l0:level1
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:43.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l0:level2
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:79.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l0:level3
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:115.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l0:level4
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:151.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l0:level5
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:187.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l0:level6
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:223.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l0:level7
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:259.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l0:level8
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:295.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l0:level9
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:331.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l1
  {mso-list-id:125514260;
  mso-list-type:hybrid;
  mso-list-template-ids:1780535580 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189;}
@list l1:level1
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l1:level2
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l1:level3
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l1:level4
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l1:level5
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l1:level6
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l1:level7
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l1:level8
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l1:level9
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l2
  {mso-list-id:187525193;
  mso-list-type:hybrid;
  mso-list-template-ids:-260821224 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189;}
@list l2:level1
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l2:level2
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l2:level3
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l2:level4
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l2:level5
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l2:level6
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l2:level7
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l2:level8
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l2:level9
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l3
  {mso-list-id:269624193;
  mso-list-type:hybrid;
  mso-list-template-ids:1782858800 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189;}
@list l3:level1
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:59.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l3:level2
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:95.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l3:level3
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:131.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l3:level4
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:167.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l3:level5
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:203.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l3:level6
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:239.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l3:level7
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:275.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l3:level8
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:311.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l3:level9
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:347.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l4
  {mso-list-id:281621763;
  mso-list-type:hybrid;
  mso-list-template-ids:-1041585004 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189;}
@list l4:level1
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l4:level2
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l4:level3
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l4:level4
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l4:level5
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l4:level6
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l4:level7
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l4:level8
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l4:level9
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l5
  {mso-list-id:315300531;
  mso-list-type:hybrid;
  mso-list-template-ids:203311870 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189;}
@list l5:level1
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:59.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l5:level2
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:95.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l5:level3
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:131.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l5:level4
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:167.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l5:level5
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:203.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l5:level6
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:239.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l5:level7
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:275.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l5:level8
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:311.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l5:level9
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:347.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l6
  {mso-list-id:346249707;
  mso-list-template-ids:-2117420668;}
@list l6:level1
  {mso-level-start-at:7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:18.0pt;
  text-indent:-18.0pt;
  mso-ansi-font-weight:bold;}
@list l6:level2
  {mso-level-text:"%1\.%2\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:18.0pt;
  text-indent:-18.0pt;
  mso-ansi-font-weight:bold;}
@list l6:level3
  {mso-level-text:"%1\.%2\.%3\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:36.0pt;
  text-indent:-36.0pt;
  mso-ansi-font-weight:bold;}
@list l6:level4
  {mso-level-text:"%1\.%2\.%3\.%4\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:36.0pt;
  text-indent:-36.0pt;
  mso-ansi-font-weight:bold;}
@list l6:level5
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:54.0pt;
  text-indent:-54.0pt;
  mso-ansi-font-weight:bold;}
@list l6:level6
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:54.0pt;
  text-indent:-54.0pt;
  mso-ansi-font-weight:bold;}
@list l6:level7
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:54.0pt;
  text-indent:-54.0pt;
  mso-ansi-font-weight:bold;}
@list l6:level8
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:72.0pt;
  text-indent:-72.0pt;
  mso-ansi-font-weight:bold;}
@list l6:level9
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.%9\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:72.0pt;
  text-indent:-72.0pt;
  mso-ansi-font-weight:bold;}
@list l7
  {mso-list-id:534775223;
  mso-list-type:hybrid;
  mso-list-template-ids:-292121832 1074397199 1074397209 1074397211 1074397199 1074397209 1074397211 1074397199 1074397209 1074397211;}
@list l7:level1
  {mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;}
@list l7:level2
  {mso-level-number-format:alpha-lower;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;}
@list l7:level3
  {mso-level-number-format:roman-lower;
  mso-level-tab-stop:none;
  mso-level-number-position:right;
  text-indent:-9.0pt;}
@list l7:level4
  {mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;}
@list l7:level5
  {mso-level-number-format:alpha-lower;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;}
@list l7:level6
  {mso-level-number-format:roman-lower;
  mso-level-tab-stop:none;
  mso-level-number-position:right;
  text-indent:-9.0pt;}
@list l7:level7
  {mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;}
@list l7:level8
  {mso-level-number-format:alpha-lower;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;}
@list l7:level9
  {mso-level-number-format:roman-lower;
  mso-level-tab-stop:none;
  mso-level-number-position:right;
  text-indent:-9.0pt;}
@list l8
  {mso-list-id:537353705;
  mso-list-type:hybrid;
  mso-list-template-ids:-323877438 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189;}
@list l8:level1
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:59.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l8:level2
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:95.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l8:level3
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:131.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l8:level4
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:167.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l8:level5
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:203.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l8:level6
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:239.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l8:level7
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:275.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l8:level8
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:311.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l8:level9
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:347.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l9
  {mso-list-id:558444505;
  mso-list-type:hybrid;
  mso-list-template-ids:-34034466 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189;}
@list l9:level1
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:41.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l9:level2
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:77.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l9:level3
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:113.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l9:level4
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:149.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l9:level5
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:185.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l9:level6
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:221.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l9:level7
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:257.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l9:level8
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:293.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l9:level9
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:329.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l10
  {mso-list-id:662657936;
  mso-list-template-ids:-2117420668;}
@list l10:level1
  {mso-level-start-at:7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:18.0pt;
  text-indent:-18.0pt;
  mso-ansi-font-weight:bold;}
@list l10:level2
  {mso-level-text:"%1\.%2\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:18.0pt;
  text-indent:-18.0pt;
  mso-ansi-font-weight:bold;}
@list l10:level3
  {mso-level-text:"%1\.%2\.%3\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:36.0pt;
  text-indent:-36.0pt;
  mso-ansi-font-weight:bold;}
@list l10:level4
  {mso-level-text:"%1\.%2\.%3\.%4\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:36.0pt;
  text-indent:-36.0pt;
  mso-ansi-font-weight:bold;}
@list l10:level5
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:54.0pt;
  text-indent:-54.0pt;
  mso-ansi-font-weight:bold;}
@list l10:level6
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:54.0pt;
  text-indent:-54.0pt;
  mso-ansi-font-weight:bold;}
@list l10:level7
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:54.0pt;
  text-indent:-54.0pt;
  mso-ansi-font-weight:bold;}
@list l10:level8
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:72.0pt;
  text-indent:-72.0pt;
  mso-ansi-font-weight:bold;}
@list l10:level9
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.%9\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:72.0pt;
  text-indent:-72.0pt;
  mso-ansi-font-weight:bold;}
@list l11
  {mso-list-id:664553366;
  mso-list-type:hybrid;
  mso-list-template-ids:173464392 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189;}
@list l11:level1
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l11:level2
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l11:level3
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l11:level4
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l11:level5
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l11:level6
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l11:level7
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l11:level8
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l11:level9
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l12
  {mso-list-id:761679304;
  mso-list-template-ids:2057977190;}
@list l12:level1
  {mso-level-start-at:6;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:18.0pt;
  text-indent:-18.0pt;
  mso-ansi-font-weight:bold;}
@list l12:level2
  {mso-level-text:"%1\.%2\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:18.0pt;
  text-indent:-18.0pt;
  mso-ansi-font-weight:bold;}
@list l12:level3
  {mso-level-text:"%1\.%2\.%3\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:36.0pt;
  text-indent:-36.0pt;
  mso-ansi-font-weight:bold;}
@list l12:level4
  {mso-level-text:"%1\.%2\.%3\.%4\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:36.0pt;
  text-indent:-36.0pt;
  mso-ansi-font-weight:bold;}
@list l12:level5
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:54.0pt;
  text-indent:-54.0pt;
  mso-ansi-font-weight:bold;}
@list l12:level6
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:54.0pt;
  text-indent:-54.0pt;
  mso-ansi-font-weight:bold;}
@list l12:level7
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:54.0pt;
  text-indent:-54.0pt;
  mso-ansi-font-weight:bold;}
@list l12:level8
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:72.0pt;
  text-indent:-72.0pt;
  mso-ansi-font-weight:bold;}
@list l12:level9
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.%9\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:72.0pt;
  text-indent:-72.0pt;
  mso-ansi-font-weight:bold;}
@list l13
  {mso-list-id:794955869;
  mso-list-type:hybrid;
  mso-list-template-ids:-699760984 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189;}
@list l13:level1
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:59.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l13:level2
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:95.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l13:level3
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:131.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l13:level4
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:167.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l13:level5
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:203.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l13:level6
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:239.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l13:level7
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:275.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l13:level8
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:311.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l13:level9
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:347.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l14
  {mso-list-id:938877890;
  mso-list-template-ids:-2117420668;}
@list l14:level1
  {mso-level-start-at:7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:18.0pt;
  text-indent:-18.0pt;
  mso-ansi-font-weight:bold;}
@list l14:level2
  {mso-level-text:"%1\.%2\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:18.0pt;
  text-indent:-18.0pt;
  mso-ansi-font-weight:bold;}
@list l14:level3
  {mso-level-text:"%1\.%2\.%3\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:36.0pt;
  text-indent:-36.0pt;
  mso-ansi-font-weight:bold;}
@list l14:level4
  {mso-level-text:"%1\.%2\.%3\.%4\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:36.0pt;
  text-indent:-36.0pt;
  mso-ansi-font-weight:bold;}
@list l14:level5
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:54.0pt;
  text-indent:-54.0pt;
  mso-ansi-font-weight:bold;}
@list l14:level6
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:54.0pt;
  text-indent:-54.0pt;
  mso-ansi-font-weight:bold;}
@list l14:level7
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:54.0pt;
  text-indent:-54.0pt;
  mso-ansi-font-weight:bold;}
@list l14:level8
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:72.0pt;
  text-indent:-72.0pt;
  mso-ansi-font-weight:bold;}
@list l14:level9
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.%9\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:72.0pt;
  text-indent:-72.0pt;
  mso-ansi-font-weight:bold;}
@list l15
  {mso-list-id:989359185;
  mso-list-template-ids:-2117420668;}
@list l15:level1
  {mso-level-start-at:7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:18.0pt;
  text-indent:-18.0pt;
  mso-ansi-font-weight:bold;}
@list l15:level2
  {mso-level-text:"%1\.%2\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:18.0pt;
  text-indent:-18.0pt;
  mso-ansi-font-weight:bold;}
@list l15:level3
  {mso-level-text:"%1\.%2\.%3\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:36.0pt;
  text-indent:-36.0pt;
  mso-ansi-font-weight:bold;}
@list l15:level4
  {mso-level-text:"%1\.%2\.%3\.%4\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:36.0pt;
  text-indent:-36.0pt;
  mso-ansi-font-weight:bold;}
@list l15:level5
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:54.0pt;
  text-indent:-54.0pt;
  mso-ansi-font-weight:bold;}
@list l15:level6
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:54.0pt;
  text-indent:-54.0pt;
  mso-ansi-font-weight:bold;}
@list l15:level7
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:54.0pt;
  text-indent:-54.0pt;
  mso-ansi-font-weight:bold;}
@list l15:level8
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:72.0pt;
  text-indent:-72.0pt;
  mso-ansi-font-weight:bold;}
@list l15:level9
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.%9\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:72.0pt;
  text-indent:-72.0pt;
  mso-ansi-font-weight:bold;}
@list l16
  {mso-list-id:1228297387;
  mso-list-type:hybrid;
  mso-list-template-ids:-4962048 597996804 1074397187 1074397189 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189;}
@list l16:level1
  {mso-level-start-at:7;
  mso-level-number-format:bullet;
  mso-level-text:-;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:41.1pt;
  text-indent:-18.0pt;
  font-family:"Arial Narrow",sans-serif;
  mso-fareast-font-family:"Times New Roman";
  mso-bidi-font-family:"Arial Narrow";
  color:#171518;}
@list l16:level2
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:77.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l16:level3
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:113.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l16:level4
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:149.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l16:level5
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:185.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l16:level6
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:221.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l16:level7
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:257.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l16:level8
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:293.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l16:level9
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:329.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l17
  {mso-list-id:1339692956;
  mso-list-type:hybrid;
  mso-list-template-ids:-644862952 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189;}
@list l17:level1
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:41.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l17:level2
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:77.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l17:level3
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:113.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l17:level4
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:149.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l17:level5
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:185.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l17:level6
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:221.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l17:level7
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:257.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l17:level8
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:293.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l17:level9
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:329.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l18
  {mso-list-id:1341466995;
  mso-list-type:hybrid;
  mso-list-template-ids:-1853470488 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189;}
@list l18:level1
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:41.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l18:level2
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:77.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l18:level3
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:113.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l18:level4
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:149.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l18:level5
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:185.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l18:level6
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:221.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l18:level7
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:257.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l18:level8
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:293.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l18:level9
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:329.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l19
  {mso-list-id:1427996563;
  mso-list-template-ids:-2117420668;}
@list l19:level1
  {mso-level-start-at:7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:18.0pt;
  text-indent:-18.0pt;
  mso-ansi-font-weight:bold;}
@list l19:level2
  {mso-level-text:"%1\.%2\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:18.0pt;
  text-indent:-18.0pt;
  mso-ansi-font-weight:bold;}
@list l19:level3
  {mso-level-text:"%1\.%2\.%3\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:36.0pt;
  text-indent:-36.0pt;
  mso-ansi-font-weight:bold;}
@list l19:level4
  {mso-level-text:"%1\.%2\.%3\.%4\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:36.0pt;
  text-indent:-36.0pt;
  mso-ansi-font-weight:bold;}
@list l19:level5
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:54.0pt;
  text-indent:-54.0pt;
  mso-ansi-font-weight:bold;}
@list l19:level6
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:54.0pt;
  text-indent:-54.0pt;
  mso-ansi-font-weight:bold;}
@list l19:level7
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:54.0pt;
  text-indent:-54.0pt;
  mso-ansi-font-weight:bold;}
@list l19:level8
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:72.0pt;
  text-indent:-72.0pt;
  mso-ansi-font-weight:bold;}
@list l19:level9
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.%9\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:72.0pt;
  text-indent:-72.0pt;
  mso-ansi-font-weight:bold;}
@list l20
  {mso-list-id:1489175168;
  mso-list-type:hybrid;
  mso-list-template-ids:1281247566 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189;}
@list l20:level1
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:59.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l20:level2
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:95.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l20:level3
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:131.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l20:level4
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:167.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l20:level5
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:203.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l20:level6
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:239.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l20:level7
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:275.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l20:level8
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:311.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l20:level9
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:347.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l21
  {mso-list-id:1734157520;
  mso-list-template-ids:285636492;}
@list l21:level1
  {mso-level-start-at:6;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:18.0pt;
  text-indent:-18.0pt;}
@list l21:level2
  {mso-level-text:"%1\.%2\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:25.1pt;
  text-indent:-18.0pt;}
@list l21:level3
  {mso-level-text:"%1\.%2\.%3\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:50.2pt;
  text-indent:-36.0pt;}
@list l21:level4
  {mso-level-text:"%1\.%2\.%3\.%4\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:57.3pt;
  text-indent:-36.0pt;}
@list l21:level5
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:82.4pt;
  text-indent:-54.0pt;}
@list l21:level6
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:89.5pt;
  text-indent:-54.0pt;}
@list l21:level7
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:96.6pt;
  text-indent:-54.0pt;}
@list l21:level8
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:121.7pt;
  text-indent:-72.0pt;}
@list l21:level9
  {mso-level-text:"%1\.%2\.%3\.%4\.%5\.%6\.%7\.%8\.%9\.";
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:128.8pt;
  text-indent:-72.0pt;}
@list l22
  {mso-list-id:1737168004;
  mso-list-type:hybrid;
  mso-list-template-ids:-248635410 1363475606 1074397209 1074397211 1074397199 1074397209 1074397211 1074397199 1074397209 1074397211;}
@list l22:level1
  {mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:41.1pt;
  text-indent:-18.0pt;
  mso-ansi-font-weight:bold;}
@list l22:level2
  {mso-level-number-format:alpha-lower;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:77.1pt;
  text-indent:-18.0pt;}
@list l22:level3
  {mso-level-number-format:roman-lower;
  mso-level-tab-stop:none;
  mso-level-number-position:right;
  margin-left:113.1pt;
  text-indent:-9.0pt;}
@list l22:level4
  {mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:149.1pt;
  text-indent:-18.0pt;}
@list l22:level5
  {mso-level-number-format:alpha-lower;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:185.1pt;
  text-indent:-18.0pt;}
@list l22:level6
  {mso-level-number-format:roman-lower;
  mso-level-tab-stop:none;
  mso-level-number-position:right;
  margin-left:221.1pt;
  text-indent:-9.0pt;}
@list l22:level7
  {mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:257.1pt;
  text-indent:-18.0pt;}
@list l22:level8
  {mso-level-number-format:alpha-lower;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:293.1pt;
  text-indent:-18.0pt;}
@list l22:level9
  {mso-level-number-format:roman-lower;
  mso-level-tab-stop:none;
  mso-level-number-position:right;
  margin-left:329.1pt;
  text-indent:-9.0pt;}
@list l23
  {mso-list-id:1837960162;
  mso-list-type:hybrid;
  mso-list-template-ids:391781568 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189;}
@list l23:level1
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:41.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l23:level2
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:77.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l23:level3
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:113.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l23:level4
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:149.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l23:level5
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:185.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l23:level6
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:221.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l23:level7
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:257.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l23:level8
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:293.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l23:level9
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:329.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l24
  {mso-list-id:1947761916;
  mso-list-type:hybrid;
  mso-list-template-ids:592222094 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189;}
@list l24:level1
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:59.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l24:level2
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:95.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l24:level3
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:131.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l24:level4
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:167.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l24:level5
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:203.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l24:level6
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:239.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l24:level7
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:275.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l24:level8
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:311.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l24:level9
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:347.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l25
  {mso-list-id:2008895875;
  mso-list-type:hybrid;
  mso-list-template-ids:-1061151728 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189;}
@list l25:level1
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:59.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l25:level2
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:95.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l25:level3
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:131.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l25:level4
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:167.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l25:level5
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:203.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l25:level6
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:239.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l25:level7
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:275.1pt;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l25:level8
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:311.1pt;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l25:level9
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  margin-left:347.1pt;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l26
  {mso-list-id:2011448459;
  mso-list-type:hybrid;
  mso-list-template-ids:-2104087918 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189;}
@list l26:level1
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l26:level2
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l26:level3
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l26:level4
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l26:level5
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l26:level6
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l26:level7
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l26:level8
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l26:level9
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l27
  {mso-list-id:2128506753;
  mso-list-type:hybrid;
  mso-list-template-ids:287183816 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189 1074397185 1074397187 1074397189;}
@list l27:level1
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l27:level2
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l27:level3
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l27:level4
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l27:level5
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l27:level6
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
@list l27:level7
  {mso-level-number-format:bullet;
  mso-level-text:\F0B7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Symbol;}
@list l27:level8
  {mso-level-number-format:bullet;
  mso-level-text:o;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:"Courier New";}
@list l27:level9
  {mso-level-number-format:bullet;
  mso-level-text:\F0A7;
  mso-level-tab-stop:none;
  mso-level-number-position:left;
  text-indent:-18.0pt;
  font-family:Wingdings;}
ol
  {margin-bottom:0cm;}
ul
  {margin-bottom:0cm;}
-->
</style>
</head>

<body lang=ES-BO style='tab-interval:35.4pt'>

<div class=WordSection1>

<p class=MsoNormal style='margin-top:1.7pt;margin-right:308.5pt;margin-bottom:
0cm;margin-left:16.1pt;margin-bottom:.0001pt;text-align:justify;line-height:
normal;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif;letter-spacing:-.05pt'>R</span></b><b><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif'>E<span
style='letter-spacing:.05pt'>S</span>O<span style='letter-spacing:.05pt'>LU</span><span
style='letter-spacing:-.05pt'>C</span>IÓN<span style='letter-spacing:-.6pt'> </span>A<span
style='letter-spacing:.05pt'>D</span><span style='letter-spacing:.1pt'>M</span>INI<span
style='letter-spacing:.15pt'>S</span>TRA<span style='letter-spacing:.05pt'>T</span>IVA</span></b><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:.3pt;margin-right:86.2pt;margin-bottom:
0cm;margin-left:16.1pt;margin-bottom:.0001pt;line-height:12.0pt;mso-line-height-rule:
exactly;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif'>AUTO<span
style='letter-spacing:.1pt'>R</span>IDAD<span style='letter-spacing:-.6pt'> </span><span
style='letter-spacing:.15pt'>D</span>E<span style='letter-spacing:-.1pt'> </span>F<span
style='letter-spacing:-.05pt'>I</span><span style='letter-spacing:.05pt'>S</span><span
style='letter-spacing:-.05pt'>C</span>A<span style='letter-spacing:.05pt'>L</span><span
style='letter-spacing:.1pt'>I</span><span style='letter-spacing:.05pt'>Z</span>AC<span
style='letter-spacing:-.05pt'>I</span>ÓN<span style='letter-spacing:-.7pt'> </span>Y<span
style='letter-spacing:.05pt'> </span><span style='letter-spacing:-.05pt'>C</span>ON<span
style='letter-spacing:.05pt'>T</span><span style='letter-spacing:.1pt'>R</span>OL<span
style='letter-spacing:-.5pt'> </span><span style='letter-spacing:.05pt'>S</span><span
style='letter-spacing:.1pt'>O</span><span style='letter-spacing:-.05pt'>C</span>IAL<span
style='letter-spacing:-.4pt'> </span>DE<span style='letter-spacing:-.05pt'> </span>BO<span
style='letter-spacing:.05pt'>S</span><span style='letter-spacing:.1pt'>Q</span><span
style='letter-spacing:-.05pt'>U</span>ES<span style='letter-spacing:-.5pt'> </span>Y<span
style='letter-spacing:.05pt'> </span>TI<span style='letter-spacing:.1pt'>E</span><span
style='letter-spacing:-.05pt'>RR</span>A DIR<span style='letter-spacing:.05pt'>E</span><span
style='letter-spacing:-.05pt'>C</span><span style='letter-spacing:.05pt'>C</span>IÓN<span
style='letter-spacing:-.55pt'> </span>DE<span style='letter-spacing:.05pt'>P</span>ART<span
style='letter-spacing:.15pt'>A</span><span style='letter-spacing:.1pt'>M</span>ENT<span
style='letter-spacing:.05pt'>A</span>L<span style='letter-spacing:-.9pt'> </span><span
style='letter-spacing:.05pt'>S</span>AN<span style='letter-spacing:.05pt'>T</span>A<span
style='letter-spacing:-.25pt'> </span><span style='letter-spacing:-.05pt'>C</span><span
style='letter-spacing:.1pt'>R</span><span style='letter-spacing:-.05pt'>U</span>Z</span></b><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:309.6pt;margin-bottom:
0cm;margin-left:16.1pt;margin-bottom:.0001pt;text-align:justify;line-height:
11.65pt;mso-line-height-rule:exactly;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><b><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif;
position:relative;top:.5pt;mso-text-raise:-.5pt;letter-spacing:-.05pt'>R</span></b><b><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
top:.5pt;mso-text-raise:-.5pt'>D-ABT<span style='letter-spacing:.05pt'>-</span>DD<span
style='letter-spacing:.05pt'>S</span><span style='letter-spacing:-.05pt'>C</span><span
style='letter-spacing:.15pt'>-</span><span style='letter-spacing:-.05pt'>R</span><span
style='letter-spacing:.1pt'>E</span>F<span style='letter-spacing:.05pt'>-</span>0<span
style='letter-spacing:.05pt'>0</span><span style='letter-spacing:.15pt'>6</span>0<span
style='letter-spacing:.1pt'>2</span><span style='letter-spacing:.05pt'>-</span>2017</span></b><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:296.9pt;margin-bottom:
0cm;margin-left:16.1pt;margin-bottom:.0001pt;text-align:justify;line-height:
12.05pt;mso-line-height-rule:exactly;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><b><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif;
position:relative;top:.5pt;mso-text-raise:-.5pt;letter-spacing:.05pt'>S</span></b><b><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
top:.5pt;mso-text-raise:-.5pt'>an<span style='letter-spacing:-.1pt'>t</span>a<span
style='letter-spacing:-.2pt'> </span><span style='letter-spacing:-.05pt'>C</span>ruz,<span
style='letter-spacing:-.25pt'> </span>


<?php echo $fecha_registro;?>
</span></b><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:.95pt;margin-right:0cm;margin-bottom:0cm;
margin-left:0cm;margin-bottom:.0001pt;line-height:11.0pt;mso-line-height-rule:
exactly;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><span
style='font-family:"Tahoma",sans-serif'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:332.35pt;margin-bottom:
0cm;margin-left:16.1pt;margin-bottom:.0001pt;text-align:justify;line-height:
normal;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif'>VIST<span
style='letter-spacing:.05pt'>O</span>S<span style='letter-spacing:-.4pt'> </span>Y<span
style='letter-spacing:.05pt'> </span><span style='letter-spacing:-.05pt'>C</span>ON<span
style='letter-spacing:.05pt'>S</span>ID<span style='letter-spacing:.1pt'>E</span><span
style='letter-spacing:-.05pt'>R</span>A<span style='letter-spacing:.15pt'>N</span>DO:</span></b><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:.35pt;margin-right:0cm;margin-bottom:0cm;
margin-left:0cm;margin-bottom:.0001pt;line-height:12.0pt;mso-line-height-rule:
exactly;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><span
style='font-size:12.0pt;font-family:"Tahoma",sans-serif'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:14.15pt;margin-bottom:
0cm;margin-left:16.1pt;margin-bottom:.0001pt;text-align:justify;line-height:
12.0pt;mso-line-height-rule:exactly;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif'>Que,
la<span style='letter-spacing:.3pt'> </span><span style='letter-spacing:-.05pt'>L</span><span
style='letter-spacing:.05pt'>e</span>y<span style='letter-spacing:.15pt'> </span>F<span
style='letter-spacing:-.05pt'>o</span>r<span style='letter-spacing:.05pt'>e</span>st<span
style='letter-spacing:.05pt'>a</span>l <span style='letter-spacing:-.05pt'>N</span><span
style='letter-spacing:.1pt'>o</span>.<span style='letter-spacing:.2pt'> </span><span
style='letter-spacing:-.05pt'>1</span><span style='letter-spacing:.05pt'>7</span><span
style='letter-spacing:-.05pt'>0</span>0<span style='letter-spacing:.1pt'> </span>d<span
style='letter-spacing:.05pt'>e</span>l<span style='letter-spacing:.1pt'> </span><span
style='letter-spacing:.05pt'>1</span>2<span style='letter-spacing:.1pt'> </span>de<span
style='letter-spacing:.3pt'> </span><span style='letter-spacing:.05pt'>j</span><span
style='letter-spacing:-.05pt'>u</span>lio<span style='letter-spacing:.15pt'> </span>de<span
style='letter-spacing:.15pt'> </span><span style='letter-spacing:.05pt'>1</span><span
style='letter-spacing:-.05pt'>9</span><span style='letter-spacing:.05pt'>9</span>6<span
style='letter-spacing:.1pt'> </span><span style='letter-spacing:.05pt'>e</span>st<span
style='letter-spacing:.05pt'>a</span>bl<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>c</span><span style='letter-spacing:.05pt'>e</span>:<span
style='letter-spacing:-.25pt'> </span>P<span style='letter-spacing:.05pt'>a</span>ra<span
style='letter-spacing:.1pt'> </span><span style='letter-spacing:.05pt'>e</span>l<span
style='letter-spacing:.15pt'> </span>o<span style='letter-spacing:.15pt'>t</span>org<span
style='letter-spacing:.05pt'>a</span>mi<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>n</span>to<span style='letter-spacing:-.35pt'> </span>y<span
style='letter-spacing:.25pt'> </span><span style='letter-spacing:-.05pt'>v</span><span
style='letter-spacing:.1pt'>i</span>g<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>nc</span>ia<span style='letter-spacing:.05pt'> </span>de<span
style='letter-spacing:.15pt'> </span>la <span style='letter-spacing:.05pt'>a</span><span
style='letter-spacing:-.05pt'>u</span>toriz<span style='letter-spacing:.05pt'>a</span><span
style='letter-spacing:-.05pt'>c</span>ión<span style='letter-spacing:.1pt'> </span>de<span
style='letter-spacing:.7pt'> </span><span style='letter-spacing:-.05pt'>f</span><span
style='letter-spacing:.05pt'>u</span><span style='letter-spacing:-.05pt'>nc</span>i<span
style='letter-spacing:.1pt'>o</span><span style='letter-spacing:-.05pt'>n</span><span
style='letter-spacing:.05pt'>a</span><span style='letter-spacing:.15pt'>m</span>i<span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>n</span>to
de<span style='letter-spacing:.6pt'> </span><span style='letter-spacing:-.05pt'>c</span><span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>n</span>tros<span
style='letter-spacing:.35pt'> </span>de<span style='letter-spacing:.6pt'> </span>pr<span
style='letter-spacing:.15pt'>o</span><span style='letter-spacing:.1pt'>c</span><span
style='letter-spacing:.05pt'>e</span>s<span style='letter-spacing:.05pt'>a</span>mi<span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>n</span>to<span
style='letter-spacing:.05pt'> </span>pri<span style='letter-spacing:.05pt'>ma</span>rio<span
style='letter-spacing:.3pt'> </span>de<span style='letter-spacing:.6pt'> </span>prod<span
style='letter-spacing:-.05pt'>uc</span>t<span style='letter-spacing:.1pt'>o</span>s<span
style='letter-spacing:.25pt'> </span><span style='letter-spacing:-.05pt'>f</span>or<span
style='letter-spacing:.05pt'>e</span>st<span style='letter-spacing:.05pt'>a</span>l<span
style='letter-spacing:.05pt'>e</span>s<span style='letter-spacing:.25pt'> </span>se
d<span style='letter-spacing:.05pt'>e</span>b<span style='letter-spacing:.05pt'>e</span>rá
pr<span style='letter-spacing:.1pt'>e</span>se<span style='letter-spacing:-.05pt'>n</span>t<span
style='letter-spacing:.05pt'>a</span>r<span style='letter-spacing:-.15pt'> </span>y<span
style='letter-spacing:.15pt'> </span><span style='letter-spacing:.05pt'>a</span><span
style='letter-spacing:-.05pt'>c</span><span style='letter-spacing:.15pt'>t</span><span
style='letter-spacing:-.05pt'>u</span><span style='letter-spacing:.05pt'>a</span>liz<span
style='letter-spacing:.05pt'>a</span>r<span style='letter-spacing:-.15pt'> </span><span
style='letter-spacing:.05pt'>a</span><span style='letter-spacing:-.05pt'>nu</span><span
style='letter-spacing:.05pt'>a</span>l<span style='letter-spacing:.05pt'>me</span><span
style='letter-spacing:-.05pt'>n</span>te<span style='letter-spacing:-.25pt'> </span><span
style='letter-spacing:.05pt'>u</span>n<span style='letter-spacing:.1pt'> </span>Progr<span
style='letter-spacing:.05pt'>a</span><span style='letter-spacing:.15pt'>m</span>a<span
style='letter-spacing:-.15pt'> </span>de<span style='letter-spacing:.15pt'> </span>A<span
style='letter-spacing:.05pt'>ba</span>st<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>c</span>i<span style='letter-spacing:.05pt'>m</span>i<span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>n</span>to<span
style='letter-spacing:-.3pt'> </span>y<span style='letter-spacing:.15pt'> </span>Pr<span
style='letter-spacing:.1pt'>oc</span><span style='letter-spacing:.05pt'>e</span>s<span
style='letter-spacing:.05pt'>a</span>mi<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>n</span>to<span style='letter-spacing:-.4pt'> </span>de<span
style='letter-spacing:.15pt'> </span>M<span style='letter-spacing:.05pt'>a</span>t<span
style='letter-spacing:.05pt'>e</span>ria Pri<span style='letter-spacing:.05pt'>m</span>a<span
style='letter-spacing:.2pt'> </span><b><span style='letter-spacing:.05pt'>(P</span>A<span
style='letter-spacing:.05pt'>P</span>M<span style='letter-spacing:.05pt'>P</span><span
style='letter-spacing:.1pt'>)</span></b>,<span style='letter-spacing:-.15pt'> </span><span
style='letter-spacing:.05pt'>e</span>n<span style='letter-spacing:.2pt'> </span><span
style='letter-spacing:.05pt'>e</span>l<span style='letter-spacing:.25pt'> </span>que<span
style='letter-spacing:.2pt'> </span>se<span style='letter-spacing:.25pt'> </span><span
style='letter-spacing:.05pt'>e</span>sp<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>c</span>i<span style='letter-spacing:-.05pt'>f</span>i<span
style='letter-spacing:.15pt'>q</span><span style='letter-spacing:-.05pt'>u</span><span
style='letter-spacing:.05pt'>e</span>n<span style='letter-spacing:-.25pt'> </span>l<span
style='letter-spacing:.05pt'>a</span>s<span style='letter-spacing:.35pt'> </span><span
style='letter-spacing:-.05pt'>fu</span><span style='letter-spacing:.15pt'>e</span><span
style='letter-spacing:-.05pt'>n</span>t<span style='letter-spacing:.05pt'>e</span>s
y<span style='letter-spacing:.25pt'> </span><span style='letter-spacing:-.05pt'>c</span><span
style='letter-spacing:.15pt'>a</span><span style='letter-spacing:-.05pt'>n</span>tid<span
style='letter-spacing:.05pt'>a</span>d<span style='letter-spacing:.05pt'>e</span>s<span
style='letter-spacing:-.1pt'> </span>a<span style='letter-spacing:.35pt'> </span><span
style='letter-spacing:-.05pt'>u</span>tiliz<span style='letter-spacing:.05pt'>a</span>r,<span
style='letter-spacing:.05pt'> </span>l<span style='letter-spacing:.15pt'>a</span>s<span
style='letter-spacing:.25pt'> </span>que<span style='letter-spacing:.2pt'> </span><span
style='letter-spacing:-.05pt'>n</span><span style='letter-spacing:.15pt'>e</span><span
style='letter-spacing:-.05pt'>c</span><span style='letter-spacing:.05pt'>e</span>s<span
style='letter-spacing:.05pt'>a</span>ri<span style='letter-spacing:.05pt'>a</span>m<span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>n</span>te
d<span style='letter-spacing:.05pt'>e</span>b<span style='letter-spacing:.05pt'>e</span>r<span
style='letter-spacing:.05pt'>á</span>n<span style='letter-spacing:1.85pt'> </span>proced<span
style='letter-spacing:.05pt'>e</span>r<span style='letter-spacing:1.9pt'> </span>de<span
style='letter-spacing:2.2pt'> </span>b<span style='letter-spacing:.1pt'>os</span>ques<span
style='letter-spacing:1.9pt'> </span>m<span style='letter-spacing:.05pt'>a</span><span
style='letter-spacing:-.05pt'>n</span><span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>j</span><span style='letter-spacing:.05pt'>a</span>d<span
style='letter-spacing:.1pt'>o</span>s,<span style='letter-spacing:1.75pt'> </span>s<span
style='letter-spacing:.05pt'>a</span>l<span style='letter-spacing:.05pt'>v</span>o<span
style='letter-spacing:2.05pt'> </span>los<span style='letter-spacing:2.25pt'> </span><span
style='letter-spacing:-.05pt'>c</span><span style='letter-spacing:.05pt'>a</span>sos<span
style='letter-spacing:2.05pt'> </span>de<span style='letter-spacing:2.2pt'> </span>d<span
style='letter-spacing:.05pt'>e</span>sm<span style='letter-spacing:.1pt'>o</span><span
style='letter-spacing:-.05pt'>n</span>te<span style='letter-spacing:2.3pt'> </span>d<span
style='letter-spacing:.05pt'>e</span>b<span style='letter-spacing:.15pt'>i</span>d<span
style='letter-spacing:.05pt'>a</span>m<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>n</span>te<span style='letter-spacing:1.7pt'> </span><span
style='letter-spacing:.05pt'>a</span><span style='letter-spacing:-.05pt'>u</span>toriz<span
style='letter-spacing:.05pt'>a</span>dos. <span style='letter-spacing:-.05pt'>D</span>i<span
style='letter-spacing:-.05pt'>ch</span>a<span style='letter-spacing:.4pt'> </span><span
style='letter-spacing:.05pt'>a</span><span style='letter-spacing:-.05pt'>u</span>toriz<span
style='letter-spacing:.05pt'>a</span><span style='letter-spacing:-.05pt'>c</span><span
style='letter-spacing:.1pt'>i</span>ón<span style='letter-spacing:.1pt'> </span><span
style='letter-spacing:-.05pt'>c</span><span style='letter-spacing:.1pt'>o</span><span
style='letter-spacing:-.05pt'>n</span>stit<span style='letter-spacing:.05pt'>u</span><span
style='letter-spacing:-.05pt'>y</span>e<span style='letter-spacing:.2pt'> </span><span
style='letter-spacing:-.05pt'>un</span>a<span style='letter-spacing:.5pt'> </span>lic<span
style='letter-spacing:.05pt'>en</span><span style='letter-spacing:-.05pt'>c</span>ia<span
style='letter-spacing:.4pt'> </span><span style='letter-spacing:.05pt'>a</span>d<span
style='letter-spacing:.05pt'>m</span>i<span style='letter-spacing:-.05pt'>n</span>istr<span
style='letter-spacing:.05pt'>a</span>ti<span style='letter-spacing:-.05pt'>v</span>a<span
style='letter-spacing:.05pt'> </span><span style='letter-spacing:-.05pt'>cuy</span>a<span
style='letter-spacing:.45pt'> </span><span style='letter-spacing:-.05pt'>c</span><span
style='letter-spacing:.1pt'>o</span><span style='letter-spacing:-.05pt'>n</span>tr<span
style='letter-spacing:.05pt'>a</span><span style='letter-spacing:-.05pt'>v</span><span
style='letter-spacing:.05pt'>en</span><span style='letter-spacing:-.05pt'>c</span>ión
da<span style='letter-spacing:.6pt'> </span>l<span style='letter-spacing:-.05pt'>u</span>g<span
style='letter-spacing:.05pt'>a</span>r<span style='letter-spacing:.45pt'> </span>a<span
style='letter-spacing:.6pt'> </span>la<span style='letter-spacing:.65pt'> </span>s<span
style='letter-spacing:-.05pt'>u</span>sp<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>n</span>sión t<span style='letter-spacing:.05pt'>e</span>mpor<span
style='letter-spacing:.05pt'>a</span>l<span style='letter-spacing:.15pt'> </span>o<span
style='letter-spacing:.5pt'> </span><span style='letter-spacing:-.05pt'>c</span><span
style='letter-spacing:.05pt'>a</span><span style='letter-spacing:-.05pt'>nc</span><span
style='letter-spacing:.05pt'>e</span>l<span style='letter-spacing:.05pt'>a</span><span
style='letter-spacing:-.05pt'>c</span>i<span style='letter-spacing:.1pt'>ó</span>n
d<span style='letter-spacing:.05pt'>ef</span>i<span style='letter-spacing:-.05pt'>n</span>iti<span
style='letter-spacing:-.05pt'>v</span>a<span style='letter-spacing:.2pt'> </span>de<span
style='letter-spacing:.5pt'> </span>l<span style='letter-spacing:.05pt'>a</span>s<span
style='letter-spacing:.45pt'> </span><span style='letter-spacing:.05pt'>a</span><span
style='letter-spacing:-.05pt'>c</span>ti<span style='letter-spacing:-.05pt'>v</span>id<span
style='letter-spacing:.05pt'>a</span>d<span style='letter-spacing:.05pt'>e</span>s,<span
style='letter-spacing:.05pt'> </span><span style='letter-spacing:.1pt'>s</span>in<span
style='letter-spacing:.4pt'> </span>p<span style='letter-spacing:.05pt'>e</span>r<span
style='letter-spacing:-.05pt'>ju</span><span style='letter-spacing:.1pt'>i</span><span
style='letter-spacing:-.05pt'>c</span>io<span style='letter-spacing:.2pt'> </span>de<span
style='letter-spacing:.5pt'> </span>l<span style='letter-spacing:.05pt'>a</span>s<span
style='letter-spacing:.45pt'> </span>s<span style='letter-spacing:.05pt'>a</span><span
style='letter-spacing:-.05pt'>nc</span>i<span style='letter-spacing:.1pt'>o</span><span
style='letter-spacing:-.05pt'>n</span><span style='letter-spacing:.15pt'>e</span>s<span
style='letter-spacing:.15pt'> </span><span style='letter-spacing:-.05pt'>c</span>i<span
style='letter-spacing:-.05pt'>v</span>il<span style='letter-spacing:.05pt'>e</span>s<span
style='letter-spacing:.3pt'> </span>y<span style='letter-spacing:.45pt'> </span>p<span
style='letter-spacing:.15pt'>e</span><span style='letter-spacing:-.05pt'>n</span><span
style='letter-spacing:.05pt'>a</span>l<span style='letter-spacing:.05pt'>e</span>s<span
style='letter-spacing:.2pt'> </span>a que<span style='letter-spacing:-.15pt'> </span>h<span
style='letter-spacing:-.05pt'>u</span><span style='letter-spacing:.1pt'>b</span>i<span
style='letter-spacing:.05pt'>e</span>se<span style='letter-spacing:-.35pt'> </span>l<span
style='letter-spacing:-.05pt'>u</span>g<span style='letter-spacing:.05pt'>a</span>r.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
12.0pt;mso-line-height-rule:exactly;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><span style='font-size:12.0pt;font-family:"Tahoma",sans-serif'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:14.3pt;margin-bottom:
0cm;margin-left:16.1pt;margin-bottom:.0001pt;text-align:justify;line-height:
12.0pt;mso-line-height-rule:exactly;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif;
letter-spacing:.05pt'>Q</span><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif;
letter-spacing:-.05pt'>u</span><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif;
letter-spacing:.05pt'>e</span><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif'>,<span
style='letter-spacing:.45pt'> </span><span style='letter-spacing:-.05pt'>v</span>isto<span
style='letter-spacing:.45pt'> </span><span style='letter-spacing:.05pt'>e</span>l<span
style='letter-spacing:.7pt'> </span><b><span style='letter-spacing:.05pt'>P</span>A<span
style='letter-spacing:.05pt'>P</span>M<span style='letter-spacing:.05pt'>P</span>,<span
style='letter-spacing:.35pt'> </span></b>se<span style='letter-spacing:.6pt'> </span>s<span
style='letter-spacing:-.05pt'>u</span>gi<span style='letter-spacing:.05pt'>e</span>re<span
style='letter-spacing:.4pt'> </span>la<span style='letter-spacing:.65pt'> </span><span
style='letter-spacing:.05pt'>a</span>prob<span style='letter-spacing:.05pt'>a</span><span
style='letter-spacing:-.05pt'>c</span>ión<span style='letter-spacing:.2pt'> </span>d<span
style='letter-spacing:.05pt'>e</span>l<span style='letter-spacing:.6pt'> </span>progr<span
style='letter-spacing:.05pt'>a</span>ma<span style='letter-spacing:.3pt'> </span>de<span
style='letter-spacing:.6pt'> </span><span style='letter-spacing:.05pt'>a</span><span
style='letter-spacing:-.1pt'>b</span><span style='letter-spacing:.05pt'>a</span>st<span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>c</span>i<span
style='letter-spacing:.05pt'>m</span>i<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>n</span>to y<span style='letter-spacing:.6pt'> </span>proces<span
style='letter-spacing:.05pt'>a</span>mi<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>n</span>to de m<span style='letter-spacing:.05pt'>a</span>t<span
style='letter-spacing:.05pt'>e</span>ria<span style='letter-spacing:-.2pt'> </span><span
style='letter-spacing:-.05pt'>201</span>6<span style='letter-spacing:-.15pt'> </span>(<span
style='letter-spacing:.05pt'>P</span>A<span style='letter-spacing:.05pt'>P</span>M<span
style='letter-spacing:.15pt'>P</span>-<span style='letter-spacing:.05pt'>2</span><span
style='letter-spacing:-.05pt'>0</span><span style='letter-spacing:.05pt'>16</span>).<span
style='letter-spacing:-.45pt'> </span><span style='letter-spacing:.05pt'>E</span>l
i<span style='letter-spacing:-.05pt'>n</span>t<span style='letter-spacing:.05pt'>e</span>r<span
style='letter-spacing:.05pt'>e</span>s<span style='letter-spacing:.05pt'>a</span>do<span
style='letter-spacing:-.35pt'> </span><span style='letter-spacing:-.05pt'>c</span><span
style='letter-spacing:.05pt'>a</span><span style='letter-spacing:-.05pt'>nc</span><span
style='letter-spacing:.05pt'>e</span>la<span style='letter-spacing:-.2pt'> </span><span
style='letter-spacing:.05pt'>e</span>l <span style='letter-spacing:-.05pt'>v</span><span
style='letter-spacing:.05pt'>a</span>lor<span style='letter-spacing:-.1pt'> </span>de<span
style='letter-spacing:.05pt'> </span>la<span style='letter-spacing:.05pt'> </span>r<span
style='letter-spacing:.05pt'>e</span>i<span style='letter-spacing:-.05pt'>n</span>s<span
style='letter-spacing:-.05pt'>c</span>ripción<span style='letter-spacing:-.5pt'>
</span>m<span style='letter-spacing:.05pt'>e</span><span style='letter-spacing:
.1pt'>d</span>i<span style='letter-spacing:.05pt'>a</span><span
style='letter-spacing:-.05pt'>n</span>te<span style='letter-spacing:-.25pt'> </span>bol<span
style='letter-spacing:.05pt'>e</span>ta<span style='letter-spacing:-.1pt'> </span>de<span
style='letter-spacing:.05pt'> </span><span style='letter-spacing:-.1pt'>p</span><span
style='letter-spacing:.05pt'>a</span><span style='letter-spacing:-.1pt'>g</span>o
<span style='letter-spacing:-.05pt'>N</span>º<span style='letter-spacing:.1pt'>7</span><span
style='letter-spacing:-.05pt'>1</span><span style='letter-spacing:.05pt'>8</span><span
style='letter-spacing:-.05pt'>4</span><span style='letter-spacing:.05pt'>5</span><span
style='letter-spacing:-.05pt'>0</span><span style='letter-spacing:.05pt'>9</span>4<span
style='letter-spacing:-.6pt'> </span>de<span style='letter-spacing:-.05pt'> f</span><span
style='letter-spacing:.15pt'>e</span><span style='letter-spacing:-.05pt'>ch</span>a<span
style='letter-spacing:-.2pt'> </span><span style='letter-spacing:.05pt'>0</span><span
style='letter-spacing:-.05pt'>1</span><span style='letter-spacing:.05pt'>/0</span><span
style='letter-spacing:-.05pt'>2</span><span style='letter-spacing:.05pt'>/</span><span
style='letter-spacing:-.05pt'>2</span><span style='letter-spacing:.05pt'>0</span><span
style='letter-spacing:-.05pt'>1</span><span style='letter-spacing:.05pt'>6</span>.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
12.0pt;mso-line-height-rule:exactly;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><span style='font-size:12.0pt;font-family:"Tahoma",sans-serif'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:14.55pt;margin-bottom:
0cm;margin-left:16.1pt;margin-bottom:.0001pt;text-align:justify;line-height:
12.0pt;mso-line-height-rule:exactly;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif;
letter-spacing:.05pt'>E</span><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif'>l<span
style='letter-spacing:.3pt'> </span>ob<span style='letter-spacing:-.05pt'>j</span><span
style='letter-spacing:.05pt'>e</span>ti<span style='letter-spacing:.05pt'>v</span>o<span
style='letter-spacing:.05pt'> </span>d<span style='letter-spacing:.05pt'>e</span>l<span
style='letter-spacing:.5pt'> </span><b><span style='letter-spacing:.05pt'>P</span>A<span
style='letter-spacing:.05pt'>P</span>M<span style='letter-spacing:.1pt'>P</span></b>,
<span style='letter-spacing:.15pt'>e</span>s<span style='letter-spacing:.3pt'> </span><span
style='letter-spacing:.05pt'>ga</span>r<span style='letter-spacing:.05pt'>a</span><span
style='letter-spacing:-.05pt'>n</span>tiz<span style='letter-spacing:.05pt'>a</span>r
que<span style='letter-spacing:.4pt'> </span>la<span style='letter-spacing:
.35pt'> </span>m<span style='letter-spacing:.05pt'>a</span>d<span
style='letter-spacing:.15pt'>e</span>ra<span style='letter-spacing:.15pt'> </span>que<span
style='letter-spacing:.3pt'> </span><span style='letter-spacing:.05pt'>a</span>rri<span
style='letter-spacing:.05pt'>b</span>e<span style='letter-spacing:.2pt'> </span>a<span
style='letter-spacing:.55pt'> </span>los<span style='letter-spacing:.35pt'> </span><span
style='letter-spacing:-.05pt'>c</span><span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>n</span>t<span style='letter-spacing:.1pt'>r</span>os<span
style='letter-spacing:.05pt'> </span>de<span style='letter-spacing:.35pt'> </span>p<span
style='letter-spacing:.15pt'>r</span>o<span style='letter-spacing:-.05pt'>c</span><span
style='letter-spacing:.05pt'>e</span>s<span style='letter-spacing:.05pt'>a</span>mi<span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>n</span>to
provenga<span style='letter-spacing:-.4pt'> </span><span style='letter-spacing:
.05pt'>e</span><span style='letter-spacing:.1pt'>x</span><span
style='letter-spacing:-.05pt'>c</span>l<span style='letter-spacing:-.05pt'>u</span>s<span
style='letter-spacing:.1pt'>i</span><span style='letter-spacing:-.05pt'>v</span><span
style='letter-spacing:.05pt'>a</span>m<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>n</span>te<span style='letter-spacing:-.65pt'> </span><span
style='letter-spacing:.15pt'>d</span>e<span style='letter-spacing:-.1pt'> </span>bosques<span
style='letter-spacing:-.35pt'> </span><span style='letter-spacing:.05pt'>ma</span><span
style='letter-spacing:-.05pt'>n</span><span style='letter-spacing:.15pt'>e</span><span
style='letter-spacing:-.05pt'>j</span><span style='letter-spacing:.05pt'>a</span>dos<span
style='letter-spacing:-.5pt'> </span>o<span style='letter-spacing:-.05pt'> </span>d<span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:.1pt'>s</span>mo<span
style='letter-spacing:-.05pt'>n</span>t<span style='letter-spacing:.05pt'>e</span>s<span
style='letter-spacing:-.5pt'> </span>d<span style='letter-spacing:.05pt'>e</span>bid<span
style='letter-spacing:.05pt'>a</span>m<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>n</span>te<span style='letter-spacing:-.55pt'> </span><span
style='letter-spacing:.05pt'>a</span><span style='letter-spacing:-.05pt'>u</span>tor<span
style='letter-spacing:.1pt'>i</span>z<span style='letter-spacing:.05pt'>a</span>dos.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
12.0pt;mso-line-height-rule:exactly;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><u><span style='font-size:12.0pt;font-family:"Tahoma",sans-serif'><o:p><span
 style='text-decoration:none'>&nbsp;</span></o:p></span></u></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:14.05pt;margin-bottom:
0cm;margin-left:16.1pt;margin-bottom:.0001pt;text-align:justify;line-height:
12.0pt;mso-line-height-rule:exactly;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif'>Que<span
style='letter-spacing:.65pt'> </span>la<span style='letter-spacing:.8pt'> </span><span
style='letter-spacing:.05pt'>Re</span>sol<span style='letter-spacing:-.05pt'>uc</span>ión<span
style='letter-spacing:.35pt'> </span><span style='letter-spacing:.1pt'>M</span>i<span
style='letter-spacing:-.05pt'>n</span>ist<span style='letter-spacing:.05pt'>e</span>r<span
style='letter-spacing:.1pt'>i</span><span style='letter-spacing:.05pt'>a</span>l<span
style='letter-spacing:.4pt'> </span><span style='letter-spacing:-.05pt'>N</span>°<span
style='letter-spacing:.7pt'> </span><span style='letter-spacing:-.05pt'>1</span><span
style='letter-spacing:.05pt'>3</span><span style='letter-spacing:-.05pt'>4</span><span
style='letter-spacing:.05pt'>/9</span><span style='letter-spacing:-.05pt'>7</span>,<span
style='letter-spacing:.45pt'> </span>r<span style='letter-spacing:.05pt'>e</span>s<span
style='letter-spacing:-.05pt'>u</span><span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:.1pt'>l</span><span style='letter-spacing:-.05pt'>v</span>e<span
style='letter-spacing:.5pt'> </span><span style='letter-spacing:.05pt'>a</span>prob<span
style='letter-spacing:.05pt'>a</span>r<span style='letter-spacing:.5pt'> </span>la<span
style='letter-spacing:.8pt'> </span><span style='letter-spacing:-.05pt'>N</span>or<span
style='letter-spacing:.05pt'>m</span>a<span style='letter-spacing:.55pt'> </span><span
style='letter-spacing:-.05pt'>T</span><span style='letter-spacing:.05pt'>é</span><span
style='letter-spacing:-.05pt'>cn</span><span style='letter-spacing:.1pt'>i</span><span
style='letter-spacing:-.05pt'>c</span>a<span style='letter-spacing:.5pt'> </span>so<span
style='letter-spacing:.1pt'>b</span>re<span style='letter-spacing:.65pt'> </span>PA<span
style='letter-spacing:.05pt'>P</span>MP,<span style='letter-spacing:.7pt'> </span><span
style='letter-spacing:.05pt'>e</span>n<span style='letter-spacing:.7pt'> </span>la<span
style='letter-spacing:.8pt'> </span><span style='letter-spacing:-.05pt'>cu</span><span
style='letter-spacing:.05pt'>a</span>l se<span style='letter-spacing:-.1pt'> </span>d<span
style='letter-spacing:.05pt'>e</span>t<span style='letter-spacing:.05pt'>al</span>l<span
style='letter-spacing:.05pt'>a</span>n<span style='letter-spacing:-.4pt'> </span>los<span
style='letter-spacing:-.1pt'> </span>r<span style='letter-spacing:.05pt'>e</span>quisit<span
style='letter-spacing:.1pt'>o</span>s<span style='letter-spacing:-.4pt'> </span>t<span
style='letter-spacing:.05pt'>é</span><span style='letter-spacing:-.05pt'>cn</span>i<span
style='letter-spacing:.1pt'>c</span>os<span style='letter-spacing:-.35pt'> </span>míni<span
style='letter-spacing:.15pt'>m</span>os<span style='letter-spacing:-.35pt'> </span>p<span
style='letter-spacing:.05pt'>a</span>ra<span style='letter-spacing:-.15pt'> </span>la<span
style='letter-spacing:-.05pt'> </span>pr<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:.1pt'>s</span><span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>n</span>t<span style='letter-spacing:.05pt'>a</span><span
style='letter-spacing:-.05pt'>c</span>ión<span style='letter-spacing:-.6pt'> </span>de<span
style='letter-spacing:-.05pt'> </span>l<span style='letter-spacing:.1pt'>o</span>s<span
style='letter-spacing:-.1pt'> </span>mismo<span style='letter-spacing:.1pt'>s</span>.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
12.0pt;mso-line-height-rule:exactly;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><span style='font-size:12.0pt;font-family:"Tahoma",sans-serif'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:14.75pt;margin-bottom:
0cm;margin-left:16.1pt;margin-bottom:.0001pt;text-align:justify;line-height:
12.0pt;mso-line-height-rule:exactly;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif;
letter-spacing:.05pt'>E</span><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif'>l<span
style='letter-spacing:.5pt'> </span>profesi<span style='letter-spacing:.1pt'>o</span><span
style='letter-spacing:-.05pt'>n</span><span style='letter-spacing:.05pt'>a</span>l<span
style='letter-spacing:.1pt'> </span>r<span style='letter-spacing:.05pt'>e</span>sp<span
style='letter-spacing:.1pt'>o</span><span style='letter-spacing:-.05pt'>n</span>s<span
style='letter-spacing:.05pt'>a</span>ble<span style='letter-spacing:.2pt'> </span>de<span
style='letter-spacing:.5pt'> </span>la<span style='letter-spacing:.55pt'> </span><span
style='letter-spacing:.05pt'>e</span>mpr<span style='letter-spacing:.05pt'>e</span>sa<span
style='letter-spacing:.2pt'> </span>que<span style='letter-spacing:.45pt'> </span>a<span
style='letter-spacing:.7pt'> </span><span style='letter-spacing:-.05pt'>c</span>o<span
style='letter-spacing:-.05pt'>n</span>t<span style='letter-spacing:.1pt'>i</span><span
style='letter-spacing:-.05pt'>nu</span><span style='letter-spacing:.05pt'>a</span><span
style='letter-spacing:-.05pt'>c</span>i<span style='letter-spacing:.1pt'>ó</span>n
se<span style='letter-spacing:.65pt'> </span>d<span style='letter-spacing:.05pt'>e</span>t<span
style='letter-spacing:.05pt'>a</span>ll<span style='letter-spacing:.05pt'>a</span>,<span
style='letter-spacing:.3pt'> </span>pr<span style='letter-spacing:.05pt'>e</span>se<span
style='letter-spacing:-.05pt'>n</span><span style='letter-spacing:.15pt'>t</span>a<span
style='letter-spacing:.25pt'> </span>p<span style='letter-spacing:.05pt'>a</span>ra<span
style='letter-spacing:.45pt'> </span>su<span style='letter-spacing:.45pt'> </span><span
style='letter-spacing:.05pt'>a</span>prob<span style='letter-spacing:.05pt'>a</span><span
style='letter-spacing:-.05pt'>c</span><span style='letter-spacing:.1pt'>i</span>ó<span
style='letter-spacing:-.05pt'>n</span>, su<span style='letter-spacing:-.15pt'> </span>Progr<span
style='letter-spacing:.05pt'>a</span>ma<span style='letter-spacing:-.4pt'> </span>de
A<span style='letter-spacing:.05pt'>ba</span>st<span style='letter-spacing:
.05pt'>e</span><span style='letter-spacing:-.05pt'>c</span>i<span
style='letter-spacing:.05pt'>m</span><span style='letter-spacing:.1pt'>i</span><span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>n</span>to<span
style='letter-spacing:-.65pt'> </span>y<span style='letter-spacing:-.1pt'> </span>Pr<span
style='letter-spacing:.1pt'>o</span><span style='letter-spacing:-.05pt'>c</span><span
style='letter-spacing:.05pt'>e</span>s<span style='letter-spacing:.05pt'>a</span>mi<span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>n</span>to<span
style='letter-spacing:-.65pt'> </span>de<span style='letter-spacing:-.05pt'> </span><span
style='letter-spacing:.1pt'>M</span><span style='letter-spacing:.05pt'>a</span>t<span
style='letter-spacing:.05pt'>e</span>ria<span style='letter-spacing:-.3pt'> </span>Pri<span
style='letter-spacing:.05pt'>ma</span>.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:14.75pt;margin-bottom:
0cm;margin-left:16.1pt;margin-bottom:.0001pt;text-align:justify;line-height:
12.0pt;mso-line-height-rule:exactly;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif'><o:p>&nbsp;</o:p></span></p>

<table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0
 style='margin-left:-.4pt;border-collapse:collapse;border:none;mso-border-alt:
 solid windowtext .5pt;mso-yfti-tbllook:1184;mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
 <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:16.75pt'>
  <td width=1400 colspan=4 valign=top style='width:1050.05pt;border:solid windowtext 1.0pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.75pt'>
  <p class=MsoNormal style='margin-top:.3pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:0cm;margin-bottom:.0001pt;line-height:normal;mso-pagination:
  none;mso-layout-grid-align:none;text-autospace:none'><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt;letter-spacing:-.05pt'>R</span></b><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt'>A<span style='letter-spacing:.05pt'>Z</span>ON<span
  style='letter-spacing:-.35pt'> </span>S<span style='letter-spacing:.15pt'>O</span><span
  style='letter-spacing:-.05pt'>C</span>IA<span style='letter-spacing:.05pt'>L</span>:
  </span></b><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif;
  position:relative;top:.5pt;mso-text-raise:-.5pt;mso-bidi-font-weight:bold'>



    <?php echo strtoupper($razon_social);?>
  </span><span
  style='font-size:10.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:1;height:18.0pt'>
  <td width=1400 colspan=4 valign=top style='width:1050.05pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:18.0pt'>
  <p class=MsoNormal style='margin-top:.3pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:0cm;margin-bottom:.0001pt;line-height:10.0pt;mso-line-height-rule:
  exactly;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt;letter-spacing:-.05pt'>R</span></b><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt'>E<span style='letter-spacing:.05pt'>P</span><span
  style='letter-spacing:-.05pt'>R</span>E<span style='letter-spacing:.05pt'>S</span><span
  style='letter-spacing:.1pt'>E</span>NT<span style='letter-spacing:.05pt'>A</span>NTE<span
  style='letter-spacing:-.75pt'> </span><span style='letter-spacing:.05pt'>L</span>EGA<span
  style='letter-spacing:.05pt'>L</span>:<span style='mso-spacerun:yes'> 
  </span></span></b><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif;
  position:relative;top:.5pt;mso-text-raise:-.5pt;mso-bidi-font-weight:bold'>

  <?php echo strtoupper($representante_legal);?>
  <span style='mso-spacerun:yes; padding: 231px;'> </span>

    
   <?php echo strtoupper($representante_legal_ci);?>
  <b><span style='letter-spacing:
  -.05pt'><o:p></o:p></span></b></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:2;height:16.75pt'>
  <td width=652 colspan=2 valign=top style='width:488.95pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.75pt'>
  <p class=MsoNormal style='margin-top:.3pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:0cm;margin-bottom:.0001pt;line-height:10.0pt;mso-line-height-rule:
  exactly;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt;letter-spacing:.15pt'>A</span></b><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt;letter-spacing:-.05pt'>C</span></b><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt'>TIV<span style='letter-spacing:.1pt'>I</span>D<span
  style='letter-spacing:.05pt'>AD: </span></span></b><span style='font-size:
  10.0pt;font-family:"Tahoma",sans-serif;position:relative;top:.5pt;mso-text-raise:
  -.5pt;letter-spacing:.05pt;mso-bidi-font-weight:bold'>

   <?php echo strtoupper($actividad);?>
  </span><span
  style='font-size:10.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
  </td>
  <td width=318 valign=top style='width:238.2pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.75pt'>
  <p class=MsoNormal style='margin-top:.3pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:0cm;margin-bottom:.0001pt;line-height:10.0pt;mso-line-height-rule:
  exactly;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt;letter-spacing:-.05pt'>C</span></b><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt'>A<span style='letter-spacing:.05pt'>T</span>EG<span
  style='letter-spacing:.15pt'>O</span><span style='letter-spacing:-.05pt'>R</span>IA:
  </span></b><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif;
  position:relative;top:.5pt;mso-text-raise:-.5pt;mso-bidi-font-weight:bold'>


   <?php echo strtoupper($categoria);?>
  </span><span
  style='font-size:10.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
  </td>
  <td width=431 valign=top style='width:322.9pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.75pt'>
  <p class=MsoNormal style='margin-top:.3pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:0cm;margin-bottom:.0001pt;line-height:10.0pt;mso-line-height-rule:
  exactly;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt;letter-spacing:-.05pt'>UBICACIÓN</span></b><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt;letter-spacing:.05pt'>: </span></b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt;letter-spacing:.05pt;mso-bidi-font-weight:bold'>


   <?php echo strtoupper($ubicacion);?>
  </span><span
  style='font-size:10.0pt;font-family:"Times New Roman",serif'><o:p></o:p></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:3;height:16.75pt'>
  <td width=312 valign=top style='width:233.9pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.75pt'>
  <p class=MsoNormal style='margin-top:.3pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:0cm;margin-bottom:.0001pt;line-height:10.0pt;mso-line-height-rule:
  exactly;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt;letter-spacing:-.05pt'>R</span></b><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt'>EG.: </span></b><span style='font-size:10.0pt;
  font-family:"Tahoma",sans-serif;position:relative;top:.5pt;mso-text-raise:
  -.5pt;mso-bidi-font-weight:bold'>


   <?php echo strtoupper($codigo_registro);?>
  <b><span style='letter-spacing:-.05pt'><o:p></o:p></span></b></span></p>
  </td>
  <td width=658 colspan=2 valign=top style='width:493.25pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.75pt'>
  <p class=MsoNormal style='margin-top:.3pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:0cm;margin-bottom:.0001pt;line-height:10.0pt;mso-line-height-rule:
  exactly;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt'>GE<span style='letter-spacing:.05pt'>S</span>TIÓN:
  </span></b><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif;
  position:relative;top:.5pt;mso-text-raise:-.5pt;mso-bidi-font-weight:bold'>


  <?php echo strtoupper($gestion);?>
  <b><span
  style='letter-spacing:-.05pt'><o:p></o:p></span></b></span></p>
  </td>
  <td width=431 valign=top style='width:322.9pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.75pt'>
  <p class=MsoNormal style='margin-top:.3pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:0cm;margin-bottom:.0001pt;line-height:10.0pt;mso-line-height-rule:
  exactly;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt'>TRAMI<span style='letter-spacing:.1pt'>T</span>E:
  </span></b><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif;
  position:relative;top:.5pt;mso-text-raise:-.5pt;mso-bidi-font-weight:bold'>


  <?php echo strtoupper($tramite);?>
  <b><span
  style='letter-spacing:-.05pt'><o:p></o:p></span></b></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:4;height:16.75pt'>
  <td width=970 colspan=3 valign=top style='width:727.15pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.75pt'>
  <p class=MsoNormal style='margin-top:.3pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:0cm;margin-bottom:.0001pt;line-height:10.0pt;mso-line-height-rule:
  exactly;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt;letter-spacing:.05pt'>P</span></b><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt;letter-spacing:-.05pt'>R</span></b><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt'>OFESI<span style='letter-spacing:.1pt'>O</span>NAL<span
  style='letter-spacing:-.7pt'> </span><span style='letter-spacing:.1pt'>R</span>E<span
  style='letter-spacing:.05pt'>SP</span>ON<span style='letter-spacing:.05pt'>S</span>AB<span
  style='letter-spacing:.1pt'>L</span>E: </span></b><u><span style='font-size:
  10.0pt;font-family:"Tahoma",sans-serif;position:relative;top:.5pt;mso-text-raise:
  -.5pt;mso-bidi-font-weight:bold'>


  <?php echo strtoupper($profesional_responsable);?>
  </span></u><b><span style='font-size:
  10.0pt;font-family:"Tahoma",sans-serif;position:relative;top:.5pt;mso-text-raise:
  -.5pt;letter-spacing:-.05pt'><o:p></o:p></span></b></p>
  </td>
  <td width=431 valign=top style='width:322.9pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.75pt'>
  <p class=MsoNormal style='margin-top:.3pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:0cm;margin-bottom:.0001pt;line-height:10.0pt;mso-line-height-rule:
  exactly;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt;letter-spacing:-.05pt'>R</span></b><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt'>EGI<span style='letter-spacing:.05pt'>S</span>T<span
  style='letter-spacing:.1pt'>R</span>O<span style='letter-spacing:-.55pt'> </span>AA:
  </span></b><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif;
  position:relative;top:.5pt;mso-text-raise:-.5pt;mso-bidi-font-weight:bold'>


  <?php echo strtoupper($registro_aa);?>
  <b><span
  style='letter-spacing:-.05pt'><o:p></o:p></span></b></span></p>
  </td>
 </tr>
 <tr style='mso-yfti-irow:5;mso-yfti-lastrow:yes;height:16.75pt'>
  <td width=312 valign=top style='width:233.9pt;border:solid windowtext 1.0pt;
  border-top:none;mso-border-top-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:16.75pt'>
  <p class=MsoNormal style='margin-top:.3pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:0cm;margin-bottom:.0001pt;line-height:10.0pt;mso-line-height-rule:
  exactly;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt'>DE<span style='letter-spacing:.05pt'>P</span>ARTA<span
  style='letter-spacing:.1pt'>M</span>ENTO: </span></b><span style='font-size:
  10.0pt;font-family:"Tahoma",sans-serif;position:relative;top:.5pt;mso-text-raise:
  -.5pt;mso-bidi-font-weight:bold'>


  <?php echo strtoupper($departamento);?>
  <b><u><span style='letter-spacing:-.05pt'><o:p></o:p></span></u></b></span></p>
  </td>
  <td width=658 colspan=2 valign=top style='width:493.25pt;border-top:none;
  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.75pt'>
  <p class=MsoNormal style='margin-top:.3pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:0cm;margin-bottom:.0001pt;line-height:10.0pt;mso-line-height-rule:
  exactly;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt;letter-spacing:.05pt'>P</span></b><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt;letter-spacing:-.05pt'>R</span></b><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt'>OVI<span style='letter-spacing:.1pt'>N</span><span
  style='letter-spacing:-.05pt'>C</span>IA: </span></b><span style='font-size:
  10.0pt;font-family:"Tahoma",sans-serif;position:relative;top:.5pt;mso-text-raise:
  -.5pt;mso-bidi-font-weight:bold'>


  <?php echo strtoupper($provincia);?>
  <b><span style='letter-spacing:-.05pt'><o:p></o:p></span></b></span></p>
  </td>
  <td width=431 valign=top style='width:322.9pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.75pt'>
  <p class=MsoNormal style='margin-top:.3pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:0cm;margin-bottom:.0001pt;line-height:10.0pt;mso-line-height-rule:
  exactly;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><b><span
  style='font-size:10.0pt;font-family:"Tahoma",sans-serif;position:relative;
  top:.5pt;mso-text-raise:-.5pt'>M<span style='letter-spacing:-.05pt'>U</span>N<span
  style='letter-spacing:.1pt'>I</span><span style='letter-spacing:-.05pt'>C</span>I<span
  style='letter-spacing:.05pt'>P</span><span style='letter-spacing:.1pt'>I</span>O:
  </span></b><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif;
  position:relative;top:.5pt;mso-text-raise:-.5pt;mso-bidi-font-weight:bold'>


  <?php echo strtoupper($municipio);?>
  <b><span
  style='letter-spacing:-.05pt'><o:p></o:p></span></b></span></p>
  </td>
 </tr>
 <![if !supportMisalignedColumns]>
 <tr height=0>
  <td width=174 style='border:none'></td>
  <td width=109 style='border:none'></td>
  <td width=157 style='border:none'></td>
  <td width=194 style='border:none'></td>
 </tr>
 <![endif]>
</table>

<p class=MsoNormal style='margin-top:.3pt;margin-right:0cm;margin-bottom:0cm;
margin-left:0cm;margin-bottom:.0001pt;line-height:10.0pt;mso-line-height-rule:
exactly;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Times New Roman",serif'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-top:1.25pt;margin-right:408.0pt;margin-bottom:
0cm;margin-left:16.1pt;margin-bottom:.0001pt;text-align:justify;line-height:
normal;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif;letter-spacing:.05pt'>P</span></b><b><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif'>OR<span
style='letter-spacing:-.25pt'> </span>T<span style='letter-spacing:.05pt'>A</span>NT<span
style='letter-spacing:.15pt'>O</span>:</span></b><span style='font-size:10.0pt;
font-family:"Tahoma",sans-serif'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:.3pt;margin-right:14.0pt;margin-bottom:
0cm;margin-left:16.1pt;margin-bottom:.0001pt;text-align:justify;line-height:
12.0pt;mso-line-height-rule:exactly;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif;
letter-spacing:.05pt'>E</span><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif'>l<span
style='letter-spacing:.75pt'> </span>s<span style='letter-spacing:-.05pt'>u</span><span
style='letter-spacing:.1pt'>s</span><span style='letter-spacing:-.05pt'>c</span>ri<span
style='letter-spacing:.05pt'>t</span>o<span style='letter-spacing:.4pt'> </span><span
style='letter-spacing:.05pt'>Re</span>sp<span style='letter-spacing:.1pt'>o</span><span
style='letter-spacing:-.05pt'>n</span>s<span style='letter-spacing:.05pt'>a</span>ble<span
style='letter-spacing:.25pt'> </span>de<span style='letter-spacing:1.0pt'> </span><span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>v</span><span
style='letter-spacing:.05pt'>a</span>l<span style='letter-spacing:-.05pt'>u</span><span
style='letter-spacing:.05pt'>a</span><span style='letter-spacing:-.05pt'>c</span><span
style='letter-spacing:.1pt'>i</span>ón<span style='letter-spacing:.4pt'> </span>y<span
style='letter-spacing:.7pt'> </span><span style='letter-spacing:.05pt'>a</span>prob<span
style='letter-spacing:.15pt'>a</span><span style='letter-spacing:-.05pt'>c</span>ión<span
style='letter-spacing:.45pt'> </span>de<span style='letter-spacing:.7pt'> </span>d<span
style='letter-spacing:.05pt'>e</span>r<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>ch</span><span style='letter-spacing:.1pt'>o</span>s<span
style='letter-spacing:.35pt'> </span>de<span style='letter-spacing:.7pt'> </span><span
style='letter-spacing:.05pt'>a</span>pr<span style='letter-spacing:.15pt'>o</span><span
style='letter-spacing:-.05pt'>v</span><span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:.1pt'>c</span><span style='letter-spacing:-.05pt'>h</span><span
style='letter-spacing:.05pt'>a</span>mi<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>n</span>to y<span style='letter-spacing:.8pt'> </span><span
style='letter-spacing:-.05pt'>u</span><span style='letter-spacing:.1pt'>s</span><span
style='letter-spacing:.4pt'>o</span>,<span style='letter-spacing:.55pt'> </span>de<span
style='letter-spacing:.85pt'> </span>la Autori<span style='letter-spacing:.05pt'>da</span>d<span
style='letter-spacing:.05pt'> </span>de<span style='letter-spacing:.4pt'> </span>F<span
style='letter-spacing:-.05pt'>i</span><span style='letter-spacing:.1pt'>s</span><span
style='letter-spacing:-.05pt'>c</span><span style='letter-spacing:.05pt'>a</span>liz<span
style='letter-spacing:.05pt'>a</span><span style='letter-spacing:-.05pt'>c</span>i<span
style='letter-spacing:.1pt'>ó</span>n y<span style='letter-spacing:.35pt'> </span>Contr<span
style='letter-spacing:.1pt'>o</span>l<span style='letter-spacing:.15pt'> </span>S<span
style='letter-spacing:.05pt'>o</span><span style='letter-spacing:-.05pt'>c</span>i<span
style='letter-spacing:.05pt'>a</span>l<span style='letter-spacing:.2pt'> </span>de<span
style='letter-spacing:.4pt'> </span>Bos<span style='letter-spacing:.15pt'>q</span><span
style='letter-spacing:-.05pt'>u</span><span style='letter-spacing:.15pt'>e</span>s<span
style='letter-spacing:.1pt'> </span>y<span style='letter-spacing:.35pt'> </span><span
style='letter-spacing:-.05pt'>T</span>i<span style='letter-spacing:.05pt'>e</span>rra<span
style='letter-spacing:.3pt'> </span>(<span style='letter-spacing:.05pt'>A</span><span
style='letter-spacing:.1pt'>B</span><span style='letter-spacing:-.05pt'>T</span>)<span
style='letter-spacing:.25pt'> </span><span style='letter-spacing:.05pt'>e</span>n<span
style='letter-spacing:.45pt'> </span><span style='letter-spacing:-.05pt'>u</span>so<span
style='letter-spacing:.3pt'> </span>de<span style='letter-spacing:.5pt'> </span>l<span
style='letter-spacing:.05pt'>a</span>s<span style='letter-spacing:.35pt'> </span><span
style='letter-spacing:-.05pt'>f</span><span style='letter-spacing:.05pt'>a</span><span
style='letter-spacing:-.05pt'>cu</span>lt<span style='letter-spacing:.05pt'>a</span><span
style='letter-spacing:.5pt'>d</span><span style='letter-spacing:.05pt'>e</span>s
<span style='letter-spacing:.1pt'>q</span><span style='letter-spacing:-.05pt'>u</span>e<span
style='letter-spacing:.35pt'> </span>le <span style='letter-spacing:-.05pt'>c</span>o<span
style='letter-spacing:.05pt'>n</span><span style='letter-spacing:-.05pt'>f</span>i<span
style='letter-spacing:.05pt'>e</span>re<span style='letter-spacing:.1pt'> </span><span
style='letter-spacing:.05pt'>e</span>l<span style='letter-spacing:.35pt'> </span><span
style='letter-spacing:.05pt'>Ré</span>gi<span style='letter-spacing:.05pt'>me</span>n<span
style='letter-spacing:.05pt'> </span>F<span style='letter-spacing:-.05pt'>o</span>r<span
style='letter-spacing:.05pt'>e</span>s<span style='letter-spacing:.15pt'>t</span><span
style='letter-spacing:.05pt'>a</span>l<span style='letter-spacing:.05pt'> </span>de<span
style='letter-spacing:.35pt'> </span><span style='letter-spacing:-.05pt'>L</span>a<span
style='letter-spacing:.35pt'> </span><span style='letter-spacing:-.05pt'>N</span><span
style='letter-spacing:.05pt'>a</span><span style='letter-spacing:-.05pt'>c</span>i<span
style='letter-spacing:.1pt'>ó</span><span style='letter-spacing:-.05pt'>n</span>,<span
style='letter-spacing:.1pt'> </span>y<span style='letter-spacing:.3pt'> </span><span
style='letter-spacing:.05pt'>a</span>l<span style='letter-spacing:.35pt'> </span><span
style='letter-spacing:.05pt'>a</span>mp<span style='letter-spacing:.15pt'>a</span>ro<span
style='letter-spacing:.1pt'> </span>de<span style='letter-spacing:.35pt'> </span>lo<span
style='letter-spacing:.35pt'> </span>que<span style='letter-spacing:.3pt'> </span><span
style='letter-spacing:.05pt'>e</span>st<span style='letter-spacing:.05pt'>a</span>bl<span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>c</span>e<span
style='letter-spacing:.05pt'> e</span>l<span style='letter-spacing:.35pt'> </span><span
style='letter-spacing:.1pt'>D</span>.<span style='letter-spacing:-.05pt'>S</span>.<span
style='letter-spacing:.25pt'> </span><span style='letter-spacing:.05pt'>0</span><span
style='letter-spacing:-.05pt'>71</span><span style='letter-spacing:.15pt'>/</span><span
style='letter-spacing:-.05pt'>2</span><span style='letter-spacing:.05pt'>0</span><span
style='letter-spacing:-.05pt'>0</span>9 A<span style='letter-spacing:.05pt'>r</span>t.<span
style='letter-spacing:.35pt'> </span><span style='letter-spacing:-.05pt'>3</span>1
<span style='letter-spacing:-.05pt'>c</span>o<span style='letter-spacing:.05pt'>n</span><span
style='letter-spacing:-.05pt'>c</span>ord<span style='letter-spacing:.05pt'>a</span><span
style='letter-spacing:-.05pt'>n</span>te<span style='letter-spacing:-.4pt'> </span><span
style='letter-spacing:-.05pt'>D</span>.<span style='letter-spacing:-.05pt'>S</span>.<span
style='letter-spacing:-.1pt'> </span><span style='letter-spacing:-.05pt'>2</span><span
style='letter-spacing:.05pt'>9</span><span style='letter-spacing:-.05pt'>8</span><span
style='letter-spacing:.05pt'>9</span><span style='letter-spacing:-.05pt'>4</span><span
style='letter-spacing:.05pt'>/20</span><span style='letter-spacing:-.05pt'>0</span>9<span
style='letter-spacing:-.6pt'> </span>A<span style='letter-spacing:.05pt'>r</span>t.<span
style='letter-spacing:-.05pt'> 1</span><span style='letter-spacing:.05pt'>4</span><span
style='letter-spacing:-.05pt'>4</span>.<o:p></o:p></span></p>

</div>

<span style='font-size:10.0pt;font-family:"Tahoma",sans-serif;mso-fareast-font-family:
"Times New Roman";mso-ansi-language:ES-BO;mso-fareast-language:ES-BO;
mso-bidi-language:AR-SA'><br clear=all style='page-break-before:always;
mso-break-type:section-break'>
</span>

<div class=WordSection2>


<p class=MsoNormal style='margin-top:.05pt;margin-right:0cm;margin-bottom:0cm;
margin-left:0cm;margin-bottom:.0001pt;line-height:9.5pt;mso-line-height-rule:
exactly;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><span
style='font-size:9.5pt;font-family:"Tahoma",sans-serif'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-top:1.25pt;margin-right:404.1pt;margin-bottom:
0cm;margin-left:5.1pt;margin-bottom:.0001pt;text-align:justify;line-height:
normal;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif;letter-spacing:-.05pt'>R</span></b><b><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif'>E<span
style='letter-spacing:.05pt'>S</span><span style='letter-spacing:-.05pt'>U</span>E<span
style='letter-spacing:.05pt'>L</span><span style='letter-spacing:.1pt'>V</span>E:</span></b><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:.3pt;margin-right:3.15pt;margin-bottom:
0cm;margin-left:5.1pt;margin-bottom:.0001pt;text-align:justify;line-height:
12.0pt;mso-line-height-rule:exactly;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><b><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif;
letter-spacing:.05pt'>P</span></b><b><span style='font-size:10.0pt;font-family:
"Tahoma",sans-serif;letter-spacing:-.05pt'>R</span></b><b><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif'>IM<span
style='letter-spacing:.05pt'>E</span><span style='letter-spacing:-.05pt'>R</span>O:<span
style='letter-spacing:2.55pt'> </span></span></b><span style='font-size:10.0pt;
font-family:"Tahoma",sans-serif'>A<span style='letter-spacing:.05pt'>p</span><span
style='letter-spacing:.1pt'>r</span>ob<span style='letter-spacing:.05pt'>a</span>r<span
style='letter-spacing:2.75pt'> </span><span style='letter-spacing:.05pt'>e</span>n<span
style='letter-spacing:2.95pt'> </span>todos<span style='letter-spacing:2.9pt'> </span>s<span
style='letter-spacing:-.05pt'>u</span>s<span style='letter-spacing:2.95pt'> </span>t<span
style='letter-spacing:.05pt'>é</span>r<span style='letter-spacing:.05pt'>m</span>i<span
style='letter-spacing:.1pt'>n</span>os;<span style='letter-spacing:2.65pt'> </span>la<span
style='letter-spacing:3.05pt'> </span><b><span style='letter-spacing:-2.85pt'><span
style='mso-spacerun:yes'> </span></span>

<?php echo strtoupper($tramite);?>

</b>,<span style='letter-spacing:
2.3pt'> </span>d<span style='letter-spacing:.05pt'>e</span>l<span
style='letter-spacing:3.0pt'> </span>Progr<span style='letter-spacing:.05pt'>a</span>ma<span
style='letter-spacing:2.75pt'> </span>de<span style='letter-spacing:3.05pt'> </span>A<span
style='letter-spacing:.05pt'>ba</span>st<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>c</span>i<span style='letter-spacing:.05pt'>m</span>i<span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>n</span>to<span
style='letter-spacing:2.45pt'> </span>y Proces<span style='letter-spacing:.05pt'>am</span>i<span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>n</span>to
de<span style='letter-spacing:.6pt'> </span>M<span style='letter-spacing:.05pt'>a</span>t<span
style='letter-spacing:.05pt'>e</span>ria<span style='letter-spacing:.35pt'> </span>pri<span
style='letter-spacing:.05pt'>m</span>a<span style='letter-spacing:.45pt'> </span>g<span
style='letter-spacing:.05pt'>e</span>stión<span style='letter-spacing:.25pt'> </span><span
style='letter-spacing:-.05pt'>2</span><span style='letter-spacing:.05pt'>0</span><span
style='letter-spacing:-.05pt'>1</span>6<span style='letter-spacing:.4pt'> </span>(<span
style='letter-spacing:.15pt'>P</span>A<span style='letter-spacing:.05pt'>P</span>M<span
style='letter-spacing:.3pt'>P</span>-<span style='letter-spacing:-.05pt'>2</span><span
style='letter-spacing:.05pt'>0</span><span style='letter-spacing:-.05pt'>16</span>),<span
style='letter-spacing:.1pt'> </span><span style='letter-spacing:-.05pt'>c</span>o<span
style='letter-spacing:.05pt'>n</span><span style='letter-spacing:-.05pt'>f</span>or<span
style='letter-spacing:.05pt'>m</span>e<span style='letter-spacing:.25pt'> </span>lo<span
style='letter-spacing:.65pt'> </span><span style='letter-spacing:.05pt'>e</span>st<span
style='letter-spacing:.05pt'>a</span>bl<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>c</span>ido<span style='letter-spacing:.15pt'> </span><span
style='letter-spacing:.05pt'>e</span>n<span style='letter-spacing:.5pt'> </span>los
<span style='letter-spacing:.05pt'>e</span>sp<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>c</span>i<span style='letter-spacing:-.05pt'>f</span><span
style='letter-spacing:.1pt'>i</span><span style='letter-spacing:-.05pt'>c</span><span
style='letter-spacing:.05pt'>a</span><span style='letter-spacing:-.05pt'>c</span>io<span
style='letter-spacing:-.05pt'>n</span><span style='letter-spacing:.15pt'>e</span>s<span
style='letter-spacing:-.7pt'> </span>de<span style='letter-spacing:-.05pt'> </span>la<span
style='letter-spacing:.05pt'> </span><span style='letter-spacing:-.05pt'>f</span><span
style='letter-spacing:.1pt'>i</span><span style='letter-spacing:-.05pt'>ch</span>a<span
style='letter-spacing:-.05pt'> </span>t<span style='letter-spacing:.05pt'>é</span><span
style='letter-spacing:-.05pt'>cn</span>i<span style='letter-spacing:-.05pt'>c</span>a<span
style='letter-spacing:-.25pt'> </span><span style='letter-spacing:.15pt'>(</span>F<span
style='letter-spacing:.2pt'>O</span>-<span style='letter-spacing:.05pt'>2</span><span
style='letter-spacing:-.05pt'>6</span>)<span style='letter-spacing:-.35pt'> </span><span
style='letter-spacing:.05pt'>de</span><span style='letter-spacing:.1pt'>s</span><span
style='letter-spacing:-.05pt'>c</span>ri<span style='letter-spacing:.05pt'>t</span>o<span
style='letter-spacing:-.25pt'> </span><span style='letter-spacing:.05pt'>an</span>t<span
style='letter-spacing:.05pt'>e</span>rior<span style='letter-spacing:.05pt'>me</span><span
style='letter-spacing:-.05pt'>n</span>t<span style='letter-spacing:.05pt'>e</span>,<span
style='letter-spacing:-.65pt'> </span><span style='letter-spacing:.05pt'>a</span>plic<span
style='letter-spacing:.05pt'>a</span><span style='letter-spacing:-.05pt'>n</span><span
style='letter-spacing:.1pt'>d</span>o<span style='letter-spacing:-.3pt'> </span>y<span
style='letter-spacing:-.1pt'> </span>r<span style='letter-spacing:.15pt'>e</span><span
style='letter-spacing:-.05pt'>c</span>o<span style='letter-spacing:-.05pt'>n</span><span
style='letter-spacing:.1pt'>o</span><span style='letter-spacing:-.05pt'>c</span>i<span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>n</span><span
style='letter-spacing:.1pt'>d</span>o<span style='letter-spacing:-.6pt'> </span>p<span
style='letter-spacing:.05pt'>a</span>ra<span style='letter-spacing:-.15pt'> </span><span
style='letter-spacing:.05pt'>e</span>llo, la<span style='letter-spacing:.05pt'>
</span>Fe<span style='letter-spacing:.05pt'> </span>públi<span
style='letter-spacing:-.05pt'>c</span>a<span style='letter-spacing:-.15pt'> </span>que
<span style='letter-spacing:-.05pt'>c</span>o<span style='letter-spacing:-.05pt'>nf</span>i<span
style='letter-spacing:.05pt'>e</span>re<span style='letter-spacing:-.2pt'> </span><span
style='letter-spacing:.15pt'>e</span>l <span style='letter-spacing:.05pt'>a</span>r<span
style='letter-spacing:.05pt'>t</span>í<span style='letter-spacing:-.05pt'>cu</span>lo<span
style='letter-spacing:-.2pt'> </span><span style='letter-spacing:-.05pt'>2</span><span
style='letter-spacing:.1pt'>7</span></span><span style='font-size:10.0pt;
font-family:Symbol;mso-bidi-font-family:Symbol'>°</span><span style='font-size:
10.0pt;font-family:"Times New Roman",serif;letter-spacing:.55pt'> </span><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif'>de<span
style='letter-spacing:.05pt'> </span>la<span style='letter-spacing:.05pt'> </span><span
style='letter-spacing:-.05pt'>L</span><span style='letter-spacing:.05pt'>e</span>y<span
style='letter-spacing:-.1pt'> </span>F<span style='letter-spacing:-.05pt'>o</span>r<span
style='letter-spacing:.15pt'>e</span>st<span style='letter-spacing:.05pt'>a</span>l<span
style='letter-spacing:-.25pt'> </span><span style='letter-spacing:-.05pt'>N</span>º
<span style='letter-spacing:-.05pt'>17</span><span style='letter-spacing:.05pt'>0</span><span
style='letter-spacing:-.05pt'>0</span>, <span style='letter-spacing:.05pt'>e</span>st<span
style='letter-spacing:.05pt'>a</span>bl<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>c</span>i<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>n</span>do<span style='letter-spacing:-.4pt'> </span>r<span
style='letter-spacing:.05pt'>e</span>spo<span style='letter-spacing:-.05pt'>n</span>s<span
style='letter-spacing:.05pt'>a</span>bilid<span style='letter-spacing:.1pt'>a</span>d<span
style='letter-spacing:-.55pt'> </span><span style='letter-spacing:-.05pt'>c</span>i<span
style='letter-spacing:-.05pt'>v</span>il y<span style='letter-spacing:-.1pt'> </span>p<span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>n</span><span
style='letter-spacing:.05pt'>a</span>l<span style='letter-spacing:-.25pt'> </span>por<span
style='letter-spacing:-.15pt'> </span>la<span style='letter-spacing:.05pt'> </span><span
style='letter-spacing:-.05pt'>v</span><span style='letter-spacing:.05pt'>e</span>r<span
style='letter-spacing:.05pt'>a</span><span style='letter-spacing:-.05pt'>c</span>id<span
style='letter-spacing:.05pt'>a</span>d<span style='letter-spacing:-.4pt'> </span>y<span
style='letter-spacing:.05pt'> </span><span style='letter-spacing:.1pt'>c</span><span
style='letter-spacing:.05pt'>a</span>b<span style='letter-spacing:.05pt'>a</span>lid<span
style='letter-spacing:.05pt'>a</span>d<span style='letter-spacing:-.4pt'> </span>de<span
style='letter-spacing:-.05pt'> </span>la<span style='letter-spacing:-.05pt'> </span>i<span
style='letter-spacing:-.05pt'>nf</span>or<span style='letter-spacing:.05pt'>ma</span><span
style='letter-spacing:-.05pt'>c</span>i<span style='letter-spacing:.1pt'>ó</span>n<span
style='letter-spacing:-.45pt'> </span><span style='letter-spacing:-.05pt'>c</span>o<span
style='letter-spacing:-.05pt'>n</span>t<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>n</span><span style='letter-spacing:.1pt'>i</span>d<span
style='letter-spacing:.05pt'>a</span>.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:3.25pt;margin-bottom:
0cm;margin-left:5.1pt;margin-bottom:.0001pt;text-align:justify;line-height:
12.0pt;mso-line-height-rule:exactly;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><b><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif;
letter-spacing:.05pt'>S</span></b><b><span style='font-size:10.0pt;font-family:
"Tahoma",sans-serif'>EGUNDO: </span></b><span style='font-size:10.0pt;
font-family:"Tahoma",sans-serif'>A<span style='letter-spacing:.15pt'>p</span>rob<span
style='letter-spacing:.05pt'>a</span>r<span style='letter-spacing:.25pt'> </span>la<span
style='letter-spacing:.55pt'> </span><span style='letter-spacing:-.05pt'>f</span>i<span
style='letter-spacing:.1pt'>c</span><span style='letter-spacing:-.05pt'>h</span>a<span
style='letter-spacing:.45pt'> </span>t<span style='letter-spacing:.05pt'>é</span><span
style='letter-spacing:-.05pt'>cn</span>i<span style='letter-spacing:-.05pt'>c</span>a<span
style='letter-spacing:.35pt'> </span>(F<span style='letter-spacing:.15pt'>O</span>-<span
style='letter-spacing:.05pt'>2</span><span style='letter-spacing:-.05pt'>6</span>)<span
style='letter-spacing:.3pt'> </span>de<span style='letter-spacing:.55pt'> </span><span
style='letter-spacing:-.05pt'>c</span><span style='letter-spacing:.05pt'>a</span>da<span
style='letter-spacing:.45pt'> </span><span style='letter-spacing:.05pt'>a</span><span
style='letter-spacing:-.05pt'>c</span>ti<span style='letter-spacing:-.05pt'>v</span>id<span
style='letter-spacing:.05pt'>a</span>d<span style='letter-spacing:.2pt'> </span>d<span
style='letter-spacing:.05pt'>e</span>l<span style='letter-spacing:.45pt'> </span>doc<span
style='letter-spacing:-.05pt'>u</span>m<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>n</span>to<span style='letter-spacing:.1pt'> </span>ob<span
style='letter-spacing:-.05pt'>j</span><span style='letter-spacing:.05pt'>e</span>to<span
style='letter-spacing:.3pt'> </span>de<span style='letter-spacing:.55pt'> </span>la<span
style='letter-spacing:.55pt'> </span>pr<span style='letter-spacing:.05pt'>e</span>se<span
style='letter-spacing:-.05pt'>n</span>te <span style='letter-spacing:.05pt'>Re</span>sol<span
style='letter-spacing:-.05pt'>uc</span>i<span style='letter-spacing:.1pt'>ó</span>n<span
style='letter-spacing:.05pt'> </span>A<span style='letter-spacing:.05pt'>d</span>mi<span
style='letter-spacing:-.05pt'>n</span>istr<span style='letter-spacing:.05pt'>a</span>ti<span
style='letter-spacing:-.05pt'>v</span><span style='letter-spacing:.05pt'>a</span>,
que<span style='letter-spacing:.4pt'> </span>sin<span style='letter-spacing:
.4pt'> </span>i<span style='letter-spacing:-.05pt'>n</span>ser<span
style='letter-spacing:.05pt'>ta</span>rse<span style='letter-spacing:.1pt'> </span><span
style='letter-spacing:-.05pt'>f</span>or<span style='letter-spacing:.05pt'>m</span>a<span
style='letter-spacing:.45pt'> </span>p<span style='letter-spacing:.05pt'>a</span>r<span
style='letter-spacing:.05pt'>t</span>e<span style='letter-spacing:.65pt'> </span>i<span
style='letter-spacing:-.05pt'>n</span>disol<span style='letter-spacing:-.05pt'>u</span>ble<span
style='letter-spacing:.15pt'> </span>de<span style='letter-spacing:.5pt'> </span>la<span
style='letter-spacing:.55pt'> </span>mism<span style='letter-spacing:.05pt'>a</span>.<span
style='letter-spacing:.2pt'> </span><span style='letter-spacing:-.05pt'>L</span>a<span
style='letter-spacing:.5pt'> </span>lic<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>nc</span>ia<span style='letter-spacing:.3pt'> </span>de
<span style='letter-spacing:-.05pt'>fu</span><span style='letter-spacing:.05pt'>n</span><span
style='letter-spacing:-.05pt'>c</span>i<span style='letter-spacing:.1pt'>o</span><span
style='letter-spacing:-.05pt'>n</span><span style='letter-spacing:.05pt'>a</span>mi<span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>n</span>to<span
style='letter-spacing:-.1pt'> </span><span style='letter-spacing:.05pt'>e</span>s<span
style='letter-spacing:.6pt'> </span><span style='letter-spacing:-.05pt'>v</span><span
style='letter-spacing:.05pt'>á</span>lida<span style='letter-spacing:.55pt'> </span><span
style='letter-spacing:.05pt'>ha</span>sta<span style='letter-spacing:.4pt'> </span><span
style='letter-spacing:.05pt'>e</span>l<span style='letter-spacing:.5pt'> </span><span
style='letter-spacing:-.05pt'>3</span>1<span style='letter-spacing:.45pt'> </span>de<span
style='letter-spacing:.65pt'> </span>dicie<span style='letter-spacing:.05pt'>m</span>bre<span
style='letter-spacing:.2pt'> </span>de<span style='letter-spacing:.65pt'> </span><span
style='letter-spacing:-.05pt'>2</span><span style='letter-spacing:.05pt'>0</span><span
style='letter-spacing:-.05pt'>1</span><span style='letter-spacing:.05pt'>6</span>;<span
style='letter-spacing:.4pt'> </span><span style='letter-spacing:-.05pt'>v</span><span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>nc</span>ida<span
style='letter-spacing:.4pt'> </span>la<span style='letter-spacing:.55pt'> </span>m<span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>nc</span><span
style='letter-spacing:.1pt'>i</span>o<span style='letter-spacing:.05pt'>na</span>da<span
style='letter-spacing:.1pt'> </span>lic<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>nc</span>ia<span style='letter-spacing:.35pt'> </span>se<span
style='letter-spacing:.65pt'> </span>t<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>n</span>drá por<span style='letter-spacing:1.75pt'>
</span><span style='letter-spacing:-.05pt'>c</span><span style='letter-spacing:
.05pt'>a</span>d<span style='letter-spacing:.1pt'>u</span><span
style='letter-spacing:-.05pt'>c</span><span style='letter-spacing:.05pt'>a</span>da<span
style='letter-spacing:1.55pt'> </span>la<span style='letter-spacing:1.9pt'> </span>misma<span
style='letter-spacing:1.7pt'> </span>de<span style='letter-spacing:1.85pt'> </span>pl<span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>n</span>o<span
style='letter-spacing:1.7pt'> </span>d<span style='letter-spacing:.05pt'>e</span>r<span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>c</span><span
style='letter-spacing:.05pt'>h</span>o<span style='letter-spacing:1.55pt'> </span>sin<span
style='letter-spacing:1.9pt'> </span><span style='letter-spacing:-.05pt'>n</span><span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>c</span><span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:.1pt'>s</span>id<span
style='letter-spacing:.05pt'>a</span>d<span style='letter-spacing:1.5pt'> </span>de<span
style='letter-spacing:1.85pt'> </span>m<span style='letter-spacing:.05pt'>a</span><span
style='letter-spacing:-.05pt'>n</span>i<span style='letter-spacing:-.05pt'>f</span><span
style='letter-spacing:.05pt'>e</span>st<span style='letter-spacing:.05pt'>a</span><span
style='letter-spacing:-.05pt'>c</span>ión<span style='letter-spacing:1.25pt'> </span><span
style='letter-spacing:.15pt'>e</span>x<span style='letter-spacing:.1pt'>p</span>r<span
style='letter-spacing:.05pt'>e</span>sa<span style='letter-spacing:1.65pt'> </span>de<span
style='letter-spacing:1.85pt'> </span>la<span style='letter-spacing:1.9pt'> </span>Autori<span
style='letter-spacing:.05pt'>da</span>d<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:394.05pt;margin-bottom:
0cm;margin-left:5.1pt;margin-bottom:.0001pt;text-align:justify;line-height:
11.7pt;mso-line-height-rule:exactly;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif;
position:relative;top:.5pt;mso-text-raise:-.5pt'>A<span style='letter-spacing:
.05pt'>d</span>mi<span style='letter-spacing:-.05pt'>n</span>istr<span
style='letter-spacing:.05pt'>a</span>ti<span style='letter-spacing:-.05pt'>v</span><span
style='letter-spacing:.05pt'>a</span>.</span><span style='font-size:10.0pt;
font-family:"Tahoma",sans-serif'><o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:.3pt;margin-right:3.2pt;margin-bottom:
0cm;margin-left:5.1pt;margin-bottom:.0001pt;text-align:justify;line-height:
12.0pt;mso-line-height-rule:exactly;mso-pagination:none;mso-layout-grid-align:
none;text-autospace:none'><b><span style='font-size:10.0pt;font-family:"Tahoma",sans-serif'>TER<span
style='letter-spacing:.05pt'>C</span>E<span style='letter-spacing:-.05pt'>R</span><span
style='letter-spacing:.1pt'>O</span>:<span style='letter-spacing:-.2pt'> </span></span></b><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif;letter-spacing:.05pt'>E</span><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif'>l<span
style='letter-spacing:.25pt'> </span>r<span style='letter-spacing:.05pt'>e</span>pr<span
style='letter-spacing:.05pt'>e</span>se<span style='letter-spacing:-.05pt'>n</span>t<span
style='letter-spacing:.05pt'>a</span><span style='letter-spacing:-.05pt'>n</span><span
style='letter-spacing:.15pt'>t</span>e<span style='letter-spacing:-.25pt'> </span><span
style='letter-spacing:-.05pt'>L</span><span style='letter-spacing:.05pt'>e</span>g<span
style='letter-spacing:.05pt'>a</span>l,<span style='letter-spacing:.1pt'> </span>y<span
style='letter-spacing:.25pt'> </span><span style='letter-spacing:.05pt'>e</span>l<span
style='letter-spacing:.25pt'> </span>profesi<span style='letter-spacing:.1pt'>o</span><span
style='letter-spacing:-.05pt'>n</span><span style='letter-spacing:.05pt'>a</span>l<span
style='letter-spacing:-.15pt'> </span>r<span style='letter-spacing:.05pt'>e</span>spo<span
style='letter-spacing:-.05pt'>n</span>s<span style='letter-spacing:.05pt'>a</span>ble<span
style='letter-spacing:.15pt'> </span>oblig<span style='letter-spacing:.1pt'>a</span>tori<span
style='letter-spacing:.05pt'>a</span>m<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>n</span>te<span style='letter-spacing:-.25pt'> </span>d<span
style='letter-spacing:.05pt'>e</span>b<span style='letter-spacing:.05pt'>e</span>n<span
style='letter-spacing:.05pt'> </span>pr<span style='letter-spacing:.05pt'>e</span>se<span
style='letter-spacing:-.05pt'>n</span>t<span style='letter-spacing:.05pt'>a</span>r,<span
style='letter-spacing:-.1pt'> </span>los I<span style='letter-spacing:-.05pt'>nf</span>or<span
style='letter-spacing:.05pt'>me</span>s<span style='letter-spacing:.25pt'> </span><span
style='letter-spacing:-.05pt'>T</span>ri<span style='letter-spacing:.05pt'>me</span>str<span
style='letter-spacing:.05pt'>a</span>l<span style='letter-spacing:.05pt'>e</span>s
<span style='letter-spacing:.05pt'>e</span>st<span style='letter-spacing:.05pt'>a</span>bl<span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>c</span>idos
(<span style='letter-spacing:.1pt'>R</span>.M.<span style='letter-spacing:.25pt'>
</span><span style='letter-spacing:.05pt'>1</span><span style='letter-spacing:
-.05pt'>34</span><span style='letter-spacing:.15pt'>/</span><span
style='letter-spacing:-.05pt'>97</span>).<span style='letter-spacing:.15pt'> </span><span
style='letter-spacing:.1pt'>C</span>o<span style='letter-spacing:-.05pt'>n</span>mi<span
style='letter-spacing:-.05pt'>n</span><span style='letter-spacing:.05pt'>a</span>r<span
style='letter-spacing:.1pt'> </span>a<span style='letter-spacing:.5pt'> </span>los<span
style='letter-spacing:.35pt'> </span>tit<span style='letter-spacing:-.05pt'>u</span>l<span
style='letter-spacing:.05pt'>a</span>r<span style='letter-spacing:.05pt'>e</span>s<span
style='letter-spacing:.2pt'> </span>de<span style='letter-spacing:.6pt'> </span>la<span
style='letter-spacing:.45pt'> </span><span style='letter-spacing:.05pt'>e</span>mpr<span
style='letter-spacing:.05pt'>e</span>sa<span style='letter-spacing:.15pt'> </span><span
style='letter-spacing:-.05pt'>f</span>or<span style='letter-spacing:.05pt'>e</span>st<span
style='letter-spacing:.05pt'>a</span>l <span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>f</span><span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>c</span>ti<span style='letter-spacing:-.05pt'>v</span>iz<span
style='letter-spacing:.05pt'>a</span>r<span style='letter-spacing:.15pt'> </span><span
style='letter-spacing:.05pt'>e</span>l<span style='letter-spacing:.5pt'> </span><span
style='letter-spacing:-.05pt'>c</span><span style='letter-spacing:.1pt'>o</span><span
style='letter-spacing:-.05pt'>n</span>trol<span style='letter-spacing:.3pt'> </span>de<span
style='letter-spacing:.5pt'> </span>i<span style='letter-spacing:-.05pt'>n</span>gr<span
style='letter-spacing:.05pt'>e</span>so<span style='letter-spacing:.2pt'> </span>y<span
style='letter-spacing:.5pt'> </span><span style='letter-spacing:.05pt'>e</span>gr<span
style='letter-spacing:.05pt'>e</span>so<span style='letter-spacing:.25pt'> </span>d<span
style='letter-spacing:.05pt'>e</span>l<span style='letter-spacing:.45pt'> </span>pro<span
style='letter-spacing:.15pt'>d</span><span style='letter-spacing:-.05pt'>uc</span>to<span
style='letter-spacing:.2pt'> </span><span style='letter-spacing:.05pt'>f</span>or<span
style='letter-spacing:.05pt'>e</span>s<span style='letter-spacing:.35pt'>t</span><span
style='letter-spacing:.05pt'>a</span>l<span style='letter-spacing:.25pt'> </span><span
style='letter-spacing:-.05pt'>c</span>on<span style='letter-spacing:.4pt'> </span>los<span
style='letter-spacing:.4pt'> </span>d<span style='letter-spacing:.1pt'>oc</span><span
style='letter-spacing:-.05pt'>u</span>m<span style='letter-spacing:.05pt'>e</span><span
style='letter-spacing:-.05pt'>n</span>tos de<span style='letter-spacing:.5pt'> </span>r<span
style='letter-spacing:.05pt'>e</span>sp<span style='letter-spacing:.05pt'>a</span>ldo
(<span style='letter-spacing:.05pt'>C</span>FO<span style='letter-spacing:-.05pt'>_D</span><span
style='letter-spacing:.15pt'>)</span>,<span style='letter-spacing:.25pt'> </span><span
style='letter-spacing:.05pt'>e</span>n<span style='letter-spacing:.5pt'> </span><span
style='letter-spacing:-.05pt'>c</span><span style='letter-spacing:.05pt'>a</span>so<span
style='letter-spacing:.45pt'> </span>de<span style='letter-spacing:.6pt'> </span>i<span
style='letter-spacing:-.05pt'>n</span><span style='letter-spacing:.1pt'>c</span><span
style='letter-spacing:.05pt'>u</span>mpli<span style='letter-spacing:.05pt'>m</span>i<span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>n</span>to
se<span style='letter-spacing:.6pt'> </span>proced<span style='letter-spacing:
.05pt'>e</span>rá<span style='letter-spacing:.3pt'> </span><span
style='letter-spacing:.05pt'>a</span>l<span style='letter-spacing:.6pt'> </span>d<span
style='letter-spacing:.05pt'>e</span><span style='letter-spacing:-.05pt'>c</span>omiso,<span
style='letter-spacing:.2pt'> </span>seg<span style='letter-spacing:.1pt'>ú</span>n<span
style='letter-spacing:.35pt'> </span><span style='letter-spacing:-.05pt'>L</span><span
style='letter-spacing:.05pt'>e</span>y<span style='letter-spacing:.6pt'> </span>F<span
style='letter-spacing:-.05pt'>o</span>r<span style='letter-spacing:.15pt'>e</span>st<span
style='letter-spacing:.05pt'>a</span>l<span style='letter-spacing:.3pt'> </span><span
style='letter-spacing:-.05pt'>17</span><span style='letter-spacing:.05pt'>0</span>0<span
style='letter-spacing:.4pt'> </span>y<span style='letter-spacing:.55pt'> </span>la<span
style='letter-spacing:.65pt'> </span><span style='letter-spacing:-.05pt'>N</span>or<span
style='letter-spacing:.05pt'>m</span>a <span style='letter-spacing:-.05pt'>T</span><span
style='letter-spacing:.05pt'>é</span><span style='letter-spacing:-.05pt'>c</span><span
style='letter-spacing:.05pt'>n</span>i<span style='letter-spacing:-.05pt'>c</span>a<span
style='letter-spacing:-.3pt'> </span><span style='letter-spacing:-.05pt'>N</span>º
<span style='letter-spacing:.05pt'>1</span><span style='letter-spacing:-.05pt'>34</span><span
style='letter-spacing:.05pt'>/9</span><span style='letter-spacing:-.05pt'>7</span>.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:2.6pt;margin-right:221.95pt;margin-bottom:
0cm;margin-left:5.1pt;margin-bottom:.0001pt;text-align:justify;line-height:
normal;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif;letter-spacing:.05pt'>E</span><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif'>s<span
style='letter-spacing:-.1pt'> </span>d<span style='letter-spacing:.05pt'>a</span>do<span
style='letter-spacing:-.2pt'> </span><span style='letter-spacing:.05pt'>e</span>n<span
style='letter-spacing:-.15pt'> </span><span style='letter-spacing:.05pt'>e</span>l<span
style='letter-spacing:-.1pt'> </span>d<span style='letter-spacing:.05pt'>e</span>sp<span
style='letter-spacing:.05pt'>a</span><span style='letter-spacing:-.05pt'>c</span><span
style='letter-spacing:.05pt'>h</span>o<span style='letter-spacing:-.4pt'> </span>d<span
style='letter-spacing:.05pt'>e</span>l<span style='letter-spacing:-.05pt'> </span><span
style='letter-spacing:.05pt'>Re</span>spo<span style='letter-spacing:-.05pt'>n</span>s<span
style='letter-spacing:.05pt'>a</span>ble<span style='letter-spacing:-.5pt'> </span>de<span
style='letter-spacing:.1pt'> </span>FYC-A<span style='letter-spacing:.15pt'>B</span><span
style='letter-spacing:-.05pt'>T</span>.<o:p></o:p></span></p>

<p class=MsoNormal style='margin-top:.95pt;margin-right:0cm;margin-bottom:0cm;
margin-left:0cm;margin-bottom:.0001pt;line-height:11.0pt;mso-line-height-rule:
exactly;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><span
style='font-family:"Tahoma",sans-serif'><o:p>&nbsp;</o:p></span></p>

<p class=MsoNormal style='margin-top:0cm;margin-right:270.05pt;margin-bottom:
0cm;margin-left:5.1pt;margin-bottom:.0001pt;text-align:justify;line-height:
normal;mso-pagination:none;mso-layout-grid-align:none;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif;letter-spacing:-.05pt'>Re</span></b><b><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif;letter-spacing:.1pt'>g</span></b><b><span
style='font-size:10.0pt;font-family:"Tahoma",sans-serif'>ís<span
style='letter-spacing:-.05pt'>t</span><span style='letter-spacing:.1pt'>r</span><span
style='letter-spacing:-.05pt'>e</span>s<span style='letter-spacing:-.05pt'>e</span>,<span
style='letter-spacing:-.5pt'> </span><span style='letter-spacing:.05pt'>C</span>o<span
style='letter-spacing:-.05pt'>m</span><span style='letter-spacing:.1pt'>u</span>ní<span
style='letter-spacing:.05pt'>q</span>u<span style='letter-spacing:-.05pt'>e</span><span
style='letter-spacing:.15pt'>s</span>e<span style='letter-spacing:-.8pt'> </span>y<span
style='letter-spacing:-.05pt'> </span>ar<span style='letter-spacing:.15pt'>c</span>hív<span
style='letter-spacing:-.05pt'>e</span><span style='letter-spacing:.15pt'>s</span><span
style='letter-spacing:-.05pt'>e</span>.</span></b><span style='font-size:10.0pt;
font-family:"Tahoma",sans-serif'><o:p></o:p></span></p>

<p class=MsoNormal><o:p>&nbsp;</o:p></p>

</div>

</body>

</html>
