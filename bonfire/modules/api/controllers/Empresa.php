<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Empresa extends Front_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
        $this->load->library('users/auth');
        $this->load->model('empresas/empresa_model');
        $this->load->model('empresas/categoria_model');
        $this->load->model('empresas/movimientoproducto_model');
        $this->load->model('empresas/agente_model');
        $this->load->model('empresas/unidad_model');
        $this->load->model('empresas/archivo_model');
        $this->load->model('api/personaabt_model');
	}

    /******************** Servicios Consultas ***************************/

    public function get_categorias() {
        $actividadId =  $this->uri->segment(4);
        $response = array();
        $typeResponse = RESPONSE_OK;

        if (isset($actividadId) && intval($actividadId) > 0) {
            $list = $this->categoria_model->Get_List_By_Actividad($actividadId);
            if (!empty($list)) {
                $response['data'] = $list;
            } else {
               $typeResponse = RESPONSE_NOT_ACCEPTABLE;
               $response['error'] = array("No existe configuracion de Categorias para el ID de Actividad o no es valido.");
            }
        } else {
           $typeResponse = RESPONSE_NOT_ACCEPTABLE;
           $response['error'] = array("Se requiere un valor válido para procesar la peticion.");
        }
        // Get the current user information.
        $this->output
                    ->set_content_type('application/json')
                    ->set_status_header($typeResponse);
        echo json_encode($response);
    }

    public function get_unidad() {
        $productoId =  $this->uri->segment(4);
        $response = array();
        $typeResponse = RESPONSE_OK;

        if (isset($productoId) && intval($productoId) > 0) {
            $unidad = $this->unidad_model->Get_By($productoId);
            if (!empty($unidad)) {
                $response['data'] = $unidad;
            } else {
               $typeResponse = RESPONSE_NOT_ACCEPTABLE;
               $response['error'] = array("No existe configuracion de Unidad para el ID de Producto o no es valido.");
            }
        } else {
           $typeResponse = RESPONSE_NOT_ACCEPTABLE;
           $response['error'] = array("Se requiere un valor válido para procesar la peticion.");
        }
        
        // Get the current user information.
        $this->output
                    ->set_content_type('application/json')
                    ->set_status_header($typeResponse);
        echo json_encode($response);
    }

    public function filter_agentes() {
        $response = array();
        $typeResponse = RESPONSE_OK;

        if (isset($_POST)) {
            $filters = new stdClass();
            $filters->filter_nombres = $this->input->post('filter_nombres');
            $filters->filter_documento = $this->input->post('filter_documento');
            $filters->numero_registro = $this->input->post('numero_registro');
            $list = $this->agente_model->Filter_By($filters);
            $response['data'] = null;
            if (!empty($list)) {
                $response['data'] = $list;
            }
        } else {
           $typeResponse = RESPONSE_NOT_ACCEPTABLE;
           $response['error'] = array("Se requiere un valor válido para procesar la peticion.");
        }
        // Get the current user information.
        $this->output
                    ->set_content_type('application/json')
                    ->set_status_header($typeResponse);
        echo json_encode($response);
    }

    public function get_current_registro() {
        $solicitudEncryptId =  $this->uri->segment(4);
        $response = array();
        $typeResponse = RESPONSE_OK;

        if (!$this->auth->is_logged_in()) {
           $typeResponse = RESPONSE_NOT_ACCEPTABLE;
           $response['error'] = array("Debe iniciar sesión para procesar la solicitud.");
        } else {
            $solicitudId = $this->session->userdata('solicitud_inscripcion');
            if (empty($solicitudId) || empty($solicitudEncryptId) || $solicitudEncryptId != md5($solicitudId)) {
                $typeResponse = RESPONSE_NOT_ACCEPTABLE;
                $response['error'] = array("No puede procesar no son validos los parametros.");
            } else {
                $solicitud = $this->empresa_model->Get_SolicitudComplete($solicitudId);
                $userLogin = $this->auth->user();
                if ($solicitud->data->codigo_usuario_id != $userLogin->user_abt_id) {
                    $typeResponse = RESPONSE_NOT_ACCEPTABLE;
                    $response['error'] = array("No puede acceder a la solicitud usted no es propietario, no es valido.");
                } else {
                    $response['data'] = $solicitud->data;
                }
            }

        }
        
        // Get the current user information.
        $this->output
                    ->set_content_type('application/json')
                    ->set_status_header($typeResponse);
        echo json_encode($response);
    }

    /***************************** Servicios Persistencia *********************************/

    /**
     * Proceso para registrar nuevas solicitudes.
     * Por el momento no se estan validando los campos y solo
     * se esta procesando el registro.
     * @return [type] [description]
     */
    public function save_solicitud_registro() {
        $response = array();
        $typeResponse = RESPONSE_OK;

        if (!$this->auth->is_logged_in()) {
           $typeResponse = RESPONSE_NOT_ACCEPTABLE;
           $response['error'] = array("Debe iniciar sesión para procesar la solicitud.");
        } else {
            if (isset($_POST)) {
                $key = $this->session->userdata('solicitud_inscripcion');
                if (isset($key)) {
                    $response['message'] = array("Ya tiene una solicitud en proceso.");
                } else { 
                    $datos = $this->Get_Datos_Inscripcion();
                    $responseSave = $this->empresa_model->Save_Solicitud_Inscripcion($datos);
                    if ($responseSave->is_valid) {
                        $this->session->set_userdata('solicitud_inscripcion', $responseSave->data);
                    }
                }
            } else {
                $response['error'] = "La petición no es correcta, contactese con la USI-ABT."; 
                $typeResponse = RESPONSE_NOT_ACCEPTABLE;
            }
        }

        // Get the current user information.
        $this->output
                    ->set_content_type('application/json')
                    ->set_status_header($typeResponse);
        echo json_encode($response);
    }

    public function aprobar_solicitud_registro() {
        $response = array();
        $typeResponse = RESPONSE_OK;

        if (!$this->auth->is_logged_in()) {
           $typeResponse = RESPONSE_NOT_ACCEPTABLE;
           $response['error'] = array("Debe iniciar sesión para continuar con el proceso.");
        } else {
            if (isset($_POST)) {
                $solicitudId =  $this->input->post('solicitud_id');
                $numeroRegistro =  $this->input->post('numero_registro');
                $funcionarioId =  $this->input->post('funcionario_id');
                $cargoFuncionario =  $this->input->post('cargo_funcionario');
                if (empty($solicitudId) || empty($numeroRegistro) 
                    || empty($funcionarioId) || empty($cargoFuncionario)) {
                    $response['error'] = array("Se requeren todos los parametros para continuar.");
                    $typeResponse = RESPONSE_NOT_ACCEPTABLE;
                } else {
                    $this->session->set_userdata('codtipoficina', 2);
                    $userLogin = $this->auth->user();
                    $data = (object)[
                        'solicitud_id' => $solicitudId,
                        'numero_registro' => $numeroRegistro,
                        'responsable_id' => $funcionarioId,
                        'responsable_cargo' => $cargoFuncionario
                    ];
                    $responseSave = $this->empresa_model->Aprobar_Solicitud($data, $userLogin->user_abt_id);
                    if ($responseSave->is_valid) {
                         $response['error'] = array("Se aprobado la solicitud");
                    }
                }
            } else {
                $response['error'] = "La petición no es correcta, contactese con la USI-ABT."; 
                $typeResponse = RESPONSE_NOT_ACCEPTABLE;
            }
        }

        // Get the current user information.
        $this->output
                    ->set_content_type('application/json')
                    ->set_status_header($typeResponse);
        echo json_encode($response);
    }

    /**
     * Proceso para registrar actualizar solicitudes.
     * Por el momento no se estan validando los campos y solo
     * se esta procesando la actualizacion de registro.
     * @return [type] [description]
     */
    public function update_solicitud_registro() {
        $response = array();
        $typeResponse = RESPONSE_OK;

        if (!$this->auth->is_logged_in()) {
           $typeResponse = RESPONSE_NOT_ACCEPTABLE;
           $response['error'] = array("Debe iniciar sesión para procesar la solicitud.");
        } else {
            if (isset($_POST)) {
                $key = $this->session->userdata('solicitud_inscripcion');
                // Control de 
                if (!isset($key) || md5($key) != $this->input->post('current_id')) {
                    $response['message'] = array("No existe solicitud en proceso.");
                } else { 
                    $datos = $this->Get_Datos_Update_Registro();
                    $responseSave = $this->empresa_model->Update_Solicitud_Inscripcion($datos);
                    if (!$responseSave->is_valid) {
                        $response['error'] = $responseSave->errors; 
                        $typeResponse = RESPONSE_NOT_ACCEPTABLE;
                    }
                }
            } else {
                $response['error'] = "La petición no es correcta, contactese con la USI-ABT."; 
                $typeResponse = RESPONSE_NOT_ACCEPTABLE;
            }
        }

        // Get the current user information.
        $this->output
                    ->set_content_type('application/json')
                    ->set_status_header($typeResponse);
        echo json_encode($response);
    }

    public function save_producto_registro() {
        $response = array();
        $typeResponse = RESPONSE_OK;

        $pathFileEmpresa = config_item('path-upload-empresa');
        $config['upload_path']          = $pathFileEmpresa;
        $config['allowed_types']        = "xls|xlsx";
        $config['max_size']             = config_item('size-upload-empresa');
        $config['max_width']            = config_item('max-width-upload-empresa');
        $config['max_height']           = config_item('max-height-upload-empresa');
        $config['file_name'] = Random::GenerarString(50, 'alpha-numeric');
        $this->load->library('upload', $config);

        if (!$this->auth->is_logged_in()) {
           $typeResponse = RESPONSE_NOT_ACCEPTABLE;
           $response['error'] = array("Debe iniciar sesión para procesar la solicitud.");
        } else {
            if (!$this->upload->do_upload('file') && !$this->input->post('exist_file'))
            {
                $error = array('error' => $this->upload->display_errors());
                $response = $error;
                $typeResponse = RESPONSE_INTERNAL_SERVER_ERROR;
            } else {
                if (isset($_POST)) {
                    $datos = $this->input->post();
                    if (!$this->input->post('exist_file')) {
                        $data = array('upload_data' => $this->upload->data());
                        $filename = $data['upload_data']['file_name'];
                        $datos['filename'] = $filename;
                    }

                    $userLogin = $this->auth->user();
                    $datos['user_id'] = $userLogin->user_abt_id;
                    $responseSave = $this->movimientoproducto_model->SaveMovimientos($datos);
                    if ($responseSave->is_valid) {
                        $producto = new stdClass();
                        $producto->producto_id = $responseSave->data;
                        $response['data'] = $producto;
                    }
                } else {
                    $response['error'] = "La petición no es correcta, contactese con la USI-ABT."; 
                    $typeResponse = RESPONSE_NOT_ACCEPTABLE;
                }
            }
        }

        // Get the current user information.
        $this->output
                    ->set_content_type('application/json')
                    ->set_status_header($typeResponse);
        echo json_encode($response);
    }

    public function save_productos_registro_empresa() {
        $response = array();
        $typeResponse = RESPONSE_OK;

        if (!$this->auth->is_logged_in()) {
            $typeResponse = RESPONSE_NOT_ACCEPTABLE;
            $response['error'] = array("Debe iniciar sesión para continuar con el proceso.");
        }

        $pathFileEmpresa = config_item('path-upload-empresa');
        $config['upload_path']          = $pathFileEmpresa;
        $config['allowed_types']        = "xls|xlsx";
        $config['max_size']             = config_item('size-upload-empresa');
        $config['max_width']            = config_item('max-width-upload-empresa');
        $config['max_height']           = config_item('max-height-upload-empresa');
        $config['file_name'] = Random::GenerarString(25, 'alpha-numeric');
        $this->load->library('upload', $config);

        if (!$this->auth->is_logged_in()) {
           $typeResponse = RESPONSE_NOT_ACCEPTABLE;
           $response['error'] = array("Debe iniciar sesión para procesar la solicitud.");
        } else {
            if ( ! $this->upload->do_upload('file'))
            {
                $error = array('error' => $this->upload->display_errors());
                $response = $error;
                $typeResponse = RESPONSE_INTERNAL_SERVER_ERROR;
            } else {
                if (!empty($this->input->post('actividad-id')) &&
                    !empty($this->input->post('empresa-id')) 
                    && (!empty($this->input->post('type-productos')) || !empty($this->input->post('type-maquinarias')))) {

                    if (!empty($this->input->post('type-productos'))) {
                        $data = array('upload_data' => $this->upload->data());
                        $filenamePath = $pathFileEmpresa . '/' . $data['upload_data']['file_name'];
                        $parseCollection = ExcelAbt::ParserProducts($filenamePath);

                        if (!$parseCollection->has_errors && $parseCollection->is_valid) {
                            $userLogin = $this->auth->user();

                            $responseSave = $this->movimientoproducto_model->SaveProductos($parseCollection->data, $this->input->post('actividad-id'), $this->input->post('empresa-id'), $userLogin->user_abt_id);
                            if (!$responseSave->is_valid) {
                                $response['error'] = $responseSave->errors;
                                $typeResponse = RESPONSE_NOT_ACCEPTABLE;
                            } else {
                                $response['data'] = $responseSave->data;
                            }
                        } else {
                            $typeResponse = RESPONSE_NOT_ACCEPTABLE;
                            $response['error'] = $parseCollection->errors;
                        }
                    } else {
                        $data = array('upload_data' => $this->upload->data());
                        $filenamePath = $pathFileEmpresa . '/' . $data['upload_data']['file_name'];
                        $parseCollection = ExcelAbt::ParserMaquinarias($filenamePath);

                        if (!$parseCollection->has_errors && $parseCollection->is_valid) {
                            $userLogin = $this->auth->user();

                            $responseSave = $this->movimientoproducto_model->SaveMaquinarias($parseCollection->data, $this->input->post('actividad-id'), $this->input->post('empresa-id'), $userLogin->user_abt_id);
                            if (!$responseSave->is_valid) {
                                $response['error'] = $responseSave->errors;
                                $typeResponse = RESPONSE_NOT_ACCEPTABLE;
                            } else {
                                $response['data'] = $responseSave->data;
                            }
                        } else {
                            $typeResponse = RESPONSE_NOT_ACCEPTABLE;
                            $response['error'] = $parseCollection->errors;
                        }
                    }
                } else {
                    $response['error'] = "Existen parametros que se requieren para completar el proceso."; 
                    $typeResponse = RESPONSE_NOT_ACCEPTABLE;
                }
            }
        } 
 
        $this->output
                    ->set_content_type('application/json')
                    ->set_status_header($typeResponse)
                    ->set_output(
                        json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    }

    public function save_detalle_maquinaria() {
        $response = array();
        $typeResponse = RESPONSE_OK;

        if (!$this->auth->is_logged_in()) {
           $typeResponse = RESPONSE_NOT_ACCEPTABLE;
           $response['error'] = array("Debe iniciar sesión para procesar la solicitud.");
        } else {
            if (isset($_POST)) {
                $datos = $this->input->post();
                $userLogin = $this->auth->user();
                $responseSave = $this->movimientoproducto_model->SaveDetalleMaquinaria($datos, $userLogin->user_abt_id);
                if ($responseSave->is_valid) {
                    $detalle = new stdClass();
                    $detalle->detalle_id = $responseSave->data;
                    $response['data'] = $detalle;
                }
            } else {
                $response['error'] = "La petición no es correcta, contactese con la USI-ABT."; 
                $typeResponse = RESPONSE_NOT_ACCEPTABLE;
            }
        }

        // Get the current user information.
        $this->output
                    ->set_content_type('application/json')
                    ->set_status_header($typeResponse);
        echo json_encode($response);
    }

    public function remove_producto_registro() {
        $response = array();
        $typeResponse = RESPONSE_OK;
        if (!$this->auth->is_logged_in()) {
           $typeResponse = RESPONSE_NOT_ACCEPTABLE;
           $response['error'] = array("Debe iniciar sesión para procesar la solicitud.");
        } else {
            if (isset($_POST)) {
                $datos = $this->input->post();
                $userLogin = $this->auth->user();
                $responseSave = $this->movimientoproducto_model->Remove($datos, $userLogin->user_abt_id);
                if (!$responseSave->is_valid) {
                    $typeResponse = RESPONSE_NOT_ACCEPTABLE;
                    $response['error'] = ['Error al procesar la eliminacion, no se ha encontrado el elemento.'];
                } else {
                    $response['message'] = array('Se ha procesado la eliminacion del producto.');
                }
            } else {
                $response['error'] = ["La petición no es correcta, contactese con la USI-ABT."]; 
                $typeResponse = RESPONSE_NOT_ACCEPTABLE;
            }
        }

        $this->output
                    ->set_content_type('application/json')
                    ->set_status_header($typeResponse)
                    ->set_output(
                        json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    }

    public function remove_detalle_maquinaria() {
        $response = array();
        $typeResponse = RESPONSE_OK;

        if (!$this->auth->is_logged_in()) {
           $typeResponse = RESPONSE_NOT_ACCEPTABLE;
           $response['error'] = array("Debe iniciar sesión para procesar la solicitud.");
        } else {
            if (isset($_POST)) {
                $datos = $this->input->post();
                $userLogin = $this->auth->user();

                $responseSave = $this->movimientoproducto_model->RemoveDetalleMaquinaria($datos, $userLogin->user_abt_id);
                if (!$responseSave->is_valid) {
                    $typeResponse = RESPONSE_NOT_ACCEPTABLE;
                    $response['error'] = array("Error al procesar la eliminacion Detalle Maquinaria.");
                } else {
                    $response['message'] = array("Se proceso la eliminacion del Detalle Maquinaria.");
                }
            } else {
                $response['error'] = ["La petición no es correcta, contactese con la USI-ABT."]; 
                $typeResponse = RESPONSE_NOT_ACCEPTABLE;
            }
        }

        $this->output
                    ->set_content_type('application/json')
                    ->set_status_header($typeResponse)
                    ->set_output(
                        json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    }

    public function finalizar_solicitud_registro() {
        $response = array();
        $typeResponse = RESPONSE_OK;

        if (!$this->auth->is_logged_in()) {
           $typeResponse = RESPONSE_NOT_ACCEPTABLE;
           $response['error'] = array("Debe iniciar sesión para procesar la solicitud.");
        } else {
            if (isset($_POST)) {
                $solicitudId = $this->session->userdata('solicitud_inscripcion');
                if (!isset($solicitudId) || empty($solicitudId)) {
                    $response['message'] = array("Ya tiene una solicitud en proceso.");
                } else { 
                    $userLogin = $this->auth->user();

                    $solicitud = $this->empresa_model->Get_SolicitudComplete($solicitudId);
                    $solicitud = $solicitud->data;
                    $this->archivo_model->Save_Update_Documentos_Fisico($solicitud);
                    $responseProcess = $this->empresa_model->Finalizar_solicitud($solicitudId, $userLogin->user_abt_id);
                    if ($responseProcess->is_valid) {
                        $this->session->sess_expiration = '1800'; //30 Minutes
                        $response['message'] = array("Se ha finalizado la solicitud.");
                    } else {
                        $typeResponse = RESPONSE_NOT_ACCEPTABLE;
                        $response['error'] = $responseProcess->errors;
                    }
                }
            } else {
                $response['error'] = "La petición no es correcta, contactese con la USI-ABT."; 
                $typeResponse = RESPONSE_NOT_ACCEPTABLE;
            }
        }

        // Get the current user information.
        $this->output
                    ->set_content_type('application/json')
                    ->set_status_header($typeResponse);
        echo json_encode($response);
    }

    private function Get_Datos_Update_Registro() {
        $userLogin = $this->auth->user();

        $datos = new stdClass();
        $datos->razon_social_nombre = $this->input->post('razon_social');
        $datos->nit = $this->input->post('numero_nit');
        $datos->numero_fundempresa = $this->input->post('numero_fundempresa');
        $datos->codigo_usuario_id = $userLogin->user_abt_id;
        $datos->solicitud_id = $this->session->userdata('solicitud_inscripcion');

        if (!empty(($this->input->post('es_propietario')))) {
            $userData = $this->personaabt_model->GetUserById($userLogin->user_abt_id);
            $datos->propietario_codpersona = $userData->codentidad;
        } else {
            $datos->propietario = $this->Get_Datos_Persona($this->input->post('propietario'));
        }

        if (!empty($this->input->post('registro_antiguo'))) {
            $datos->registro_antiguo = $this->input->post('registro_antiguo');
        }
        $datos->sucursales = array();
        if (!empty($this->input->post('sucursales')) && sizeof($this->input->post('sucursales')) > 0) {
            $sucursales = $this->input->post('sucursales');
            foreach ($sucursales as $sucursal) {
                $sucursalObj = new stdClass();
                $sucursalObj->index_sucursal = $sucursal['index_sucursal'];
                $sucursalObj->direccion = $sucursal['direccion'];
                if (!empty($sucursal['agente_registro']))
                    $sucursalObj->agente_registro = $sucursal['agente_registro'];
                $sucursalObj->coordenada_x = $sucursal['coordenada_x'];
                $sucursalObj->coordenada_y = $sucursal['coordenada_y'];
                $sucursalObj->municipio_id = $sucursal['municipio_id'];

                if (!empty($sucursal['empresa_sucursal_id']))
                    $sucursalObj->empresa_sucursal_id = $sucursal['empresa_sucursal_id'];

                $sucursalObj->representantes = array();
                if (!empty($sucursal['representantes'])) {
                    $sucursalObj->representantes = $this->Get_Data_Representantes($sucursal['representantes']);
                }

                if (!empty($sucursal['actividades'])) {
                    // Falta Validar
                    $sucursalObj->actividades = $sucursal['actividades'];
                }
                array_push($datos->sucursales, $sucursalObj);
            }

        }

        return $datos;
    } 

    private function Get_Datos_Inscripcion() {
        $userLogin = $this->auth->user();

        $datos = new stdClass();
        $datos->razon_social_nombre = $this->input->post('razon_social');
        $datos->nit = $this->input->post('numero_nit');
        $datos->numero_fundempresa = $this->input->post('numero_fundempresa');
        $datos->codigo_usuario_id = $userLogin->user_abt_id;

        if (!empty(($this->input->post('es_propietario')))) {
            $userData = $this->personaabt_model->GetUserById($userLogin->user_abt_id);
            $datos->propietario_codpersona = $userData->codentidad;
        } else {
            $datos->propietario = $this->Get_Datos_Persona($this->input->post('propietario'));
        }

        if (!empty($this->input->post('registro_antiguo'))) {
            $datos->registro_antiguo = $this->input->post('registro_antiguo');
        }
        $datos->sucursales = array();
        if (!empty($this->input->post('sucursales')) && sizeof($this->input->post('sucursales')) > 0) {
            $sucursales = $this->input->post('sucursales');
            foreach ($sucursales as $sucursal) {
                $sucursalObj = new stdClass();
                $sucursalObj->index_sucursal = $sucursal['index_sucursal'];
                $sucursalObj->direccion = $sucursal['direccion'];
                if (!empty($sucursal['agente_registro']))
                    $sucursalObj->agente_registro = $sucursal['agente_registro'];
                $sucursalObj->coordenada_x = $sucursal['coordenada_x'];
                $sucursalObj->coordenada_y = $sucursal['coordenada_y'];
                $sucursalObj->municipio_id = $sucursal['municipio_id'];
                $sucursalObj->representantes = array();
                if (!empty($sucursal['representantes'])) {
                    $sucursalObj->representantes = $this->Get_Data_Representantes($sucursal['representantes']);
                }

                if (!empty($sucursal['actividades'])) {
                    // Falta Validar
                    $sucursalObj->actividades = $sucursal['actividades'];
                }
                array_push($datos->sucursales, $sucursalObj);
            }

        }

        return $datos;
    } 

    private function Get_Data_Representantes($collection) {
        $representantes = array();
        foreach ($collection as $representante) {
            array_push($representantes, $this->Get_Datos_Persona($representante));
        }
        return $representantes;
    }

    private function Get_Datos_Persona($data) {
        $persona = new stdClass();
        $persona->nombres = $data['nombres'];
        $persona->appaterno = $data['apellido-paterno'];
        $persona->apmaterno = $data['apellido-materno'];
        $persona->codpaisorigen = $data['pais-nacionalidad'];
        $persona->codtipodocid = $data['tipo-documento'];
        $persona->nrodocid = $data['numero-documento'];
        $persona->celular = $data['telf-celular'];

        if (isset($data['email']) && !empty($data['email']))
            $persona->email = $data['email'];

        if (!empty($data['departamento-extension'])) {
            $persona->emitido = $data['departamento-extension'];
        }

        if (!empty($data['telf-fijo'])) {
            $persona->telefono = $data['telf-fijo'];
        }
        if (isset($data['codpersona']) && !empty($data['codpersona']))
            $persona->codpersona = $data['codpersona'];

        return $persona;
    }
}