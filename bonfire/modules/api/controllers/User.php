<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class User extends Front_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->library('users/auth');
		$this->load->model('api/personaabt_model');
		$this->load->model('api/tipodocumento_model');
		$this->load->model('api/pais_model');
	}
	

	public function reset_password() {
		$response = array();
		$typeResponse = RESPONSE_OK;
		if (isset($_POST['email']) && !empty($this->input->post('email'))) {
			$responseUsuario = $this->personaabt_model->GetUserByEmail($this->input->post('email'));

			if (isset($responseUsuario) && !empty($responseUsuario)) {
				 $responseReset = $this->personaabt_model->Reset_Password($responseUsuario->CodUsuario);
				 //print_r($responseReset);
				 if ($responseReset->is_valid) {
	                 $this->load->library('emailer/emailer');
	                    $data = array(
	                        'to'      => $this->input->post('email'),
	                        'subject' => lang('us_reset_pass_subject'),
	                        'message' => $this->load->view(
	                            '_emails/forgot_password',
	                            array('link' => $responseReset->data),
	                            true
	                        ),
	                     );

	                // ********************** ENvio de Email por Resetear Contraseña **************************
	               //      $sendEmail = $this->emailer->send($data);
	               //      print_r($sendEmail);exit;
	               //      if ($sendEmail) {
				            // $response['error'] = array("Error al enviar el email");
				            // $typeResponse = RESPONSE_NOT_ACCEPTABLE;
	               //      } else {
	               //          $response['message'] = array("Revise su email para ver la informacion de su cuenta.");
	               //      }
	               		$newPass = $responseReset->data;
	               		$username = $responseUsuario->Login;
	                    $response['message'] = array("Revise su email para verificar la informacion de su cuenta ={$newPass}, {$username}.");
                 } else {
		            $response['error'] = array("Error al resetear su contraseña, contactese con la ABT.");
		            $typeResponse = RESPONSE_NOT_ACCEPTABLE;
                 }
			} else {
	            $response['error'] = array("Cuenta de correo no valida.");
	            $typeResponse = RESPONSE_NOT_ACCEPTABLE;
			}
		} else {
            $response['error'] = array("Es requerido el email");
            $typeResponse = RESPONSE_NOT_ACCEPTABLE;
		}

		$this->output
	            ->set_content_type('application/json')
	            ->set_status_header($typeResponse);
		echo json_encode($response);
	}

	public function login_front() {
		$response = array();
		$typeResponse = RESPONSE_OK;

        if ($this->auth->is_logged_in() !== false) {
            $response['error'] = array("El usuario ya inicio sesion");
            $typeResponse = RESPONSE_NOT_ACCEPTABLE;
        }

        if (isset($_POST['login'])) {
        	$responseAuth = $this->auth->login_front($this->input->post('username'), 
        			$this->input->post('password'), 
        			$this->input->post('remember_me') == '1');

	        if (!$responseAuth->is_valid || $responseAuth->has_errors === true) {
	        	$response['error'] = $responseAuth->errors;
	        	$typeResponse = RESPONSE_NOT_ACCEPTABLE;
	        } else {
	        	
	            log_activity(
	                $this->auth->user_id(),
	                lang('us_log_logged') . ': ' . $this->input->ip_address(),
	                'users'
	            );
	        }
        }

		$this->output
	            ->set_content_type('application/json')
	            ->set_status_header($typeResponse);
		echo json_encode($response);
	}

	/**
	 * Accion Api para el registro de datos de datos 
	 * de los propietarios
	 * @return [type] [description]
	 */
	public function registrar() {
		$response = array();
		$typeResponse = RESPONSE_OK;

		if (isset($_POST)) {

			$datos = new stdClass();
			$responseValidator = $this->Validar_Datos($this->input->post());
			if ($responseValidator['es_valido']) {
				$datos->nombres = $this->input->post('nombres');
				$datos->apellido_paterno = $this->input->post('apellido-paterno');
				$datos->apellido_materno = $this->input->post('apellido-materno');
				$datos->pais_nacionalidad = $this->input->post('pais-nacionalidad');
				$datos->tipo_documento = $this->input->post('tipo-documento');
				$datos->numero_documento = $this->input->post('numero-documento');
				$datos->password = $this->input->post('password');
				$datos->email = $this->input->post('email');
				$datos->telefono_celular = $this->input->post('telf-celular');
				$datos->telefono_fijo = $this->input->post('telf-fijo');

				if ($datos->pais_nacionalidad == Pais_model::PAIS_BOLIVIA_ID) {
					$datos->sigla_departamento_extension = $this->input->post('departamento-extension');
				} else
					$datos->sigla_departamento_extension = null;

				$responseRegistro = $this->personaabt_model->Save_Propietario($datos);


				if (!$responseRegistro->is_valid || $responseRegistro->has_errors) {
					$response['error'] = $responseRegistro->errors;
					$typeResponse = RESPONSE_NOT_ACCEPTABLE;
				} else {
					// Auto login
					$responseAuth = $this->auth->login_front($responseRegistro->data, $datos->password, false);
        			$response['message'] = array('Se ha completado el proceso de registro sin problemas, inicie sesion.');
				}
				
			} else {
				$response['error'] = $responseValidator['messages'];
				$typeResponse = RESPONSE_NOT_ACCEPTABLE;
			}

		} else {
			$response['error'] = "Se requiere datos para procesar la peticion"; 
			$typeResponse = RESPONSE_NOT_ACCEPTABLE;
		}
		$this->output
		            ->set_content_type('application/json')
		            ->set_status_header($typeResponse);
		echo json_encode($response);
	}

	private function Validar_Datos($datos) {
		$response = array('es_valido' => true, 'messages' => array());
		$messages = array();

		if (!isset($datos['nombres']) || 
			isset($datos['nombres']) && empty($datos['nombres'])) {
			array_push($messages, 'Debe ingresar valor para el campo "Nombres"');
		}
		if (!isset($datos['apellido-paterno']) ||
			isset($datos['apellido-paterno']) && empty($datos['apellido-paterno'])) {
			array_push($messages, 'Debe ingresar valor para el campo "Apellido Paterno"');
		}

		if (!isset($datos['pais-nacionalidad'])) {
			array_push($messages, 'Debe ingresar valor para el campo "Pais Nacionalidad"');
		} else {
			if ($datos['pais-nacionalidad'] == Pais_model::PAIS_BOLIVIA_ID) {
				if (!isset($datos['departamento-extension']) ||
					isset($datos['departamento-extension']) && empty($datos['departamento-extension'])) {
					array_push($messages, 'Debe ingresar la sigla para "Extensión de Documento Identidad"');
				}
			}
		}

		if (!isset($datos['tipo-documento']) || 
			isset($datos['tipo-documento']) && empty($datos['tipo-documento'])) {
			array_push($messages, 'Debe ingresar valor para el campo "Tipo de Documento"');
		}

		if (!isset($datos['numero-documento']) || 
			isset($datos['numero-documento']) && empty($datos['numero-documento'])) {
			array_push($messages, 'Debe ingresar valor para el campo "Número de Documento"');
		}

		if (!isset($datos['email']) ||
			isset($datos['email']) && empty($datos['email'])) {
			array_push($messages, 'Debe ingresar valor para el campo "Email"');
		}

		if (!isset($datos['password']) ||
			isset($datos['password']) && empty($datos['password'])) {
			array_push($messages, 'Debe ingresar valor para el campo "Contraseña" para crear la cuenta de usuario');
		}

		// if (!isset($datos['g-recaptcha-response']) || 
		// 	isset($datos['g-recaptcha-response']) && empty($datos['g-recaptcha-response'])) {
		// 	array_push($messages, 'Debe ingresar valor de recapcha para continuar con el proceso');
		// } else {
		// 	$captchaValido = RecaptchaApp::Verificar($datos['g-recaptcha-response']);
		// 	if (!$captchaValido) {
		// 		array_push($messages, 'Debe ingresar valor válido de recapcha para continuar con el proceso');
		// 	}
		// }



		if (count($messages) > 0) {
			$response['es_valido'] = false;
			$response['messages'] = $messages;
		}
		return $response;
	}
}