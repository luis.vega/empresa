<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class File extends Front_Controller
{
	public function __construct()
	{
		parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('empresas/archivo_model');
        $this->load->library('users/auth');
        $this->load->helper('download');
	}
	
	public function upload_archivo_empresa() {
		$response = array();
		$typeResponse = RESPONSE_OK;

        if (!$this->auth->is_logged_in()) {
            $typeResponse = RESPONSE_NOT_ACCEPTABLE;
            $response['error'] = array("Debe iniciar sesión para continuar con el proceso.");
        }

        $config['upload_path']          = config_item('path-upload-empresa');
        $config['allowed_types']        = implode('|', $this->archivo_model->Get_Type_File_Upload());
        $config['max_size']             = config_item('size-upload-empresa');
        $config['max_width']            = config_item('max-width-upload-empresa');
        $config['max_height']           = config_item('max-height-upload-empresa');
		$config['file_name'] = Random::GenerarString(25, 'alpha-numeric');
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file'))
        {
            $error = array('error' => $this->upload->display_errors());
            $response = $error;
            $response['filename'] = $this->input->post('config-file-name');
            $typeResponse = RESPONSE_INTERNAL_SERVER_ERROR;
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            $filenameSave = $data['upload_data']['file_name'];
            $userLogin = $this->auth->user();
            $response = $this->archivo_model->Saveupdate_File_Empresa(
                    $this->input->post('empresa-id'), 
                    $filenameSave,
                    $this->input->post('config-file-name'),
                    $this->input->post('config-file-id'),
                    $userLogin->user_abt_id);
            $response['message'] = "Archivo guardado con exito";
        }

		$this->output
		            ->set_content_type('application/json')
		            ->set_status_header($typeResponse);
		echo json_encode($response);
	}

    public function upload_archivo_sucursal() {
        $response = array();
        $typeResponse = RESPONSE_OK;

        if (!$this->auth->is_logged_in()) {
            $typeResponse = RESPONSE_NOT_ACCEPTABLE;
            $response['error'] = array("Debe iniciar sesión para continuar con el proceso.");
         }

        $config['upload_path']          = config_item('path-upload-empresa');
        $config['allowed_types']        = implode('|', $this->archivo_model->Get_Type_File_Upload());
        $config['max_size']             = config_item('size-upload-empresa');
        $config['max_width']            = config_item('max-width-upload-empresa');
        $config['max_height']           = config_item('max-height-upload-empresa');
        $config['file_name'] = Random::GenerarString(25, 'alpha-numeric');
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file'))
        {
            $error = array('error' => $this->upload->display_errors());
            $response = $error;
            $response['filename'] = $this->input->post('config-file-name');
            $typeResponse = RESPONSE_INTERNAL_SERVER_ERROR;
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            $filenameSave = $data['upload_data']['file_name'];
            $userLogin = $this->auth->user();
            $datos = new stdClass();
            $datos->filename_save = $filenameSave;
            $datos->empresa_sucursal_id = $this->input->post('sucursal-id');
            $datos->config_archivo_Id = $this->input->post('config-file-id');
            $datos->filename = $this->input->post('config-file-name');
            if (!empty($this->input->post('type-agente'))) {
                $datos->tipo = Archivo_model::TIPO_DOC_PRESTADO_AGENTE;
            }
            if (!empty($this->input->post('type-representante'))) {
                $datos->tipo = Archivo_model::TIPO_DOC_PRESTADO_REPRESENTANTE;
                $datos->entidad_id = $this->input->post('entidad-id');
            }
            if (!empty($this->input->post('type-actividad'))) {
                $datos->tipo = Archivo_model::TIPO_DOC_PRESTADO_ACTIVIDAD;
                $datos->entidad_id = $this->input->post('entidad-id');
            }
            if (!empty($this->input->post('type-sucursal'))) {
                $datos->tipo = Archivo_model::TIPO_DOC_SUCURSAL;
            }
            if (!empty($this->input->post('type-empresa'))) {
                $datos->tipo = Archivo_model::TIPO_DOC_EMPRESA;
                $datos->entidad_id = $this->input->post('entidad-id');
            }
            $response = $this->archivo_model->Saveupdate_File_Sucursal($datos, $userLogin->user_abt_id);
            $response['message'] = "Archivo guardado con exito";
        }

        $this->output
                    ->set_content_type('application/json')
                    ->set_status_header($typeResponse);
        echo json_encode($response);
    }

    public function remove_upload_archivo_sucursal() {
        $response = array();
        $typeResponse = RESPONSE_OK;

        if (!$this->auth->is_logged_in()) {
            $typeResponse = RESPONSE_NOT_ACCEPTABLE;
            $response['error'] = array("Debe iniciar sesión para continuar con el proceso.");
         }

        $userLogin = $this->auth->user();
        $datos = new stdClass();
        $datos->empresa_sucursal_id = $this->input->post('sucursal-id');
        $datos->config_archivo_Id = $this->input->post('config-file-id');
        $datos->filename = $this->input->post('config-file-name');
        if (!empty($this->input->post('type-agente'))) {
            $datos->tipo = Archivo_model::TIPO_DOC_PRESTADO_AGENTE;
        }
        if (!empty($this->input->post('type-representante'))) {
            $datos->tipo = Archivo_model::TIPO_DOC_PRESTADO_REPRESENTANTE;
            $datos->entidad_id = $this->input->post('entidad-id');
        }
        if (!empty($this->input->post('type-actividad'))) {
            $datos->tipo = Archivo_model::TIPO_DOC_PRESTADO_ACTIVIDAD;
            $datos->entidad_id = $this->input->post('entidad-id');
        }
        if (!empty($this->input->post('type-sucursal'))) {
            $datos->tipo = Archivo_model::TIPO_DOC_SUCURSAL;
        }
        if (!empty($this->input->post('type-empresa'))) {
            $datos->tipo = Archivo_model::TIPO_DOC_EMPRESA;
            $datos->empresa_sucursal_id = $this->session->userdata('solicitud_inscripcion');
            $responseRemove = $this->archivo_model->Remove_File_Empresa($datos, $userLogin->user_abt_id);
            if ($responseRemove->is_valid) {
                $response['message'] = "Archivo eliminado con exito";
            } else {
                $typeResponse = RESPONSE_NOT_ACCEPTABLE;
                $response['error'] = array("No de puedo eliminar el archivo.");
            }
        } else {
            $responseRemove = $this->archivo_model->Remove_File_Sucursal($datos, $userLogin->user_abt_id);
            if ($responseRemove->is_valid) {
                $response['message'] = "Archivo eliminado con exito";
            } else {
                $typeResponse = RESPONSE_NOT_ACCEPTABLE;
                $response['error'] = array("No de puedo eliminar el archivo.");
            }
        }


        $this->output
                    ->set_content_type('application/json')
                    ->set_status_header($typeResponse);
        echo json_encode($response);
    }

    public function download_file_empresa()
    {
        $pathBase = config_item('path-upload-empresa');
        $filename =  $this->uri->segment(4);
        
        if (!$this->auth->is_logged_in() || empty($filename)) {
            Template::redirect('/NoPuedeAcceder');
        }

        $pathFile = $pathBase . $filename;
        $data = file_get_contents($pathFile);
        $this->load->helper('download');
        force_download($filename, $data);
    }
}