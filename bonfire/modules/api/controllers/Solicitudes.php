<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Solicitudes extends Front_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('empresas/provincia_model');
	}
	public function List_By_Id_Departamento() {
		$idDepartamento =  $this->uri->segment(4);
		$response = array();
		$typeResponse = 200;
		if (isset($idDepartamento) and intval($idDepartamento) > 0) {
			$listProvincias = $this->provincia_model->Get_List_By_Id_Departamento($idDepartamento);
			$response['data'] = $listProvincias;
		} else {
			$typeResponse = 403;
			$response['error'] = "Id departamento vacio o tiene valor no válido";
		}
	
		$this->output
		->set_content_type('application/json')
		->set_status_header($typeResponse);
		echo json_encode($response);
	}
}