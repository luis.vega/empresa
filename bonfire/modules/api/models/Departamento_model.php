<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * User Model.
 *
 * The central way to access and perform CRUD on users.
 *
 * @package Bonfire\Modules\Empresas\Models\tipoActividad_model
 * @author  Luis Vega
 */
class Departamento_model extends BF_Model {

	const SANTA_CRUZ = 1; 
	const BENI = 2; 
	const PANDO = 3; 
	const LA_PAZ = 4; 
	const ORURO = 5; 
    const POTOSI = 6; 
    const CHUQUISACA = 7; 
    const TARIJA = 8; 
	const COCHABAMBA = 9; 
	
	protected $table_name = 'departamento';

	public function Get_List() {

    	$auxPrefix = $this->db->dbprefix;
    	$this->db->dbprefix = '';

        $this->preFind();
        $list = parent::find_all();

        $this->db->dbprefix = $auxPrefix;
        return $list;
	}

	    /**
     * Set the select and join portions of the SQL query for the find* methods.
     *
     * @todo Set this in the before_find observer?
     *
     * @return void
     */
    protected function preFind()
    {
        if (empty($this->selects)) {
            $this->select(["{$this->table_name}.*", 'nombre']);
        }
    }
	
}