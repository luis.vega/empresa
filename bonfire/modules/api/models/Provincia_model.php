<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * User Model.
 *
 * The central way to access and perform CRUD on users.
 *
 * @package Bonfire\Modules\Empresas\Models\tipoActividad_model
 * @author  Luis Vega
 */
class Provincia_model extends BF_Model {
	
	protected $table_name = 'tec_provincia';

	public function Get_List() {

    	$auxPrefix = $this->db->dbprefix;
    	$this->db->dbprefix = '';

        $this->preFind();
        $list = parent::find_all();

        $this->db->dbprefix = $auxPrefix;
        return $list;
	}

    public function Get_List_By_Id_Departamento($idDepartamento) {

        $list = $this->filter_by('coddepartamento', $idDepartamento);

        return $list;
    }

    /**
     * Find a single user based on a field/value match, including role information.
     *
     * @param string $field The field to match. If 'both', attempt to find a user
     * with the $value field matching either the username or email.
     * @param string $value The value to search for.
     * @param string $type  The type of where clause to create ('and' or 'or').
     *
     * @return bool|object An object with the user's info, or false on failure.
     */
    private function filter_by($field = null, $value = null, $type = 'and')
    {   
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $this->order_by('nombre', 'asc');
        $this->preFind();

        $list = parent::find_all_by($field, $value, $type);
        $this->db->dbprefix = $auxPrefix;

        return $list;
    }


	/**
     * Set the select and join portions of the SQL query for the find* methods.
     *
     * @todo Set this in the before_find observer?
     *
     * @return void
     */
    protected function preFind()
    {
        if (empty($this->selects)) {
            $this->select(["{$this->table_name}.*", 'nombre']);
        }
    }
	
}