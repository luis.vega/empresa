<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * User Model.
 *
 * The central way to access and perform CRUD on users.
 *
 * @package Bonfire\Modules\Api\Models\pais_model
 * @author  Luis Vega
 * 
 */
class PersonaAbt_model extends BF_Model {
	
    protected $table_name = 'abt_persona';
    protected $table_persona_abt = 'abt_persona';
    protected $table_usuario_abt = 'sys_usuario';
    protected $table_usuario_bonfire = 'bf_users';
	protected $table_grupousuario_abt = 'sys_grupo';

    const TIPO_PERSONA_NATURAL = 'N';
    const TIPO_PERSONA_JUDICIAL = 'J';


    const TIPO_USUARIO_EMPRESA = 'E';
    const TIPO_USUARIO_AGENTE_AUXILIAR = 'A';
    const TIPO_USUARIO_REPRESENTANTE = 'R';
    const TIPO_USUARIO_FUNCIONARIO_ABT = 'F';


    const GRUPO_USUARIO_PROPIETARIO = 75;
    const GRUPO_USUARIO_REPRESENTANTE = 57;

    const PERSONA_ESTADO_ACTIVO = 'A';

    const ROL_BONFIRE_USER = 4;

	public function Get_List() {

    	$auxPrefix = $this->db->dbprefix;
    	$this->db->dbprefix = '';

        $this->preFind();
        $list = parent::find_all();

        $this->db->dbprefix = $auxPrefix;
        return $list;
	}

    public function Save_Propietario($datos) {
        $response = ResponseApp::Validate();
        $personaDoc = $this->GetPersonByDocumento($datos->numero_documento, 
                $datos->pais_nacionalidad, $datos->tipo_documento, $datos->sigla_departamento_extension);
        if (isset($personaDoc) && !empty($personaDoc)) {
            array_push($response->errors, 'Ya existe un propietario con los datos de Documento de Identidad');
            $response->has_errors = true;
        }

        $personaByEmail = $this->GetPersonByEmail($datos->email);
        if (isset($personaByEmail) && !empty($personaByEmail)) {
            array_push($response->errors, 'Ya existe un propietario con los datos de Email');
            $response->has_errors = true;
        }

        if ($response->has_errors)
            return $response;

        $this->db->trans_begin();
        try {
            
            $auxPrefix = $this->db->dbprefix;
            $this->db->dbprefix = '';
            $savePersonaId = $this->insert_entity($this->GetDataSave($datos));
            $this->db->dbprefix = $auxPrefix;


            // Procesar Registro de Usuario
            $username = $this->GenerateUsername($datos->nombres, $datos->apellido_paterno, $datos->apellido_materno);
            $users = $this->GetUsersByUsename($username);
            if (isset($users) && !empty($users) && is_array($users)) {
                $username .= sizeof($users) + 1;
            }

            $fullApellidos = $datos->apellido_paterno;
            $fullApellidos .= empty($datos->apellido_materno) ? NULL : ' ' .  $datos->apellido_materno;
            $datosUser = array(
                'codgrupo' => self::GRUPO_USUARIO_PROPIETARIO,
                'login' => $username,
                'pw' => md5($datos->password),
                'nombres' => $datos->nombres,
                'apellidos' => $fullApellidos,
                'estado' => true,
                'tipo' => self::TIPO_USUARIO_EMPRESA,
                'codentidad' => $savePersonaId
            );
            $auxPrefix = $this->db->dbprefix;
            $this->db->dbprefix = '';
            $this->table_name = $this->table_usuario_abt;
            $saveUsuarioId = $this->insert_entity($datosUser);
            $this->table_name = $this->table_persona_abt;
            $this->db->dbprefix = $auxPrefix;


            // Crear Cuenta Bonfire
            $datosUserBonfire = array(
                'role_id' => self::ROL_BONFIRE_USER,
                'email' => $datos->email,
                'username' => $username,
                'password_hash' => '',
                'display_name' => $fullApellidos,
                'timezone' => 'UM8',
                'language' => 'english',
                'active' => true,
                'force_password_reset' => false,
                'user_abt_id' => $saveUsuarioId
            );

            $this->table_name = $this->table_usuario_bonfire;
            $usuarioAbtId = $this->insert_entity($datosUserBonfire);
            $this->table_name = $this->table_persona_abt;

            $response->is_valid = true;
            $response->data = $username;

            if($this->db->trans_status() === FALSE || !isset($saveUsuarioId) || !isset($usuarioAbtId)){
               $this->db->trans_rollback();
            }else{
               $this->db->trans_commit();
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
        }
        $this->db->trans_commit();

        return $response;

    }

    public function Save_Update_Persona($datos) {
        $response = ResponseApp::Validate();
        $personaDoc = $this->GetPersonAbtByDocumento($datos->numero_documento, 
                $datos->pais_nacionalidad, $datos->tipo_documento, $datos->sigla_departamento_extension);

        $personaByEmail = $this->GetPersonAbtByEmail($datos->email);

        if ($response->has_errors)
            return $response;

        $this->db->trans_begin();
        try {
            $auxPrefix = $this->db->dbprefix;           
            $this->db->dbprefix = '';

            if (empty($personaDoc) && empty($personaByEmail)) {
                $savePersonaId = $this->insert_entity($this->GetDataSave($datos));
            } else {
                if (isset($personaDoc) && !empty($personaDoc)) {
                    // $data = $this->GetDataSave($datos);
                    // unset($data['codpersona']);
                    // $this->db->update($this->table_name, $data, ['codpersona' => $personaByEmail->codpersona]);
                    $savePersonaId = $personaDoc->codpersona;
                } else {
                    // $data = $this->GetDataSave($datos);
                    // $this->db->update($this->table_name, $data, ['codpersona' => $personaDoc->codpersona]);
                    $savePersonaId =$personaByEmail->codpersona;
                }
            }

            $this->db->dbprefix = $auxPrefix;

            $response->is_valid = true;
            $response->data = $savePersonaId;

            if($this->db->trans_status() === FALSE){
               $this->db->trans_rollback();
            }else{
               $this->db->trans_commit();
            }
        } catch (Exception $e) {
            $this->db->trans_rollback();
        }
        $this->db->trans_commit();

        return $response;

    }

    public function Reset_Password($userId) {
        $response = ResponseApp::Validate();
        $user = $this->GetUserById($userId);
        if (!isset($user)) {
            array_push($response->errors, 'No existe usuario con los el ID correspondiente');
            $response->has_errors = true;
            return $response;
        }

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $this->table_name = $this->table_usuario_abt;

        $hash = Random::GenerarString(10, 'alpha-numeric') . '$';
        $hashEncrypt = md5($hash);
        $result = $this->db->update($this->table_name, 
            array('Pw' => $hashEncrypt, 'fecha_reset_pw' => date('Y-m-j H:i:s', strtotime("now"))), 
            array('codusuario' => $userId));

        $this->db->dbprefix = $auxPrefix;
        $this->table_name = $this->table_persona_abt;

        $response->is_valid = true;
        $response->data = $hash;
        return $response;
    }

    public function GetUserById($userId) 
    {
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $this->join(
            $this->table_persona_abt,
            "{{$this->table_persona_abt}.codpersona={$this->table_usuario_abt}.codentidad"
        );

        $this->where('codusuario', $userId);

        $this->table_name = $this->table_usuario_abt;
        $users = $this->find_all();

        $this->db->dbprefix = $auxPrefix;
        $this->table_name = $this->table_persona_abt;

        if (!empty($users) && sizeof($users) == 1)
            return $users[0];

        return $users;
    }

    public function GetPersonaById($personaId) 
    {
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $this->select(["{$this->table_persona_abt}.*, concat(
            {$this->table_persona_abt}.nombres, ' ', 
            {$this->table_persona_abt}.appaterno, ' ',
            {$this->table_persona_abt}.apmaterno
        ) as nombre_apellido, pais.nombre as persona_pais"]);

        $this->join(
            "abt_pais pais",
            "pais.codpais={$this->table_name}.codpaisorigen"
        );

        $persona = $this->find_by('codpersona', $personaId);
        $this->db->dbprefix = $auxPrefix;

        return $persona;
    }

    public function GetPersonasRepresentanteById($sucursalId) 
    {
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $this->select(["{$this->table_persona_abt}.*, concat(
            {$this->table_persona_abt}.nombres, ' ', 
            {$this->table_persona_abt}.appaterno, ' ',
            {$this->table_persona_abt}.apmaterno
        ) as nombre_apellido, pais.nombre as persona_pais"]);

        $this->join(
            "abt_representante_legal rl",
            "rl.representante_codpersona={$this->table_name}.codpersona"
        );
        $this->join(
            "abt_pais pais",
            "pais.codpais={$this->table_name}.codpaisorigen"
        );

        $personas = $this->find_all_by('abt_sucursal_id', $sucursalId);
        $this->db->dbprefix = $auxPrefix;

        return $personas;
    }

    public function GetUserByEmail($email) {
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $this->join(
            $this->table_persona_abt,
            "{{$this->table_persona_abt}.codpersona={$this->table_usuario_abt}.codentidad"
        );

        $this->select(["{$this->table_usuario_abt}.*, {$this->table_persona_abt}.*"]);
        $this->where('codgrupo', self::GRUPO_USUARIO_PROPIETARIO);
        $this->where('tipo', self::TIPO_USUARIO_EMPRESA);
        $this->where('codentidad !=', NULL);
        $this->where('email', $email);

        $this->table_name = $this->table_usuario_abt;
        $users = $this->find_all();

        $this->db->dbprefix = $auxPrefix;
        $this->table_name = $this->table_persona_abt;

        if (!empty($users) && sizeof($users) == 1)
            return $users[0];

        return null;
    }


    public function GetUserByUsername($username) {
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $this->select(["{$this->table_usuario_abt}.*"]);
        $this->where('codgrupo', self::GRUPO_USUARIO_PROPIETARIO);
        $this->where('tipo', self::TIPO_USUARIO_EMPRESA);
        $this->where('codentidad !=', NULL);
        $this->where('login', $username);

        $this->table_name = $this->table_usuario_abt;
        $users = $this->find_all();

        $this->db->dbprefix = $auxPrefix;
        $this->table_name = $this->table_persona_abt;

        if (!empty($users) && sizeof($users) == 1)
            return $users[0];

        return null;
    }

    private function GetUsersByUsename($username) {
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $this->select(["{$this->table_usuario_abt}.*"]);
        $this->where('codentidad !=', NULL);
        $this->where('login LIKE ', "%{$username}%");

        $this->table_name = $this->table_usuario_abt;
        $users = $this->find_all();

        $this->db->dbprefix = $auxPrefix;
        $this->table_name = $this->table_persona_abt;

        return $users;
    }

    private function GenerateUsername($nombres, $apellidoPaterno, $apellidoMaterno) {
        $nombresUtf = Msecurity::sanear($nombres);
        $apellidoPaternoUtf = Msecurity::sanear($apellidoPaterno);
        $apellidoMaternoUtf = Msecurity::sanear($apellidoMaterno);
        $username = str_split($nombresUtf)[0][0];
        $username .= $apellidoPaternoUtf;
        $username .= str_split($apellidoMaternoUtf)[0];
        $username = strtolower($username);

        return $username;
    }

    private function GetDataSave($datos) {
        $dataSave = array(
            'nombres' => $datos->nombres,
            'appaterno' => $datos->apellido_paterno,
            'apmaterno' => $datos->apellido_materno,
            'celular' => $datos->telefono_celular,
            'telefono' => $datos->telefono_fijo,
            'codpaisorigen' => $datos->pais_nacionalidad,
            'tipopersona' => self::TIPO_PERSONA_NATURAL,
            'estado' => self::PERSONA_ESTADO_ACTIVO,
            'codtipodocid' => $datos->tipo_documento,
            'nrodocid' => $datos->numero_documento,
            'emitido' => $datos->sigla_departamento_extension,
            'email' => $datos->email
        );
        return $dataSave;
    }

    private function GetPersonByEmail($email) {
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $this->preFindJoinUsuario();
        $fields = array('email' => $email, 'tipopersona' => self::TIPO_PERSONA_NATURAL);
        $persona = parent::find_by($fields);
        $this->db->dbprefix = $auxPrefix;
        return $persona;
    }

    private function GetPersonAbtByEmail($email) {
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $this->table_name = $this->table_persona_abt;
        $fields = array('email' => $email, 'tipopersona' => self::TIPO_PERSONA_NATURAL);
        $persona = parent::find_by($fields);
        $this->db->dbprefix = $auxPrefix;

        return $persona;
    }



    private function GetPersonByDocumento($numeroDocumento, $idPaisNacionalidad, $tipoDocumento, $siglaExpedicion) {
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $this->table_name = $this->table_persona_abt;
        $this->preFindJoinUsuario();
        $fields = array('nrodocid' => $numeroDocumento, 
                       'codpaisorigen' => $idPaisNacionalidad,
                       'codtipodocid' => $tipoDocumento,
                        'tipopersona' => self::TIPO_PERSONA_NATURAL);
        if (isset($siglaExpedicion) && !empty($siglaExpedicion)) {
            $fields['emitido'] = $siglaExpedicion;
        }
        $persona = parent::find_by($fields);
        $this->db->dbprefix = $auxPrefix;
        return $persona;
    }

    private function GetPersonAbtByDocumento($numeroDocumento, $idPaisNacionalidad, $tipoDocumento, $siglaExpedicion) {
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $this->table_name = $this->table_persona_abt;
        $fields = array('nrodocid' => $numeroDocumento, 
                       'codpaisorigen' => $idPaisNacionalidad,
                       'codtipodocid' => $tipoDocumento,
                        'tipopersona' => self::TIPO_PERSONA_NATURAL);
        if (isset($siglaExpedicion) && !empty($siglaExpedicion)) {
            $fields['emitido'] = $siglaExpedicion;
        }
        $persona = parent::find_by($fields);
        $this->db->dbprefix = $auxPrefix;
        return $persona;
    }

    /**
     * Find a single user based on a field/value match, including role information.
     *
     * @param string $field The field to match. If 'both', attempt to find a user
     * with the $value field matching either the username or email.
     * @param string $value The value to search for.
     * @param string $type  The type of where clause to create ('and' or 'or').
     *
     * @return bool|object An object with the user's info, or false on failure.
     */
    private function FilterBy($field = null, $value = null, $type = 'and')
    {   
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $this->preFind();

        $persona = parent::find_by($field, $value, $type);
        $this->db->dbprefix = $auxPrefix;

        return $persona;
    }

	/**
     * Set the select and join portions of the SQL query for the find* methods.
     *
     * @todo Set this in the before_find observer?
     *
     * @return void
     */
    protected function preFind()
    {
        if (empty($this->selects)) {
            $this->select(["{$this->table_name}.*", 'nrodocid']);
        }
    }

        /**
     * Set the select and join portions of the SQL query for the find* methods.
     *
     * @todo Set this in the before_find observer?
     *
     * @return void
     */
    protected function preFindJoinUsuario()
    {
        if (empty($this->selects)) {
            $this->select(["{$this->table_name}.*", 'nrodocid']);
        }
        $tipoUsuario = self::TIPO_USUARIO_EMPRESA;
        $grupoUsuario = self::GRUPO_USUARIO_PROPIETARIO;
        $this->join(
            $this->table_usuario_abt,
            "{$this->table_usuario_abt}.codentidad = {$this->table_name}.codpersona 
                and {$this->table_usuario_abt}.tipo='{$tipoUsuario}'
                and {$this->table_usuario_abt}.codgrupo={$grupoUsuario}"
        );
    }

    protected function preFindJoinUsuarioInPersona()
    {
        if (empty($this->selects)) {
            $this->select(["{$this->table_usuario_abt}.*"]);
        }
        $tipoPersona = self::TIPO_PERSONA_NATURAL;
        $estado = self::PERSONA_ESTADO_ACTIVO;
        $this->join(
            $this->table_persona_abt,
            "{{$this->table_persona_abt}.codpersona=$this->table_usuario_abt}.codentidad
                and {$this->table_persona_abt}.tipopersona='{$tipoPersona}'
                and {$this->table_persona_abt}.estado={$estado}"
        );
    }
	
}