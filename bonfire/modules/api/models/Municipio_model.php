<?php defined('BASEPATH') || exit('No direct script access allowed');

/**
 * User Model.
 *
 * The central way to access and perform CRUD on users.
 *
 * @package Bonfire\Modules\Empresas\Models\tipoActividad_model
 * @author  Luis Vega
 */
class Municipio_model extends BF_Model {
	
	protected $table_name = 'tec_municipio';

	public function Get_List() {

    	$auxPrefix = $this->db->dbprefix;
    	$this->db->dbprefix = '';

        $this->preFind();
        $list = parent::find_all();

        $this->db->dbprefix = $auxPrefix;
        return $list;
	}

    public function Get_List_Ordenada_By_Departamentos() {

        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';
        $fields = <<<EOF
    `m`.`codmunicipio` AS `codmunicipio`,
    `d`.`CodDepartamento` AS `coddepartamento`,
    `m`.`codprovincia` AS `codprovincia`,
     d.nombre as departamento,
     p.nombre as provincia,
     m.nombre as municipio,
     d.nombre as nombdpto,
    concat(
        CONVERT(`m`.`nombre` USING utf8),
        ' - ',
        CONVERT(`p`.`nombre` USING utf8),
        ' - ',
        CONVERT(
            ucase(`d`.`Nombre`)USING utf8
        )
    )AS `nombre`,
    concat(
        ucase(`d`.`Nombre`),
        ' - ',
        `m`.`nombre`
    )AS `depmun`
EOF;
    $ordenFields = <<<EOF
CONVERT(`m`.`nombre` USING utf8),
' - ',
CONVERT(`p`.`nombre` USING utf8),
' - ',
CONVERT(
    ucase(`d`.`Nombre`)USING utf8
)
EOF;
        $this->db->select($fields);
        $this->db->from("{$this->table_name} m");
        $this->db->join('tec_provincia p', 'm.codprovincia=p.codprovincia');
        $this->db->join('departamento d', 'p.coddepartamento = d.CodDepartamento');
        
        $this->order_by($ordenFields);

        $query = $this->db->get();
        $list = $query->result();

        $this->db->dbprefix = $auxPrefix;

        return $list;
    }

    /**
     * Find a single user based on a field/value match, including role information.
     *
     * @param string $field The field to match. If 'both', attempt to find a user
     * with the $value field matching either the username or email.
     * @param string $value The value to search for.
     * @param string $type  The type of where clause to create ('and' or 'or').
     *
     * @return bool|object An object with the user's info, or false on failure.
     */
    private function filter_by($field = null, $value = null, $type = 'and')
    {   
        $auxPrefix = $this->db->dbprefix;
        $this->db->dbprefix = '';

        $this->order_by('nombre', 'asc');
        $this->preFind();

        $list = parent::find_all_by($field, $value, $type);

        $this->db->dbprefix = $auxPrefix;

        return $list;
    }


	/**
     * Set the select and join portions of the SQL query for the find* methods.
     *
     * @todo Set this in the before_find observer?
     *
     * @return void
     */
    protected function preFind()
    {
        if (empty($this->selects)) {
            $this->select(["{$this->table_name}.*", 'nombre']);
        }
    }
	
}