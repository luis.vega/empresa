<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Msecurity {

    public function __construct()
    {
        self::$ci =& get_instance();
    }

    public static function sanear($string){

    	$string = strtoupper($string);

	    if($string) {

	    	$prohibited = array(" SELECT ", 
								" * ", 
								" DELETE ", 
								" FROM ", 
								" UPDATE ", 
								" INSERT ", 
								" UNION ALL ", 
								" ALL ", 
								" ORDER ", 
								" TRUNCATE ", 
								" EDIT ", 
								" SHOW ", 
								" TABLES ",
								" TABLE ",  
								" DATABASE ",
								"SELECT ", 
								"* ", 
								"DELETE ", 
								"FROM ", 
								"UPDATE ", 
								"INSERT ", 
								"UNION ALL ", 
								"ALL ", 
								"ORDER ", 
								"TRUNCATE ", 
								"EDIT ", 
								"SHOW ", 
								"TABLES ",
								"TABLE ",  
								"DATABASE ",
								" SELECT", 
								" *", 
								" DELETE", 
								" FROM", 
								" UPDATE", 
								" INSERT", 
								" UNION ALL", 
								" ALL", 
								" ORDER", 
								" TRUNCATE", 
								" EDIT", 
								" SHOW", 
								" TABLES",
								" TABLE",  
								" DATABASE",);

	    	$prohibited2 = $prohibited;

			$notprohibited = array("", "", "", "", "", "", "", "", "", "", 
								   "", "", "", "", "", "", "", "", "", "", 
								   "", "", "", "", "", "", "", "", "", "", 
								   "", "", "", "", "", "", "", "", "", "", 
								   "", "", "", "", "", "", "", "", "", ""
								  );
			
			

			$array_string = explode(" ", $string);

			$string = "";

			foreach ($array_string as $value) {
				if(self::search_array($prohibited2, $value))
					$value = "";
				$string .=$value." ";			
			}

			$string = str_replace($prohibited, $notprohibited, $string);

		    $string = trim($string);

		    $string = str_replace(
		        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
		        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
		        $string
		    );

		    $string = str_replace(
		        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
		        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
		        $string
		    );

		    $string = str_replace(
		        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
		        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
		        $string
		    );

		    $string = str_replace(
		        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
		        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
		        $string
		    );

		    $string = str_replace(
		        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
		        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
		        $string
		    );

		    $string = str_replace(
		        array('ñ', 'Ñ', 'ç', 'Ç'),
		        array('n', 'N', 'c', 'C',),
		        $string
		    );

		    $string = str_replace(
	        
	        
	        array("\\", "¨", "º", "-", "~",
	             "#", "@", "|", "!", "\"",
	             "·", "$", "%", "&", "/",
	             "(", ")", "?", "'", "¡",
	             "¿", "[", "^", "`", "]",
	             "+", "}", "{", "¨", "´",
	             ">", "< ", ";", ",", ":",
	             ".", "-","_"),
	        '',
	        $string
	    	);
	    	

	    	$string = strip_tags($string);
	    	$string = str_replace('"',"",$string);
			$string = str_replace("'","",$string);
			$string = trim($string);
			$string = htmlentities($string);
			
			
			
		}

		return $string;
	}

	public static function search_array($array, $value){
		$next = false;
		for($i=0; $i<count($array); $i++){
			if($array[$i] == $value) $next = true;
		}
		return $next;
	}
}