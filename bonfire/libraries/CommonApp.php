<?php defined('BASEPATH') || exit('No direct script access allowed');
require_once(BFPATH . 'libraries/GoogleRecaptcha/Recaptchalib.php');

/**
* 
*/
class CommonApp 
{
	
    public function __construct()
    {
        self::$ci =& get_instance();
    }

    /**
     * [Validate description]
     */
    public static function DateNow() {
        return date('Y-m-j H:i:s', strtotime("now"));
    }

    public static function DateTimeToString($date) {
        return $date->format('Y-m-j H:i:s');
    }

}