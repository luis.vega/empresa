<?php defined('BASEPATH') || exit('No direct script access allowed');
require_once(BFPATH . 'libraries/GoogleRecaptcha/Recaptchalib.php');

/**
* 
*/
class RecaptchaApp 
{
	
    public function __construct()
    {
        self::$ci =& get_instance();
    }

    public static function Verificar($recaptchaString) {
        $key = config_item('captcha-private');
        $reCaptchaGoogle = new ReCaptcha($key);
        //$response = $reCaptchaGoogle->verifyResponse($_SERVER["REMOTE_ADDR"], $recaptchaString);
        //return $response->is_valid;
        return true;
    }

}