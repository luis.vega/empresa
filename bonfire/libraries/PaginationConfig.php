<?php defined('BASEPATH') || exit('No direct script access allowed');
require_once(BFPATH . 'libraries/RandGen/Rand_gen.php');

/**
* 
*/
class PaginationConfig
{

	/**
	 * https://www.codeigniter.com/userguide3/libraries/pagination.html
	 * 
	 * @param string $baseUrl
	 * @param int $totalItems
	 * @param int $ItemsPerPage
	 * @return array
	 */
    public static function getBootstrapConfig($baseUrl, $totalItems, $ItemsPerPage = 10 ) 
    {
    	$config = array();
		$config['base_url'] = $baseUrl;
		$config['total_rows'] = $totalItems;
		$config['per_page'] = $ItemsPerPage;
		
		$config['page_query_string'] = TRUE;
		$config['use_page_numbers'] = TRUE;
		
		$config['uri_segment'] = 1;
		$config['num_links'] = 2;
		
		$config['full_tag_open'] = '<div class="text-center"><nav aria-label="Page navigation"><ul class="pagination">';
		$config['full_tag_close'] = '</ul></nav></div>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_link'] = FALSE;//'<span aria-hidden="true">First</span>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_link'] = FALSE;//'<span aria-hidden="true">Last</span>';
		$config['last_tag_close'] = '</li>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = '<span aria-hidden="true">&laquo;</span>';
		$config['prev_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = '<span aria-hidden="true">&raquo;</span>';
		$config['next_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><span>';
		$config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
    	
		return $config;
    }

}