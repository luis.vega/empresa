<?php defined('BASEPATH') || exit('No direct script access allowed');
require_once(BFPATH . 'libraries/RandGen/Rand_gen.php');

/**
* 
*/
class Random 
{
	
    public function __construct()
    {
        self::$ci =& get_instance();
    }

    public static function GenerarString($size, $tipo) {
    	$randomGenerate = new Rand_gen();
    	$stringRandom = $randomGenerate->generate($size, $tipo);
    	return $stringRandom;
    }

}