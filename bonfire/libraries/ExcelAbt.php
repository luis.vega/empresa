<?php defined('BASEPATH') || exit('No direct script access allowed');
require_once(BFPATH . 'libraries/PhpExcel/PHPExcel.php');
//require_once dirname(__FILE__) . '/../Classes/PHPExcel.php';

/**
* 
*/


class ExcelAbt 
{
	
    public function __construct()
    {
        self::$ci =& get_instance();
    }

    /**
     * [Validate description]
     */
    public static function ParserProducts($tplFilePath) {
        $response = ResponseApp::Validate();
        $excelReader = PHPExcel_IOFactory::createReaderForFile($tplFilePath);
        $excelObj = $excelReader->load($tplFilePath);
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();
        $productos = [];
        for ($row = 3; $row <= $lastRow; $row++) {
            $producto = trim($worksheet->getCell('A'.$row)->getValue());
            $especie = trim($worksheet->getCell('B'.$row)->getValue());
            $cantidad = trim($worksheet->getCell('C'.$row)->getValue());
            $unidad = trim($worksheet->getCell('D'.$row)->getValue());
            $observaciones = trim($worksheet->getCell('E'.$row)->getValue());
            $rendimiento = trim($worksheet->getCell('F'.$row)->getValue());

            // Temina el parse de Datos
            if (empty($producto) && empty($especie) && empty($cantidad) &&
                empty($unidad) && empty($observaciones) && empty($rendimiento)) {
                break;
            }
            if (empty($producto) || empty($especie) || empty($cantidad) ||
                empty($unidad) || empty($observaciones) || empty($rendimiento)) {
                array_push($response->errors, "Existen campos vacios en la fila {$row}, formato no válido.");
                $response->has_errors = true;
                break;
            }

            $cantidad = str_replace(',', '.', $cantidad);
            if (!is_numeric($cantidad) || floatval($cantidad) < 1 && intval($cantidad) < 1) {
                array_push($response->errors, "La Cantidad en la fila {$row}, no tiene valor no válido.");
                $response->has_errors = true;
                break;
            }

            if (!is_numeric($rendimiento) || intval($rendimiento) < 1) {
                array_push($response->errors, "La Unidad en la fila {$row}, no tiene valor no válido.");
                $response->has_errors = true;
                break;
            }
            $productoObject = new stdClass();
            $productoObject->producto = $producto;
            $productoObject->especie = $especie;
            $productoObject->cantidad = $cantidad;
            $productoObject->unidad = $unidad;
            $productoObject->observaciones = $observaciones;
            $productoObject->rendimiento = $rendimiento;
            array_push($productos, $productoObject);
        }

        if (!$response->has_errors && sizeof($productos) > 0) {
            $response->data = $productos;
            $response->is_valid = true;
        }
        unlink($tplFilePath);

        return $response;
    }


    /**
     * [Validate description]
     */
    public static function ParserMaquinarias($tplFilePath) {
        $response = ResponseApp::Validate();
        $excelReader = PHPExcel_IOFactory::createReaderForFile($tplFilePath);
        $excelObj = $excelReader->load($tplFilePath);
        $worksheet = $excelObj->getSheet(0);
        $lastRow = $worksheet->getHighestRow();
        $maquinarias = [];
        for ($row = 3; $row <= $lastRow; $row++) {
            $maquinaEquipo = trim($worksheet->getCell('A'.$row)->getValue());
            $cantidad = trim($worksheet->getCell('B'.$row)->getValue());
            $modelo = trim($worksheet->getCell('C'.$row)->getValue());
            $especificacionTecnica = trim($worksheet->getCell('D'.$row)->getValue());
            $anioCompra = trim($worksheet->getCell('E'.$row)->getValue());

            // Temina el parse de Datos
            if (empty($maquinaEquipo) && empty($cantidad) &&
                empty($modelo) && empty($especificacionTecnica) && empty($anioCompra)) {
                break;
            }
            if (empty($maquinaEquipo) || empty($cantidad)) {
                array_push($response->errors, "Existen campos vacios en la fila {$row} que son requeridos, formato no válido.");
                $response->has_errors = true;
                break;
            }

            if (!is_numeric($cantidad) || intval($cantidad) < 1) {
                array_push($response->errors, "La Cantidad en la fila {$row}, no tiene valor no válido.");
                $response->has_errors = true;
                break;
            }

            if (!empty($anioCompra) &&  (!is_numeric($anioCompra) || intval($anioCompra) < 1)) {
                array_push($response->errors, "La Año de compra en la fila {$row}, no tiene valor no válido.");
                $response->has_errors = true;
                break;
            }
            $maquinaObject = new stdClass();
            $maquinaObject->maquinaria_equipo = $maquinaEquipo;
            $maquinaObject->cantidad = $cantidad;
            $maquinaObject->modelo = $modelo;
            $maquinaObject->especificacion_tecnica = $especificacionTecnica;
            $maquinaObject->anio_compra = $anioCompra;
            array_push($maquinarias, $maquinaObject);
        }

        if (!$response->has_errors && sizeof($maquinarias) > 0) {
            $response->data = $maquinarias;
            $response->is_valid = true;
        }
        unlink($tplFilePath);

        return $response;
    }
}